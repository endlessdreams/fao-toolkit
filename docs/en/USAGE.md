# Usage

The command wiews:fetch have a list of other formats to be exported to.

To get information of WIEWS institute code registered organisation and selecting output format like JSON.
Note that all the results is an array of row items of some sort. It is allowed to enter the wiews code of interrest as a comma seperated list.

```shell
fao-toolkit wiews:fetch --format=var_dump -- NOR051,SWE054
```


```shell
fao-toolkit wiews:fetch --format=yaml -- NOR051,SWE054
```


Beside the more basic example of usage, that you found in the readme file, there is some more options.

First we open the file .env and changes the environent (ENV) to dev. In this way you will be able to turn on a simulated FAO API server to test against.

Then you open another terminal window and run the following command:

```shell
fao-toolkit server:ctrl run --test
```

If you want, you can run this server in the background by the following command:

```shell
fao-toolkit server:ctrl start --test
```

No response? It shouldn't be either it runs quietly. Just check the status of the server by:

```shell
fao-toolkit server:ctrl status --test
```

If you want, to be able to "peek" at the stats you were able to do with an ordinary forground run. Call the following command:

```shell
fao-toolkit server:ctrl read-stats --test
```

Finally, stop the server:

```shell
fao-toolkit server:ctrl stop --test
```

For now let the server be running foreground or background.

---

Next we simulate a tasks like registration of GLIS (glis:register), without already having DOI for the first time. 
If several gene banks are hosted on the same server, you have to explicit refer to which gene bank / institute you will
register SMTA's. Then you use the run-as parameter with the WIEWS-code.

```shell
fao-toolkit glis:register --test --run-as=XXX001 --skip-invalid --unsigned-doi --filter="XXX %"
```

A time should pass here.

Then we make a call to only update those GLIS that have been updated after a date, preferable last time you made a glis:update or first glis:register.

```shell
fao-toolkit glis:update --test --run-as=XXX001 --skip-invalid --from=2024-01-01
```

Then we make another glis:register call for those that have no DOI yet.

```shell
fao-toolkit glis:register --test --run-as=XXX001 --skip-invalid --unsigned-doi --filter="XXX %"
```

After this we make a smta:register call between two dates, let's say between the beginning of last year and the end.

```shell
fao-toolkit smta:register --test --run-as=XXX001 --from=2018-01-01 --to=2024-01-01 --skip-invalid
```

---

Note that calls had a run-as set, which is needed on instances with several providers.


Now, do not forget to update .env ENV variable to 'prod'-environment with FAO production credentials and configurations set, 
before you make the calls without command option test. ENV=Prod and --test is usually expected to be FAO test API. 



---

