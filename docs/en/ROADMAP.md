# Roadmap

Future plans are the following:

- Add config as new command section for adminstrating the config files.
  - Fao-Toolkit config:validate for validating config 
  - Fao-Toolkit config:generate --env=dev --interactive 
  - Fao-Toolkit config:serialize --env=dev --format=yaml
- Add Schedule as new command section for scheduling feature
- Add glis:target
- Add glis:transfer
