# Setting up database connection

Before you can start configuring the config file, you need to make sure that necessery db connection required. Se section Installation below.

Next step is to make sure php database drivers is installed in your Operating System.

When this is in place, you can start configuring the FaoConfig file. See more in Configuration section.

## Support

The software is built upon mainly Yii 3 and Symfony libraries.
The types of databases that is supported are the following:

- [MSSQL](https://www.microsoft.com/en-us/sql-server/sql-server-2019) of versions **2017, 2019, 2022**.
- [MySQL](https://www.mysql.com/) of versions **5.7 - 8.0**.
- [MariaDB](https://mariadb.org/) of versions **10.4 - 10.9**.
- [Oracle](https://www.oracle.com/database/) of versions **12c - 21c**.
- [PostgreSQL](https://www.postgresql.org/) of versions **9.6 - 15**.
- [SQLite](https://www.sqlite.org/index.html) of version **3.3 and above**.

Fao-Toolkit is tested with SQLite, MSSQL, MySQL and PostgreSQL. The rest of supported types of databases should be 
supported if they aren't please let me know.

## Installation

Depending on which type of database your Information system is using, make any of the following Yii Db library installations. 
To install Yii DB, you must select the driver you want to use and install it with [Composer](https://getcomposer.org/).

For [MSSQL](https://github.com/yiisoft/db-mssql):

```bash
composer require yiisoft/db-mssql
```

For [MySQL/MariaDB](https://github.com/yiisoft/db-mysql):

```bash
composer require yiisoft/db-mysql
```

For [Oracle](https://github.com/yiisoft/db-oracle):

```bash
composer require yiisoft/db-oracle
```

For [PostgreSQL](https://github.com/yiisoft/db-pgsql):

```bash
composer require yiisoft/db-pgsql
```

Besides if this, make sure that you installed php db drivers too.
