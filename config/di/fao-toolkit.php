<?php

declare(strict_types=1);

use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\Base\ActorsDenormalizer;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbConnectionService;
use EndlessDreams\FaoToolkit\Entity\Glis\Service\GlisGetModuleCommandService;
use EndlessDreams\FaoToolkit\Entity\Glis\Service\GlisRegisterModuleCommandService;
use EndlessDreams\FaoToolkit\Entity\Glis\Service\GlisUpdateModuleCommandService;
use EndlessDreams\FaoToolkit\Entity\Base\ListElementDenormalizer;
use EndlessDreams\FaoToolkit\Entity\Smta\Service\SmtaRegisterModuleCommandService;
use EndlessDreams\FaoToolkit\Service\Parser\ParserServiceBuilder;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Command\ServerCtrlCommand;
use Psr\Log\LoggerInterface;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Cache\SchemaCache;
use Yiisoft\Definitions\Reference;

/** @var array $params */
return [
    DbConnectionService::class => [
        'class' => DbConnectionService::class,
        '__construct()' => [
            Reference::to(SchemaCache::class),
            Reference::to(LoggerInterface::class),
        ],
    ],
    FaoConfig::class . 'Instance' => [
        /*'class' => FaoConfig::class,
        '__construct()' => [],*/
        'definition' =>  fn (
            ParserServiceBuilder $parserBuilder,
            ActorsDenormalizer $actorsDenormalizer,
            ListElementDenormalizer $listElementDenormalizer,
            DbConnectionService $dbConnectionService
        ): FaoConfig
        => (
        $parser = $parserBuilder
            ->setDenormalizers([$actorsDenormalizer, $listElementDenormalizer, ])
            ->build()
        )
            ->denormalize($params['endlessdreams/fao-toolkit']['fao-config'], FaoConfig::class)
            ->setDbConnectionService($dbConnectionService)
            ->setParser($parser),
    ],

    Aliases::class => [
        'definition' =>
            fn () => new Aliases($params['yiisoft/aliases']['aliases'])
    ],

    FaoConfig::class /* .'Instance2' */ => [
        /*'class' => FaoConfig::class,
        '__construct()' => [],*/
        'definition' =>  fn (
            ParserServiceBuilder $parserBuilder,
            ActorsDenormalizer $actorsDenormalizer,
            ListElementDenormalizer $listElementDenormalizer,
            DbConnectionService $dbConnectionService,
            SmtaRegisterModuleCommandService $smtaRegisterModuleCommandService,
            GlisRegisterModuleCommandService $glisRegisterModuleCommandService,
            GlisUpdateModuleCommandService $glisUpdateModuleCommandService,
            GlisGetModuleCommandService $glisGetModuleCommandService,
            Aliases $aliases

        ): FaoConfig
        => (
            $parser = $parserBuilder
                ->setDenormalizers([$actorsDenormalizer, $listElementDenormalizer, ])
                ->setModuleCommandServices(
                    [
                        'smta:register' => $smtaRegisterModuleCommandService,
                        'glis:register' => $glisRegisterModuleCommandService,
                        'glis:update' => $glisUpdateModuleCommandService,
                        'glis:get' => $glisGetModuleCommandService,
                    ]
                )
                ->build()
        )
            ->denormalize(
                $params['endlessdreams/fao-toolkit']['fao-config'],
                FaoConfig::class,
                'xml',
                ['groups' => ['Default']]
            )
            ->setClientVersion((string)$params['yiisoft/yii-console']['version'])
            ->setDbConnectionService($dbConnectionService)
            ->setParser($parser)
            ->setAliases($aliases)
            ->setEmailSender($params['endlessdreams/fao-toolkit']['mailer']['emailFrom'])
    ],

    ServerCtrlCommand::class => [
        'definition' =>
            fn (
                FaoConfig $faoconfig,
                Aliases $aliases
            ): ServerCtrlCommand =>
                new ServerCtrlCommand($faoconfig, $aliases),
    ],
];
