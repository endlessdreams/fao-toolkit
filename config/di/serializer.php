<?php

declare(strict_types=1);

use EndlessDreams\FaoToolkit\Entity\Base\ActorsDenormalizer;
use EndlessDreams\FaoToolkit\Entity\Base\ListElementDenormalizer;
use EndlessDreams\FaoToolkit\Entity\Glis\Service\GlisRegisterModuleCommandService;
use EndlessDreams\FaoToolkit\Entity\Glis\Service\GlisUpdateModuleCommandService;
use EndlessDreams\FaoToolkit\Entity\Smta\Service\SmtaRegisterModuleCommandService;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use EndlessDreams\FaoToolkit\Service\Parser\ParserServiceBuilder;
use EndlessDreams\FaoToolkit\Service\Parser\ValidatorBuilder;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\Mapping\Loader\LoaderInterface;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Mapping\Factory\MetadataFactoryInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Validator\Mapping\Loader\AttributeLoader AS ValidatorAttributeLoader;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Yiisoft\Definitions\Reference;
use Yiisoft\Definitions\ReferencesArray;

/** @var array $params */
return [
    /*AnnotationReader::class => [
        'class' => AnnotationReader::class,
        '__construct()' => [],
    ],
    LoaderInterface::class => [
        'class' => Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader::class,
        '__construct()' => [
            'reader' => ReferencesArray::from([AnnotationReader::class]),
        ],
    ],*/
    AttributeLoader::class => [
        'class' => Symfony\Component\Serializer\Mapping\Loader\AttributeLoader::class,
    ],
    ClassMetadataFactoryInterface::class => [
        'class' => ClassMetadataFactory::class,
        '__construct()' => [
            'LoaderInterface' => ReferencesArray::from([AttributeLoader::class]),
        ],
    ],
    ValidatorAttributeLoader::class => [
        'class' => ValidatorAttributeLoader::class,
    ],
    MetadataFactoryInterface::class => [
        'definition' => fn() => new LazyLoadingMetadataFactory(new ValidatorAttributeLoader()),
    ],
    LazyLoadingMetadataFactory::class => [
        'definition' => fn() => new LazyLoadingMetadataFactory(new ValidatorAttributeLoader()),
    ],
    ValidatorBuilder::class => [
        'class' => ValidatorBuilder::class,
        '__construct()' => [
        ],
    ],
    ValidatorInterface::class => [
        'definition' => fn (
            ValidatorBuilder $validaton,
            LazyLoadingMetadataFactory $metadataFactory
        ): ValidatorInterface
            => $validaton->setMetadataFactory($metadataFactory)->create(),
    ],
    ParserServiceBuilder::class => [
        '__construct()' => [
            Reference::to(ValidatorInterface::class),
        ],
    ],
    ParserService::class => [
        'definition' =>  fn (
            ParserServiceBuilder $parserBuilder,
            ActorsDenormalizer $actorsDenormalizer,
            ListElementDenormalizer $listElementDenormalizer,
            SmtaRegisterModuleCommandService $smtaRegisterRowHelper,
            GlisRegisterModuleCommandService $glisRegisterRowHelper,
            GlisUpdateModuleCommandService $glisUpdateRowHelper
        ): ParserService
        => (
        $parser = $parserBuilder
            ->setDenormalizers([$actorsDenormalizer, $listElementDenormalizer, ])
            ->setModuleCommandServices(
                [
                    'smta:register' => $smtaRegisterRowHelper,
                    'glis:register' => $glisRegisterRowHelper,
                    'glis:update' => $glisUpdateRowHelper,
                ]
            )
            ->build())
    ]
];
