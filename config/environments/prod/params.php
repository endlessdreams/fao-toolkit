<?php

declare(strict_types=1);

/**
 * @var array|null $faoConfig
 */
$faoConfig = file_exists(__DIR__ . '/fao-params-local.php')
    ? include(__DIR__ . '/fao-params-local.php')
    : (file_exists(__DIR__ . '/fao-params-dist.php')
        ? include(__DIR__ . '/fao-params-dist.php')
        : null);

// Update the URLs to FAOs servers. Only the test server is set in installation.
// Correct production server URL you will get from FAO.
$faoConfig['server_urls'] = [
    'smta' => [
        'prod' => $_ENV['FT_SMTA_API_URL'] ?: 'https://127.0.0.2:8443/itt/index.php?r=extsys/uploadxml',
        'test' => $_ENV['FT_SMTA_API_TEST_URL'] ?: 'https://127.0.0.2:8443/itt/index.php?r=extsys/uploadxml',
    ],
    'glis' => [
        'prod' => $_ENV['FT_GLIS_API_URL'] ?: 'https://127.0.0.2:8443/glis/xml/manager',
        'test' => $_ENV['FT_GLIS_API_TEST_URL'] ?: 'http://127.0.0.2:8443/glis/xml/manager',
    ],
];

return [
    'yiisoft/yii-debug' => [
        'enabled' => false,
    ],
    'yiisoft/yii-console' => [
        'commands' => require __DIR__ . '/commands.php',
    ],
    'endlessdreams/fao-toolkit' => [
        'fao-config' => $faoConfig,
        'db-batch-size' => 1,
    ],
];
