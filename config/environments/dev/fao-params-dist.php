<?php

/**
 * Fao-toolkit configuration.
 *
 * Copy this file to `fao-params.php` and change its settings as required.
 * `fao-params.php` is ignored by git and so local and sensitive data like usernames and passwords
 * will not "leak" to git.
 */

declare(strict_types=1);

return [
    'options' => [
        'version' => '3',
    ],

    'databases' => [
        '@defaultModule' => 'smta',
        'database' => [
            0 => [
                '@module' => 'smta',
                'driver' => $_ENV['FT_DEV_DB_DRIVER'] ?: 'sqlite',
                'host' => $_ENV['FT_DEV_DB_HOST'] ?: 'localhost',
                'database_name' => $_ENV['FT_DEV_DB_DATABASE'] ?: '@resources/database/smta-dev.sq3',
                'port' => $_ENV['FT_DEV_DB_PORT'] ?: null,
                'options' => null,
                'username' => $_ENV['FT_DEV_DB_USERNAME'] ?: 'dbuser',
                'password' => $_ENV['FT_DEV_DB_PASSWORD'] ?: 'dbpassword',
            ],
/*
            1 => [
                '@module' => 'glis',
                'driver' => $_ENV['FT_DEV_DB_DRIVER'] ?: 'sqlite',
                'host' => $_ENV['FT_DEV_DB_HOST'] ?: 'localhost',
                'database_name' => $_ENV['FT_DEV_DB_DATABASE'] ?: '@resources/database/glis-dev.sq3',
                'port' => $_ENV['FT_DEV_DB_PORT'] ?: null,
                'options' => null,
                'username' => $_ENV['FT_DEV_DB_USERNAME'] ?: 'dbuser',
                'password' => $_ENV['FT_DEV_DB_PASSWORD'] ?: 'dbpassword',
            ],
*/
        ],
    ],

    'map' => [
        /* SMTA order mapping */
        'table_order' => $_ENV['FT_MAP_TABLE_ORDER'] ?: 'easysmta',
        'table_item' => $_ENV['FT_MAP_TABLE_ITEM'] ?: 'easysmtaitem',
        'columns_order' => [
            'id' => 'id',
            'symbol' => 'symbol',
            'date' => 'date',
            'type' => 'type',
            'language' => 'language',
            'shipName' => 'ship_name',
            'recipient_type' => 'recipient_type',
            'recipient_pid' => null,
            'recipient_name' => 'recipient_name',
            'recipient_address' => 'recipient_address',
            'recipient_country' => 'recipient_country',
            'document_location' => 'document_location',
            'document_retInfo' => 'document_retinfo',
            'document_pdf' => 'document_pdf',
            'fao_institute_code' => 'fao_institute_code',
        ],

        'columns_item' => [
            'crop' => 'crop',
            'sampleID' => 'sampleid',
            'PUD' => 'pud',
            'ancestry' => 'ancestry',
        ],

        /* GLIS mapping */

        /* True accession table. Needs to be updatable, and got the column DOI */
        'table_accession' => $_ENV['FT_MAP_TABLE_ACCESSION'] ?: 'glis',

        /* Data view that collects necessary MCPD data to FAO.
         * If ordinary accession table and table that contains all GLIS data are the same.
         * Use the same table name, and set field table_cachedglis to null.
        */
        'table_glis' => $_ENV['FT_MAP_TABLE_GLIS'] ?: 'glis',

        /* Cache table that glis dataview depends on containing a DOI column. Needs to be updatable */
        'table_cachedglis' => $_ENV['FT_MAP_TABLE_CACHEDGLIS'] ?: null,

        /* Just add your table name instead of null to start using sub tables for multiple data */
        'table_glis_cropname' => $_ENV['FT_MAP_TABLE_GLIS_CROPNAME'] ?: null,
        'table_glis_target' => $_ENV['FT_MAP_TABLE_GLIS_TARGET'] ?: null,
        'table_glis_name' => $_ENV['FT_MAP_TABLE_GLIS_NAME'] ?: null,
        'table_glis_id' => $_ENV['FT_MAP_TABLE_GLIS_ID'] ?: null,
        'table_glis_collector' => $_ENV['FT_MAP_TABLE_GLIS_COLLECTOR'] ?: null,
        'table_glis_breeder' => $_ENV['FT_MAP_TABLE_GLIS_BREEDER'] ?: null,

        /* Has to be defined, since all database engines doesn't have views with write capabilties */
        /* Name of table where column containing doi resides. */
        'columns_accession' => [
            'sampledoi' => $_ENV['FT_MAP_COLUMNS_ACCESSION_SAMPLEDOI'] ?: 'sampledoi',
        ],

        'where_accession' => [
            'glis_id' => $_ENV['FT_MAP_WHERE_ACCESSION_GLIS_ID'] ?: 'glis_id'
            /* 'sampleid' => '{accession_number_part1} {accession_number_part2} {accession_number_part3}' */
            /* If cropname is needed, then include a cropname key and value here. */
        ],

        'columns_glis' => [
            /* This column might be named accession_id */
            'glis_id' => 'glis_id',

            /* FAO WIEWS Institute code of the organization where the PGRFA is conserved.
             * See "Actor or location element" for details */
            'lwiews' => 'lwiews',

            /* Easy-SMTA PID of the organization, legal entity or individual conserving the PGRFA.
             * See "Actor or location element" for details. */
            'lpid' => 'lpid',

            /* Surname and name of individuals or organization or legal entity conserving the PGRFA.
             * See "Actor or location element" for details. */
            'lname' => 'lname',

            /* Address of the organization, legal entity or individual conserving the PGRFA.
             * See "Actor or location element" for details. */
            'laddress' => 'laddress',

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the organization,
             * legal entity or individual conserving the PGRFA.
             * See "Actor or location element" and Table 7 for details. */
            'lcountry' => 'lcountry',

            /* A Digital Object Identifier (DOI) obtained from a service other than GLIS
             * and that is already assigned to the PGRFA. */
            'sampledoi' => 'sampledoi',

            /* A string that uniquely identifies the PGRFA that is being registered
            in holder’s collection. This value will be returned by GLIS in the
            response message and is assumed to be used to associate the
            DOI to the corresponding material in the local database. You must
            provide <sampleid> even if <sampledoi> is provided. In case
            you use the DOI as unique identifier in your local database, you
            can repeat it in <sampleid>. Mandatory. Please read more
            details in the “HTTPS reply” chapter. */
            'sampleid' => 'sampleid',

            /* Date in which PGRFA became part of the collection.
             * Date fragments (YYYY-MM and YYYY) are also accepted. */
            'date' => 'date',

            /* Method through which the PGRFA has been obtained.
             * Mandatory. See Table 1 for the codes accepted by this element. */
            'method' => 'method',

            /* The taxon of the genus for the PGRFA. At least one between <genus> and <cropname> must be provided. */
            'genus' => 'genus',

            /* Specific epithet of the PGRFA scientific name. If not provided, sp. is assumed */
            'species' => 'species',

            /* Common name of the PGRFA or crop name. At least one between <genus> and <cropname> must be provided.
             * Multiple <name> elements are allowed. */
            'cropname' => 'cropname',

            /* Alternative to cropname. Cropnames is json coded array. To use cropnames,
             * disable cropname by set it to null */
            /* json example: {"name": ["<some cropname>","Another cropname"] } */
            'cropnames' => 'cropnames',

            /* URL of the target page where additional information on the PGRFA can be found.
             * The URL must be URL-encoded. Any number of <target> elements can be provided. */
            'tvalue' => 'tvalue',

            /* Code of the keyword indicating the type of information that will be found at the given target URL.
             * See Table 2 below. */
            'tkw' => 'tkw',

            /* Alternative to tvalue and tkw. targets is json coded array with value and multiple kw keys.
             * Use it as cropnames. Disable tvalue and tkw first. */
            /* json example: {"target":[{"value": "<some value>", "kws": ["kw":"<kw1>","kw":"<kw2>",...]}]} */
            'targets' => 'targets',

            /*
            DOI of the PGRFA(s) from which the current PGRFA was derived.
            The DOI must be registered in GLIS. Please note that the number
            of allowed DOIs depends on the method as follows:
            Acquisition: 1 DOI
            In-house copy: 1 DOI
            In-house variant: 1 DOI
            Novel distinct PGRFA: 1 or more DOIs
            Observation - Natural: 0 DOI
            Observation – Inherited: 0 DOI
            */
            'progdoi' => 'progdoi',

            /* Alternative for multiple PROGDOIs. JSON encoded. */
            /* json example: {"doi": ["<DOI1>","<DOI2>",...] */
            'progdois' => 'progdois',

            /* Biological status of the PGRFA. See Table 3 below. */
            'biostatus' => 'biostatus',

            /* Authority for the specific epithet */
            'spauth' => 'spauth',

            /* Any additional infra-specific taxon such as subspecies, variety, form, Group and so on */
            'subtaxa' => 'subtaxa',

            /* Authority for the subtaxon at the most detailed level provided */
            'stauth' => 'stauth',

            /* Other name or designations of the PGRFA. Any number of <name> elements can be provided. */
            'nvalue' => 'nvalue',

            /* Alternative to nvalue. Nvalues is json encoded array. To use nvalues, disable nvalue by set it to null */
            'names' => 'names',

            /* Code of the identifier type. The attribute is mandatory. See Table 4 below. */
            'itype' => 'itype',

            /* Other identifier of the type indicated by the attribute type assigned to the PGRFA */
            'ivalue' => 'ivalue',

            /* Alternative to itype and ivalue. Ids is json coded array of id element with a value,
             * and a type attribute. To use ids. Disable ttype and ivalue. */
            /* json example: {"id":[{"type": "<some type>", "value": "<some value>"}]} */
            'ids' => 'ids',

            /* Code that identifies the status of the PGRFA with regard to the MLS. See Table 5 below. */
            'mlsstatus' => 'mlsstatus',

            /*Indicates whether the PGRFA no longer exists.
             * Allowed value is y/n with y=material is no longer available and n=material available */
            'hist' => 'hist',

            /* FAO WIEWS code of the organization providing the PGRFA. See "Actor element” for details. */
            'pwiews' => 'pwiews',

            /* Easy-SMTA PID of the organization, legal entity or individual providing the PGRFA.
             * See "Actor element" for details. */
            'ppid' => 'ppid',

            /* Surname and name for individuals or organization or legal entity name of the provider.
             * See "Actor element" for details. */
            'pname' => 'pname',

            /* Address of the organization, legal entity or individual providing the PGRFA.
             * multiple lines are accepted. See "Actor element" for details. */
            'paddress' => 'paddress',

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the organization,
             * legal entity or individual providing the PGRFA. See "Actor element" and Table 7 for details. */
            'pcountry' => 'pcountry',

            /* Unique identifier for the PGRFA in the provider's management. */
            'psampleid' => 'psampleid',

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the country
             * in which the PGRFA material was either collected or bred or selected,
             * or the first country in the known history of the PGRFA. See Table 7 for details */
            'provenance' => 'provenance',

            /* FAO WIEWS code of the organization collecting the PGRFA.
             * See "Actor element" for details. Any number of <collector> can be provided. */
            'cwiews' => 'cwiews',

            /* Easy-SMTA PID of the organization, legal entity or individual collecting the PGRFA.
             * See "Actor element" for details. */
            'cpid' => 'cpid',

            /* Surname and name for individuals or organization or legal entity name of the collector.
             * See "Actor element" and Table 7 for details. */
            'cname' => 'cname',

            /* Address of the organization, legal entity or individual collecting the PGRFA.
             * Multiple lines are accepted. See "Actor element" and Table 7 for details. */
            'caddress' => 'caddress',

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the country
             * of the organization, legal entity or individual collecting the PGRFA.
             * See "Actor element" and Table 7 for details. */
            'ccountry' => 'ccountry',

            /* Alternative for multiple collectors. */
            /* json example:
             * {
             *  "collector":
             *      [
             *          {
             *              "wiews": "<cwiews>",
             *              "pid": "<cpid>",
             *              "name": "<cname>",
             *              "address": "<caddress>",
             *              "country": "<ccountry>"
             *          }
             *      ]
             * }
             */
            'collectors' => 'collectors',

            /* Identifier assigned by the collector(s) to the PGRFA collected. */
            'csampleid' => 'csampleid',

            /* Identifier of the collecting mission. */
            'missid' => 'missid',

            /* Description of the location where the PGRFA was collected. */
            'site' => 'site',

            /* Latitude where the PGRFA was collected in either dd°mm'ss"X (where X is N or S) format
             * or ddd.xxxxx (up to 5 decimals, preceded by minus sign for S) format. No spaces are allowed. */
            'clat' => 'clat',

            /* Longitude where the PGRFA was collected in either dd°mm'ss"X (where X is E or W) format
             * or ddd.xxxxx (up to 5 decimals, preceded by minus sign for W) format. No spaces are allowed. */
            'clon' => 'clon',

            /* Uncertainty of lat/lon coordinates */
            'uncert' => 'uncert',

            /* Geodetic datum of the lat/lon coordinates */
            'datum' => 'datum',

            /* Georeferencing method */
            'georef' => 'georef',

            /* Elevation of collecting site in metres above sea level */
            'elevation' => 'elevation',

            /* Date on which the PGRFA was collected. Date fragments (YYYY-MM and YYYY) are also accepted. */
            'cdate' => 'cdate',

            /* Code of the nature of the location where the PGRFA was collected. See Table 6 below. */
            'source' => 'source',

            /* FAO WIEWS code of the breeding organization.
             * See "Actor element" for details. Any number of <breeder> can be provided */
            'bwiews' => 'bwiews',

            /* Easy-SMTA PID of the breeding organization, legal entity or individual.
             * See "Actor element" for details. */
            'bpid' => 'bpid',

            /* Surname and name for individuals or organization name of the breeder. See "Actor element" for details. */
            'bname' => 'bname',

            /* Address of the breeding organization, legal entity or individual. Multiple lines are accepted.
             * See "Actor element" for details. */
            'baddress' => 'baddress',

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the breeding
             * organization, legal entity or individual. See "Actor element" and Table 7 for details. */
            'bcountry' => 'bcountry',

            /* Alternative for multiple breeders. */
            /* json example:
             * {
             *  "breeder":
             *      [
             *          {
             *              "wiews": "<bwiews>",
             *              "pid": "<bpid>",
             *              "name": "<bname>",
             *              "address": "<baddress>",
             *              "country": "<bcountry>"
             *          }
             *      ]
             * }
             */
            'breeders' => 'breeders',

            /* Pedigree or other description of the ancestry of the PGRFA and how it was bred. */
            'ancestry' => 'ancestry',

            /* Last update for filter */
            'lastUpdate' => 'last_update',
        ],

        'columns_cachedglis' => [
            'glis_id' => $_ENV['FT_MAP_COLUMNS_CACHEDGLIS_GLIS_ID'] ?: 'glis_id',
            /* Needed to reset, during extended test */
            'doi' => $_ENV['FT_MAP_COLUMNS_CACHEDGLIS_DOI'] ?: 'doi',
        ],

        'columns_glis_cropname' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'name' => 'name',
        ],

        'columns_glis_target' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'value' => 'value',
            'kw' => 'kw',
        ],

        'columns_glis_name' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'name' => 'name',
        ],

        'columns_glis_id' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'type' => 'type',
            'value' => 'value',
        ],

        'columns_glis_collector' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'wiews' => 'wiews',
            'pid' => 'pid',
            'name' => 'name',
            'address' => 'address',
            'country' => 'country',
        ],

        'columns_glis_breeder' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'wiews' => 'wiews',
            'pid' => 'pid',
            'name' => 'name',
            'address' => 'address',
            'country' => 'country',
        ],

    ],

    'providers' => [
        '@defaultInstituteCode' => $_ENV['FT_DEV_PI_CODE'] ?: 'default_institute',
        'provider' => [
            0 => [
                '@instituteCode' => $_ENV['FT_DEV_PI_CODE'] ?: 'default_institute',
                'type' => $_ENV['FT_DEV_PI_TYPE'] ?: 'or',
                'pids' => [
                    'prod' => $_ENV['FT_DEV_PI_PID'] ?: 'YOUR INSTITUTE PID CODE',
                    'test' => $_ENV['FT_DEV_PI_PID_TEST'] ?: 'YOUR INSTITUTE PID CODE',
                ],
                'name' => $_ENV['FT_DEV_PI_NAME'] ?: 'YOUR INSTITUTE NAME',
                'address' => $_ENV['FT_DEV_PI_ADDRESS'] ?: 'YOUR INSTITUTE ADDRESS',
                'country' => $_ENV['FT_DEV_PI_COUNTRY_CODE'] ?: 'XXX',
                'email' => $_ENV['FT_DEV_PI_EMAIL'] ?: 'contact@any.institute',
                'providing_for' => ($_ENV['FT_DEV_PI_PROVIDING_FOR'] ?: null !== null)
                    ? array_map(
                        fn ($a) => trim($a, "'"),
                        explode(',', $_ENV['FT_DEV_PI_PROVIDING_FOR'])
                    )
                    : [],
                'credentials' => [
                    'credential' => [
                        0 => [
                            '@env' => 'prod',
                            'username' => null,
                            'password' => null,
                        ],
                        1 => [
                            '@env' => 'test',
                            'username' => $_ENV['FT_DEV_API_TEST_USERNAME'] ?: 'testuser',
                            'password' => $_ENV['FT_DEV_API_TEST_PASSWORD'] ?: 'password',
                        ],
                    ],
                ]
            ],
/*
            1 => [
                '@instituteCode' => $_ENV['FT_DEV_PI2_CODE'] ?: 'other_institute',
                'type' => $_ENV['FT_DEV_PI2_TYPE'] ?: 'or',
                'pids' => [
                    'prod' => $_ENV['FT_DEV_PI2_PID'] ?: 'YOUR INSTITUTE PID CODE',
                    'test' => $_ENV['FT_DEV_PI2_PID_TEST'] ?: 'YOUR INSTITUTE PID CODE',
                ],
                'name' => $_ENV['FT_DEV_PI_NAME'] ?: 'YOUR OTHER INSTITUTE NAME',
                'address' => $_ENV['FT_DEV_PI_ADDRESS'] ?: 'YOUR OTHER INSTITUTE ADDRESS',
                'country' => $_ENV['FT_DEV_PI_COUNTRY_CODE'] ?: 'XXX',
                'email' => $_ENV['FT_DEV_PI_EMAIL'] ?: 'contact@any.other.institute',
                'providing_for' => ($_ENV['FT_DEV_PI_PROVIDING_FOR'] ?: null !== null)
                    ? array_map(
                        fn($a) => trim($a, "'"),
                        explode(',', $_ENV['FT_DEV_PI_PROVIDING_FOR'])
                    )
                    : [],
                'credentials' => [
                    'credential' => [
                        0 => [
                            '@env' => 'prod',
                            'username' => null,
                            'password' => null,
                        ],
                        1 => [
                            '@env' => 'test',
                            'username' => null,
                            'password' => null,
                        ],
                    ],
                ]
            ],
*/
        ],
    ],
];
