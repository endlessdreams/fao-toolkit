<?php

declare(strict_types=1);

use App\Command\GlisGetCommand;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Command\ServerCtrlCommand;
use Symfony\Component\Serializer\Command\DebugCommand as DebugSerializerCommand;
use Symfony\Component\Validator\Command\DebugCommand as DebugValidatorCommand;

/** @return array<string, string> */
return [
    ServerCtrlCommand::getDefaultName() => ServerCtrlCommand::class,
    GlisGetCommand::getDefaultName() => GlisGetCommand::class,
    DebugValidatorCommand::getDefaultName() => DebugValidatorCommand::class,
    DebugSerializerCommand::getDefaultName() => DebugSerializerCommand::class,
];
