<?php

/**
 * Fao-toolkit configuration.
 *
 * Copy this file to `fao-params.php` and change its settings as required.
 * `fao-params.php` is ignored by git and so local and sensitive data like usernames and passwords
 * will not "leak" to git.
 */

declare(strict_types=1);

return [
    'options' => [
        'version' => '3',
        'force_migration' => true,
    ],
    'databases' => [
        '@defaultModule' => 'smta',
        'database' => [
            0 => [
                '@module' => 'smta',
                'driver' => 'sqlite',
                'host' => null,
                'database_name' => '@resources/database/smta-test.sq3',
                'port' => null,
                'options' => null,
                'username' => '',
                'password' => '',
            ],
            1 => [
                '@module' => 'glis',
                'driver' => 'sqlite',
                'host' => null,
                'database_name' => '@resources/database/glis-test.sq3',
                'port' => null,
                'options' => null,
                'username' => '',
                'password' => '',
            ],
        ],
    ],
    'map' => [
        'table_order' => 'easysmta',
        'table_item' => 'easysmtaitem',
        'columns_order' => [
            'id' => 'id',
            'symbol' => 'symbol',
            'date' => 'date',
            'type' => 'type',
            'language' => 'language',
            'shipName' => 'shipname',
            'recipient_type' => 'recipient_type',
            'recipient_pid' => null,
            'recipient_name' => 'recipient_name',
            'recipient_address' => 'recipient_address',
            'recipient_country' => 'recipient_country',
            'document_location' => 'document_location',
            'document_retInfo' => 'document_retinfo',
            'document_pdf' => 'document_pdf',
            'fao_institute_code' => 'fao_institute_code',
        ],
        'columns_item' => [
            'crop' => 'crop',
            'sampleID' => 'sampleid',
            'PUD' => 'pud',
            'ancestry' => 'ancestry',
        ],
        'table_accession' => 'glis', /* Needs to be updatable */

        'table_glis' => 'glis', /* Data view that collects necessary MCPD data to FAO */
        /* Just add your table name instead of null tostart using subtables for multiple data*/
        'table_glis_cropname' => null /*'glis_cropname'*/,
        'table_glis_target' => null /*'glis_target'*/,
        'table_glis_name' => null /*'glis_name'*/,
        'table_glis_id' => null /*'glis_id'*/,
        'table_glis_collector' => 'glis_collector',
        'table_glis_breeder' => 'glis_breeder',
        /* Has to be defined, since all database engines doesn't have views with write capabilties */
        /* Name of table where column containing doi resides. */
        'columns_accession' => [
            'sampledoi' => 'sampledoi',
        ],
        'where_accession' => [
            'glis_id' => 'glis_id'
            /* 'sampleid' => '{accession_number_part1} {accession_number_part2} {accession_number_part3}' */
            /* If cropname is needed, then include a cropname key and value here. */
        ],
        'columns_glis' => [
            /* This column might be named accession_id */
            'glis_id' => 'glis_id'

            /* FAO WIEWS Institute code of the organization where the PGRFA is conserved.
             * See "Actor or location element” for details */
            ,'lwiews' => 'lwiews'

            /* Easy-SMTA PID of the organization, legal entity or individual conserving the PGRFA.
             * See "Actor or location element" for details. */
            ,'lpid' => 'lpid'

            /* Surname and name of individuals or organization or legal entity conserving the PGRFA.
             * See "Actor or location element" for details. */
            ,'lname' => 'lname'

            /* Address of the organization, legal entity or individual conserving the PGRFA.
             * See "Actor or location element" for details. */
            ,'laddress' => 'laddress'

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the organization,
             * legal entity or individual conserving the PGRFA.
             * See "Actor or location element" and Table 7 for details. */
            ,'lcountry' => 'lcountry'

            /* A Digital Object Identifier (DOI) obtained from a service other than GLIS
             * and that is already assigned to the PGRFA. */
            ,'sampledoi' => 'sampledoi'

            /* A string that uniquely identifies the PGRFA that is being registered
            in holder’s collection. This value will be returned by GLIS in the
            response message and is assumed to be used to associate the
            DOI to the corresponding material in the local database. You must
            provide <sampleid> even if <sampledoi> is provided. In case
            you use the DOI as unique identifier in your local database, you
            can repeat it in <sampleid>. Mandatory. Please read more
            details in the “HTTPS reply” chapter. */
            ,'sampleid' => 'sampleid'

            /* Date in which PGRFA became part of the collection. Date fragments (YYYY-MM and YYYY) are also accepted.*/
            ,'date' => 'date'

            /* Method through which the PGRFA has been obtained. Mandatory.
             * See Table 1 for the codes accepted by this element. */
            ,'method' => 'method'

            /* The taxon of the genus for the PGRFA. At least one between <genus> and <cropname> must be provided. */
            ,'genus' => 'genus'

            /* Specific epithet of the PGRFA scientific name. If not provided, sp. is assumed */
            ,'species' => 'species'

            /* Common name of the PGRFA or crop name. At least one between <genus> and <cropname> must be provided.
             * Multiple <name> elements are allowed. */
            ,'cropname' => 'cropname'

            /* Alternative to cropname. Cropnames is json coded array.
             * To use cropnames, disable cropname by set it to null */
            ,'cropnames' => 'cropnames'

            /* URL of the target page where additional information on the PGRFA can be found.
             * The URL must be URL-encoded. Any number of <target> elements can be provided. */
            /* json example: [{"cropname":"<some cropname>"},{"cropname":"Another cropname"}] */
            ,'tvalue' => 'tvalue'

            /* Code of the keyword indicating the type of information that will be found at the given target URL.
             * See Table 2 below. */
            ,'tkw' => 'tkw'

            /* Alternative to tvalue and tkw. targets is json coded array with value and multiple kw keys.
             * Use it as cropnames. Disable tvalue and tkw first. */
            /* json example: [{"value": "<some value>", "kws": ["kw":"<kw1>","kw":"<kw2>",...]}] */
            ,'targets' => 'targets'

            /*
                DOI of the PGRFA(s) from which the current PGRFA was derived.
            The DOI must be registered in GLIS. Please note that the number
            of allowed DOIs depends on the method as follows:
            Acquisition: 1 DOI
            In-house copy: 1 DOI
            In-house variant: 1 DOI
            Novel distinct PGRFA: 1 or more DOIs
            Observation - Natural: 0 DOI
            Observation – Inherited: 0 DOI
            */
            ,'progdoi' => 'progdoi'

            /* Alternative for multiple PROGDOIs. JSON encoded. */
            /* json example: {"dois": ["<DOI1>","<DOI2>",...] */
            ,'progdois' => 'progdois'

            /* Biological status of the PGRFA. See Table 3 below. */
            ,'biostatus' => 'biostatus'

            /* Authority for the specific epithet */
            ,'spauth' => 'spauth'

            /* Any additional infra-specific taxon such as subspecies, variety, form, Group and so on */
            ,'subtaxa' => 'subtaxa'

            /* Authority for the subtaxon at the most detailed level provided */
            ,'stauth' => 'stauth'

            /* Other name or designations of the PGRFA. Any number of <name> elements can be provided. */
            ,'nvalue' => 'nvalue'

            /* Alternative to nvalue. Nvalues is json encoded array. To use nvalues, disable nvalue by set it to null */
            ,'names' => 'names'

            /* Code of the identifier type. The attribute is mandatory. See Table 4 below. */
            ,'itype' => 'itype'

            /* Other identifier of the type indicated by the attribute type assigned to the PGRFA */
            ,'ivalue' => 'ivalue'

            /* Alternative to itype and ivalue. Ids is json coded array of id element with a value,
             * and a type attribute. To use ids. Disable ttype and ivalue. */
            /* json example: [{"type": "<some type>", "value": "<some value>"}] */
            ,'ids' => 'ids'

            /* Code that identifies the status of the PGRFA with regard to the MLS. See Table 5 below. */
            ,'mlsstatus' => 'mlsstatus'

            /* Indicates whether the PGRFA no longer exists. Allowed value is y/n with y=material is no longer available
             * and n=material available */
            ,'hist' => 'hist'

            /* FAO WIEWS code of the organization providing the PGRFA. See "Actor element” for details. */
            ,'pwiews' => 'pwiews'

            /* Easy-SMTA PID of the organization, legal entity or individual providing the PGRFA.
             * See "Actor element” for details. */
            ,'ppid' => 'ppid'

            /* Surname and name for individuals or organization or legal entity name of the provider.
             * See "Actor element” for details. */
            ,'pname' => 'pname'

            /* Address of the organization, legal entity or individual providing the PGRFA.
             * Multiple lines are accepted. See "Actor element” for details. */
            ,'paddress' => 'paddress'

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the organization,
             * legal entity or individual providing the PGRFA. See "Actor element" and Table 7 for details. */
            ,'pcountry' => 'pcountry'

            /* Unique identifier for the PGRFA in the provider's management. */
            ,'psampleid' => 'psampleid'

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the country
             * in which the PGRFA material was either collected or bred or selected, or the first country
             * in the known history of the PGRFA. See Table 7 for details */
            ,'provenance' => 'provenance'

            /* FAO WIEWS code of the organization collecting the PGRFA. See "Actor element” for details.
             * Any number of <collector> can be provided. */
            ,'cwiews' => 'cwiews'

            /* Easy-SMTA PID of the organization, legal entity or individual collecting the PGRFA.
             * See "Actor element” for details. */
            ,'cpid' => 'cpid'

            /* Surname and name for individuals or organization or legal entity name of the collector.
             * See "Actor element” and Table 7 for details. */
            ,'cname' => 'cname'

            /* Address of the organization, legal entity or individual collecting the PGRFA.
             * Multiple lines are accepted. See "Actor element” and Table 7 for details. */
            ,'caddress' => 'caddress'

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the country
             * of the organization, legal entity or individual collecting the PGRFA.
             * See "Actor element” and Table 7 for details. */
            ,'ccountry' => 'ccountry'

            /* Alternative for multiple collectors. */
            /* json example:
             * [
             *  {
             *      "wiews": "<cwiews>",
             *      "pid": "<cpid>",
             *      "name": "<cname>",
             *      "address": "<caddress>",
             *      "country": "<ccountry>"
             *  }
             * ]
             */
            ,'collectors' => 'collectors'

            /* Identifier assigned by the collector(s) to the PGRFA collected. */
            ,'csampleid' => 'csampleid'

            /* Identifier of the collecting mission. */
            ,'missid' => 'missid'

            /* Description of the location where the PGRFA was collected. */
            ,'site' => 'site'

            /* Latitude where the PGRFA was collected in either dd°mm'ss"X (where X is N or S) format
             * or ddd.xxxxx (up to 5 decimals, preceded by minus sign for S) format. No spaces are allowed. */
            ,'clat' => 'clat'

            /* Longitude where the PGRFA was collected in either dd°mm'ss"X (where X is E or W) format
             * or ddd.xxxxx (up to 5 decimals, preceded by minus sign for W) format. No spaces are allowed. */
            ,'clon' => 'clon'

            /* Uncertainty of lat/lon coordinates */
            ,'uncert' => 'uncert'

            /* Geodetic datum of the lat/lon coordinates */
            ,'datum' => 'datum'

            /* Georeferencing method */
            ,'georef' => 'georef'

            /* Elevation of collecting site in metres above sea level */
            ,'elevation' => 'elevation'

            /* Date on which the PGRFA was collected. Date fragments (YYYY-MM and YYYY) are also accepted. */
            ,'cdate' => 'cdate'

            /* Code of the nature of the location where the PGRFA was collected. See Table 6 below. */
            ,'source' => 'source'

            /* FAO WIEWS code of the breeding organization. See "Actor element" for details.
             * Any number of <breeder> can be provided */
            ,'bwiews' => 'bwiews'

            /* Easy-SMTA PID of the breeding organization, legal entity or individual.
             * See "Actor element" for details. */
            ,'bpid' => 'bpid'

            /* Surname and name for individuals or organization name of the breeder. See "Actor element" for details. */
            ,'bname' => 'bname'

            /* Address of the breeding organization, legal entity or individual. Multiple lines are accepted.
             * See "Actor element" for details. */
            ,'baddress' => 'baddress'

            /* ISO-3166 alpha-3 country code (https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3) of the breeding
             * organization, legal entity or individual. See "Actor element" and Table 7 for details. */
            ,'bcountry' => 'bcountry'

            /* Alternative for multiple breeders. */
            /* json example:
            * [
             *  {
             *      "wiews": "<bwiews>",
             *      "pid": "<bpid>",
             *      "name": "<bname>",
             *      "address": "<baddress>",
             *      "country": "<bcountry>"
             *  }
             * ]
             */
            ,'breeders' => 'breeders'

            /* Pedigree or other description of the ancestry of the PGRFA and how it was bred. */
            ,'ancestry' => 'ancestry'

            /* Last update for filter */
            ,'lastUpdate' => 'last_update'
        ],
        'columns_glis_cropname' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'name' => 'name',
        ],
        'columns_glis_target' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'value' => 'value',
            'kw' => 'kw',
        ],
        'columns_glis_name' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'name' => 'name',
        ],
        'columns_glis_id' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'type' => 'type',
            'value' => 'value',
        ],
        'columns_glis_collector' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'wiews' => 'wiews',
            'pid' => 'pid',
            'name' => 'name',
            'address' => 'address',
            'country' => 'country',
        ],
        'columns_glis_breeder' => [
            /* PK name of this table isn't of interest. Only FK to glis/accession table */
            'glis_id' => 'glis_id',
            'wiews' => 'wiews',
            'pid' => 'pid',
            'name' => 'name',
            'address' => 'address',
            'country' => 'country',
        ],
    ],
    'providers' => [
        '@defaultInstituteCode' => 'XXX001' ?: 'default_institute',
        'provider' => [
            0 => [
                '@instituteCode' => 'XXX001' ?: 'default_institute',
                'type' => 'or',
                'pids' => [
                    'prod' => '00AA00',
                    'test' => '00AA00',
                ],
                'name' => 'Acme',
                'address' => 'Acme street',
                'country' => 'SWE',
                'email' => 'contact@acme.institute',
                'providing_for' => ['XXX002'],
                'credentials' => [
                    'credential' => [
                        0 => [
                            '@env' => 'prod',
                            'username' => null,
                            'password' => null,
                        ],
                        1 => [
                            '@env' => 'test',
                            'username' => 'testuser',
                            'password' => 'password',
                        ],
                    ],
                ]
            ],
            1 => [
                '@instituteCode' => 'XXX002',
                'type' => 'or',
                'pids' => [
                    'prod' => '00AA01',
                    'test' => '00AA01',
                ],
                'name' => 'Anti-Acme',
                'address' => 'Acme Avenue 1',
                'country' => 'SWE',
                'email' => 'contact@anti-acme.institute',
                'providing_for' => [],
                'credentials' => [
                    'credential' => [
                        0 => [
                            '@env' => 'prod',
                            'username' => null,
                            'password' => null,
                        ],
                        1 => [
                            '@env' => 'test',
                            'username' => null,
                            'password' => null,
                        ],
                    ],
                ]
            ],
        ],
    ],
];
