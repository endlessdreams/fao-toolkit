<?php

declare(strict_types=1);

if (!defined('TYPE_OF_TEST_FAO_CONFIG')) {
    define('TYPE_OF_TEST_FAO_CONFIG', $_SERVER['TYPE_OF_TEST_FAO_CONFIG'] ?? 'default');
}

/**
 * Config files for different databases.
 * If a test is going to perform on:
 * - Sqlsrv/Mssql, do it on a database prepared for unicode.
 * CREATE DATABASE smta COLLATE Latin1_General_100_CI_AI_SC_UTF8;
 * USE smta;
 * CREATE LOGIN fao WITH PASSWORD = 'P@ssw0rd', DEFAULT_DATABASE = smta;
 * CREATE USER fao FOR LOGIN fao;
 * GRANT ALL to fao;
 *
 * - MySql
 * CREATE DATABASE smta;
 * USE smta;
 * CREATE USER 'fao'@'localhost' IDENTIFIED BY 'P@ssw0rd';
 * GRANT ALL PRIVILEGES ON smta.* TO 'fao'@'localhost';
 * FLUSH PRIVILEGES;
 *
 * - PostgreSQL
 * CREATE DATABASE smta;
 * USE smta;
 * CREATE USER fao WITH ENCRYPTED PASSWORD 'P@ssw0rd';
 * GRANT ALL PRIVILEGES ON DATABASE smta TO fao;
 *
 * @var array|null $faoConfig
 */
$faoConfig = match (TYPE_OF_TEST_FAO_CONFIG) {
    'mssql' => file_exists(__DIR__ . '/fao-params-mssql-dist.php')
        ? include(__DIR__ . '/fao-params-mssql-dist.php')
        : null,
    'mysql' => file_exists(__DIR__ . '/fao-params-mysql-dist.php')
        ? include(__DIR__ . '/fao-params-mysql-dist.php')
        : null,
    'pgsql' => file_exists(__DIR__ . '/fao-params-pgsql-dist.php')
        ? include(__DIR__ . '/fao-params-pgsql-dist.php')
        : null,
    default => file_exists(__DIR__ . '/fao-params-dist.php')
        ? include(__DIR__ . '/fao-params-dist.php')
        : null,
};

$faoConfig['server_urls'] = [
    'smta' => [
        'prod' => 'https://127.0.0.2:8443/itt/index.php?r=extsys/uploadxml',
        'test' => 'http://127.0.0.2:8443/itt/index.php?r=extsys/uploadxml',
    ],
    'glis' => [
        'prod' => 'https://127.0.0.2:8443/glis/xml/manager',
        'test' => 'http://127.0.0.2:8443/glis/xml/manager',
    ],
];

return [
    'yiisoft/yii-debug' => [
        'enabled' => false,
    ],
    'yiisoft/yii-console' => [
        'commands' => require __DIR__ . '/commands.php',
    ],
    'endlessdreams/fao-toolkit' => [
        'fao-config' => $faoConfig,
        'db-batch-size' => 1,
    ],
];
