<?php

declare(strict_types=1);

use App\Command\GlisAddTargetsCommand;
use App\Command\GlisRegisterCommand;
use App\Command\GlisTransferCommand;
use App\Command\GlisUpdateCommand;
use App\Command\SmtaRegisterCommand;

/**
 * @return array<string, string>
 */
return [
    SmtaRegisterCommand::getDefaultName() => SmtaRegisterCommand::class,
    GlisRegisterCommand::getDefaultName() => GlisRegisterCommand::class,
    GlisUpdateCommand::getDefaultName() => GlisUpdateCommand::class,
    GlisTransferCommand::getDefaultName() => GlisTransferCommand::class,
    GlisAddTargetsCommand::getDefaultName() => GlisAddTargetsCommand::class,
    \App\Command\WiewsFetchCommand::getDefaultName() => \App\Command\WiewsFetchCommand::class
];
