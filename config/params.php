<?php

declare(strict_types=1);

use Psr\Log\LogLevel;
use Yiisoft\Db\Sqlite\Dsn;

require_once __DIR__ . '/version.php';

return [
    'yiisoft/aliases' => [
        'aliases' => [
            '@root' => dirname(__DIR__),
            '@runtime' => '@root/runtime',
            '@src' => '@root/src',
            '@resources' => '@root/resources',
        ],
    ],

    'yiisoft/yii-console' => [
        'commands' => require __DIR__ . '/commands.php',
        'name' => 'Fao-Toolkit',
        'version' => empty($ver = defined('APP_VERSION') ? APP_VERSION : '') ? 'dev-master#3.0' : $ver
    ],

    'yiisoft/db-sqlite' => [
        'dsn' => (new Dsn('sqlite', dirname(__DIR__, 1) . '/resources/database/sqlite.sq3'))->__toString(),
    ],

    'yiisoft/log-target-file' => [
        'fileTarget' => [
            'file' => '@runtime/logs/app.log',
            'levels' => [
                LogLevel::EMERGENCY,
                LogLevel::ERROR,
                LogLevel::WARNING,
                LogLevel::INFO,
                LogLevel::DEBUG,
            ],
            'categories' => [],
            'except' => [],
            'exportInterval' => 1,
            'dirMode' => 0755,
            'fileMode' => null,
        ],
        'fileRotator' => [
            'maxFileSize' => 10240,
            'maxFiles' => 5,
            'fileMode' => null,
            'compressRotatedFiles' => false,
        ],
    ],
    'yiisoft/mailer' => [
        'messageBodyTemplate' => [
            'viewPath' => '@resources/mail',
        ],
        'fileMailer' => [
            'fileMailerStorage' => '@runtime/mail',
        ],
        'useSendmail' => false,
        'writeToFiles' => array_key_exists('FT_MAILER_WRITE_TO_FILES', $_ENV)
            && ! empty($_ENV['FT_MAILER_WRITE_TO_FILES'])
            ? filter_var($_ENV['FT_MAILER_WRITE_TO_FILES'], FILTER_VALIDATE_BOOLEAN)
            : filter_var((getenv('FT_MAILER_WRITE_TO_FILES', true) ?: true), FILTER_VALIDATE_BOOLEAN),
    ],
    'symfony/mailer' => [
        'esmtpTransport' => [
            'scheme' => array_key_exists('FT_MAILER_ESMTP_SCHEME', $_ENV)
                && ! empty($_ENV['FT_MAILER_ESMTP_SCHEME'])
                ? $_ENV['FT_MAILER_ESMTP_SCHEME']
                // "smtps": using TLS, "smtp": without using TLS.
                : (getenv('FT_MAILER_ESMTP_SCHEME', true) ?: 'smtps'),
            'host' => array_key_exists('FT_MAILER_ESMTP_HOST', $_ENV) && ! empty($_ENV['FT_MAILER_ESMTP_HOST'])
                ? $_ENV['FT_MAILER_ESMTP_HOST']
                : (getenv('FT_MAILER_ESMTP_HOST', true) ?: 'smtp.gmail.com'),
            'port' => array_key_exists('FT_MAILER_ESMTP_PORT', $_ENV) && ! empty($_ENV['FT_MAILER_ESMTP_PORT'])
                ? intval($_ENV['FT_MAILER_ESMTP_PORT'])
                : intval(getenv('FT_MAILER_ESMTP_PORT', true) ?: 465),
            'username' => array_key_exists('FT_MAILER_ESMTP_USERNAME', $_ENV)
                && ! empty($_ENV['FT_MAILER_ESMTP_USERNAME'])
                ? $_ENV['FT_MAILER_ESMTP_USERNAME']
                : (getenv('FT_MAILER_ESMTP_USERNAME', true) ?: ''),
            'password' => array_key_exists('FT_MAILER_ESMTP_PASSWORD', $_ENV)
                && ! empty($_ENV['FT_MAILER_ESMTP_PASSWORD'])
                ? $_ENV['FT_MAILER_ESMTP_PASSWORD']
                : (getenv('FT_MAILER_ESMTP_PASSWORD', true) ?: ''),
        ],
    ],

    'endlessdreams/fao-toolkit' => [
        'mailer' => [
            'emailFrom' => array_key_exists('FT_MAILER_EMAIL_FROM', $_ENV) && ! empty($_ENV['FT_MAILER_EMAIL_FROM'])
                ? $_ENV['FT_MAILER_EMAIL_FROM'] : (getenv('FT_MAILER_EMAIL_FROM', true) ?: '<your-system-email>')
        ],
    ],

];
