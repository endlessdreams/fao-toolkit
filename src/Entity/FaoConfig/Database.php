<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 *
 * <database module="[module]">*
 *   <driver>[driver]</driver>
 *   <host>[host]</host>
 *   <database_name>[database_name]</database_name>
 *   <port>[port]</port>
 *   <options>[...]</options>
 *   <username>[username]</username>
 *   <password>[password]</password>
 * </database>
 */
class Database
{
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('@module')]
    private ?string $module = null;

    /*
     Add dynamic tester that test against installed database drivers
     */
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $driver = null;
    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    private ?string $host = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('database_name')]
    private ?string $databaseName = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    private ?string $port = null;

    /**
     * @var array|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    private ?array $options = null;

    /**
     * @var string
     */
    #[Groups(['Default'])]
    private string $username = '';

    /**
     * @var string
     */
    #[Groups(['Default'])]
    private string $password = '';

    /**
     * @return string|null
     */
    public function getModule(): ?string
    {
        return $this->module;
    }

    /**
     * @param string|null $module
     * @return $this
     */
    public function setModule(?string $module): Database
    {
        $this->module = $module;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDriver(): ?string
    {
        return $this->driver;
    }

    /**
     * @param string|null $driver
     * @return $this
     */
    public function setDriver(?string $driver): Database
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @param string|null $host
     * @return $this
     */
    public function setHost(?string $host): Database
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDatabaseName(): ?string
    {
        return $this->databaseName;
    }

    /**
     * @param string|null $databaseName
     * @return $this
     */
    public function setDatabaseName(?string $databaseName): Database
    {
        $this->databaseName = $databaseName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPort(): ?string
    {
        return $this->port;
    }

    /**
     * @param string|null $port
     * @return $this
     */
    public function setPort(?string $port): Database
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     * @return $this
     */
    public function setOptions(?array $options): Database
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return $this
     */
    public function setUsername(?string $username): Database
    {
        $this->username = $username ?? '';
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return $this
     */
    public function setPassword(?string $password): Database
    {
        $this->password = $password ?? '';
        return $this;
    }
}

