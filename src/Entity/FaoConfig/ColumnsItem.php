<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\Base\ColumnsBase;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 *
 * <ColumnsItem>
 *   <crop>[crop]</crop>
 *   <sampleID>[sampleID]</sampleID>
 *   <PUD>[PUD]</PUD>
 *   <ancestry>[ancestry]</ancestry>
 * </ColumnsItem>
 */
class ColumnsItem extends ColumnsBase
{
    /**
     * @var int|null
     */
    #[Ignore]
    private ?int $orderid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $crop = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('sampleID')]
    private ?string $sampleID = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('PUD')]
    private ?string $pud = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $ancestry = null;

    /**
     * @return int|null
     */
    public function getOrderid(): ?int
    {
        return $this->orderid;
    }

    /**
     * @param int|null $orderid
     * @return $this
     */
    public function setOrderid(?int $orderid): ColumnsItem
    {
        $this->orderid = $orderid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCrop(): ?string
    {
        return $this->crop;
    }

    /**
     * @param string|null $crop
     * @return $this
     */
    public function setCrop(?string $crop): ColumnsItem
    {
        $this->crop = $crop;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleID(): ?string
    {
        return $this->sampleID;
    }

    /**
     * @param string|null $sampleID
     * @return $this
     */
    public function setSampleID(?string $sampleID): ColumnsItem
    {
        $this->sampleID = $sampleID;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPud(): ?string
    {
        return $this->pud;
    }

    /**
     * @param string|null $pud
     * @return $this
     */
    public function setPud(?string $pud): ColumnsItem
    {
        $this->pud = $pud;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAncestry(): ?string
    {
        return $this->ancestry;
    }

    /**
     * @param string|null $ancestry
     * @return $this
     */
    public function setAncestry(?string $ancestry): ColumnsItem
    {
        $this->ancestry = $ancestry;
        return $this;
    }
}
