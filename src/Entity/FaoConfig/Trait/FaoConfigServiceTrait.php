<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig\Trait;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Database;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbConnectionService;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Annotation\Ignore;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Connection\ConnectionInterface;

/**
 *
 */
trait FaoConfigServiceTrait
{
    /**
     * @var string|null
     */
    #[Ignore]
    private ?string $clientVersion = null;

    /**
     * @var string|null
     */
    #[Ignore]
    private ?string $selectedProviderCode = null;

    /**
     * @var ParserService|null
     */
    #[Ignore]
    private ?ParserService $parser = null;

    /**
     * @var string|null
     */
    #[Ignore]
    private ?string $emailSender = null;

    /**
     * @var Aliases|null
     */
    #[Ignore]
    private ?Aliases $aliases = null;

    /**
     * @param string|null $clientVersion
     * @return FaoConfig
     */
    public function setClientVersion(?string $clientVersion): FaoConfig
    {
        $this->clientVersion = $clientVersion;
        return $this;
    }

    public function getClientVersion(): ?string
    {
        return $this->clientVersion;
    }


    /**
     * @param string|null $selectedFaoEnvironment
     * @return FaoConfig
     */
    #[Ignore]
    public function setSelectedFaoEnvironment(?string $selectedFaoEnvironment): FaoConfig
    {
        $this->getProviders()?->setSelectedFaoEnvironment($selectedFaoEnvironment);
        return $this;
    }

    /**
     * @param string|null $selectedProviderCode
     * @return FaoConfig
     */
    public function setSelectedProviderCode(?string $selectedProviderCode): FaoConfig
    {
        $this->selectedProviderCode = $selectedProviderCode;
        return $this;
    }

    /**
     * @return Provider|null
     */
    #[Ignore]
    public function getSelectedProvider(): ?Provider
    {
        return $this->getProviders()?->findProvider($this->selectedProviderCode);
    }

    /**
     * @param string $pid
     * @return string|null
     */
    #[Ignore]
    public function getProviderWiewByPid(string $pid): ?string
    {
        /** @var Provider $provider */
        foreach ($this->getProviders()?->getProviders() ?? [] as $provider) {
            if ($provider->getPid() === $pid) {
                return $provider->getInstituteCode();
            }
        }
        return null;
    }

    /**
     * @return Database|null
     */
    #[Ignore]
    public function getSelectedDatabase(): ?Database
    {
        $database = $this->getDatabases();
        return $database?->getDatabase($database->getSelectedModule())
            ?? $database?->getDatabase($database->getDefaultModule());
    }

    /**
     * @param string|null $module
     * @return ConnectionInterface|null
     * @throws Exception
     */
    #[Ignore]
    public function getDbConnection(?string $module = null): ?ConnectionInterface
    {
        return $this->getDatabases()?->getDbConnection($module);
    }

    /**
     * @return DbConnectionService|null
     */
    #[Ignore]
    public function getDbConnectionService(): ?DbConnectionService
    {
        return $this->getDatabases()?->getService();
    }

    /**
     * @param DbConnectionService|null $dbConnectionService
     * @return FaoConfig
     */
    #[Ignore]
    public function setDbConnectionService(?DbConnectionService $dbConnectionService): FaoConfig
    {
        $this->getDatabases()?->setService($dbConnectionService);
        return $this;
    }

    /**
     * @return ParserService|null
     */
    public function getParser(): ?ParserService
    {
        return $this->parser;
    }

    /**
     * @param ParserService|null $parser
     * @return FaoConfig
     */
    public function setParser(?ParserService $parser): FaoConfig
    {
        $this->parser = $parser;
        return $this;
    }

    /**
     * @param InputInterface $input
     * @return FaoConfig
     */
    #[Ignore]
    public function setConsoleInputState(InputInterface $input): FaoConfig
    {

        $module = explode(':', (string)$input->getArgument('command'))[0];
        $this
            ->setSelectedFaoEnvironment($input->getOption('test') ? 'test' : 'prod')
            ->setSelectedProviderCode((string)$input->getOption('run-as'));
        if (! ($this->getDatabases()?->getDatabase($module) instanceof Database)) {
            $module = $this->getDatabases()?->getDefaultModule();
        }
        $this->getDatabases()?->setSelectedModule($module);
        return $this;
    }

    /**
     * @return string|null
     */
    #[Ignore]
    public function getServerUrl(): ?string
    {
        $module = $this->databases?->getSelectedModule();
        $faoEnv = $this->getSelectedProvider()?->getSelectedFaoEnvironment()
            ?? $this->getProviders()?->findDefaultProvider()?->getSelectedFaoEnvironment();

        return (
            isset($module) && isset($this->serverUrls) && array_key_exists($module, ($this->serverUrls))
            && is_array($this->serverUrls[$module])
            && isset($faoEnv) && array_key_exists($faoEnv, $this->serverUrls[$module])
        ) ? (string)$this->serverUrls[$module][$faoEnv] : null;
    }

    /**
     * @return string|null
     */
    public function getEmailSender(): ?string
    {
        return $this->emailSender;
    }

    /**
     * @param string|null $emailSender
     * @return FaoConfig
     */
    public function setEmailSender(?string $emailSender): FaoConfig
    {
        $this->emailSender = $emailSender;
        return $this;
    }

    /**
     * @return Aliases|null
     */
    public function getAliases(): ?Aliases
    {
        return $this->aliases;
    }

    /**
     * @param Aliases|null $aliases
     * @return FaoConfig
     */
    public function setAliases(?Aliases $aliases): FaoConfig
    {
        $this->aliases = $aliases;
        return $this;
    }
}
