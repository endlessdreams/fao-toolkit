<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 *
 * <Credentials>
 *   <credential env="[env]">*
 *     <username<[username]</username>
 *     <password>[password]</password>
 *   </credential>
 * </Credentials>
 */
class Credentials
{
    /**
     * @var Credential[]
     */
    #[Groups(['Default'])]
    private array $credentials = [];

    /**
     * @var string|null
     */
    #[ignore]
    private ?string $selected = null;


    /**
     * @return Credential|null
     */
    public function getProd(): ?Credential
    {
        return $this->getCredential('prod');
    }

    /**
     * @param Credential|null $prod
     * @return $this
     */
    public function setProd(?Credential $prod): Credentials
    {
        $prod?->setEnv('prod');
        if (($index = $this->findCredential('prod')) !== null) {
            unset($this->credentials[$index]);
        }

        if (isset($prod)) {
            $this->addCredential($prod);
        }

        return $this;
    }

    /**
     * @return Credential|null
     */
    public function getTest(): ?Credential
    {
        return $this->getCredential('test');
    }

    /**
     * @param Credential|null $test
     * @return $this
     */
    public function setTest(?Credential $test): Credentials
    {
        $test?->setEnv('test');
        if (($index = $this->findCredential('test')) !== null) {
            unset($this->credentials[$index]);
        }
        if (isset($test)) {
            $this->addCredential($test);
        }
        return $this;
    }

    /**
     * @return Credential[]
     */
    public function getCredentials(): array
    {
        return $this->credentials;
    }

    /**
     * @param Credential[] $credentials
     * @return $this
     */
    public function setCredentials(array $credentials): Credentials
    {
        $this->credentials = $credentials;
        return $this;
    }

    /**
     * @param Credential $credential
     * @return void
     */
    public function addCredential(Credential $credential): void
    {
//        if ($this->credentials === null) {
//            $this->credentials = [];
//        }
        $this->credentials[] = $credential;
    }

    /**
     * @param string $credentialName
     * @return Credential|null
     */
    #[Ignore]
    public function getCredential(string $credentialName): ?Credential
    {
        foreach ($this->credentials as $index => $credential) {
            if ($credential->getEnv() === $credentialName) {
                return $credential;
            }
        }
        return null;
    }

    /**
     * @param string|null $env
     * @return void
     */
    #[Ignore]
    public function setSelectedFaoEnvironment(?string $env): void
    {
        $this->selected = $env;
    }

    /**
     * @return Credential|null
     */
    public function getSelected(): ?Credential
    {
        if (!isset($this->selected)) {
            return null;
        }
        return $this->getCredential($this->selected);
    }


    /**
     * @param string $env
     * @return int|null
     */
    protected function findCredential(string $env): ?int
    {
        foreach ($this->credentials as $index => $credential) {
            if ($credential->getEnv() === $env) {
                /** @var int $index */
                return $index;
            }
        }
        return null;
    }
}
