<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use ReflectionAttribute;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Yiisoft\Strings\StringHelper;

/**
 *
 * <map>
 *     <table_order>[table_order]</table_order>
 *     <table_item>[table_item]</table_item>
 *     <columns_order>
 *         ...
 *     </columns_order>
 *     <columns_item>
 *         ...
 *     </columns_item>
 *     <table_accession>[table_accession]</table_accession>
 *     <table_glis>[table_glis]</table_glis>
 *     <table_cachedglis>[table_cachedglis]</table_cachedglis>
 *     <table_glis_cropname>[table_glis_cropname]</table_glis_cropname>
 *     <table_glis_target>[table_glis_target]</table_glis_target>
 *     <table_glis_name>[table_glis_name]</table_glis_name>
 *     <table_glis_id>[table_glis_id]</table_glis_id>
 *     <table_glis_collector>[table_glis_collector]</table_glis_collector>
 *     <table_glis_breeder>[table_glis_breeder]</table_glis_breeder>
 *     <columns_accession>
 *         ...
 *     </columns_accession>
 *     <columns_cachedglis>
 *         ...
 *     </columns_cachedglis>
 *     <columns_glis>
 *         ...
 *     </columns_glis>
 *     <columns_glis_cropname>
 *         ...
 *     </columns_glis_cropname>
 *     <columns_glis_target>
 *         ...
 *     </columns_glis_target>
 *     <columns_glis_name>
 *         ...
 *     </columns_glis_name>
 *     <columns_glis_id>
 *         ...
 *     </columns_glis_id>
 *     <columns_glis_collector>
 *         ...
 *     </columns_glis_collector>
 *     <columns_glis_breeder>
 *         ...
 *     </columns_glis_breeder>
 *     <where_accession>
 *         ...
 *     </where_accession>
 * </map>
 */
class Map
{
    /**
     * Enter your institutes table name of 'table_order'
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('table_order')]
    private ?string $tableOrder = null;

    /**
     * Enter your institutes table name of 'table_item'
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('table_item')]
    private ?string $tableItem = null;

    /**
     * @var ColumnsOrder|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_order')]
    private ?ColumnsOrder $columnsOrder = null;

    /**
     * @var ColumnsItem|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_item')]
    private ?ColumnsItem $columnsItem = null;

    /**
     * Enter your institutes table name of 'table_accession'
     * Needs to be updatable and have a DOI column.
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('table_accession')]
    private ?string $tableAccession = null;

    /**
     * Enter your institutes table/view name of 'table_glis',
     * that collects necessary MCPD data to FAO.
     *
     * @var string|null
     */
    #[SerializedName('table_glis')]
    #[Groups(['Default'])]
    private ?string $tableGlis = null;

    /**
     * Enter your institutes table name of 'table_cachedglis',
     * that 'table_glis' is based on. If 'table_glis' is not a view then let this value be null.
     *
     * @var string|null
     */
    #[SerializedName('table_cachedglis')]
    #[Groups(['Default'])]
    private ?string $tableCachedglis = null;

    /* Just add your table name instead of null to start using sub-tables for multiple data */

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    #[SerializedName('table_glis_cropname')]
    private ?string $tableGlisCropname = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    #[SerializedName('table_glis_target')]
    private ?string $tableGlisTarget = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    #[SerializedName('table_glis_name')]
    private ?string $tableGlisName = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    #[SerializedName('table_glis_id')]
    private ?string $tableGlisId = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    #[SerializedName('table_glis_collector')]
    private ?string $tableGlisCollector = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    #[SerializedName('table_glis_breeder')]
    private ?string $tableGlisBreeder = null;

    /**
     * @var ColumnsAccession|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_accession')]
    private ?ColumnsAccession $columnsAccession = null;

    /**
     * @var ColumnsCachedglis|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_cachedglis')]
    private ?ColumnsCachedglis $columnsCachedglis = null;

    /**
     * @var ColumnsGlis|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_glis')]
    private ?ColumnsGlis $columnsGlis = null;

    /**
     * @var ColumnsGlisName|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_glis_cropname')]
    private ?ColumnsGlisName $columnsGlisCropname = null;

    /**
     * @var ColumnsGlisTarget|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_glis_target')]
    private ?ColumnsGlisTarget $columnsGlisTarget = null;

    /**
     * @var ColumnsGlisName|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_glis_name')]
    private ?ColumnsGlisName $columnsGlisName = null;

    /**
     * @var ColumnsGlisId|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_glis_id')]
    private ?ColumnsGlisId $columnsGlisId = null;

    /**
     * @var ColumnsGlisActor|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_glis_collector')]
    private ?ColumnsGlisActor $columnsGlisCollector = null;

    /**
     * @var ColumnsGlisActor|null
     */
    #[Groups(['Default'])]
    #[SerializedName('columns_glis_breeder')]
    private ?ColumnsGlisActor $columnsGlisBreeder = null;

    /**
     * @var WhereAccession|null
     */
    #[Groups(['Default'])]
    #[SerializedName('where_accession')]
    private ?WhereAccession $whereAccession = null;

    /**
     * @return string|null
     */
    public function getTableOrder(): ?string
    {
        return $this->tableOrder;
    }

    /**
     * @param string|null $tableOrder
     * @return $this
     */
    public function setTableOrder(?string $tableOrder): Map
    {
        $this->tableOrder = $tableOrder;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableItem(): ?string
    {
        return $this->tableItem;
    }

    /**
     * @param string|null $tableItem
     * @return $this
     */
    public function setTableItem(?string $tableItem): Map
    {
        $this->tableItem = $tableItem;
        return $this;
    }

    /**
     * @return ColumnsOrder|null
     */
    public function getColumnsOrder(): ?ColumnsOrder
    {
        return $this->columnsOrder;
    }

    /**
     * @param ColumnsOrder|null $columnsOrder
     * @return $this
     */
    public function setColumnsOrder(?ColumnsOrder $columnsOrder): Map
    {
        $this->columnsOrder = $columnsOrder;
        return $this;
    }

    /**
     * @return ColumnsItem|null
     */
    public function getColumnsItem(): ?ColumnsItem
    {
        return $this->columnsItem;
    }

    /**
     * @param ColumnsItem|null $columnsItem
     * @return $this
     */
    public function setColumnsItem(?ColumnsItem $columnsItem): Map
    {
        $this->columnsItem = $columnsItem;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableAccession(): ?string
    {
        return $this->tableAccession;
    }

    /**
     * @param string|null $tableAccession
     * @return $this
     */
    public function setTableAccession(?string $tableAccession): Map
    {
        $this->tableAccession = $tableAccession;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableGlis(): ?string
    {
        return $this->tableGlis;
    }

    /**
     * @param string|null $tableGlis
     * @return $this
     */
    public function setTableGlis(?string $tableGlis): Map
    {
        $this->tableGlis = $tableGlis;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableCachedglis(): ?string
    {
        return $this->tableCachedglis;
    }

    /**
     * @param string|null $tableCachedglis
     * @return $this
     */
    public function setTableCachedglis(?string $tableCachedglis): Map
    {
        $this->tableCachedglis = $tableCachedglis;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableGlisCropname(): ?string
    {
        return $this->tableGlisCropname;
    }

    /**
     * @param string|null $tableGlisCropname
     * @return $this
     */
    public function setTableGlisCropname(?string $tableGlisCropname): Map
    {
        $this->tableGlisCropname = $tableGlisCropname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableGlisTarget(): ?string
    {
        return $this->tableGlisTarget;
    }

    /**
     * @param string|null $tableGlisTarget
     * @return $this
     */
    public function setTableGlisTarget(?string $tableGlisTarget): Map
    {
        $this->tableGlisTarget = $tableGlisTarget;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableGlisName(): ?string
    {
        return $this->tableGlisName;
    }

    /**
     * @param string|null $tableGlisName
     * @return $this
     */
    public function setTableGlisName(?string $tableGlisName): Map
    {
        $this->tableGlisName = $tableGlisName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableGlisId(): ?string
    {
        return $this->tableGlisId;
    }

    /**
     * @param string|null $tableGlisId
     * @return $this
     */
    public function setTableGlisId(?string $tableGlisId): Map
    {
        $this->tableGlisId = $tableGlisId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableGlisCollector(): ?string
    {
        return $this->tableGlisCollector;
    }

    /**
     * @param string|null $tableGlisCollector
     * @return $this
     */
    public function setTableGlisCollector(?string $tableGlisCollector): Map
    {
        $this->tableGlisCollector = $tableGlisCollector;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableGlisBreeder(): ?string
    {
        return $this->tableGlisBreeder;
    }

    /**
     * @param string|null $tableGlisBreeder
     * @return $this
     */
    public function setTableGlisBreeder(?string $tableGlisBreeder): Map
    {
        $this->tableGlisBreeder = $tableGlisBreeder;
        return $this;
    }

    /**
     * @return ColumnsAccession|null
     */
    public function getColumnsAccession(): ?ColumnsAccession
    {
        return $this->columnsAccession;
    }

    /**
     * @param ColumnsAccession|null $columnsAccession
     * @return $this
     */
    public function setColumnsAccession(?ColumnsAccession $columnsAccession): Map
    {
        $this->columnsAccession = $columnsAccession;
        return $this;
    }

    /**
     * @return ColumnsCachedglis|null
     */
    public function getColumnsCachedglis(): ?ColumnsCachedglis
    {
        return $this->columnsCachedglis;
    }

    /**
     * @param ColumnsCachedglis|null $columnsCachedglis
     * @return $this
     */
    public function setColumnsCachedglis(?ColumnsCachedglis $columnsCachedglis): Map
    {
        $this->columnsCachedglis = $columnsCachedglis;
        return $this;
    }

    /**
     * @return ColumnsGlis|null
     */
    public function getColumnsGlis(): ?ColumnsGlis
    {
        return $this->columnsGlis;
    }

    /**
     * @param ColumnsGlis|null $columnsGlis
     * @return $this
     */
    public function setColumnsGlis(?ColumnsGlis $columnsGlis): Map
    {
        $this->columnsGlis = $columnsGlis;
        return $this;
    }

    /**
     * @return ColumnsGlisName|null
     */
    public function getColumnsGlisCropname(): ?ColumnsGlisName
    {
        return $this->columnsGlisCropname;
    }

    /**
     * @param ColumnsGlisName|null $columnsGlisCropname
     * @return $this
     */
    public function setColumnsGlisCropname(?ColumnsGlisName $columnsGlisCropname): Map
    {
        $this->columnsGlisCropname = $columnsGlisCropname;
        return $this;
    }

    /**
     * @return ColumnsGlisTarget|null
     */
    public function getColumnsGlisTarget(): ?ColumnsGlisTarget
    {
        return $this->columnsGlisTarget;
    }

    /**
     * @param ColumnsGlisTarget|null $columnsGlisTarget
     * @return $this
     */
    public function setColumnsGlisTarget(?ColumnsGlisTarget $columnsGlisTarget): Map
    {
        $this->columnsGlisTarget = $columnsGlisTarget;
        return $this;
    }

    /**
     * @return ColumnsGlisName|null
     */
    public function getColumnsGlisName(): ?ColumnsGlisName
    {
        return $this->columnsGlisName;
    }

    /**
     * @param ColumnsGlisName|null $columnsGlisName
     * @return $this
     */
    public function setColumnsGlisName(?ColumnsGlisName $columnsGlisName): Map
    {
        $this->columnsGlisName = $columnsGlisName;
        return $this;
    }

    /**
     * @return ColumnsGlisId|null
     */
    public function getColumnsGlisId(): ?ColumnsGlisId
    {
        return $this->columnsGlisId;
    }

    /**
     * @param ColumnsGlisId|null $columnsGlisId
     * @return $this
     */
    public function setColumnsGlisId(?ColumnsGlisId $columnsGlisId): Map
    {
        $this->columnsGlisId = $columnsGlisId;
        return $this;
    }

    /**
     * @return ColumnsGlisActor|null
     */
    public function getColumnsGlisCollector(): ?ColumnsGlisActor
    {
        return $this->columnsGlisCollector;
    }

    /**
     * @param ColumnsGlisActor|null $columnsGlisCollector
     * @return $this
     */
    public function setColumnsGlisCollector(?ColumnsGlisActor $columnsGlisCollector): Map
    {
        $this->columnsGlisCollector = $columnsGlisCollector;
        return $this;
    }

    /**
     * @return ColumnsGlisActor|null
     */
    public function getColumnsGlisBreeder(): ?ColumnsGlisActor
    {
        return $this->columnsGlisBreeder;
    }

    /**
     * @param ColumnsGlisActor|null $columnsGlisBreeder
     * @return $this
     */
    public function setColumnsGlisBreeder(?ColumnsGlisActor $columnsGlisBreeder): Map
    {
        $this->columnsGlisBreeder = $columnsGlisBreeder;
        return $this;
    }

    /**
     * @return WhereAccession|null
     */
    public function getWhereAccession(): ?WhereAccession
    {
        return $this->whereAccession;
    }

    /**
     * @param WhereAccession|null $whereAccession
     * @return $this
     */
    public function setWhereAccession(?WhereAccession $whereAccession): Map
    {
        $this->whereAccession = $whereAccession;
        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-suppress MixedAssignment
     */
    public function toTableNamesArray(): array
    {
        $props = (new ReflectionClass($this))->getProperties();
        $arr = [];
        array_walk(
            $props,
            function (ReflectionProperty $prop) use (&$arr) {
                $ignoreCnt = count($prop->getAttributes(Ignore::class, ReflectionAttribute::IS_INSTANCEOF));
                if ($ignoreCnt === 0 && StringHelper::startsWith($prop->getName(), 'table')) {
                    $value = $prop->getValue($this);
                    if (is_string($value)) {
                        $arr[$prop->getName()] = $value;
                    }
                }
            }
        );
        return $arr;
    }
}
