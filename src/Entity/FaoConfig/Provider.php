<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Yiisoft\Arrays\ArrayHelper;

/**
 *
 * <Provider instituteCode="[instituteCode]">
 *   <type>[type]</type>
 *   <pids>[...]</pids>
 *   <name>[name]</name>
 *   <address>[address]</address>
 *   <country>[country]</country>
 *   <email>[email]</email>
 *   <providingFor>[...]</providingFor>
 *   <credentials>
 *     <credential env="[env]">*
 *       <username<[username]</username>
 *       <password>[password]</password>
 *     </credential>
 *   </credentials>
 * </Provider>
 */
class Provider
{
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('@instituteCode')]
    private ?string $instituteCode = null;

    /**
     * @var string|null
     */
    #[Assert\Choice(choices: ['or'], message: 'Type has only "or" as valid option')]
    #[Groups(['Default'])]
    private ?string $type = null;

    /**
     * @var Options|null
     */
    #[Groups(['Default'])]
    private ?Options $pids = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    private ?string $name = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[Assert\NotBlank(allowNull: true)]
    private ?string $address = null;
    /**
     * @var string|null
     */
    #[Assert\AtLeastOneOf(
        constraints: [
            new Assert\Country(alpha3: true),
            new Assert\Choice(
                callback: [ ChoiceHelper::class ,'getExtraCountryCodeChoices' ],
                message: 'This value is not a valid FAO extended country.'
            ),
            new Assert\Blank(),
        ],
        message: 'Country value should satisfy at least one of the following constraints:',
    )]
    #[Groups(['Default'])]
    private ?string $country = null;
    /**
     * @var string|null
     */
    #[Assert\Email]
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    private ?string $email = null;

    /**
     * @var array|null
     */
    #[Groups(['Default'])]
    private ?array $providingFor = [];

    /**
     * @var Credentials|null
     */
    #[Groups(['Default'])]
    private ?Credentials $credentials = null;

    /**
     * @var string|null
     */
    #[Ignore]
    private ?string $selectedFaoEnvironment = null;

    /**
     *
     */
    public function __construct()
    {
    }

    /**
     * @return string|null
     */
    public function getInstituteCode(): ?string
    {
        return $this->instituteCode;
    }

    /**
     * @param string|null $instituteCode
     * @return $this
     */
    public function setInstituteCode(?string $instituteCode): Provider
    {
        $this->instituteCode = $instituteCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return $this
     */
    public function setType(?string $type): Provider
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Options|null
     */
    public function getPids(): ?Options
    {
        return $this->pids;
    }

    /**
     * @param Options|null $pids
     * @return $this
     */
    public function setPids(Options|null $pids): Provider
    {
        if (!isset($pids)) {
            return $this;
        }

        $this->pids = $pids;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): Provider
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return $this
     */
    public function setAddress(?string $address): Provider
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return $this
     */
    public function setCountry(?string $country): Provider
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return $this
     */
    public function setEmail(?string $email): Provider
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getProvidingFor(): ?array
    {
        return $this->providingFor;
    }

    /**
     * @param array|null $providingFor
     * @return $this
     */
    public function setProvidingFor(?array $providingFor): Provider
    {
        $this->providingFor = $providingFor;
        return $this;
    }

    /**
     * @param string $instituteCode
     * @return bool
     */
    public function isProvidingFor(string $instituteCode): bool
    {
        return ArrayHelper::isIn($instituteCode, $this->providingFor ?? []);
    }

    /**
     * @return Credentials|null
     */
    public function getCredentials(): ?Credentials
    {
        return $this->credentials;
    }

    /**
     * @param Credentials|null $credentials
     * @return $this
     */
    public function setCredentials(?Credentials $credentials): Provider
    {
        $this->credentials = $credentials;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSelectedFaoEnvironment(): ?string
    {
        return $this->selectedFaoEnvironment;
    }

    /**
     * @param string|null $selectedFaoEnvironment
     * @return $this
     */
    public function setSelectedFaoEnvironment(?string $selectedFaoEnvironment): Provider
    {
        $this->selectedFaoEnvironment = $selectedFaoEnvironment;
        $this->credentials?->setSelectedFaoEnvironment($selectedFaoEnvironment);
        return $this;
    }

    /**
     * @param string|null $env
     * @return string|null
     */
    #[Ignore]
    public function getPid(?string $env = null): ?string
    {
        if (isset($env)) {
            /** @var Options $pids */
            $pids = $this->pids;

            /** @var ?Options $data */
            return $pids->offsetExists($env) ? (string)$pids->offsetGet($env) : null;
        }
        return match ($this->selectedFaoEnvironment) {
            'prod','test' => $this->getPid($this->selectedFaoEnvironment),
            default => null,
        };
    }
}
