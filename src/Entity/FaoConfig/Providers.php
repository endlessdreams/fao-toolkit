<?php
/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * <Providers defaultInstituteCode=[defaultInstituteCode]>
 *  <provider instituteCode="[instituteCode]">
 *    <type>[type]</type>
 *    <pids>[...]</pids>
 *    <name>[name]</name>
 *    <address>[address]</address>
 *    <country>[country]</country>
 *    <email>[email]</email>
 *    <providingFor>[...]</providingFor>
 *    <credentials>
 *      <credential env="[env]">*
 *        <username<[username]</username>
 *        <password>[password]</password>
 *      </credential>
 *    </credentials>
 *  </provider>
 * </Providers>
 */
class Providers //implements \IteratorAggregate, \Countable, \ArrayAccess
{
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('@defaultInstituteCode')]
    private ?string $defaultInstituteCode = null;

    /**
     * @var Provider[]
     */
    #[Groups(['Default'])]
    #[SerializedName('provider')]
    private array $providers = [];

    /**
     * @return string|null
     */
    public function getDefaultInstituteCode(): ?string
    {
        return $this->defaultInstituteCode;
    }

    /**
     * @param string|null $defaultInstituteCode
     * @return $this
     */
    public function setDefaultInstituteCode(?string $defaultInstituteCode): Providers
    {
        $this->defaultInstituteCode = $defaultInstituteCode;
        return $this;
    }

    /**
     * @return Provider[]
     */
    public function getProviders(): array
    {
        return $this->providers;
    }

    /**
     * @param Provider[] $providers
     * @return $this
     */
    public function setProviders(array $providers): Providers
    {
        $this->providers = $providers;
        return $this;
    }

    /**
     * @param string|null $instituteCode
     * @return Provider|null
     */
    public function findProvider(?string $instituteCode): ?Provider
    {
        foreach ($this->providers as $index => $provider) {
            if ($provider->getInstituteCode() === $instituteCode) {
                return $provider;
            }
        }
        return null;
    }

    /**
     * @return Provider|null
     */
    public function findDefaultProvider(): ?Provider
    {
        return $this->findProvider($this->defaultInstituteCode);
    }

    /**
     * @param string|null $selectedFaoEnvironment
     * @return $this
     */
    #[Ignore]
    public function setSelectedFaoEnvironment(?string $selectedFaoEnvironment): Providers
    {
        foreach ($this->providers as $index => $provider) {
            $provider->setSelectedFaoEnvironment($selectedFaoEnvironment);
        }
        return $this;
    }
}
