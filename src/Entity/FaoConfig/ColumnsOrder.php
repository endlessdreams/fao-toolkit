<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\Base\ColumnsBase;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 *
 * <ColumnsOrder>
 *   <id>[id]</id>
 *   <symbol>[symbol]</symbol>
 *   <date>[date]</date>
 *   <type>[type]</type>
 *   <language>[language]</language>
 *   <shipName>[shipName]</shipName>
 *   <recipient_type>[recipient_type]</recipient_type>
 *   <recipient_pid>[recipient_pid]</recipient_pid>
 *   <recipient_name>[recipient_name]</recipient_name>
 *   <recipient_address>[recipient_address]</recipient_address>
 *   <recipient_country>[recipient_country]</recipient_country>
 *   <document_location>[document_location]</document_location>
 *   <document_retInfo>[document_retInfo]</document_retInfo>
 *   <document_pdf>[document_pdf]</document_pdf>
 *   <fao_institute_code>[fao_institute_code]</fao_institute_code>
 * </ColumnsOrder>
 */
class ColumnsOrder extends ColumnsBase
{
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $id = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $symbol = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $date = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $type = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $language = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $shipName = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('recipient_type')]
    private ?string $recipientType = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('recipient_pid')]
    private ?string $recipientPid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('recipient_name')]
    private ?string $recipientName = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('recipient_address')]
    private ?string $recipientAddress = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('recipient_country')]
    private ?string $recipientCountry = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('document_location')]
    private ?string $documentLocation = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('document_retInfo')]
    private ?string $documentRetinfo = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('document_pdf')]
    private ?string $documentPdf = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('fao_institute_code')]
    private ?string $faoInstituteCode = null;

    /**
     * @return string|null
     */

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return $this
     */
    public function setId(?string $id): ColumnsOrder
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * @param string|null $symbol
     * @return $this
     */
    public function setSymbol(?string $symbol): ColumnsOrder
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return $this
     */
    public function setDate(?string $date): ColumnsOrder
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return $this
     */
    public function setType(?string $type): ColumnsOrder
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     * @return $this
     */
    public function setLanguage(?string $language): ColumnsOrder
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShipName(): ?string
    {
        return $this->shipName;
    }

    /**
     * @param string|null $shipName
     * @return $this
     */
    public function setShipName(?string $shipName): ColumnsOrder
    {
        $this->shipName = $shipName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientType(): ?string
    {
        return $this->recipientType;
    }

    /**
     * @param string|null $recipientType
     * @return $this
     */
    public function setRecipientType(?string $recipientType): ColumnsOrder
    {
        $this->recipientType = $recipientType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientPid(): ?string
    {
        return $this->recipientPid;
    }

    /**
     * @param string|null $recipientPid
     * @return $this
     */
    public function setRecipientPid(?string $recipientPid): ColumnsOrder
    {
        $this->recipientPid = $recipientPid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    /**
     * @param string|null $recipientName
     * @return $this
     */
    public function setRecipientName(?string $recipientName): ColumnsOrder
    {
        $this->recipientName = $recipientName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientAddress(): ?string
    {
        return $this->recipientAddress;
    }

    /**
     * @param string|null $recipientAddress
     * @return $this
     */
    public function setRecipientAddress(?string $recipientAddress): ColumnsOrder
    {
        $this->recipientAddress = $recipientAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientCountry(): ?string
    {
        return $this->recipientCountry;
    }

    /**
     * @param string|null $recipientCountry
     * @return $this
     */
    public function setRecipientCountry(?string $recipientCountry): ColumnsOrder
    {
        $this->recipientCountry = $recipientCountry;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentLocation(): ?string
    {
        return $this->documentLocation;
    }

    /**
     * @param string|null $documentLocation
     * @return $this
     */
    public function setDocumentLocation(?string $documentLocation): ColumnsOrder
    {
        $this->documentLocation = $documentLocation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentRetinfo(): ?string
    {
        return $this->documentRetinfo;
    }

    /**
     * @param string|null $documentRetinfo
     * @return $this
     */
    public function setDocumentRetinfo(?string $documentRetinfo): ColumnsOrder
    {
        $this->documentRetinfo = $documentRetinfo;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentPdf(): ?string
    {
        return $this->documentPdf;
    }

    /**
     * @param string|null $documentPdf
     * @return $this
     */
    public function setDocumentPdf(?string $documentPdf): ColumnsOrder
    {
        $this->documentPdf = $documentPdf;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFaoInstituteCode(): ?string
    {
        return $this->faoInstituteCode;
    }

    /**
     * @param string|null $faoInstituteCode
     * @return $this
     */
    public function setFaoInstituteCode(?string $faoInstituteCode): ColumnsOrder
    {
        $this->faoInstituteCode = $faoInstituteCode;
        return $this;
    }
}
