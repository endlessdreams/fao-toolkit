<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\Base\ColumnsBase;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *
 * <ColumnsAccession>
 *     <sampledoi>[sampledoi]<sampledoi>
 * </ColumnsAccession>
 */
class ColumnsAccession extends ColumnsBase
{
    /*
    / * Has to be defined, since all database engines doesn't have views with write capabilties * /
    / * Name of table where column containing doi resides. * /
    'columns_accession' => [
    'sampledoi' => 'sampledoi',
    ],
    */
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $sampledoi = null;

    /**
     * @return string|null
     */
    public function getSampledoi(): ?string
    {
        return $this->sampledoi;
    }

    /**
     * @param string|null $sampledoi
     * @return $this
     */
    public function setSampledoi(?string $sampledoi): ColumnsAccession
    {
        $this->sampledoi = $sampledoi;
        return $this;
    }
}
