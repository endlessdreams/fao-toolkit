<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbConnectionService;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Yiisoft\Db\Mssql\Connection as MssqlConnection;
use Yiisoft\Db\Mysql\Connection as MysqlConnection;
use Yiisoft\Db\Pgsql\Connection as PgsqlConnection;
use Yiisoft\Db\Sqlite\Connection as SqliteConnection;
use Yiisoft\Db\Oracle\Connection as OracleConnection;

/**
 *
 * <databases>
 *   <database module="[module]">*
 *     <driver>[driver]</driver>
 *     <host>[host]</host>
 *     <database_name>[database_name]</database_name>
 *     <port>[port]</port>
 *     <options>[...]</options>
 *     <username>[username]</username>
 *     <password>[password]</password>
 *   </database>
 * </databases>
 */
class Databases
{
    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Groups(['Default'])]
    #[SerializedName('@defaultModule')]
    private ?string $defaultModule = null;

    /**
     * @var array<int,Database>|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('database')]
    private ?array $databases = [];

    /**
     * @var DbConnectionService|null
     */
    #[Ignore]
    private ?DbConnectionService $service = null;

    /**
     * @var string|null
     */
    #[Ignore]
    private ?string $selectedModule = null;

    /**
     * @return string|null
     */
    public function getDefaultModule(): ?string
    {
        return $this->defaultModule;
    }

    /**
     * @param string|null $defaultModule
     * @return $this
     */
    public function setDefaultModule(?string $defaultModule): Databases
    {
        $this->defaultModule = $defaultModule;
        return $this;
    }

    /**
     * @return array<int,Database>|null
     */
    public function getDatabases(): ?array
    {
        return $this->databases;
    }

    /**
     * @param array<int,Database>|null $databases
     * @return Databases|null
     */
    public function setDatabases(?array $databases): ?Databases
    {
        $this->databases = $databases;
        return $this;
    }

    /**
     * @param Database|array<string,mixed> $database
     * @return Databases
     */
    public function addDatabase(Database|array $database): Databases
    {
        if (is_array($database)) {
            $databaseObj = new Database();
            /**
             * @var string $key
             * @var string $value
             */
            foreach ($database as $key => $value) {
                switch ($key) {
                    case '@databaseModule':
                        call_user_func([$databaseObj, 'setDatabaseModule'], $value);
                        break;
                    case 'database_name':
                        call_user_func([$databaseObj, 'setDatabaseName'], $value);
                        break;
                    case 'driver':
                    case 'host':
                    case 'port':
                    case 'options':
                    case 'username':
                    case 'password':
                        call_user_func([$databaseObj, 'set' . ucfirst($key)], $value);
                        break;
                }
            }
        } else {
            $databaseObj = $database;
        }
        $this->databases[] = $databaseObj;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasDatabases(): bool
    {
        return count($this->databases ?? []) > 0;
    }


    /**
     * @param Database $database
     * @return int|false
     */
    public function findDatabase(Database $database): int|false
    {
        foreach ($this->databases ?? [] as $index => $existedDatabase) {
            if ($existedDatabase === $database) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param Database $database
     * @return void
     */
    public function removeDatabase(Database $database): void
    {
        $index = $this->findDatabase($database);
        if (is_int($index) && isset($this->databases) && array_key_exists($index, $this->databases)) {
            unset($this->databases[$index]);
        }
    }

    /**
     * @param string|null $module
     * @return Database|null
     */
    #[Ignore]
    public function getDatabase(?string $module): ?Database
    {
        foreach ($this->databases ?? [] as $index => $existedDatabase) {
            if ($existedDatabase->getModule() === $module) {
                return $existedDatabase;
            }
        }
        if (isset($this->defaultModule)) {
            return $this->getDatabase($this->defaultModule);
        }
        return null;
    }

    /**
     * @return DbConnectionService|null
     */
    public function getService(): ?DbConnectionService
    {
        return $this->service;
    }

    /**
     * @param DbConnectionService|null $service
     * @return $this
     */
    public function setService(?DbConnectionService $service): Databases
    {
        $this->service = $service;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSelectedModule(): ?string
    {
        return $this->selectedModule;
    }

    /**
     * @param string|null $selectedModule
     * @return $this
     */
    public function setSelectedModule(?string $selectedModule): Databases
    {
        $this->selectedModule = $selectedModule;
        return $this;
    }

    /**
     * @param string|null $module
     *
     * @return MssqlConnection|MysqlConnection|OracleConnection|PgsqlConnection|SqliteConnection
     *
     * @throws Exception
     * @throws \Throwable
     *
     * @psalm-suppress MixedInferredReturnType
     * @psalm-suppress UndefinedClass
     */
    #[Ignore]
    public function getDbConnection(
        ?string $module = null
    ): MssqlConnection|MysqlConnection|OracleConnection|PgsqlConnection|SqliteConnection {
        $module ??= $this->selectedModule;
        $this->service?->setDatabase(
            $this->getDatabase($module)
                ?? throw new Exception("Was not able to get a database parameter of module $module.")
        );
        return $this->service?->getConnection()
            ?? throw new Exception('Was not able to get a db connection.');
    }
}
