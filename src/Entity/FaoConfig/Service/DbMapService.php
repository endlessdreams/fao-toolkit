<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig\Service;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\Base\ColumnsInterface;
use DateTime;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Generator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use Yiisoft\Db\Command\CommandInterface;
use Yiisoft\Db\Command\DataType;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Db\Expression\Expression;
use Yiisoft\Db\Query\Query;
use Yiisoft\Db\QueryBuilder\Condition\InCondition;
use Yiisoft\Db\QueryBuilder\Condition\NotCondition;

/**
 *
 */
class DbMapService
{
    /**
     * @var string|null
     */
    private ?string $faoInstituteCodeQueryColumn = null;

    /**
     * @var string|null
     */
    private ?string $uniqueKeyQueryColumn = null;

    /**
     * @var string|null
     */
    private ?string $dateRangeColumn = null;

    /**
     * @var CommandInterface|null
     */
    private ?CommandInterface $rowQueryCommand = null;

    /**
     * @var string|null
     */
    private ?string $mainTable = null;

    /**
     * @param FaoConfig $faoConfig
     */
    public function __construct(public readonly FaoConfig $faoConfig)
    {
    }

    /**
     * @param InputInterface|Query $inputOrQuery
     * @return Generator
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function getQueryGenerator(InputInterface|Query $inputOrQuery): Generator
    {
        if ($inputOrQuery instanceof InputInterface) {
            $rowQueryCommand = $this->generateRowQueryCommandByInput($inputOrQuery);
            $inputOrQuery = $this->getQuery($inputOrQuery);
        } else {
            $rowQueryCommand = $this->rowQueryCommand;
        }

        $rowQueryCommand?->bindParam(':pk', $pk, DataType::INTEGER);
        /** @var int|string $pk */
        foreach ($inputOrQuery->createCommand()->queryColumn() as $pk) {
            if (is_array($row = $rowQueryCommand?->queryOne())) {
                yield $row;
            }
        }
    }

    /**
     * @param InputInterface $input
     * @return Query
     * @throws DbMapException
     */
    public function getQuery(InputInterface $input): Query
    {
        $this->mainTable = match ($input->getArgument('command')) {
            'smta:register' => 'table_order',
            'glis:register', 'glis:update' => 'table_glis',
            default => throw new DbMapException('Command is not supported to get rows from database.'),
        };
        $query = $this->getTableQuery($this->mainTable)
            ?? throw new DbMapException('Could not get table definition of requested table.');
        $this->addWhereToQuery($query, $input);
        return $query;
    }

    /**
     * @param InputInterface $input
     * @return Query
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function getOrderQuery(InputInterface $input): Query
    {
        $mainTable = match ($input->getArgument('command')) {
            'smta:register' => 'table_order',
            'glis:register', 'glis:update', 'glis:get' => 'table_glis',
            default => throw new DbMapException('Command is not supported to get rows from database.'),
        };
        $query = $this->getTableQuery($mainTable)
            ?? throw new DbMapException('Could not get table definition of requested table.');
        $this->addWhereToQuery($query, $input);
        $this->addOrderByAndOffsetAndLimitToQuery($query, $input);
        $orderPkColumn = match ($input->getFirstArgument()) {
            'glis:register','glis:update', 'glis:get' => 'glisId',
            default => 'id'
        };
        $query->select("[[$orderPkColumn]]");

        return $query;
    }

    /**
     * @param InputInterface $input
     * @return CommandInterface
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function generateRowQueryCommandByInput(InputInterface $input): CommandInterface
    {
        [$mainTable, $pkColumn] = match ($input->getArgument('command')) {
            'smta:register'
                => ['table_order', $this->faoConfig->getMap()?->getColumnsOrder()->getId()],
            'glis:register', 'glis:update', 'glis:get'
                => ['table_glis', $this->faoConfig->getMap()?->getColumnsGlis()->getGlisId()],
            default => throw new DbMapException('Command is not supported to get rows from database.'),
        };

        $query = $this->getTableQuery($mainTable)
            ?? throw new DbMapException('Could not get table definition of requested table.');

        return $query->where("[[$pkColumn]] = :pk")->createCommand();
    }

    /**
     * @param string $faoTableName
     * @return string|null
     * @throws DbMapException
     */
    public function getTableName(string $faoTableName): ?string
    {
        $map = $this->faoConfig->getMap();
        return match ($faoTableName) {
            'table_order' => $map?->getTableOrder(),
            'table_item' => $map?->getTableItem(),
            'table_accession' => $map?->getTableAccession(),
            'table_cachedglis' => $map?->getTableCachedglis(),
            'table_glis' => $map?->getTableGlis(),
            'table_glis_cropname' => $map?->getTableGlisCropname(),
            'table_glis_target' => $map?->getTableGlisTarget(),
            'table_glis_name' => $map?->getTableGlisName(),
            'table_glis_id' => $map?->getTableGlisId(),
            'table_glis_collector' => $map?->getTableGlisCollector(),
            'table_glis_breeder' => $map?->getTableGlisBreeder(),
            default => throw new DbMapException(
                "Requested Fao table name implemented to retrieve client table name. [Table name: $faoTableName]"
            )
        };
    }

    /**
     * @param string $faoTableName
     * @param array $extraFields
     * @param array $excludeFields
     * @return array|null
     * @throws DbMapException
     */
    public function getColumnsSelectArrayByTable(
        string $faoTableName,
        array $extraFields = [],
        array $excludeFields = []
    ): ?array {
        return $this->getColumnsEntityByTable($faoTableName)
            ?->toSelectArray([], $extraFields, true, $excludeFields);
    }

    /**
     * @param string $faoTableName
     * @return ColumnsInterface|null
     * @throws DbMapException
     */
    public function getColumnsEntityByTable(string $faoTableName): ?ColumnsInterface
    {
        $map = $this->faoConfig->getMap();
        $columns = match ($faoTableName) {
            'table_order' => $map?->getColumnsOrder(),
            'table_item' => $map?->getColumnsItem(),
            'table_accession' => $map?->getColumnsAccession(),
            'table_cachedglis' => $map?->getColumnsCachedglis(),
            'table_glis' => $map?->getColumnsGlis(),
            'table_glis_cropname' => $map?->getColumnsGlisCropname(),
            'table_glis_target' => $map?->getColumnsGlisTarget(),
            'table_glis_name' => $map?->getColumnsGlisName(),
            'table_glis_id' => $map?->getColumnsGlisId(),
            'table_glis_collector' => $map?->getColumnsGlisCollector(),
            'table_glis_breeder' => $map?->getColumnsGlisBreeder(),
            default => throw new DbMapException(
                "Requested Fao table name is not implemented to retrieve client table columns. "
                . "[Table name: $faoTableName]"
            )
        };
        return ($columns instanceof ColumnsInterface)
            ? $columns
            : (in_array($faoTableName, ['table_cachedglis'])
                ? null
                : throw new DbMapException('Unexpected error. Wrong class.'));
    }

    /**
     * @param string $faoTableName
     * @param array $extraFields
     * @return Query|null
     * @throws DbMapException
     * @throws \Exception
     */
    public function getTableQuery(string $faoTableName, array $extraFields = []): ?Query
    {
        if (
            ($db = $this->faoConfig->getDbConnection()) !== null
            && !StringHelper::isEmpty($clientTableName = $this->getTableName($faoTableName))
            && !empty($clientColumns = $this->getColumnsSelectArrayByTable($faoTableName, $extraFields) ?? [])
        ) {
            /** @var string $clientTableName */
            return (new Query($db))->select($clientColumns)->from($clientTableName);
        }
        return null;
    }

    /**
     * @param Query $query
     * @param InputInterface $input
     * @return void
     * @throws DbMapException
     * @throws \Exception
     */
    public function addWhereToQuery(Query &$query, InputInterface $input): void
    {
        $quoter = $this->faoConfig->getDbConnection()?->getQuoter();
        //codecept_debug($input);
        [$module,$command] = $this->getModuleCommand($input);

        $columnsMap = match ($module) {
            'smta' => $this->faoConfig->getMap()?->getColumnsOrder()?->toArray(),
            'glis' => $this->faoConfig->getMap()?->getColumnsGlis()?->toArray(),
        };

        $dateColumn =
            is_array($columnsMap)
            && isset($this->dateRangeColumn)
            && array_key_exists($this->dateRangeColumn, $columnsMap)
            ? (string)$columnsMap[$this->dateRangeColumn]
            : (
                is_array($columnsMap)
                && isset($this->dateRangeColumn)
                && array_key_exists($this->dateRangeColumn, $columnsMap)
                ? (string)$columnsMap['date']
                : 'id'
            );

        $orderBy = $this->getOrderBy($input);

        $firstOrderByColumn = (
            is_array($orderBy)
            && count($orderBy) > 0
            && is_array($columnsMap)
            && count($columnsMap) > 0
            && array_key_exists($orderBy[0], $columnsMap)
        )
            ? (string)$columnsMap[$orderBy[0]]
            : $dateColumn;

        # Added since 3.0.8 -- To make sure that only GLIS records with DOI will be listed.
        if ($module === 'glis' && $command === 'update') {
            $query->andWhere(new NotCondition(['sampledoi' => null]));
        }

        // Get common parameters...
        foreach ($input->getOptions() as $option => $optionValue) {
            switch ($option) {
                case 'from':
                    if (!isset($optionValue)) {
                        break;
                    }
                    if (
                        $this->dateRangeColumn === 'date'
                        && is_string($optionValue)
                        && !($this->isValidDate($optionValue))
                    ) {
                        throw new DbMapException('Invalid formatted from date. Please use format [YYY-mm-dd].');
                    }
                    $query->andWhere(['>=', $firstOrderByColumn, $optionValue]);
                    break;
                case 'to':
                    if (!isset($optionValue)) {
                        break;
                    }
                    if (
                        $this->dateRangeColumn === 'date'
                        && !(
                            is_string($optionValue)
                            && $this->isValidDate($optionValue)
                        )
                    ) {
                        throw new DbMapException('Invalid formatted to date. Please use format [yyyy-mm-dd].');
                    }
                    $query->andWhere(['<', $firstOrderByColumn, $optionValue]);
                    break;
                case 'filter':
                    if (!isset($optionValue)) {
                        break;
                    }
                    if (
                        is_string($optionValue) && trim($optionValue) !== '' && is_string($this->uniqueKeyQueryColumn)
                    ) {
                        //$query->andWhere(['like', $this->uniqueKeyQueryColumn, $optionValue]);
                        $query->andWhere(
                            match ($this->faoConfig->getDbConnection()?->getDriverName()) {
                                'sqlsrv'
                                    => new Expression(
                                        (string)$quoter?->quoteColumnName($this->uniqueKeyQueryColumn)
                                        . " like " . (string)$quoter?->quoteValue($optionValue)
                                    ),
                                'oci'
                                    => new Expression(
                                        " REGEXP_LIKE ("
                                        . (string)$quoter?->quoteColumnName($this->uniqueKeyQueryColumn)
                                        . ",:uniqueKey)",
                                        [
                                            ':uniqueKey' => (string)$quoter?->quoteValue($optionValue)
                                        ]
                                    ),
                                'mysql', 'pgsql', 'cubrid'
                                    => new Expression(
                                        " REGEXP_LIKE("
                                        . (string)$quoter?->quoteColumnName($this->uniqueKeyQueryColumn)
                                        . ",:uniqueKey)",
                                        [
                                            ':uniqueKey' => (string)$quoter?->quoteValue($optionValue)
                                        ]
                                    ),
                                'sqlite'
                                    => new Expression(
                                        (string)$quoter?->quoteColumnName($this->uniqueKeyQueryColumn)
                                        . " REGEXP " . (string)$quoter?->quoteValue($optionValue)
                                    ),
                                default
                                => '1=1'
                            }
                        );
                    }
                    break;
                case 'run-as':
                    if (!isset($optionValue)) {
                        throw new DbMapException('Option --run-as is required.');
                    }
                    if (is_string($optionValue) && trim($optionValue) !== '') {
                        $query->andWhere(
                            [
                                '=',
                                $this->getFaoInstituteCodeQueryColumn() ?? 'fao_institute_code', $optionValue
                            ]
                        );
                    }
                    break;
                case 'unsigned-doi':
                    if (is_bool($optionValue) && $optionValue) {
                        $query->andWhere(['sampledoi' => null]);
                    }
                    break;
                case 'sampledoi':
                case 'sampleid':
                case 'symbol':
                    /*$sampledoi = (is_string($optionValue))
                        ? $this->recursiveSplitArrayOfCommaSepStringsIntoOneArray([$optionValue])
                        : (
                            is_array($optionValue)
                            ? $this->recursiveSplitArrayOfCommaSepStringsIntoOneArray($optionValue)
                            : []
                        );
                    if ($sampledoi !== []) {
                        $sampledoiColumnName = (string)$this->faoConfig->getMap()?->getColumnsGlis()->getSampledoi();
                        $query->andWhere(new InCondition($sampledoiColumnName, 'in', $sampledoi));
                    }*/
                    $this->commaSplitOptionValueArrayIntoWhereInStatement(
                        $query,
                        match ($option) {
                            'sampledoi' => (string)$this->faoConfig->getMap()?->getColumnsGlis()->getSampledoi(),
                            'sampleid' => (string)$this->faoConfig->getMap()?->getColumnsGlis()->getSampleid(),
                            'symbol' => (string)$this->faoConfig->getMap()?->getColumnsOrder()->getSymbol(),
                        },
                        $optionValue
                    );
                    break;
                //case 'sampleid':
                    /*
                    $sampleid = (is_string($optionValue))
                        ? $this->recursiveSplitArrayOfCommaSepStringsIntoOneArray([$optionValue])
                        : (
                            is_array($optionValue)
                            ? $this->recursiveSplitArrayOfCommaSepStringsIntoOneArray($optionValue)
                            : []
                        );
                    if ($sampleid !== []) {
                        $sampleidColumnName = (string)$this->faoConfig->getMap()?->getColumnsGlis()->getSampleid();
                        $query->andWhere(new InCondition($sampleidColumnName, 'in', $sampleid));
                    }*/
                    /*$this->commaSplitOptionValueArrayIntoWhereInStatement(
                        $query,
                        (string)$this->faoConfig->getMap()?->getColumnsGlis()->getSampleid(),
                        $optionValue
                    );*/
                    //break;
                //case 'symbol':
            /*
                    $this->commaSplitOptionValueArrayIntoWhereInStatement(
                        $query,
                        (string)$this->faoConfig->getMap()?->getColumnsOrder()->getSymbol(),
                        $optionValue
                    );
                    break;*/
            }
        }
    }

    /**
     * @param Query $query
     * @param string $optionColumnName
     * @param string|array $optionValue
     * @return void
     */
    private function commaSplitOptionValueArrayIntoWhereInStatement(
        Query &$query,
        string $optionColumnName,
        string|array $optionValue
    ): void {
        $optionAsArray = (is_string($optionValue))
            ? $this->recursiveSplitArrayOfCommaSepStringsIntoOneArray([$optionValue])
            : (
                is_array($optionValue)
                ? $this->recursiveSplitArrayOfCommaSepStringsIntoOneArray($optionValue)
                : []
            );
        if ($optionAsArray !== []) {
            //$optionColumnName = (string)$this->faoConfig->getMap()?->getColumnsGlis()->getSampleid();
            $query->andWhere(new InCondition($optionColumnName, 'in', $optionAsArray));
        }
        return;
    }

    /**
     * @param string[] $array
     * @return string[]
     */
    private function recursiveSplitArrayOfCommaSepStringsIntoOneArray(array $array): array
    {
        $newArray = [];
        foreach ($array as $string) {
            $newArray = array_merge(
                $newArray,
                array_map(fn (string $item): string => trim($item), explode(',', $string))
            );
        }
        return $newArray;
    }

    /**
     * @param InputInterface $input
     * @return string[]|null
     *
     * @psalm-suppress MixedAssignment
     */
    private function getOrderBy(InputInterface $input): ?array
    {

        $orderBy = $input->getOption('order-by');
        $orderBy = (
            !isset($orderBy)
            || is_string($orderBy) && StringHelper::isEmpty($orderBy)
                ? null
                : $orderBy
        ) ?? $this->dateRangeColumn;
        return !isset($orderBy)
            ? (
                ($input->hasOption('from') || $input->hasOption('to'))
            ? [$this->dateRangeColumn] : [$this->uniqueKeyQueryColumn]
            )
            : (
                is_string($orderBy)
                    ? [$orderBy]
                    : (is_array($orderBy) ? $orderBy : null)
            );
    }

    /**
     * @param Query $query
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    public function addOrderByToQuery(Query &$query, InputInterface $input, OutputInterface $output): void
    {
        /** @var string[]|null $orderBy */
        $orderBy = $this->getOrderBy($input);
        foreach ($orderBy ?? [] as $oneOrderByElement) {
            $oneOrderByElementSplitted = explode(' ', $oneOrderByElement);

            $sortDir = match (count($oneOrderByElementSplitted) >= 2 ? strtoupper($oneOrderByElement[1]) : 'ASC') {
                'DESC' => 'DESC',
                default => 'ASC'
            };

            /** @var string[] $oneOrderByElement */
            $oneOrderByElementFirstPart = array_shift($oneOrderByElementSplitted);
            $query->addOrderBy("[[$oneOrderByElementFirstPart]] $sortDir");
        }

        if (!isset($orderBy) || count($orderBy) === 0) {
            $query->orderBy($this->uniqueKeyQueryColumn ?? 'id');
        }
    }

    /**
     * @param InputInterface $input
     * @param string|null $sortDir
     * @return string|null
     * @throws \Exception
     */
    protected function prepareOrderedFilterExpression(InputInterface $input, string $sortDir = null): ?string
    {
        $sortDir ??= 'ASC';
        $driver = $this->faoConfig->getDbConnection()?->getDriverName();
        $filter = mb_ereg_replace('[%]', '', (string)$input->getOption('filter'));

        return isset($filter) && !StringHelper::isEmpty($filter) ? match ($driver) {
            'sqlsrv' => "CONVERT(INT,REPLACE([{$this->uniqueKeyQueryColumn}],'$filter','')) $sortDir",
            'mysql' => "CONVERT(REGEXP_REPLACE(`{$this->uniqueKeyQueryColumn}`,'$filter','')), INTEGER) $sortDir",
            'pgsql' => "CAST(REGEXP_REPLACE(\"{$this->uniqueKeyQueryColumn}\",'$filter','') AS INT) $sortDir",
            'oci' => "CAST(REGEX_REPLACE(\"{$this->uniqueKeyQueryColumn}\",'$filter','') AS INTEGER) $sortDir",
            'sqlite' => "CAST(REGEX_REPLACE('$filter','',\"{$this->uniqueKeyQueryColumn}\") AS INTEGER) $sortDir",
        } : null;
    }

    /**
     * @param InputInterface $input
     * @return Expression
     * @throws \Exception
     */
    protected function prepareRowOrderExpression(InputInterface $input): Expression
    {
        /** @var string[]|string|null $orderBy */
        $orderBy = $input->getOption('order-by');
        if (is_scalar($orderBy)) {
            $orderBy = [$orderBy];
        }
        $orderBy = count($orderBy ?? []) === 0
            ? (
                ($input->hasOption('from') || $input->hasOption('to'))
                ? [$this->dateRangeColumn]
                : [$this->uniqueKeyQueryColumn]
            )
            : $orderBy;

        $rowOrderOrderByArray = [];

        foreach ($orderBy as $oneOrderByElement) {
            if (!isset($oneOrderByElement)) {
                continue;
            }
            $oneOrderByElementSplitted = explode(' ', $oneOrderByElement);

            $sortDir = match (count($oneOrderByElementSplitted) >= 2 ? strtoupper($oneOrderByElement[1]) : 'ASC') {
                'DESC' => 'DESC',
                default => 'ASC'
            };

            /** @var string[] $oneOrderByElement */
            $oneOrderByElementFirstPart = array_shift($oneOrderByElementSplitted);

            $oneOrderByElementFirstPart =
                ($oneOrderByElementFirstPart === $this->uniqueKeyQueryColumn)
                ?
                    $this->prepareOrderedFilterExpression($input, $sortDir)
                    ?? "{{main}}.[[$oneOrderByElementFirstPart]] $sortDir"
                : "{{main}}.[[$oneOrderByElementFirstPart]] $sortDir";

            $rowOrderOrderByArray[] = $oneOrderByElementFirstPart;
        }

        return new Expression(
            'ROW_NUMBER() OVER (ORDER BY '
            . implode(', ', $rowOrderOrderByArray)
            . ')'
        );
    }

    /**
     * @param Query $query
     * @param InputInterface $input
     * @return void
     * @throws Throwable
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function addOrderByAndOffsetAndLimitToQuery(Query &$query, InputInterface $input): void
    {
        $offset = (string)($input->getOption('offset') ?? 0);
        $limit = $input->getOption('limit') !== null ? (string)$input->getOption('limit') : null;

        $queryMain = clone $query;
        $db = $this->faoConfig->getDbConnection();

        if (isset($db)) {
            $queryRowOrder = (new Query($db))
                ->Select(['*','_ROW_NUMBER_' => $this->prepareRowOrderExpression($input)])
                ->From(['main' => $queryMain]);

            $query = (new Query($db))
                ->From(['wrapper' => $queryRowOrder]);
            $betweenFrom = (int)$offset;
            $betweenTo = isset($limit) ? (int)((int)$limit + (int)$offset) : (int)$query->count();
            $query
                ->Where(['between', '_ROW_NUMBER_', ($betweenFrom + 1), $betweenTo ]);
        }
    }

    /**
     * @param InputInterface $input
     * @return string[]
     */
    public function getModuleCommand(InputInterface $input): array
    {
        return explode(':', (string)$input->getArgument('command'));
    }

    /**
     * @param string $date
     * @param string $format
     * @return bool
     */
    protected function isValidDate(string $date, string $format = 'Y-m-d'): bool
    {
        $dateTime = DateTime::createFromFormat($format, $date);
        return $dateTime && $dateTime->format($format) === $date;
    }

    /**
     * @return string|null
     */
    public function getFaoInstituteCodeQueryColumn(): ?string
    {
        return $this->faoInstituteCodeQueryColumn;
    }

    /**
     * @param string|null $faoInstituteCodeQueryColumn
     * @return $this
     */
    public function setFaoInstituteCodeQueryColumn(?string $faoInstituteCodeQueryColumn): DbMapService
    {
        $this->faoInstituteCodeQueryColumn = $faoInstituteCodeQueryColumn;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUniqueKeyQueryColumn(): ?string
    {
        $this->uniqueKeyQueryColumn ??= 'id';
        return $this->uniqueKeyQueryColumn;
    }

    /**
     * @param string|null $uniqueKeyQueryColumn
     * @return $this
     */
    public function setUniqueKeyQueryColumn(?string $uniqueKeyQueryColumn): DbMapService
    {
        $this->uniqueKeyQueryColumn = $uniqueKeyQueryColumn;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDateRangeColumn(): ?string
    {
        return $this->dateRangeColumn;
    }

    /**
     * @param string|null $dateRangeColumn
     * @return $this
     */
    public function setDateRangeColumn(?string $dateRangeColumn): DbMapService
    {
        $this->dateRangeColumn = $dateRangeColumn;
        return $this;
    }

    /**
     * @param CommandInterface|null $rowQueryCommand
     * @return $this
     */
    public function setRowQueryCommand(?CommandInterface $rowQueryCommand): DbMapService
    {
        $this->rowQueryCommand = $rowQueryCommand;
        return $this;
    }
}
