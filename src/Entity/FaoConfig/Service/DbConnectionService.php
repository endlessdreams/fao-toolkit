<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig\Service;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Database;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbConnectionException;
use Composer\InstalledVersions;
use Exception;
use Psr\Log\LoggerInterface;
use Throwable;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Cache\SchemaCache;
use Yiisoft\Strings\StringHelper;

/**
 *
 */
class DbConnectionService
{
    /**
     * @var array|string[]
     */
    private array $installedDrivers = [];

    /**
     * @var Database|null
     */
    private ?Database $database = null;

    /**
     * @param SchemaCache $schemaCache
     * @param LoggerInterface $logger
     * @param Aliases $aliases
     */
    public function __construct(
        private readonly SchemaCache $schemaCache,
        private readonly LoggerInterface $logger,
        private readonly Aliases $aliases
    ) {
        foreach (
            [
                'yiisoft/db-sqlite', 'yiisoft/db-pgsql', 'yiisoft/db-mysql', 'yiisoft/db-mssql', 'yiisoft/db-oracle'
            ] as $package
        ) {
            if (InstalledVersions::isInstalled($package)) {
                $this->installedDrivers[] = match ($package = str_replace('yiisoft/db-', '', $package)) {
                    'sqlite','pgsql','mysql' => $package,
                    'mssql' => 'sqlsrv',
                    'oracle' => 'oci',
                };
            }
        }
    }

    /**
     * @return Database|null
     */
    public function getDatabase(): ?Database
    {
        return $this->database;
    }

    /**
     * @param Database $database
     * @return $this
     */
    public function setDatabase(Database $database): DbConnectionService
    {
        $this->database = $database;
        return $this;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDsn(): string
    {
        $driver = $this->database?->getDriver();
        if (!in_array($driver, $this->installedDrivers)) {
            throw new Exception("Database driver $driver was not installed.");
        }

        return match ($driver) {
            'sqlite' =>
            $this->getDnsString(
                '\\Yiisoft\\Db\\Sqlite\\Dsn',
                [
                    $driver,
                    $this->aliases->get($this->database?->getDatabaseName() ?? '')
                ]
            ),
            'sqlsrv' =>
            $this->getDnsString(
                '\\Yiisoft\\Db\\Mssql\\Dsn',
                [
                    $driver,
                    $this->database?->getHost() ?? '',
                    $this->database?->getDatabaseName(),
                    $this->database?->getPort() ?? '1433'
                    /*,
                    $this->database?->getOptions()*/
                ]
            ),
            'pgsql' =>
            $this->getDnsString(
                '\\Yiisoft\\Db\\Pgsql\\Dsn',
                [
                    $driver,
                    $this->database?->getHost() ?? '',
                    $this->database?->getDatabaseName(),
                    $this->database?->getPort(),
                    //$this->database?->getOptions()
                ]
            ),
            'mysql' =>
            $this->getDnsString(
                '\\Yiisoft\\Db\\Mysql\\Dsn',
                [
                    $driver,
                    $this->database?->getHost() ?? '',
                    $this->database?->getDatabaseName(),
                    $this->database?->getPort(),
                    $this->database?->getOptions() ?? []
                ]
            ),
            'oci' =>
            $this->getDnsString(
                '\\Yiisoft\\Db\\Oracle\\Dsn',
                [
                    $driver,
                    $this->database?->getHost() ?? '',
                    $this->database?->getDatabaseName(),
                    $this->database?->getPort(),
                    $this->database?->getOptions()
                ]
            ),
            default => null,
        } ?? throw new Exception(
            "Database driver "
            . StringHelper::uppercaseFirstCharacter($driver ?? '')
            . " was not handled properly in DNS resolver."
        );
    }

    /**
     * @param string $classname
     * @param array $arguments
     * @return string|null
     *
     * @psalm-suppress MixedMethodCall
     */
    protected function getDnsString(string $classname, array $arguments): ?string
    {
        if (class_exists($classname)) {
            $obj = new $classname(
                ...$arguments
            );
            if (method_exists($obj, '__toString')) {
                return (string)$obj->__toString();
            }
        }
        return null;
    }

    /**
     * @return \Yiisoft\Db\Mssql\Connection|\Yiisoft\Db\Mysql\Connection|\Yiisoft\Db\Oracle\Connection|\Yiisoft\Db\Pgsql\Connection|\Yiisoft\Db\Sqlite\Connection
     *
     * @throws DbConnectionException|Throwable
     *
     * @psalm-suppress UndefinedClass
     */
    public function getConnection(): \Yiisoft\Db\Mssql\Connection | \Yiisoft\Db\Mysql\Connection
    | \Yiisoft\Db\Oracle\Connection | \Yiisoft\Db\Pgsql\Connection | \Yiisoft\Db\Sqlite\Connection
    {
        $driver = $this->database?->getDriver();
        try {
            switch ($driver) {
                case 'sqlite':
                    $connection = new \Yiisoft\Db\Sqlite\Connection(
                        driver: new \Yiisoft\Db\Sqlite\Driver(
                            dsn: $this->getDsn()
                        ),
                        schemaCache: $this->schemaCache
                    );
                    $connection->setLogger($this->logger);
                    break;
                case 'sqlsrv':
                    $connection = new \Yiisoft\Db\Mssql\Connection(
                        driver: new \Yiisoft\Db\Mssql\Driver(
                            dsn: $this->getDsn(),
                            username: $this->database?->getUsername() ?? '',
                            password: $this->database?->getPassword() ?? ''
                        ),
                        schemaCache: $this->schemaCache
                    );
                    $connection->setLogger($this->logger);
                    break;
                case 'pgsql':
                    $connection = new \Yiisoft\Db\Pgsql\Connection(
                        driver: new \Yiisoft\Db\Pgsql\Driver(
                            dsn: $this->getDsn(),
                            username: $this->database?->getUsername() ?? '',
                            password: $this->database?->getPassword() ?? ''
                        ),
                        schemaCache: $this->schemaCache
                    );
                    $connection->setLogger($this->logger);
                    break;
                case 'mysql':
                    $connection = new \Yiisoft\Db\Mysql\Connection(
                        driver: new \Yiisoft\Db\Mysql\Driver(
                            dsn: $this->getDsn(),
                            username: $this->database?->getUsername() ?? '',
                            password: $this->database?->getPassword() ?? ''
                        ),
                        schemaCache: $this->schemaCache
                    );
                    $connection->setLogger($this->logger);
                    break;
                case 'oci':
                    $connection = new \Yiisoft\Db\Oracle\Connection(
                        driver: new \Yiisoft\Db\Oracle\Driver(
                            dsn: $this->getDsn(),
                            username: $this->database?->getUsername() ?? '',
                            password: $this->database?->getPassword() ?? ''
                        ),
                        schemaCache: $this->schemaCache
                    );
                    $connection->setLogger($this->logger);
                    break;
                default:
                    throw new DbConnectionException("Database driver $driver was not handled in Connection resolver.");
            }
        } catch (DbConnectionException $e) {
            $re = '/Database connection for (\w+)/s';
            $this->addSupportException($e, $re);
        } catch (Exception | Throwable $e) {
            $re = '/Class "Yiisoft\\\\Db\\\\(.+)\\\\Connection" not found/s';
            $this->addSupportException($e, $re);

            $re = '/Database driver (\w+)/is';
            $this->addSupportException($e, $re);
        }
        return $connection
            ?? throw new DbConnectionException("Database connection for $driver could not be resolved.");
    }

    /**
     * @param Exception|Throwable $e
     * @param string $re
     * @return void
     * @throws DbConnectionException
     * @throws Exception|Throwable
     *
     * @psalm-suppress ArgumentTypeCoercion
     */
    protected function addSupportException(Exception|Throwable $e, string $re): void
    {
        if (
            !\EndlessDreams\FaoToolkit\Service\Helper\StringHelper::isEmpty($e->getMessage())
            && preg_match($re, $e->getMessage(), $matches, PREG_UNMATCHED_AS_NULL) === 1
        ) {
            $dbType = $matches[1];
            match ($dbType) {
                'Sqlite',
                'Mssql',
                'Pgsql',
                'Mysql',
                'Oracle' => throw new DbConnectionException(
                    "Configuration file assumes support for $dbType." . PHP_EOL
                    . "You can add support by running:" . PHP_EOL
                    . "<fg=white;options=bold>composer require yiisoft/db-" . mb_strtolower($dbType) . '</>' . PHP_EOL
                ),
                default => throw $e,
            };
        }
    }
}
