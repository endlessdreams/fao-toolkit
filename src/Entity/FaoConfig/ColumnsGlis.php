<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\Base\ColumnsBase;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 */
class ColumnsGlis extends ColumnsBase
{
    /**
     * PK name of this table isn't of interest. Only FK to glis/accession table
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('glis_id')]
    private ?string $glisId = null;

    /**
     * Use only this when table Clis is no MCPD Cache table, and contains actual writable doi column.
     *
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    private ?string $doi = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $lwiews = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $lpid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $lname = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $laddress = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $lcountry = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $sampledoi = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $sampleid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $date = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $method = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $genus = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $species = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $cropname = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $cropnames = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $tvalue = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $tkw = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $targets = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $progdoi = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $progdois = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $biostatus = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $spauth = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $subtaxa = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $stauth = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $nvalue = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $names = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $itype = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $ivalue = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $ids = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $mlsstatus = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $hist = null;

    /* FAO WIEWS code of the organization providing the PGRFA. See "Actor element” for details. */


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $pwiews = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $ppid = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $pname = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $paddress = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $pcountry = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $psampleid = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $provenance = null;

    /* FAO WIEWS code of the organization collecting the PGRFA.
     * See "Actor element” for details. Any number of <collector> can be provided. */

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $cwiews = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $cpid = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $cname = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $caddress = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $ccountry = null;

    /* Alternative for multiple collectors. */
    /* json example:
     * [{"wiews": "<cwiews>", "pid": "<cpid>", "name": "<cname>", "address": "<caddress>", "country": "<ccountry>"}]
     */

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $collectors = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $csampleid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $missid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $site = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $clat = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $clon = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $uncert = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $datum = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $georef = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $elevation = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $cdate = null;
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $source = null;

    /* FAO WIEWS code of the breeding organization.
     * See "Actor element" for details. Any number of <breeder> can be provided */

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $bwiews = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $bpid = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $bname = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $baddress = null;


    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $bcountry = null;


/* Alternative for multiple breeders. */
/* json example:
 *  [{"wiews": "<bwiews>", "pid": "<bpid>", "name": "<bname>", "address": "<baddress>", "country": "<bcountry>"}]
 */

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $breeders = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $ancestry = null;

    /* Last update for filter */

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('last_update')]
    private ?string $lastUpdate = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('fao_institute_code')]
    private ?string $faoInstituteCode = null;

    /**
     * @return string|null
     */
    public function getGlisId(): ?string
    {
        return $this->glisId;
    }

    /**
     * @param string|null $glisId
     * @return $this
     */
    public function setGlisId(?string $glisId): ColumnsGlis
    {
        $this->glisId = $glisId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDoi(): ?string
    {
        return $this->doi;
    }

    /**
     * @param string|null $doi
     * @return $this
     */
    public function setDoi(?string $doi): ColumnsGlis
    {
        $this->doi = $doi;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLwiews(): ?string
    {
        return $this->lwiews;
    }

    /**
     * @param string|null $lwiews
     * @return $this
     */
    public function setLwiews(?string $lwiews): ColumnsGlis
    {
        $this->lwiews = $lwiews;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLpid(): ?string
    {
        return $this->lpid;
    }

    /**
     * @param string|null $lpid
     * @return $this
     */
    public function setLpid(?string $lpid): ColumnsGlis
    {
        $this->lpid = $lpid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLname(): ?string
    {
        return $this->lname;
    }

    /**
     * @param string|null $lname
     * @return $this
     */
    public function setLname(?string $lname): ColumnsGlis
    {
        $this->lname = $lname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLaddress(): ?string
    {
        return $this->laddress;
    }

    /**
     * @param string|null $laddress
     * @return $this
     */
    public function setLaddress(?string $laddress): ColumnsGlis
    {
        $this->laddress = $laddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLcountry(): ?string
    {
        return $this->lcountry;
    }

    /**
     * @param string|null $lcountry
     * @return $this
     */
    public function setLcountry(?string $lcountry): ColumnsGlis
    {
        $this->lcountry = $lcountry;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampledoi(): ?string
    {
        return $this->sampledoi;
    }

    /**
     * @param string|null $sampledoi
     * @return $this
     */
    public function setSampledoi(?string $sampledoi): ColumnsGlis
    {
        $this->sampledoi = $sampledoi;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleid(): ?string
    {
        return $this->sampleid;
    }

    /**
     * @param string|null $sampleid
     * @return $this
     */
    public function setSampleid(?string $sampleid): ColumnsGlis
    {
        $this->sampleid = $sampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return $this
     */
    public function setDate(?string $date): ColumnsGlis
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @param string|null $method
     * @return $this
     */
    public function setMethod(?string $method): ColumnsGlis
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenus(): ?string
    {
        return $this->genus;
    }

    /**
     * @param string|null $genus
     * @return $this
     */
    public function setGenus(?string $genus): ColumnsGlis
    {
        $this->genus = $genus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpecies(): ?string
    {
        return $this->species;
    }

    /**
     * @param string|null $species
     * @return $this
     */
    public function setSpecies(?string $species): ColumnsGlis
    {
        $this->species = $species;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCropname(): ?string
    {
        return $this->cropname;
    }

    /**
     * @param string|null $cropname
     * @return $this
     */
    public function setCropname(?string $cropname): ColumnsGlis
    {
        $this->cropname = $cropname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCropnames(): ?string
    {
        return $this->cropnames;
    }

    /**
     * @param string|null $cropnames
     * @return $this
     */
    public function setCropnames(?string $cropnames): ColumnsGlis
    {
        $this->cropnames = $cropnames;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTvalue(): ?string
    {
        return $this->tvalue;
    }

    /**
     * @param string|null $tvalue
     * @return $this
     */
    public function setTvalue(?string $tvalue): ColumnsGlis
    {
        $this->tvalue = $tvalue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTkw(): ?string
    {
        return $this->tkw;
    }

    /**
     * @param string|null $tkw
     * @return $this
     */
    public function setTkw(?string $tkw): ColumnsGlis
    {
        $this->tkw = $tkw;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargets(): ?string
    {
        return $this->targets;
    }

    /**
     * @param string|null $targets
     * @return $this
     */
    public function setTargets(?string $targets): ColumnsGlis
    {
        $this->targets = $targets;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProgdoi(): ?string
    {
        return $this->progdoi;
    }

    /**
     * @param string|null $progdoi
     * @return $this
     */
    public function setProgdoi(?string $progdoi): ColumnsGlis
    {
        $this->progdoi = $progdoi;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProgdois(): ?string
    {
        return $this->progdois;
    }

    /**
     * @param string|null $progdois
     * @return $this
     */
    public function setProgdois(?string $progdois): ColumnsGlis
    {
        $this->progdois = $progdois;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBiostatus(): ?string
    {
        return $this->biostatus;
    }

    /**
     * @param string|null $biostatus
     * @return $this
     */
    public function setBiostatus(?string $biostatus): ColumnsGlis
    {
        $this->biostatus = $biostatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpauth(): ?string
    {
        return $this->spauth;
    }

    /**
     * @param string|null $spauth
     * @return $this
     */
    public function setSpauth(?string $spauth): ColumnsGlis
    {
        $this->spauth = $spauth;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubtaxa(): ?string
    {
        return $this->subtaxa;
    }

    /**
     * @param string|null $subtaxa
     * @return $this
     */
    public function setSubtaxa(?string $subtaxa): ColumnsGlis
    {
        $this->subtaxa = $subtaxa;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStauth(): ?string
    {
        return $this->stauth;
    }

    /**
     * @param string|null $stauth
     * @return $this
     */
    public function setStauth(?string $stauth): ColumnsGlis
    {
        $this->stauth = $stauth;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNvalue(): ?string
    {
        return $this->nvalue;
    }

    /**
     * @param string|null $nvalue
     * @return $this
     */
    public function setNvalue(?string $nvalue): ColumnsGlis
    {
        $this->nvalue = $nvalue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNames(): ?string
    {
        return $this->names;
    }

    /**
     * @param string|null $names
     * @return $this
     */
    public function setNames(?string $names): ColumnsGlis
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItype(): ?string
    {
        return $this->itype;
    }

    /**
     * @param string|null $itype
     * @return $this
     */
    public function setItype(?string $itype): ColumnsGlis
    {
        $this->itype = $itype;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIvalue(): ?string
    {
        return $this->ivalue;
    }

    /**
     * @param string|null $ivalue
     * @return $this
     */
    public function setIvalue(?string $ivalue): ColumnsGlis
    {
        $this->ivalue = $ivalue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIds(): ?string
    {
        return $this->ids;
    }

    /**
     * @param string|null $ids
     * @return $this
     */
    public function setIds(?string $ids): ColumnsGlis
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMlsstatus(): ?string
    {
        return $this->mlsstatus;
    }

    /**
     * @param string|null $mlsstatus
     * @return $this
     */
    public function setMlsstatus(?string $mlsstatus): ColumnsGlis
    {
        $this->mlsstatus = $mlsstatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHist(): ?string
    {
        return $this->hist;
    }

    /**
     * @param string|null $hist
     * @return $this
     */
    public function setHist(?string $hist): ColumnsGlis
    {
        $this->hist = $hist;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPwiews(): ?string
    {
        return $this->pwiews;
    }

    /**
     * @param string|null $pwiews
     * @return $this
     */
    public function setPwiews(?string $pwiews): ColumnsGlis
    {
        $this->pwiews = $pwiews;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPpid(): ?string
    {
        return $this->ppid;
    }

    /**
     * @param string|null $ppid
     * @return $this
     */
    public function setPpid(?string $ppid): ColumnsGlis
    {
        $this->ppid = $ppid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPname(): ?string
    {
        return $this->pname;
    }

    /**
     * @param string|null $pname
     * @return $this
     */
    public function setPname(?string $pname): ColumnsGlis
    {
        $this->pname = $pname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaddress(): ?string
    {
        return $this->paddress;
    }

    /**
     * @param string|null $paddress
     * @return $this
     */
    public function setPaddress(?string $paddress): ColumnsGlis
    {
        $this->paddress = $paddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPcountry(): ?string
    {
        return $this->pcountry;
    }

    /**
     * @param string|null $pcountry
     * @return $this
     */
    public function setPcountry(?string $pcountry): ColumnsGlis
    {
        $this->pcountry = $pcountry;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPsampleid(): ?string
    {
        return $this->psampleid;
    }

    /**
     * @param string|null $psampleid
     * @return $this
     */
    public function setPsampleid(?string $psampleid): ColumnsGlis
    {
        $this->psampleid = $psampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProvenance(): ?string
    {
        return $this->provenance;
    }

    /**
     * @param string|null $provenance
     * @return $this
     */
    public function setProvenance(?string $provenance): ColumnsGlis
    {
        $this->provenance = $provenance;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCwiews(): ?string
    {
        return $this->cwiews;
    }

    /**
     * @param string|null $cwiews
     * @return $this
     */
    public function setCwiews(?string $cwiews): ColumnsGlis
    {
        $this->cwiews = $cwiews;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCpid(): ?string
    {
        return $this->cpid;
    }

    /**
     * @param string|null $cpid
     * @return $this
     */
    public function setCpid(?string $cpid): ColumnsGlis
    {
        $this->cpid = $cpid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCname(): ?string
    {
        return $this->cname;
    }

    /**
     * @param string|null $cname
     * @return $this
     */
    public function setCname(?string $cname): ColumnsGlis
    {
        $this->cname = $cname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCaddress(): ?string
    {
        return $this->caddress;
    }

    /**
     * @param string|null $caddress
     * @return $this
     */
    public function setCaddress(?string $caddress): ColumnsGlis
    {
        $this->caddress = $caddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCcountry(): ?string
    {
        return $this->ccountry;
    }

    /**
     * @param string|null $ccountry
     * @return $this
     */
    public function setCcountry(?string $ccountry): ColumnsGlis
    {
        $this->ccountry = $ccountry;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCollectors(): ?string
    {
        return $this->collectors;
    }

    /**
     * @param string|null $collectors
     * @return $this
     */
    public function setCollectors(?string $collectors): ColumnsGlis
    {
        $this->collectors = $collectors;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCsampleid(): ?string
    {
        return $this->csampleid;
    }

    /**
     * @param string|null $csampleid
     * @return $this
     */
    public function setCsampleid(?string $csampleid): ColumnsGlis
    {
        $this->csampleid = $csampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMissid(): ?string
    {
        return $this->missid;
    }

    /**
     * @param string|null $missid
     * @return $this
     */
    public function setMissid(?string $missid): ColumnsGlis
    {
        $this->missid = $missid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSite(): ?string
    {
        return $this->site;
    }

    /**
     * @param string|null $site
     * @return $this
     */
    public function setSite(?string $site): ColumnsGlis
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getClat(): ?string
    {
        return $this->clat;
    }

    /**
     * @param string|null $clat
     * @return $this
     */
    public function setClat(?string $clat): ColumnsGlis
    {
        $this->clat = $clat;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getClon(): ?string
    {
        return $this->clon;
    }

    /**
     * @param string|null $clon
     * @return $this
     */
    public function setClon(?string $clon): ColumnsGlis
    {
        $this->clon = $clon;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUncert(): ?string
    {
        return $this->uncert;
    }

    /**
     * @param string|null $uncert
     * @return $this
     */
    public function setUncert(?string $uncert): ColumnsGlis
    {
        $this->uncert = $uncert;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDatum(): ?string
    {
        return $this->datum;
    }

    /**
     * @param string|null $datum
     * @return $this
     */
    public function setDatum(?string $datum): ColumnsGlis
    {
        $this->datum = $datum;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGeoref(): ?string
    {
        return $this->georef;
    }

    /**
     * @param string|null $georef
     * @return $this
     */
    public function setGeoref(?string $georef): ColumnsGlis
    {
        $this->georef = $georef;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElevation(): ?string
    {
        return $this->elevation;
    }

    /**
     * @param string|null $elevation
     * @return $this
     */
    public function setElevation(?string $elevation): ColumnsGlis
    {
        $this->elevation = $elevation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCdate(): ?string
    {
        return $this->cdate;
    }

    /**
     * @param string|null $cdate
     * @return $this
     */
    public function setCdate(?string $cdate): ColumnsGlis
    {
        $this->cdate = $cdate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     * @return $this
     */
    public function setSource(?string $source): ColumnsGlis
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBwiews(): ?string
    {
        return $this->bwiews;
    }

    /**
     * @param string|null $bwiews
     * @return $this
     */
    public function setBwiews(?string $bwiews): ColumnsGlis
    {
        $this->bwiews = $bwiews;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBpid(): ?string
    {
        return $this->bpid;
    }

    /**
     * @param string|null $bpid
     * @return $this
     */
    public function setBpid(?string $bpid): ColumnsGlis
    {
        $this->bpid = $bpid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBname(): ?string
    {
        return $this->bname;
    }

    /**
     * @param string|null $bname
     * @return $this
     */
    public function setBname(?string $bname): ColumnsGlis
    {
        $this->bname = $bname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBaddress(): ?string
    {
        return $this->baddress;
    }

    /**
     * @param string|null $baddress
     * @return $this
     */
    public function setBaddress(?string $baddress): ColumnsGlis
    {
        $this->baddress = $baddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBcountry(): ?string
    {
        return $this->bcountry;
    }

    /**
     * @param string|null $bcountry
     * @return $this
     */
    public function setBcountry(?string $bcountry): ColumnsGlis
    {
        $this->bcountry = $bcountry;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBreeders(): ?string
    {
        return $this->breeders;
    }

    /**
     * @param string|null $breeders
     * @return $this
     */
    public function setBreeders(?string $breeders): ColumnsGlis
    {
        $this->breeders = $breeders;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAncestry(): ?string
    {
        return $this->ancestry;
    }

    /**
     * @param string|null $ancestry
     * @return $this
     */
    public function setAncestry(?string $ancestry): ColumnsGlis
    {
        $this->ancestry = $ancestry;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastUpdate(): ?string
    {
        return $this->lastUpdate;
    }

    /**
     * @param string|null $lastUpdate
     * @return $this
     */
    public function setLastUpdate(?string $lastUpdate): ColumnsGlis
    {
        $this->lastUpdate = $lastUpdate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFaoInstituteCode(): ?string
    {
        return $this->faoInstituteCode;
    }

    /**
     * @param string|null $faoInstituteCode
     * @return $this
     */
    public function setFaoInstituteCode(?string $faoInstituteCode): ColumnsGlis
    {
        $this->faoInstituteCode = $faoInstituteCode;
        return $this;
    }
}
