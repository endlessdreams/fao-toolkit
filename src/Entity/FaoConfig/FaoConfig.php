<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Trait\FaoConfigServiceTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Main Entity of FaoConfig model
 *
 * <FaoConfig>
 *     <options>[...]</options>
 *     <databases>
 *         <database module="[module]">*
 *             <driver>[driver]</driver>
 *             <host>[host]</host>
 *             <database_name>[database_name]</database_name>
 *             <port>[port]</port>
 *             <options>[...]</options>
 *             <username>[username]</username>
 *             <password>[password]</password>
 *         </database>
 *     </databases>
 *     <map>
 *         ...
 *     </map>
 *     <providers defaultInstituteCode=[defaultInstituteCode]>
 *       <provider instituteCode="[instituteCode]">*
 *         <type>[type]</type>
 *         <pids>[...]</pids>
 *         <name>[name]</name>
 *         <address>[address]</address>
 *         <country>[country]</country>
 *         <email>[email]</email>
 *         <providingFor>[...]</providingFor>
 *         <credentials>
 *           <credential env="[env]">*
 *             <username<[username]</username>
 *             <password>[password]</password>
 *           </credential>
 *         </credentials>
 *       </provider>
 *     </providers>
 *     <serverUrl>[...]</serverUrl>
 * </FaoConfig>
 */
class FaoConfig
{
    use FaoConfigServiceTrait;

    /**
     * @var Options|null
     */
    #[Groups(['Default'])]
    private ?Options $options = null;

    /**
     * @var Databases|null
     */
    #[Assert\NotBlank]
    #[Groups(['Default'])]
    private ?Databases $databases = null;

    /**
     * @var Map|null
     */
    #[Groups(['Default'])]
    private ?Map $map = null;

    /**
     * @var Providers|null
     */
    #[Assert\NotBlank]
    #[Groups(['Default'])]
    private ?Providers $providers = null;

    /**
     * @var array|null
     */
    #[Groups(['Default'])]
    private ?array $serverUrls = null;

    /**
     *
     */
    public function __construct()
    {
        $this->options = new Options();
    }

    /**
     * @return Options|null
     */
    public function getOptions(): ?Options
    {
        return $this->options;
    }

    /**
     * @param Options $options
     * @return $this
     */
    public function setOptions(Options $options): FaoConfig
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return Databases|null
     */
    public function getDatabases(): ?Databases
    {
        return $this->databases;
    }

    /**
     * @param Databases|null $databases
     * @return $this
     */
    public function setDatabases(?Databases $databases): FaoConfig
    {
        $this->databases = $databases;
        return $this;
    }

    /**
     * @return Map|null
     */
    public function getMap(): ?Map
    {
        return $this->map;
    }

    /**
     * @param Map|null $map
     * @return $this
     */
    public function setMap(?Map $map): FaoConfig
    {
        $this->map = $map;
        return $this;
    }

    /**
     * @return Providers|null
     */
    public function getProviders(): ?Providers
    {
        return $this->providers;
    }

    /**
     * @param Providers|null $providers
     * @return $this
     */
    public function setProviders(?Providers $providers): FaoConfig
    {
        $this->providers = $providers;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getServerUrls(): ?array
    {
        return $this->serverUrls;
    }

    /**
     * @param array|null $serverUrls
     * @return $this
     */
    public function setServerUrls(?array $serverUrls): FaoConfig
    {
        $this->serverUrls = $serverUrls;
        return $this;
    }
}
