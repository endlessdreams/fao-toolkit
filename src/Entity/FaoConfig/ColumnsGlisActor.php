<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\Base\ColumnsBase;
use EndlessDreams\FaoToolkit\Entity\Base\HasGlisIdInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 *
 * <ColumnsGlisActor>
 *   <glis_id>[glis_id]</glis_id>
 *   <wiews>[wiews]</wiews>
 *   <pid>[pid]</pid>
 *   <name>[name]</name>
 *   <address>[address]</address>
 *   <country>[country]</country>
 * </ColumnsGlisActor>
 */
class ColumnsGlisActor extends ColumnsBase implements HasGlisIdInterface
{
    /**
     * PK name of this table isn't of interest. Only FK to glis/accession table
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('glis_id')]
    private ?string $glisId = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $wiews = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $pid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $name = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $address = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $country = null;

    /**
     * @return string|null
     */
    public function getGlisId(): ?string
    {
        return $this->glisId;
    }

    /**
     * @param string|null $glisId
     * @return $this
     */
    public function setGlisId(?string $glisId): ColumnsGlisActor
    {
        $this->glisId = $glisId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWiews(): ?string
    {
        return $this->wiews;
    }

    /**
     * @param string|null $wiews
     * @return $this
     */
    public function setWiews(?string $wiews): ColumnsGlisActor
    {
        $this->wiews = $wiews;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPid(): ?string
    {
        return $this->pid;
    }

    /**
     * @param string|null $pid
     * @return $this
     */
    public function setPid(?string $pid): ColumnsGlisActor
    {
        $this->pid = $pid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): ColumnsGlisActor
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return $this
     */
    public function setAddress(?string $address): ColumnsGlisActor
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return $this
     */
    public function setCountry(?string $country): ColumnsGlisActor
    {
        $this->country = $country;
        return $this;
    }
}
