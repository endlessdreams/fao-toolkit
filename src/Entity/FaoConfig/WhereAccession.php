<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 *
 *  <WhereAccession>
 *    <glis_id>[glis_id]</glis_id>
 *    <sampleid>[sampleid]</sampleid>
 *    <cropname>[cropname]</cropname>
 *  </WhereAccession>
 */
class WhereAccession
{
/*
     'where_accession' => [
    'glis_id' => 'glis_id'
        / * 'sampleid' => "CONCAT(accession_number_part1, ' ', accession_number_part2, ' ', accession_number_part3)" * /
        / * If cropname is needed, then include a cropname key and value here. * /
    ],
 */
    /**
     * Prefered condition key, FK to Glis table
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('glis_id')]
    private ?string $glisId = null;

    /**
     * Use this if accession table has an accession number column or parts that can create it
     * example: "CONCAT(accession_number_part1, ' ', accession_number_part2, ' ', accession_number_part3)"
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $sampleid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $cropname = null;

    /**
     * @return string|null
     */
    public function getGlisId(): ?string
    {
        return $this->glisId;
    }

    /**
     * @param string|null $glisId
     * @return $this
     */
    public function setGlisId(?string $glisId): WhereAccession
    {
        $this->glisId = $glisId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleid(): ?string
    {
        return $this->sampleid;
    }

    /**
     * @param string|null $sampleid
     * @return $this
     */
    public function setSampleid(?string $sampleid): WhereAccession
    {
        $this->sampleid = $sampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCropname(): ?string
    {
        return $this->cropname;
    }

    /**
     * @param string|null $cropname
     * @return $this
     */
    public function setCropname(?string $cropname): WhereAccession
    {
        $this->cropname = $cropname;
        return $this;
    }
}
