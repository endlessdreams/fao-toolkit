<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <Credential env="[env]">
 *   <username<[username]</username>
 *   <password>[password]</password>
 * </Credential>
 */
class Credential
{
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('@env')]
    private ?string $env = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    private ?string $username = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    private ?string $password = null;

    /**
     * @return string|null
     */
    public function getEnv(): ?string
    {
        return $this->env;
    }

    /**
     * @param string|null $env
     * @return $this
     */
    public function setEnv(?string $env): Credential
    {
        $this->env = $env;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return $this
     */
    public function setUsername(?string $username): Credential
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return $this
     */
    public function setPassword(?string $password): Credential
    {
        $this->password = $password;
        return $this;
    }
}
