<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\FaoConfig
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\FaoConfig;

use EndlessDreams\FaoToolkit\Entity\Base\ColumnsBase;
use EndlessDreams\FaoToolkit\Entity\Base\HasGlisIdInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 *
 * <ColumnsGlisTarget>
 *   <glis_id>[glis_id]</glis_id>
 *   <value>[value]</value>
 *   <kw>[kw]</kw>
 * </ColumnsGlisTarget>
 */
class ColumnsGlisTarget extends ColumnsBase implements HasGlisIdInterface
{
    /**
     * PK name of this table isn't of interest. Only FK to glis/accession table
     *
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('glis_id')]
    private ?string $glisId = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $value = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $kw = null;

    /**
     * @return string|null
     */
    public function getGlisId(): ?string
    {
        return $this->glisId;
    }

    /**
     * @param string|null $glisId
     * @return $this
     */
    public function setGlisId(?string $glisId): ColumnsGlisTarget
    {
        $this->glisId = $glisId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return $this
     */
    public function setValue(?string $value): ColumnsGlisTarget
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKw(): ?string
    {
        return $this->kw;
    }

    /**
     * @param string|null $kw
     * @return $this
     */
    public function setKw(?string $kw): ColumnsGlisTarget
    {
        $this->kw = $kw;
        return $this;
    }
}
