<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use EndlessDreams\FaoToolkit\Entity\Base\Actor;
use EndlessDreams\FaoToolkit\Entity\Base\Entity;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 *
 * <get username="[username]" password="[password]">
 *   <location> <!-- Location element -->
 *     <wiews>[lwiews]</wiews>
 *     <pid>[lpid]</pid>
 *     <name>[lname]</name>
 *     <address>[laddress]</address>
 *     <country>[lcountry]</country>
 *   </location>
 *   <sampledoi>[sampledoi]</sampledoi>
 * </get>
 */
class Get extends Entity
{
    /**
     * @var string|null
     */
    #[Assert\NotBlank(groups: ['get'])]
    #[Assert\Length(
        max: 128,
        maxMessage: '@username cannot be longer than {{ limit }} characters.',
        groups: ['get']
    )]
    #[Groups(['get'])]
    #[SerializedName('@username')]
    private ?string $username = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(groups: ['get'])]
    #[Assert\Length(
        max: 128,
        maxMessage: '@password cannot be longer than {{ limit }} characters.',
        groups: ['get']
    )]
    #[Groups(['get'])]
    #[SerializedName('@password')]
    # [\SensitiveParameter] // TODO: Uncomment when shifting to PHP8.2
    private ?string $password = null;

    /**
     * @var int|null
     */
    #[Ignore]
    private ?int $rowNumber = null;

    /**
     * @var Actor|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Actor $location = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Sampledoi cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $sampledoi = null;

    // Methods

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return $this
     */
    public function setUsername(?string $username): Get
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return $this
     */
    public function setPassword(?string $password): Get
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return Actor|null
     */
    public function getLocation(): ?Actor
    {
        return $this->location;
    }

    /**
     * @param Actor|null $location
     * @return $this
     */
    public function setLocation(?Actor $location): Get
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampledoi(): ?string
    {
        return $this->sampledoi;
    }

    /**
     * @param string|null $sampledoi
     * @return $this
     */
    public function setSampledoi(?string $sampledoi): Get
    {
        $sampledoi = StringHelper::isEmpty($sampledoi) ? null : $sampledoi;
        $this->sampledoi = $sampledoi;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRowNumber(): ?int
    {
        return $this->rowNumber;
    }

    /**
     * @param int|null $rowNumber
     * @return $this
     */
    public function setRowNumber(?int $rowNumber): Get
    {
        $this->rowNumber = $rowNumber;
        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed $payload
     * @return void
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, mixed $payload): void
    {
        // Check Mandatory locaton
        if ($this->location?->isAllEmpty() ?? true) {
            $context->buildViolation('Property location is mantatory.')
                ->atPath('location.Actor')
                ->addViolation();
        }
    }
}
