<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <breeding>
 *   <breeders>
 *     <breeder>* <!-- Actor element -->
 *       <wiews>[wiews]</wiews>
 *       <pid>[pid]</pid>
 *       <name>[name]</name>
 *       <address>[address]</address>
 *       <country>[country]</country>
 *     </breeder>
 *   </breeders>
 *   <ancestry>[ancestry]</ancestry>
 * </breeding>
 */
class Breeding
{
    /**
     * @var Breeders|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Breeders $breeders = null;
    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 65536,
        maxMessage: 'Ancestry cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $ancestry = null;

    /**
     * @return Breeders|null
     */
    public function getBreeders(): ?Breeders
    {
        return $this->breeders;
    }

    /**
     * @param Breeders $breeders
     * @return Breeding
     */
    public function setBreeders(Breeders $breeders): Breeding
    {
        $this->breeders = $breeders;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAncestry(): ?string
    {
        return $this->ancestry;
    }

    /**
     * @param string|null $ancestry
     * @return Breeding
     */
    public function setAncestry(?string $ancestry): Breeding
    {
        $this->ancestry = $ancestry;
        return $this;
    }
}
