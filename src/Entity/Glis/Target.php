<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <target>
 *   <value>[tvalue]</value>
 *   <kws>
 *     <kw>[tkw]</kw>*
 *   </kws>
 * </target>
 */
class Target
{
    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 256,
        maxMessage: 'Value cannot be longer than {{ limit }} characters',
    )]
    #[Assert\Url]
    #[Groups(['Default'])]
    #[SerializedName('value')]
    private ?string $value = null;

    /**
     * @var Kws|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Kws $kws = null;

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return $this|null
     */
    public function setValue(?string $value): ?Target
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return Kws|null
     */
    public function getKws(): ?Kws
    {
        return $this->kws;
    }

    /**
     * @param Kws|null $kws
     * @return $this
     */
    public function setKws(?Kws $kws): Target
    {
        $this->kws = $kws;
        return $this;
    }
}
