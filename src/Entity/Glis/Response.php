<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 */
class Response implements ResponseBodyInterface
{
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $sampleid = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $genus = null;

    /**
     * @var string|null
     */
    #[Assert\AtLeastOneOf([
        new Assert\When(
            expression: 'this.getError() == ""',
            constraints: [new Assert\NotBlank(),],
        ),
        new Assert\When(
            expression: 'this.getError() != ""',
            constraints: [new Assert\Blank()],
        ),
        new Assert\Count(min: 0, max: 0),
    ])]
    #[Groups(['Default'])]
    private ?string $doi = null;

    /**
     * @var string|null
     */
    #[Assert\AtLeastOneOf([
        new Assert\When(
            expression: 'this.getDoi() == ""',
            constraints: [new Assert\NotBlank(),],
        ),
        new Assert\When(
            expression: 'this.getDoi() != ""',
            constraints: [new Assert\Blank()],
        ),
        new Assert\Count(min: 0, max: 0),
    ])]
    #[Groups(['Default'])]
    private ?string $error = null;

    /**
     * @return string|null
     */
    public function getSampleid(): ?string
    {
        return $this->sampleid;
    }

    /**
     * @param string|null $sampleid
     * @return $this
     */
    public function setSampleid(?string $sampleid): Response
    {
        $this->sampleid = $sampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenus(): ?string
    {
        return $this->genus;
    }

    /**
     * @param string|null $genus
     * @return $this
     */
    public function setGenus(?string $genus): Response
    {
        $this->genus = $genus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDoi(): ?string
    {
        return $this->doi;
    }

    /**
     * @param string|null $doi
     * @return $this
     */
    public function setDoi(?string $doi): Response
    {
        $this->doi = $doi;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @param string|null $error
     * @return $this
     */
    public function setError(?string $error): Response
    {
        $this->error = $error;
        return $this;
    }
}
