<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <ids>
 *   <id type="[itype]">[ivalue]</id>*
 * </ids>
 */
class Ids
{
    /**
     * @var Id[]
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('id')]
    private array $ids = [];

    /**
     * @return Id[]
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * @param Id[] $ids
     * @return void
     */
    public function setIds(array $ids): void
    {
        $this->ids = $ids;
    }

    /**
     * @param Id $id
     * @return void
     */
    public function addId(Id $id): void
    {
        $this->ids[] = $id;
    }

    /**
     * @return bool
     */
    public function hasIds(): bool
    {
        return count($this->ids) > 0;
    }

    /**
     * @param Id $id
     * @return int|false
     */
    public function findId(Id $id): int|false
    {
        /**
         * @var int $index
         * @var Id $existedId
         */
        foreach ($this->ids as $index => $existedId) {
            if ($existedId === $id) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param Id $id
     * @return void
     */
    public function removeId(Id $id): void
    {
        if ((($index = $this->findId($id)) !== false) && array_key_exists($index, $this->ids)) {
            unset($this->ids[$index]);
        }
    }
}
