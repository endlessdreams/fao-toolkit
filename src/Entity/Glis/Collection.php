<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <collection>
 *   <collectors>
 *     <collector>*
 *       <!-- Actor element -->
 *       <wiews>[cwiews]</wiews>
 *       <pid>[cpid]</pid>
 *       <name>[cname]</name>
 *       <address>[caddress]</address>
 *       <country>[ccountry]</country>
 *     </collector>
 *   </collectors>
 *   <sampleid>[csampleid]</sampleid>
 *   <missid>[missid]</missid>
 *   <site>[site]</site>
 *   <lat>[clat]</lat>
 *   <lon>[clon]</lon>
 *   <uncert>[uncert]</uncert>
 *   <datum>[datum]</datum>
 *   <georef>[georef]</georef>
 *   <elevation>[elevation]</elevation>
 *   <date>[cdate]</date>
 *   <source>[source]</source>
 * </collection>
 */
class Collection
{
    /**
     * @var Collectors|null
     */
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Collectors $collectors = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Sampleid cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $sampleid = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Missid cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $missid = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Site cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $site = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 10,
        maxMessage: 'Lat cannot be longer than {{ limit }} characters',
    )]
    #[Assert\AtLeastOneOf(
        [
            new Assert\Regex(
                pattern:'/^\d{1,2}°[0-5]\d’[0-5]\d”[NS]$/', // /^\d{1,2}[°][0-5]\d[’][0-5]\d[”][NS]$/
                message: 'Latitude value {{ value }} does not match format dd°mm’ss”[NS].' . PHP_EOL
            ),
            new Assert\Regex(
                pattern:'/^[-]?\d{1,2}([.]\d{1,5})?$/',
                message: 'Latitude value {{ value }} does not match format (-)dd.xxxxx.' . PHP_EOL
            ),
        ],
        message: 'This value should satisfy at least one of the following constraints:' . PHP_EOL
    )]
    #[Groups(['Default'])]
    private ?string $lat = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 10,
        maxMessage: 'Lon cannot be longer than {{ limit }} characters',
    )]
    #[Assert\AtLeastOneOf(
        [
            new Assert\Regex(
                pattern:'/^\d{1,3}°[0-5]\d’[0-5]\d”[EW]$/',
                message: 'Latitude value {{ value }} does not match format ddd°mm’ss”[EW]' . PHP_EOL
            ),
            new Assert\Regex(
                pattern:'/^[-]?\d{1,3}([.]\d{1,5})?$/',
                message: 'Latitude value {{ value }} does not match format (-)ddd.xxxxx' . PHP_EOL
            ),
        ],
        message: 'This value should satisfy at least one of the following constraints:' . PHP_EOL
    )]
    #[Groups(['Default'])]
    private ?string $lon = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 16,
        maxMessage: 'Uncert cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $uncert = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 16,
        maxMessage: 'Datum cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $datum = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Georef cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $georef = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 16,
        maxMessage: 'Elevation cannot be longer than {{ limit }} characters',
    )]
    #[Assert\Regex(pattern:'/^\d+$/', message: 'Elevation must be an integer metres above sea level')]
    #[Groups(['Default'])]
    private ?string $elevation = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 10,
        maxMessage: 'Date cannot be longer than {{ limit }} characters',
    )]
    #[Assert\AtLeastOneOf(
        [
            new Assert\Regex(
                pattern: '/^\d{4}[-]([0][1-9]|[1][0-2])[-]([0][1-9]|[12]\d|[3][01])$/',
                message: 'Date was not written in format YYYY-MM-DD.' . PHP_EOL
            ),
            new Assert\Regex(
                pattern: '/^\d{4}[-]([0][1-9]|[1][0-2])$/',
                message: 'Date was not written in format YYYY-MM.' . PHP_EOL
            ),
            new Assert\Regex(
                pattern: '/^\d{4}$/',
                message: 'Date was not written in format YYYY.' . PHP_EOL
            ),
        ],
        message: 'This date should satisfy at least one of the following formats:' . PHP_EOL
    )]
    #[Groups(['Default'])]
    private ?string $date = null;

    //[10, 11, 12, 13, 14, 15, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 40, 50, 60, 61, 62, 99]
    //callback: [ChoiceHelper::class, 'getGlisSourceChoices'],
    /*
     #[Assert\Length(
        max: 2,
        maxMessage: 'Source cannot be longer than {{ limit }} characters',
        groups: ['Default']
    )]
     */

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 2,
        maxMessage: 'Source cannot be longer than {{ limit }} characters',
    )]
    #[Assert\Choice(
        callback: [ChoiceHelper::class, 'getGlisSourceChoices'],
        message: 'Provided {{ label }} value {{ value }} is not valid, please use any of: [{{ choices }}].',
        groups: ['Default','Collection','Glis']
    )]
    #[Groups(['Default'])]
    private ?string $source = null;

    /**
     * @return Collectors|null
     */
    public function getCollectors(): ?Collectors
    {
        return $this->collectors;
    }

    /**
     * @param Collectors $collectors
     * @return Collection
     */
    public function setCollectors(Collectors $collectors): Collection
    {
        $this->collectors = $collectors;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleid(): ?string
    {
        return $this->sampleid;
    }

    /**
     * @param string|null $sampleid
     * @return Collection
     */
    public function setSampleid(?string $sampleid): Collection
    {
        $this->sampleid = $sampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMissid(): ?string
    {
        return $this->missid;
    }

    /**
     * @param string|null $missid
     * @return Collection
     */
    public function setMissid(?string $missid): Collection
    {
        $this->missid = $missid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSite(): ?string
    {
        return $this->site;
    }

    /**
     * @param string|null $site
     * @return Collection
     */
    public function setSite(?string $site): Collection
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLat(): ?string
    {
        return $this->lat;
    }

    /**
     * @param string|null $lat
     * @return Collection
     */
    public function setLat(?string $lat): Collection
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLon(): ?string
    {
        return $this->lon;
    }

    /**
     * @param string|null $lon
     * @return Collection
     */
    public function setLon(?string $lon): Collection
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUncert(): ?string
    {
        return $this->uncert;
    }

    /**
     * @param string|null $uncert
     * @return Collection
     */
    public function setUncert(?string $uncert): Collection
    {
        $this->uncert = $uncert;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDatum(): ?string
    {
        return $this->datum;
    }

    /**
     * @param string|null $datum
     * @return Collection
     */
    public function setDatum(?string $datum): Collection
    {
        $this->datum = $datum;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGeoref(): ?string
    {
        return $this->georef;
    }

    /**
     * @param string|null $georef
     * @return Collection
     */
    public function setGeoref(?string $georef): Collection
    {
        $this->georef = $georef;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElevation(): ?string
    {
        return $this->elevation;
    }

    /**
     * @param string|null $elevation
     * @return Collection
     */
    public function setElevation(?string $elevation): Collection
    {
        $this->elevation = $elevation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return Collection
     */
    public function setDate(?string $date): Collection
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     * @return Collection
     */
    public function setSource(?string $source): Collection
    {
        $this->source = $source;
        return $this;
    }
}
