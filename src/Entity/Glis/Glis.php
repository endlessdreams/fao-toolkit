<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use EndlessDreams\FaoToolkit\Entity\Base\Actor;
use EndlessDreams\FaoToolkit\Entity\Base\Entity;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use DateTime;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Root Entity in the Glis Model
 * <glis username="[username]" password="[password]">
 *   <location> <!-- Location element -->
 *     <wiews>[lwiews]</wiews>
 *     <pid>[lpid]</pid>
 *     <name>[lname]</name>
 *     <address>[laddress]</address>
 *     <country>[lcountry]</country>
 *   </location>
 *   <sampledoi>[sampledoi]</sampledoi>
 *   <sampleid>[sampleid]</sampleid>
 *   <date>[date]</date>
 *   <method>[method]</method>
 *   <genus>[genus]</genus>
 *   <cropnames>
 *     <name>[cropname]</name>*
 *   </cropnames>
 *   <targets>
 *     <target>*
 *       <value>[tvalue]</value>
 *       <kws>
 *         <kw>[tkw]</kw>*
 *       </kws>
 *     </target>
 *   </targets>
 *   <progdoi>
 *     <doi>[progdoi]</doi>*
 *   </progdoi>
 *   <biostatus>[biostatus]</biostatus>
 *   <species>[species]</species>
 *   <spauth>[spauth]</spauth>
 *   <subtaxa>[subtaxa]</subtaxa>
 *   <stauth>[stauth]</stauth>
 *   <names>
 *     <name>[nvalue]</name>*
 *   </names>
 *   <ids>
 *     <id type="[itype]">[ivalue]</id>*
 *   </ids>
 *   <mlsstatus>[mlsstatus]</mlsstatus>
 *   <historical>[hist]</historical>
 *   <acquisition>
 *     <provider>
 *       <!-- Actor element -->
 *       <wiews>[pwiews]</wiews>
 *       <pid>[ppid]</pid>
 *       <name>[pname]</name>
 *       <address>[paddress]</address>
 *       <country>[pcountry]</country>
 *     </provider>
 *     <sampleid>[psampleid]</sampleid>
 *     <provenance>[provenance]</provenance>
 *   </acquisition>
 *   <collection>
 *     <collectors>
 *       <collector>*
 *         <!-- Actor element -->
 *         <wiews>[cwiews]</wiews>
 *         <pid>[cpid]</pid>
 *         <name>[cname]</name>
 *         <address>[caddress]</address>
 *         <country>[ccountry]</country>
 *       </collector>
 *     </collectors>
 *     <sampleid>[csampleid]</sampleid>
 *     <missid>[missid]</missid>
 *     <site>[site]</site>
 *     <lat>[clat]</lat>
 *     <lon>[clon]</lon>
 *     <uncert>[uncert]</uncert>
 *     <datum>[datum]</datum>
 *     <georef>[georef]</georef>
 *     <elevation>[elevation]</elevation>
 *     <date>[cdate]</date>
 *     <source>[source]</source>
 *   </collection>
 *   <breeding>
 *     <breeders>
 *       <breeder>* <!-- Actor element -->
 *         <wiews>[wiews]</wiews>
 *         <pid>[pid]</pid>
 *         <name>[name]</name>
 *         <address>[address]</address>
 *         <country>[country]</country>
 *       </breeder>
 *     </breeders>
 *     <ancestry>[ancestry]</ancestry>
 *   </breeding>
 * </glis>
 */
class Glis extends Entity
{
    /**
     * @var string|null
     */
    #[Assert\NotBlank(groups: ['register','update'])]
    #[Assert\Length(
        max: 128,
        maxMessage: '@username cannot be longer than {{ limit }} characters.',
        groups: ['register','update']
    )]
    #[Groups(['register','update'])]
    #[SerializedName('@username')]
    private ?string $username = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(groups: ['register','update'])]
    #[Assert\Length(
        max: 128,
        maxMessage: '@password cannot be longer than {{ limit }} characters.',
        groups: ['register','update']
    )]
    #[Groups(['register','update'])]
    #[SerializedName('@password')]
    # [\SensitiveParameter] // TODO: Uncomment when shifting to PHP8.2
    private ?string $password = null;

    /**
     * @var int|null
     */
    #[Ignore]
    private ?int $rowNumber = null;

    /**
     * @var Actor|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Actor $location = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true, groups: ['register','get'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Sampledoi cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $sampledoi = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Sampleid cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $sampleid = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 10,
        maxMessage: 'Date cannot be longer than {{ limit }} characters.',
    )]
    #[Assert\AtLeastOneOf(
        [
            new Assert\Regex(
                pattern: '/^\d{4}[-]([0][1-9]|[1][0-2])[-]([0][1-9]|[12]\d|[3][01])$/',
                message: 'Date was not written in format YYYY-MM-DD.' . PHP_EOL
            ),
            new Assert\Regex(
                pattern: '/^\d{4}[-]([0][1-9]|[1][0-2])$/',
                message: 'Date was not written in format YYYY-MM.' . PHP_EOL
            ),
            new Assert\Regex(
                pattern: '/^\d{4}$/',
                message: 'Date was not written in format YYYY.' . PHP_EOL
            ),
        ],
        message: 'This date should satisfy at least one of the following formats:' . PHP_EOL
    )]
    #[Groups(['Default'])]
    private ?string $date = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 4,
        maxMessage: 'Method cannot be longer than {{ limit }} characters.',
    )]
    #[Assert\Choice(callback: [ChoiceHelper::class ,'getGlisMethodChoices'])]
    #[Groups(['Default'])]
    private ?string $method = null;

    /**
     * @var string|null
     * Need to add a common validation check for the whole Register class
     */
    #[Assert\Length(
        max: 64,
        maxMessage: 'Genus cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $genus = null;

    /**
     * @var Names|null
     */
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('cropnames')]
    private ?Names $cropnames = null;

    /**
     * @var Targets|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Targets $targets = null;

    /**
     * @var Dois|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('progdoi')]
    private ?Dois $progdois = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 3,
        maxMessage: 'Biostatus cannot be longer than {{ limit }} characters.',
    )]
    #[Assert\Choice(
        callback: [ChoiceHelper::class ,'getGlisSampstatOptionalChoices'],
        message: 'Provided biostatus value {{ value }} is not valid, please use any of: [{{ choices }}].'
    )]
    #[Groups(['Default'])]
    private ?string $biostatus = null;

    /**
     * @var string|null
     */
    # [Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Species cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $species = null;

    /**
     * @var string|null
     */
    # [Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 64,
        maxMessage: 'Spauth cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['glis','register','update','get'])]
    private ?string $spauth = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Subtaxa cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $subtaxa = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 64,
        maxMessage: 'Stauth cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $stauth = null;

    /**
     * @var Names|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Names $names = null;


    /**
     * @var Ids|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Ids $ids = null;


    //  [0, 1, 11, 12, 13, 14, 15]
//callback: [ChoiceHelper::class, 'getGlisMlsStatusChoices'], /*'getValidMlsstatusChoices'*/

    /**
     * @var string|null
     */
    # [Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 2,
        maxMessage: 'Provided mlsstatus cannot be longer than {{ limit }} characters.',
    )]
    #[Assert\AtLeastOneOf(
        [
            new Assert\Blank(),
            new Assert\Choice(
                callback: [ChoiceHelper::class, 'getGlisMlsStatusChoices'], /*'getValidMlsstatusChoices'*/
                message: 'Provided mlsstatus value {{ value }} is not valid, please use any of: [{{ choices }}].'
            )
        ]
    )]
    #[Groups(['Default'])]
    private ?string $mlsstatus = null;


    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 1,
        maxMessage: 'Historical cannot be longer than {{ limit }} characters.',
    )]
    #[Assert\AtLeastOneOf(
        [
            new Assert\Blank(),
            new Assert\Choice(
                callback: [ChoiceHelper::class, 'getYesOrNoChoices'], /*'getValidHistoricalChoices',*/
                message: 'Provided historical value {{value}} is not valid, please use any of: [{{ choices }}].'
            )
        ]
    )]
    #[Groups(['Default'])]
    private ?string $historical = null;


    /**
     * @var Acquisition|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Acquisition $acquisition = null;

    /**
     * @var Collection|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Collection $collection = null;

    /**
     * @var Breeding|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Breeding $breeding = null;


    /**
     * @var DateTime|null
     */
    #[Ignore]
    private ?DateTime $lastUpdate = null;

    // Methods

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return $this
     */
    public function setUsername(?string $username): Glis
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return $this
     */
    public function setPassword(?string $password): Glis
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return Actor|null
     */
    public function getLocation(): ?Actor
    {
        return $this->location;
    }

    /**
     * @param Actor|null $location
     * @return $this
     */
    public function setLocation(?Actor $location): Glis
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampledoi(): ?string
    {
        return $this->sampledoi;
    }

    /**
     * @param string|null $sampledoi
     * @return $this
     */
    public function setSampledoi(?string $sampledoi): Glis
    {
        $sampledoi = StringHelper::isEmpty($sampledoi) ? null : $sampledoi;
        $this->sampledoi = $sampledoi;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleid(): ?string
    {
        return $this->sampleid;
    }

    /**
     * @param string|null $sampleid
     * @return $this
     */
    public function setSampleid(?string $sampleid): Glis
    {
        $this->sampleid = $sampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return $this
     */
    public function setDate(?string $date): Glis
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @param string|null $method
     * @return $this
     */
    public function setMethod(?string $method): Glis
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenus(): ?string
    {
        return $this->genus;
    }

    /**
     * @param string|null $genus
     * @return $this
     */
    public function setGenus(?string $genus): Glis
    {
        $this->genus = $genus;
        return $this;
    }

    /**
     * @return Names|null
     */
    public function getCropnames(): ?Names
    {
        return $this->cropnames;
    }

    /**
     * @param Names|null $cropnames
     * @return $this
     */
    public function setCropnames(?Names $cropnames): Glis
    {
        $this->cropnames = $cropnames;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasCropnames(): bool
    {
        return isset($this->cropnames) && $this->cropnames->hasNames();
    }

    /**
     * @return Targets|null
     */
    public function getTargets(): ?Targets
    {
        return $this->targets;
    }

    /**
     * @param Targets|null $targets
     * @return $this
     */
    public function setTargets(?Targets $targets): Glis
    {
        $this->targets = $targets;
        return $this;
    }

    /**
     * @return Dois|null
     */
    public function getProgdois(): ?Dois
    {
        return $this->progdois;
    }

    /**
     * @param Dois|null $progdois
     * @return $this
     */
    public function setProgdois(?Dois $progdois): Glis
    {
        $this->progdois = $progdois;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getBiostatus(): ?string
    {
        return $this->biostatus;
    }

    /**
     * @param string|null $biostatus
     * @return $this
     */
    public function setBiostatus(?string $biostatus): Glis
    {
        $this->biostatus = $biostatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpecies(): ?string
    {
        return $this->species;
    }

    /**
     * @param string|null $species
     * @return $this
     */
    public function setSpecies(?string $species): Glis
    {
        $this->species = $species;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpauth(): ?string
    {
        return $this->spauth;
    }

    /**
     * @param string|null $spauth
     * @return $this
     */
    public function setSpauth(?string $spauth): Glis
    {
        $this->spauth = $spauth;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubtaxa(): ?string
    {
        return $this->subtaxa;
    }

    /**
     * @param string|null $subtaxa
     * @return $this
     */
    public function setSubtaxa(?string $subtaxa): Glis
    {
        $this->subtaxa = $subtaxa;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStauth(): ?string
    {
        return $this->stauth;
    }

    /**
     * @param string|null $stauth
     * @return $this
     */
    public function setStauth(?string $stauth): Glis
    {
        $this->stauth = $stauth;
        return $this;
    }

    /**
     * @return Names|null
     */
    public function getNames(): ?Names
    {
        return $this->names;
    }

    /**
     * @param Names|null $names
     * @return $this
     */
    public function setNames(?Names $names): Glis
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return Ids|null
     */
    public function getIds(): ?Ids
    {
        return $this->ids;
    }

    /**
     * @param Ids|null $ids
     * @return $this
     */
    public function setIds(?Ids $ids): Glis
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMlsstatus(): ?string
    {
        return $this->mlsstatus;
    }

    /**
     * @param string|null $mlsstatus
     * @return $this
     */
    public function setMlsstatus(?string $mlsstatus): Glis
    {
        $this->mlsstatus = $mlsstatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHistorical(): ?string
    {
        return $this->historical;
    }

    /**
     * @param string|null $historical
     * @return $this
     */
    public function setHistorical(?string $historical): Glis
    {
        $this->historical = $historical;
        return $this;
    }

    /**
     * @return Acquisition|null
     */
    public function getAcquisition(): ?Acquisition
    {
        return $this->acquisition;
    }

    /**
     * @param Acquisition|null $acquisition
     * @return $this
     */
    public function setAcquisition(?Acquisition $acquisition): Glis
    {
        $this->acquisition = $acquisition;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getCollection(): ?Collection
    {
        return $this->collection;
    }

    /**
     * @param Collection|null $collection
     * @return $this
     */
    public function setCollection(?Collection $collection): Glis
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * @return Breeding|null
     */
    public function getBreeding(): ?Breeding
    {
        return $this->breeding;
    }

    /**
     * @param Breeding|null $breeding
     * @return $this
     */
    public function setBreeding(?Breeding $breeding): Glis
    {
        $this->breeding = $breeding;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLastUpdate(): ?DateTime
    {
        return $this->lastUpdate;
    }

    /**
     * @param DateTime|null $lastUpdate
     * @return $this
     */
    public function setLastUpdate(?DateTime $lastUpdate): Glis
    {
        $this->lastUpdate = $lastUpdate;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRowNumber(): ?int
    {
        return $this->rowNumber;
    }

    /**
     * @param int|null $rowNumber
     * @return $this
     */
    public function setRowNumber(?int $rowNumber): Glis
    {
        $this->rowNumber = $rowNumber;
        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed $payload
     * @return void
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, mixed $payload): void
    {
        // Check Mandatory locaton
        if ($this->location?->isAllEmpty() ?? true) {
            $context->buildViolation('Property location is mantatory.')
                ->atPath('location.Actor')
                ->addViolation();
        }

        // TODO: Check if At least one of !empty($genus) or callback hasCropname() is true...
        $cnt = 0;
        if (
            !StringHelper::isEmpty($this->genus)
        ) {
            $cnt++;
        }



        /*
        * FIX: $name has a double nature Either string or object.
        * TODO: Force it to be only one nature in the Denormalizer.
        **/
        //fn(Name|string $value) => !StringHelper::isEmpty(is_string($value) ? $value : $value->getValue())
        /** @var Name|string $value */

        if (
            $this->hasCropnames()
            && ($names = $this->cropnames?->getNames()) !== null
            && (
                $names = array_filter($names, fn(mixed $value): bool
                    => get_class($value) === Name::class && StringHelper::isEmpty($value->getValue()) === false)
            )
            && (count($names) > 0)
        ) {
            $cnt++;
        }

        if ($cnt === 0) {
            $context->buildViolation('Either Genus or at least one cropname is valid.')
            ->atPath('genus-cropname')
            ->addViolation();
        }

        $ProgdoisCnt = $this->getProgdois()?->count();
        switch ($this->getMethod()) {
            case 'acqu':
            case 'ihcp':
            case 'ihva':
                if (isset($ProgdoisCnt) && $ProgdoisCnt > 1) {
                    $context->buildViolation(
                        'Only one progdoi is allowed for method = "' . ( (string)$this->getMethod() ) . '".'
                    )   ->atPath('progdois')
                        ->addViolation();
                }
                break;
            case 'obna':
            case 'obin':
                if (isset($ProgdoisCnt) && $ProgdoisCnt > 0) {
                    $context->buildViolation(
                        'No progdoi is allowed for method = "' . ( (string)$this->getMethod() ) . '".'
                    )   ->atPath('progdois')
                        ->addViolation();
                }
                break;
            case 'nodi':
                break;
            default:
        }

        // TODO: Check if At least one of $acquisition, $collection or $breeding exists...
    }
}
