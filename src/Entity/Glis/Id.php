<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 *
 * <id type="[itype]">[ivalue]</id>
 */
class Id
{
    /**
     * @var string|null
     */
    # [Assert\NotBlank]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Value cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    #[SerializedName('#')]
    private ?string $value = null;

    /**
     * @var string|null
     */
    # [Assert\NotBlank]
    #[Assert\Length(
        max: 16,
        maxMessage: 'Type cannot be longer than {{ limit }} characters',
    )]
    #[Assert\Choice(
        callback: [ChoiceHelper::class, 'getGlisITypeOptionalChoices'],
        message: 'Provided @type value {{ value }} is not valid, please use any of: [{{ choices }}].'
    )]
    #[Groups(['Default'])]
    #[SerializedName('@type')]
    private ?string $type = null;

    // ChoiceHelper::getGlisITypeOptions()

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return $this
     */
    public function setValue(?string $value): Id
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Id
     */
    public function setType(?string $type): Id
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed $payload
     * @return void
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, mixed $payload): void
    {
        if (!StringHelper::isEmpty($this->value) && StringHelper::isEmpty($this->type)) {
            $choices = implode(
                ', ',
                array_map(fn(mixed $item): string => '"' . $item . '"', ChoiceHelper::getGlisITypeChoices())
            );
            $context->buildViolation(
                'Provided @type value "" is not valid, please use any of: [' . $choices . '].'
            )
                ->atPath('type')
                ->addViolation();
        }
    }
}
