<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <names>
 *   <name>[nvalue]</name>*
 * </names>
 */
class Names
{
    /**
     * @var Name[]
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('name')]
    private array $names = [];

    /**
     * @return Name[]
     */
    public function getNames(): array
    {
        return $this->names;
    }

    /**
     * @param Name[]|string[] $names
     * @return Names
     */
    public function setNames(array $names): Names
    {
        $this->names = [];
        /**
         * @var int|string $index
         * @var Name|string $name */
        foreach ($names as $index => $name) {
            if (is_string($name)) {
                $this->addName($name);
            } else {
                $this->names[] = $name;
            }
        }

        return $this;
    }

    /**
     * @param String $name
     * @return Names
     */
    public function addName(string $name): Names
    {
        $this->names[] = (new Name())->setValue($name);
        return $this;
    }

    /**
     * @return bool
     */
    public function hasNames(): bool
    {
        return count($this->names) > 0;
    }

    /**
     * @param String $name
     * @return int|false
     */
    public function findName(string $name): int|false
    {
        /**
         * @var int $index
         * @var Name $existedName
         */
        foreach ($this->names as $index => $existedName) {
            if ($existedName->getValue() === $name) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param String $name
     * @return void
     */
    public function removeName(string $name): void
    {
        if ((($index = $this->findName($name)) !== false ) && array_key_exists($index, $this->names)) {
            unset($this->names[$index]);
        }
    }
}
