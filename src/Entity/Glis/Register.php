<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use EndlessDreams\FaoToolkit\Entity\Base\Actor;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 *
 */
class Register
{
    /**
     * @var string|null
     */
    #[SerializedName('@username')]
    private ?string $username = null;

    /**
     * @var string|null
     */
    #[SerializedName('@password')]
    private ?string $password = null;

    #[Ignore]
    private ?int $rowNumber = null;

    /**
     * @var Actor|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    private ?Actor $location = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true, groups: ['Register'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Sampledoi cannot be longer than {{ limit }} characters',
    )]
    private ?string $sampledoi = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Sampleid cannot be longer than {{ limit }} characters',
    )]
    private ?string $sampleid = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 10,
        maxMessage: 'Date cannot be longer than {{ limit }} characters',
    )]
    #[Assert\AtLeastOneOf([
        new Assert\Regex('/^\d{4}[-]([0][1-9]|[1][0-2])[-]([0][1-9]|[12]\d|[3][01])$/'),
        new Assert\Regex('/^\d{4}[-]([0][1-9]|[1][0-2])$/'),
        new Assert\Regex('/^\d{4}$/'),
    ])]
    private ?string $date = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 4,
        maxMessage: 'Method cannot be longer than {{ limit }} characters',
    )]
    #[Assert\Choice(callback: 'getValidMethodChoices')]
    private ?string $method = null;

    /**
     * @var string|null
     * Need to add a common validation check for the whole Register class
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 64,
        maxMessage: 'Genus cannot be longer than {{ limit }} characters',
    )]
    private ?string $genus = null;

    /**
     * @var Names|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[SerializedName('cropnames')]
    private ?Names $cropnames = null;

    /**
     * @var Targets|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    private ?Targets $targets = null;

    /**
     * @var Dois|null
     */
    #[Assert\AtLeastOneOf([
        new Assert\Blank(),
        new Assert\When(
            expression: 'this.getMethod() == "acqu"',
            constraints: [new Assert\Count(min: 1, max: 1)],
        ),
        new Assert\When(
            expression: 'this.getMethod() == "ihcp"',
            constraints: [new Assert\Count(min: 1, max: 1)],
        ),
        new Assert\When(
            expression: 'this.getMethod() == "ihva"',
            constraints: [new Assert\Count(min: 1, max: 1)],
        ),
        new Assert\When(
            expression: 'this.getMethod() == "nodi"',
            constraints: [new Assert\Count(min: 1)],
        ),
        new Assert\When(
            expression: 'this.getMethod() == "obna"',
            constraints: [new Assert\Count(min: 0, max: 0)],
        ),
        new Assert\When(
            expression: 'this.getMethod() == "obin"',
            constraints: [new Assert\Count(min: 0, max: 0)],
        ),
        new Assert\Count(min: 0, max: 0),
    ])]
    private ?Dois $progdois = null;

    /**
     * @var string|null
     */
    private ?string $biostatus = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Species cannot be longer than {{ limit }} characters',
    )]
    private ?string $species = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 64,
        maxMessage: 'Spauth cannot be longer than {{ limit }} characters',
    )]
    private ?string $spauth = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Subtaxa cannot be longer than {{ limit }} characters',
    )]
    private ?string $subtaxa = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Length(
        max: 64,
        maxMessage: 'Stauth cannot be longer than {{ limit }} characters',
    )]
    private ?string $stauth = null;

    /**
     * @var Names|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    private ?Names $names = null;

    /**
     * @var Ids|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    private ?Ids $ids = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Choice(
        callback: [ChoiceHelper::class, 'getGlisMlsStatusChoices'], /*'getValidMlsstatusChoices'*/
        message: 'Provided {{label}} value {{value}} is not valid, please use any of: [{{ choices }}].'
    )]
    private ?string $mlsstatus = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Choice(
        callback: [ChoiceHelper::class, 'getYesOrNoChoices'], /*'getValidHistoricalChoices',*/
        message: 'Provided {{label}} value {{value}} is not valid, please use any of: [{{ choices }}].'
    )]
    private ?string $historical = null;

    /**
     * @var Acquisition|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    private ?Acquisition $acquisition = null;

    /**
     * @var Collection|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    private ?Collection $collection = null;

    /**
     * @var Breeding|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    private ?Breeding $breeding = null;

    // Methods

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return Register
     */
    public function setUsername(?string $username): Register
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return Register
     */
    public function setPassword(?string $password): Register
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return Actor|null
     */
    public function getLocation(): ?Actor
    {
        return $this->location;
    }

    /**
     * @param Actor|null $location
     * @return Register
     */
    public function setLocation(?Actor $location): Register
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampledoi(): ?string
    {
        return $this->sampledoi;
    }

    /**
     * @param string|null $sampledoi
     * @return Register
     */
    public function setSampledoi(?string $sampledoi): Register
    {
        $sampledoi = StringHelper::isEmpty($sampledoi) ? null : $sampledoi;
        $this->sampledoi = $sampledoi;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleid(): ?string
    {
        return $this->sampleid;
    }

    /**
     * @param string|null $sampleid
     * @return Register
     */
    public function setSampleid(?string $sampleid): Register
    {
        $this->sampleid = $sampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return Register
     */
    public function setDate(?string $date): Register
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @param string|null $method
     * @return Register
     */
    public function setMethod(?string $method): Register
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenus(): ?string
    {
        return $this->genus;
    }

    /**
     * @param string|null $genus
     * @return Register
     */
    public function setGenus(?string $genus): Register
    {
        $this->genus = $genus;
        return $this;
    }

    /**
     * @return Names|null
     */
    public function getCropnames(): ?Names
    {
        return $this->cropnames;
    }

    /**
     * @param Names|null $cropnames
     * @return Register
     */
    public function setCropnames(?Names $cropnames): Register
    {
        $this->cropnames = $cropnames;
        return $this;
    }

    public function hasCropnames(): bool
    {
        return isset($this->cropnames) && $this->cropnames->hasNames();
    }

    /**
     * @return Targets|null
     */
    public function getTargets(): ?Targets
    {
        return $this->targets;
    }

    /**
     * @param Targets|null $targets
     * @return Register
     */
    public function setTargets(?Targets $targets): Register
    {
        $this->targets = $targets;
        return $this;
    }

    /**
     * @return Dois|null
     */
    public function getProgdois(): ?Dois
    {
        return $this->progdois;
    }

    /**
     * @param Dois|null $progdois
     * @return Register
     */
    public function setProgdois(?Dois $progdois): Register
    {
        $this->progdois = $progdois;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBiostatus(): ?string
    {
        return $this->biostatus;
    }

    /**
     * @param string|null $biostatus
     * @return Register
     */
    public function setBiostatus(?string $biostatus): Register
    {
        $this->biostatus = $biostatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpecies(): ?string
    {
        return $this->species;
    }

    /**
     * @param string|null $species
     * @return Register
     */
    public function setSpecies(?string $species): Register
    {
        $this->species = $species;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpauth(): ?string
    {
        return $this->spauth;
    }

    /**
     * @param string|null $spauth
     * @return Register
     */
    public function setSpauth(?string $spauth): Register
    {
        $this->spauth = $spauth;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubtaxa(): ?string
    {
        return $this->subtaxa;
    }

    /**
     * @param string|null $subtaxa
     * @return Register
     */
    public function setSubtaxa(?string $subtaxa): Register
    {
        $this->subtaxa = $subtaxa;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStauth(): ?string
    {
        return $this->stauth;
    }

    /**
     * @param string|null $stauth
     * @return Register
     */
    public function setStauth(?string $stauth): Register
    {
        $this->stauth = $stauth;
        return $this;
    }

    /**
     * @return Names|null
     */
    public function getNames(): ?Names
    {
        return $this->names;
    }

    /**
     * @param Names|null $names
     * @return Register
     */
    public function setNames(?Names $names): Register
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return Ids|null
     */
    public function getIds(): ?Ids
    {
        return $this->ids;
    }

    /**
     * @param Ids|null $ids
     * @return Register
     */
    public function setIds(?Ids $ids): Register
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMlsstatus(): ?string
    {
        return $this->mlsstatus;
    }

    /**
     * @param string|null $mlsstatus
     * @return Register
     */
    public function setMlsstatus(?string $mlsstatus): Register
    {
        $this->mlsstatus = $mlsstatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHistorical(): ?string
    {
        return $this->historical;
    }

    /**
     * @param string|null $historical
     * @return Register
     */
    public function setHistorical(?string $historical): Register
    {
        $this->historical = $historical;
        return $this;
    }

    /**
     * @return Acquisition|null
     */
    public function getAcquisition(): ?Acquisition
    {
        return $this->acquisition;
    }

    /**
     * @param Acquisition|null $acquisition
     * @return Register
     */
    public function setAcquisition(?Acquisition $acquisition): Register
    {
        $this->acquisition = $acquisition;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getCollection(): ?Collection
    {
        return $this->collection;
    }

    /**
     * @param Collection|null $collection
     * @return Register
     */
    public function setCollection(?Collection $collection): Register
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * @return Breeding|null
     */
    public function getBreeding(): ?Breeding
    {
        return $this->breeding;
    }

    /**
     * @param Breeding|null $breeding
     * @return Register
     */
    public function setBreeding(?Breeding $breeding): Register
    {
        $this->breeding = $breeding;
        return $this;
    }

    public function getRowNumber(): ?int
    {
        return $this->rowNumber;
    }

    public function setRowNumber(?int $rowNumber): Register
    {
        $this->rowNumber = $rowNumber;
        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed $payload
     * @return void
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, mixed $payload): void
    {
        // TODO: Check if At least one of !empty($genus) or callback hasCropname() is true...
        if (
            !(
                !StringHelper::isEmpty($this->genus)
                && count($context->getValidator()->validateProperty($this, 'genus')) === 0
                ||
                $this->hasCropnames()
                && count($context->getValidator()->validateProperty($this, 'cropnames')) === 0
            )
        ) {
            $context->buildViolation(
                'Either Genus or at least one cropname is valid'
            )
            ->atPath('Genus-Cropname')
            ->addViolation();
        }

        // TODO: Check if At least one of $acquisition, $collection or $breeding exists...
    }
}
