<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <kws>
 *   <kw>[tkw]</kw>*
 * </kws>
 */
class Kws
{
    /**
     * @var Kw[]|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('kw')]
    private ?array $kws = [];

    /**
     * @return Kw[]|null
     */
    public function getKws(): ?array
    {
        return $this->kws;
    }

    /**
     * @param Kw[]|null $kws
     * @return Kws|null
     */
    public function setKws(?array $kws): ?Kws
    {
        $this->kws = $kws;
        return $this;
    }

    /**
     * @param String $kw
     * @return Kws
     */
    public function addKw(string $kw): Kws
    {
        $this->kws[] = (new Kw())->setValue($kw);
        return $this;
    }

    /**
     * @return bool
     */
    public function hasKws(): bool
    {
        return count($this->kws ?? []) > 0;
    }

    /**
     * @param String $kw
     * @return int|false
     */
    public function findKw(string $kw): int|false
    {
        /**
         * @var int $index
         * @var Kw $existedKw
         */
        foreach ($this->kws ?? [] as $index => $existedKw) {
            if ($existedKw->getValue() === $kw) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param String $kw
     * @return void
     */
    public function removeKw(string $kw): void
    {
        if (
            isset($this->kws)
            && (($index = $this->findKw($kw)) !== false)
            && array_key_exists($index, $this->kws)
        ) {
            unset($this->kws[$index]);
        }
    }
}
