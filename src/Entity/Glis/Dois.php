<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use ArrayAccess;
use Countable;
use IteratorAggregate;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Ignore;
use Symfony\Component\Validator\Constraints as Assert;
use Yiisoft\Arrays\ArrayAccessTrait;

/**
 * <dois>
 *   <doi>[progdoi]</doi>*
 * </dois>
 *
 * @template-implements ArrayAccess<int, string>
 * @template-implements IteratorAggregate<int, string>
 */
class Dois implements IteratorAggregate, ArrayAccess, Countable
{
    use ArrayAccessTrait;

    /**
     * @var int
     */
    #[Ignore]
    private int $position = 0;

    /**
     * @var String[]
     */
    #[Assert\NotBlank]
    # [Assert\Regex(pattern:'/^10.\d{4,9}/[-._;()/:A-Z0-9]+$/i', message: '{{ value }} is not a valid DOI')]
    #[Groups(['Default'])]
    #[SerializedName('doi')]
    private array $data = [];

    /**
     * @return String[]
     *
     * @psalm-suppress MixedReturnTypeCoercion
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param String[] $data
     * @return void
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @param String $doi
     * @return void
     */
    public function addData(string $doi): void
    {
        $this->data[] = $doi;
    }

    /**
     * @return bool
     */
    public function hasDois(): bool
    {
        return count($this->data) > 0;
    }

    /**
     * @param String $doi
     * @return int|false
     */
    public function findDoi(string $doi): int|false
    {
        /**
         * @var int $index
         * @var string $existedDoi
         */
        foreach ($this->data as $index => $existedDoi) {
            if ($existedDoi === $doi) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param String $doi
     * @return void
     */
    public function removeDoi(string $doi): void
    {
        if (
            (($index = $this->findDoi($doi)) !== false)
            &&
            array_key_exists($index, $this->data)
        ) {
            unset($this->data[$index]);
        }
    }
}
