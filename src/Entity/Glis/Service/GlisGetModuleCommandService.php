<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis\Service;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\Glis\Get;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use Yiisoft\Db\Command\CommandInterface;

/**
 *
 */
final class GlisGetModuleCommandService extends GlisModuleCommandService implements ModuleCommandServiceInterface
{
    /**
     * @var CommandInterface|null
     */
    private ?CommandInterface $updateDoiCommand = null;
    /**
     * @var CommandInterface|null
     */
    private ?CommandInterface $updateCachedDoiCommand = null;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->entityClass = Get::class;
        $this->entityXmlRootName = 'get';

        $this->parserGroups = ['Default','Get'];
        $this->validatorGroups = ['Default','Get'];

        $this->entityResponseClass = Glis::class;
        $this->responseXmlRootName = 'glis';
    }

    /**
     * @param DbMapService $dbMapService
     * @return void
     */
    public function init(DbMapService $dbMapService): void
    {
        parent::init($dbMapService);
        $this->apiProviderCredential = $dbMapService
            ->faoConfig->getSelectedProvider()?->getCredentials()->getSelected();
    }
}
