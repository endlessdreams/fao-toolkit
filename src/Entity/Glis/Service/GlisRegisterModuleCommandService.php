<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis\Service;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\ColumnsAccession;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\ColumnsCachedglis;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\WhereAccession;
use EndlessDreams\FaoToolkit\Entity\Glis\Response;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use Throwable;
use Yiisoft\Db\Command\CommandInterface;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidArgumentException;
use Yiisoft\Db\Exception\InvalidConfigException;

/**
 *
 */
final class GlisRegisterModuleCommandService extends GlisModuleCommandService implements ModuleCommandServiceInterface
{
    /**
     * @var CommandInterface|null
     */
    private ?CommandInterface $updateDoiCommand = null;
    /**
     * @var CommandInterface|null
     */
    private ?CommandInterface $updateCachedDoiCommand = null;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->entityXmlRootName = 'register';
    }

    /**
     * @param DbMapService $dbMapService
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function init(DbMapService $dbMapService): void
    {
        parent::init($dbMapService);
        $this->apiProviderCredential = $dbMapService
            ->faoConfig->getSelectedProvider()?->getCredentials()->getSelected();

        $this->initUpdateAccessionWithDoi($dbMapService);
    }

    /**
     * @param DbMapService $dbMapService
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws \Exception
     */
    private function initUpdateAccessionWithDoi(DbMapService $dbMapService): void
    {
        $this->db = $dbMapService->faoConfig->getDbConnection();

        $tableAccession = $dbMapService->getTableName('table_accession');
        $tableAccessionColumns = $dbMapService->getColumnsEntityByTable('table_accession');
        $whereAccession = $dbMapService->faoConfig->getMap()?->getWhereAccession();
        $columnGlisId = $whereAccession?->getGlisId();
        $tableCachedGlis = $dbMapService->getTableName('table_cachedglis');
        $tableCachedGlisColumns = $dbMapService->getColumnsEntityByTable('table_cachedglis');

        if (
            $tableAccessionColumns instanceof ColumnsAccession
            && $whereAccession instanceof WhereAccession
            && isset($tableAccession)
            && isset($columnGlisId)
        ) {
            $columnDoi = $tableAccessionColumns->getSampledoi();
            $sql = "UPDATE {{%$tableAccession}} SET [[$columnDoi]]=:doi WHERE [[$columnGlisId]]=:glis";
            $this->updateDoiCommand = $this->db?->createCommand($sql);

            if (
                !StringHelper::isEmpty($tableCachedGlis)
                && $tableCachedGlisColumns instanceof ColumnsCachedglis
                && !StringHelper::isEmpty($tableCachedGlisDoiColumn = (string)$tableCachedGlisColumns->getDoi())
                && !StringHelper::isEmpty($tableCachedGlisGlisIdColumn = (string)$tableCachedGlisColumns->getGlisId())
            ) {
                $sql = "UPDATE {{%$tableCachedGlis}} SET [[$tableCachedGlisDoiColumn]]=:doi "
                    . "WHERE [[$tableCachedGlisGlisIdColumn]]=:glis";
                $this->updateCachedDoiCommand = $this->db?->createCommand($sql);
            }
        }
    }

    /**
     * @param Response $responseBody
     * @param int $pkValue
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function updateAccessionWithDoi(
        \EndlessDreams\FaoToolkit\Entity\Glis\Response $responseBody,
        int $pkValue
    ): void {
        if (!($this->db instanceof ConnectionInterface)) {
            throw new \Exception('Unable to get database connection.');
        }
        $doiValue = (string)$responseBody->getDoi();
        if (empty($doiValue)) {
            throw new \Exception('Update method expected doi value.');
        }
        $transaction = $this->db->beginTransaction();

        try {
            $params = [':doi' => $doiValue, ':glis' => $pkValue];
            $this->updateDoiCommand?->bindValues($params)->execute();
            $this->updateCachedDoiCommand?->bindValues($params)->execute();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
