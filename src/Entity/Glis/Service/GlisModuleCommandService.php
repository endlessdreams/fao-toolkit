<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis\Service;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credential;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Entity\Glis\Response;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandService;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use Throwable;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;

/**
 *
 */
class GlisModuleCommandService extends ModuleCommandService implements ModuleCommandServiceInterface
{
    use GlisModuleCommandServiceTrait;

    /**
     * @var Credential|null
     */
    protected ?Credential $apiProviderCredential = null;

    /**
     * @var ConnectionInterface|null
     */
    protected ?ConnectionInterface $db = null;

    /**
     *
     */
    public function __construct()
    {
        $this->entityClass = Glis::class;
        $this->entityXmlRootName = 'register';

        $this->parserGroups = ['Default','Glis'];
        $this->validatorGroups = ['Default','Glis'];

        $this->entityResponseClass = Response::class;
        $this->responseXmlRootName = 'response';

        $this->faoConfigInternalTableName = 'table_glis';
        $this->uniqueKeyInInternalTable = 'sampleid';

        $this->logMsgBeginning = 'GLIS sampleID';

        $this->faoInstituteCodeQueryColumn = 'lwiews';

        $this->rowPk = 'glisId';

        $this->dateRangeColumn = 'lastUpdate';
    }


    /**
     * @param DbMapService $dbMapService
     * @return void
     */
    public function init(DbMapService $dbMapService): void
    {
        parent::init($dbMapService);
        $this->apiProviderCredential = $dbMapService
            ->faoConfig->getSelectedProvider()?->getCredentials()->getSelected();
    }

    /**
     * @param array $row
     * @param int|null $index
     * @param array $context
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function parseRow(array &$row, int|null $index, array &$context): void
    {
        codecept_debug('Parse Row - Begin');
        $provider = $this->getProvider($row);
        $this->populateLocation($row, $provider);
        $this->populateAcquisitionWithoutProviders($row);
        $this->populateCollectionWithoutCollectors($row);
        $this->populateBreedingWithoutBreeders($row);
        codecept_debug('Parse Row - Start populate parse Json');
        $this->populateRowWithSubTableOrParsedJson($row);
        codecept_debug('Parse Row - Start populate single subrow descriptors');
        $this->populateWithSingleSubRowDescriptors($row);
        codecept_debug('Parse Row - End');
    }

    /**
     * @param string $instituteCodeColumn
     * @return void
     */
    public function setFaoInstituteCodeQueryColumn(string $instituteCodeColumn): void
    {
        $this->faoInstituteCodeQueryColumn = $instituteCodeColumn;
    }
}
