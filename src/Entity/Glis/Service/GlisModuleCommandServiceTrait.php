<?php

namespace EndlessDreams\FaoToolkit\Entity\Glis\Service;

use EndlessDreams\FaoToolkit\Entity\Base\HasGlisIdInterface;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\Glis\Get;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Service\Helper\ArrayJsonHelper;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper as EDStringHelper;
use Throwable;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Strings\StringHelper;

/**
 *
 */
trait GlisModuleCommandServiceTrait
{
    /**
     * @param array $row
     * @return Provider
     * @throws \Exception
     */
    public function getProvider(array $row): Provider
    {
        $provider = $this->dbMapService?->faoConfig?->getProviders()?->findProvider((string)$row[$this->faoInstituteCodeQueryColumn]);
        if (!isset($provider)) {
            throw new \Exception('No provider is available');
        }
        return $provider;
    }

    /**
     * @param array $row
     * @param Provider $provider
     * @return void
     */
    protected function populateLocation(array &$row, Provider $provider): void
    {
        $row['location'] = [
            'pid' => $provider->getPid($provider->getSelectedFaoEnvironment()),
            'wiews' => $provider->getInstituteCode(),
            'name' => $provider->getName(),
            'address' => $provider->getAddress(),
            'country' => $provider->getCountry(),
        ];
        $this->batchUnset(
            $row,
            [
                'lwiews','lpid','lname','laddress','lcountry'
            ]
        );
    }

    /**
     * @param array $row
     * @return void
     */
    protected function populateAcquisitionWithoutProviders(array &$row): void
    {
        $row['acquisition'] = [
            'provider' => [
                'wiews' => $row['pwiews'],
                'pid' => $row['ppid'],
                'name' => $row['pname'],
                'address' => $row['paddress'],
                'country' => $row['pcountry'],
            ],
            'sampleid' => $row['psampleid'],
            'provenance' => $row['provenance']
        ];
        $this->batchUnset(
            $row,
            [
                'pwiews', 'ppid', 'pname', 'paddress', 'pcountry', 'psampleid', 'provenance'
            ]
        );
    }

    /**
     * @param array $row
     * @return void
     */
    protected function populateCollectionWithoutCollectors(array &$row): void
    {
        $row['collection'] = [
            'collectors' => null,
            'sampleid' => $row['csampleid'],
            'missid' => $row['missid'],
            'site' => $row['site'],
            'lat' => $row['clat'],
            'lon' => $row['clon'],
            'uncert' => $row['uncert'],
            'datum' => $row['datum'],
            'georef' => $row['georef'],
            'elevation' => $row['elevation'],
            'date' => $row['cdate'],
            'source' => $row['source'],
        ];
        $this->batchUnset(
            $row,
            [
                'csampleid', 'missid', 'site', 'clat', 'clon', 'uncert', 'datum', 'georef', 'elevation', 'cdate', 'source'
            ]
        );
    }

    /**
     * @param array $row
     * @return void
     */
    protected function populateBreedingWithoutBreeders(array &$row): void
    {
        $row['breeding'] = [
            'breeders' => null,
            'ancestry' => $row['ancestry'],
        ];
        unset($row['ancestry']);
    }

    /**
     * @param array $row
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    protected function populateRowWithSubTables(array &$row): void
    {
        foreach (['collector','breeder','cropname','name','id','target'] as $subTableType) {
            $subTableTypeResult = $this->parseSubRows('table_glis_' . $subTableType, $row['glisId']);
            if (!isset($subTableTypeResult) || count($subTableTypeResult)===0) {
                continue;
            }

            $row[$subTableType.'s'] = [
                ($subTableType === 'cropname') ? 'name' : $subTableType
                =>
                    match ($subTableType) {
                        'name','cropname' =>
                        $this->buildHashKeyList($subTableTypeResult),
                        'id' =>
                        array_map(
                            fn(array $el) => ['@type' => $el['type'], '#' => $el['value']],
                            $subTableTypeResult
                        ),
                        'target' =>
                        $this->buildTargetList($subTableTypeResult),
                        default => $subTableTypeResult,
                    }
            ];

        }
    }

    /**
     * @param array $row
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    protected function populateRowWithSubTableOrParsedJson(array &$row): void
    {
        $this->populateRowWithSubTables($row);

        ArrayJsonHelper::parseJsonKeyValues($row, ['collectors','providers','breeders','cropnames','names','ids','targets','progdois']);
        ArrayJsonHelper::createSubArrayIfNeeded($row);
        foreach (['collection' => 'collectors','breeding' => 'breeders'] as $source => $actors) {
            if (array_key_exists($source, $row)
                && is_array($row[$source])
                && array_key_exists($actors, $row[$source])
            ) {
                if (array_key_exists($actors, $row) && is_array($row[$actors])) {
                    $row[$source][$actors] = $row[$actors];
                    unset($row[$actors]);
                } elseif (ArrayJsonHelper::isJson($row[$source][$actors])) {
                    ArrayJsonHelper::parseJsonKeyValues($row[$source], [$actors]);
                } else {
                    /** @var string $actor */
                    $actor = StringHelper::rtrim($actors,'s');
                    $prefix = StringHelper::substring($source,0,1);

                    $actorRow = [
                        'wiews' => $row[$prefix.'wiews'],
                        'pid' => $row[$prefix.'pid'],
                        'name' => $row[$prefix.'name'],
                        'address' => $row[$prefix.'address'],
                        'country' => $row[$prefix.'country'],
                    ];
                    if (!$this->isAllArrayElementsNull($actorRow)) {
                        $row[$source][$actors] = [$actor => []];
                        $row[$source][$actors][$actor][] = $actorRow;
                    } else {
                        unset($row[$source][$actors]);
                    }
                }
            }
        }
    }

    /**
     * @param array $row
     * @return void
     */
    protected function populateWithSingleSubRowDescriptors(array &$row): void
    {
        $row['historical'] = (string)$row['hist'];

        if (!isset($row['cropnames']) && isset($row['cropname'])) {
            $row['cropnames'] = ['name' => [$row['cropname']]];
        }

        if (!isset($row['progdois']) && isset($row['progdoi'])) {
            $row['progdois'] = ['dois' => [$row['progdoi']]];
        }

        if (!isset($row['targets']) && isset($row['tvalue']) && isset($row['tkw'])) {
            $row['targets'] = ['target' => ['value' => $row['tvalue'], 'kws' => ['kw' => [$row['tkw']]]]];
        }

        if (!isset($row['names']) && isset($row['nvalue'])) {
            $row['names'] = ['name' => [$row['nvalue']]];
        }

        if (!isset($row['ids']) && isset($row['itype']) && isset($row['ivalue'])) {
            $row['ids'] = ['id' => ['@type' => $row['itype'], '#' => $row['ivalue']]];
        }

        $this->batchUnset(
            $row,
            [
                'hist', 'cropname', 'tvalue', 'tkw', 'nvalue', 'itype', 'ivalue'
            ]
        );
        $row;
    }

    /**
     * @param array $array
     * @return bool
     */
    protected function isAllArrayElementsNull(array $array): bool
    {
        foreach ($array as $item) {
            if (isset($item)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $row
     * @param int|null $index
     * @param array $context
     * @return Glis|Get
     */
    public function parseAndDenormalize(array $row, int|null $index, array &$context = []): Glis|Get
    {
        /** @var Glis | Get $object */
        $object = parent::parseAndDenormalize($row,$index,$context);
        if ($this->apiProviderCredential !== null) {
            $object
                ->setUsername($this->apiProviderCredential->getUsername())
                ->setPassword($this->apiProviderCredential->getPassword());
        }
        return $object;
    }

    /**
     * @param string $tableName
     * @param mixed $rowId
     * @return array|null
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    private function parseSubRows(string $tableName, mixed $rowId): ?array
    {
        if (
            EDStringHelper::isEmpty($this->dbMapService?->getTableName($tableName))
            || empty(($glisColumnsEntityTable = $this->dbMapService?->getColumnsEntityByTable($tableName)))
        ) {
            return null;
        }
        if(!($glisColumnsEntityTable instanceof HasGlisIdInterface) || $glisColumnsEntityTable->getGlisId() === null ) {
            return null;
        }
        $query = $this
            ->dbMapService
            ?->getTableQuery($tableName)
            ->select(
                $glisColumnsEntityTable->toSelectArray([],[],true,['glisId'])
            )->where([(string)$glisColumnsEntityTable->getGlisId() => (int)$rowId]);
        return $query->all();
    }

    /**
     * @param array $tableArray
     * @param string $key
     * @return array
     */
    protected function buildHashKeyList(array $tableArray, string $key = 'name'): array
    {
        $hashKeyList = [];
        /** @var array<array-key,mixed> $item */
        foreach ($tableArray as $item) {
            $hashKeyList[] = ['#' => $item[$key]];
        }
        return $hashKeyList;
    }

    /**
     * @param array<array-key,array<array-key,mixed>> $tableArray
     * @return array<int,array<string,mixed|array<string,string>>>
     */
    protected function buildTargetList(array $tableArray): array
    {
        /** @var array<int,array<string,mixed|array<string,string>>> $targetList */
        $targetList = [];
        foreach ($tableArray as $item) {
            /** @var string[] $item */
            if (($index = $this->findElementKeyInElementList('value', $item['value'], $targetList)) === false) {
                $targetList[] = ['value' => $item['value'], 'kws' => ['kw' => []]];
                $index = $this->findElementKeyInElementList('value', $item['value'], $targetList);
            }

            /** @var string[] $item */
            $targetList[(int)$index]['kws']['kw'][] = $item['kw'];
        }
        return $targetList;
    }

    /**
     * @param string $key
     * @param string $value
     * @param array<array-key,array<array-key,mixed>> $list
     * @return int|false
     */
    protected function findElementKeyInElementList(string $key, string $value, array $list): int|false
    {
        foreach ($list as $index => $element) {
            if (array_key_exists($key, $element) && $element[$key] === $value) {
                /** @var int $index */
                return $index;
            }
        }
        return false;
    }

    /**
     * @param array<array-key,mixed> $row
     * @param string[] $keys
     * @return void
     */
    protected function batchUnset(array &$row, array $keys): void
    {
        foreach ($keys as $key) {
            unset($row[$key]);
        }
    }
}
