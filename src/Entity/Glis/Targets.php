<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * <targets>
 *     <target>*
 *         <value>[tvalue]</value>
 *         <kws>
 *             <kw>[tkw]</kw>*
 *         </kws>
 *     </target>
 * </targets>
 */
class Targets
{
    /**
     * @var Target[]|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('target')]
    private ?array $targets = [];

    /**
     * @return Target[]|null
     */
    public function getTargets(): ?array
    {
        return $this->targets;
    }

    /**
     * @param Target[]|null $targets
     * @return Targets|null
     */
    public function setTargets(?array $targets): ?Targets
    {
        $this->targets = $targets;
        return $this;
    }

    /**
     * @param Target $target
     * @return Targets
     */
    public function addTarget(Target $target): Targets
    {
        $this->targets[] = $target;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasTargets(): bool
    {
        return count($this->targets ?? []) > 0;
    }

    /**
     * @param Target $target
     * @return int|false
     */
    public function findTarget(Target $target): int|false
    {
        /**
         * @var int $index
         * @var Target $existedTarget
         */
        foreach ($this->targets ?? [] as $index => $existedTarget) {
            if ($existedTarget === $target) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param Target $target
     * @return void
     */
    public function removeTarget(Target $target): void
    {
        if (is_array($this->targets) && is_int($index = $this->findTarget($target))) {
            unset($this->targets[$index]);
        }
    }
}
