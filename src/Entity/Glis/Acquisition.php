<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Glis
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Glis;

use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use EndlessDreams\FaoToolkit\Entity\Base\Actor;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 *
 * <acquisition>
 *   <provider>
 *     <!-- Actor element -->
 *     <wiews>[pwiews]</wiews>
 *     <pid>[ppid]</pid>
 *     <name>[pname]</name>
 *     <address>[paddress]</address>
 *     <country>[pcountry]</country>
 *   </provider>
 *   <sampleid>[psampleid]</sampleid>
 *   <provenance>[provenance]</provenance>
 * </acquisition>
 */
class Acquisition
{
    /**
     * @var Actor|null
     */
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Actor $provider = null;

    /**
     * @var string|null
     */
    # [Assert\NotBlank(allowNull: true)]
    #[Groups(['Default'])]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Sampleid cannot be longer than {{ limit }} characters',
    )]
    private ?string $sampleid = null;

    /**
     * @var string|null
     */
    #[Assert\AtLeastOneOf([
        new Assert\Country(alpha3: true),
        new Assert\Choice(
            callback: [ChoiceHelper::class, 'getExtraCountryCodeChoices'],
            message: 'This value is not a valid FAO extended country.'
        ),
        new Assert\Blank(),
    ])]
    #[Groups(['Default'])]
    private ?string $provenance = null;

    /**
     * @return Actor|null
     */
    public function getProvider(): ?Actor
    {
        return $this->provider;
    }

    /**
     * @param Actor $provider
     * @return Acquisition
     */
    public function setProvider(Actor $provider): Acquisition
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleid(): ?string
    {
        return $this->sampleid;
    }

    /**
     * @param string|null $sampleid
     * @return Acquisition
     */
    public function setSampleid(?string $sampleid): Acquisition
    {
        $this->sampleid = $sampleid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProvenance(): ?string
    {
        return $this->provenance;
    }

    /**
     * @param string|null $provenance
     * @return Acquisition
     */
    public function setProvenance(?string $provenance): Acquisition
    {
        $this->provenance = $provenance;
        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed $payload
     * @return void
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, mixed $payload): void
    {
//        if (!StringHelper::isEmpty($this->sampleid) && $this->provider?->isAllEmpty()) {
//            $context->buildViolation('Valid acquisition.provider.Actor is required, when acquisition.sampleid is set.')
//                ->atPath('property.Actor')
//                ->addViolation();
//        }
    }
}
