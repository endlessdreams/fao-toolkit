<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Smta
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Smta;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Child and collection entity Materials in the Smta model
 * <materials>
 *   </material/> *
 * <materials>
 *
 */
class Materials
{
    /**
     * @var Material[]
     */
    #[Assert\Valid]
    #[Groups(['Default'])]
    #[SerializedName('material')]
    private array $Materials = [];

    /**
     * @return Material[]
     */
    public function getMaterials(): array
    {
        return $this->Materials;
    }

    /**
     * @param Material[] $Materials
     * @return Materials
     */
    public function setMaterials(array $Materials): Materials
    {
        $this->Materials = $Materials;
        return $this;
    }

    /**
     * @param Material $Material
     * @return Materials
     */
    public function addMaterial(Material $Material): Materials
    {
        $this->Materials[] = $Material;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasMaterials(): bool
    {
        return count($this->Materials) > 0;
    }

    /**
     * @param Material $Material
     * @return int|false
     */
    public function findMaterial(Material $Material): int|false
    {
        foreach ($this->Materials as $index => $existedMaterial) {
            if ($existedMaterial === $Material) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param Material $Material
     * @return void
     */
    public function removeMaterial(Material $Material): void
    {
        if (($index = $this->findMaterial($Material)) !== false) {
            unset($this->Materials[$index]);
        }
    }
}
