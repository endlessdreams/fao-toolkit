<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Smta
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Smta;

use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyErrorInterface;
use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Main response Entity Response of Smta model
 * <smta>
 *   <symbol>[symbol]</symbol>
 *   <providerPID>[provPID]</providerPID>
 *   <result>[result]</result>
 *   <error>
 *     <code>[code]</code>
 *     <msg>[message]</msg>
 *   </error>
 *   ...
 * </smta>
 *
 */
class Response implements ResponseBodyErrorInterface, ResponseBodyInterface
{
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Symbol cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    private ?string $symbol = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    #[SerializedName('providerPID')]
    private ?string $providerPID = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $result = null;

    /**
     * @var Error[]|null
     */
    #[Groups(['Default'])]
    #[SerializedName('error')]
    private ?array $errors = null;

    /**
     * @return string|null
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * @param string|null $symbol
     * @return $this
     */
    public function setSymbol(?string $symbol): Response
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProviderPID(): ?string
    {
        return $this->providerPID;
    }

    /**
     * @param string|null $providerPID
     * @return $this
     */
    public function setProviderPID(?string $providerPID): Response
    {
        $this->providerPID = $providerPID;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResult(): ?string
    {
        return $this->result;
    }

    /**
     * @param string|null $result
     * @return $this
     */
    public function setResult(?string $result): Response
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return Error[]|null
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     * @param Error[]|null $errors
     * @return $this
     */
    public function setErrors(?array $errors): Response
    {
        $this->errors = $errors ?? [];
        return $this;
    }

    /**
     * @param Error $error
     * @return void
     */
    public function addError(Error $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return count($this->errors ?? []) > 0;
    }

    /**
     * @param Error $error
     * @return int|string|false
     */
    public function findError(Error $error): int|string|false
    {
        foreach ($this->errors ?? [] as $index => $existedError) {
            if ($existedError === $error) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param Error $error
     * @return void
     */
    public function removeError(Error $error): void
    {
        $index = $this->findError($error);
        if (
            is_int($index)
            && is_array($this->errors)
            && array_key_exists($index, $this->errors)
        ) {
            unset($this->errors[$index]);
        }
    }

    /**
     * @return string|null
     */
    #[Ignore]
    public function getError(): ?string
    {
        if (isset($this->result) && $this->result == 'KO') {
            /** @var Error $error */
            return implode(
                "\n",
                array_map(
                    fn(Error $error): string
                        => implode(': ' . PHP_EOL, [(string)$error->getCode(),(string)$error->getMsg()]),
                    $this->errors ?? []
                )
            );
        }
        return null;
    }
}
