<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Smta
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Smta;

use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Child entity document in the Smta model
 * <document>
 *   <location>[location]</location>
 *   <retInfo>[retInfo]</retInfo>
 *   <pdf>[pdf]</pdf>*
 * </document>
 *
 */
class Document
{
    /**
     * @var string|null
     */
    #[Assert\Choice(
        callback: [ChoiceHelper::class ,'getSmtaLocationChoices'],
        message: 'Provided {{ label }} value {{ value }} is not valid, please use any of: [{{ choices }}].'
    )]
    #[Groups(['Default'])]
    private ?string $location = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'RetInfo cannot be longer than {{ limit }} characters',
    )]
    #[Assert\When(
        expression: 'this.getLocation() != "s"',
        constraints: [
            new Assert\NotBlank()
        ],
    )]
    #[Groups(['Default'])]
    #[SerializedName('retInfo')]
    private ?string $retInfo = null;

    /**
     * Optional base64 encoded Pdf file
     *
     * @var string|null
     */
    #[Assert\Regex(
        pattern: /*'/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{4}|[A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{2}={2})$/m',*/
        '/^((?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{4}|[A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{2}={2})\s?)+$/m',
        message: 'Pdf have to be base64 encoded.'
    )]
    #[Assert\NotBlank(allowNull: true)]
    #[Groups(['register'])]
    private ?string $pdf = null;

    /**
     * Optional attribute referring to Pdf url
     *
     * @var string|null
     */
    #[Assert\Url(
        protocols: ['http', 'https', 'file'],
        groups: ['url']
    )]
    #[Assert\NotBlank(allowNull: true, groups: ['url'])]
    #[Groups(['url'])]
    #[SerializedName('@pdfUrl')]
    private ?string $pdfUrl = null;

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     * @return $this
     */
    public function setLocation(?string $location): Document
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRetInfo(): ?string
    {
        return $this->retInfo;
    }

    /**
     * @param string|null $retInfo
     * @return $this
     */
    public function setRetInfo(?string $retInfo): Document
    {
        $this->retInfo = $retInfo;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdf(): ?string
    {
        return $this->pdf;
    }

    /**
     * @param string|null $pdf
     * @return $this
     */

    public function setPdf(?string $pdf): Document
    {
        $this->pdf = $pdf;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdfUrl(): ?string
    {
        return $this->pdfUrl;
    }

    /**
     * @param string|null $pdfUrl
     * @return $this
     */
    public function setPdfUrl(?string $pdfUrl): Document
    {
        $this->pdfUrl = $pdfUrl;
        return $this;
    }
}
