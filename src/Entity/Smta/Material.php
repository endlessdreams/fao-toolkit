<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Smta
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Smta;

use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Child entity Material in the Smta model
 * <material>
 *   <crop>[crop]</crop>
 *   <sampleID>[sampleID]</sampleID>
 *   <pud>[PUD]</pud>
 *   <ancestry>[ancestry]</ancestry>
 * </material> *
 *
 */
class Material
{
    /**
     * @var string|null
     */

    #[Assert\NotBlank]
    #[Assert\Length(
        max: 64,
        maxMessage: 'Crop name cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $crop = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 128,
        maxMessage: 'SampleID cannot be longer than {{ limit }} characters',
    )]
    #[Groups(['Default'])]
    #[SerializedName('sampleID')]
    private ?string $sampleID = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Choice(
        callback: [ChoiceHelper::class ,'getYesOrNoChoices'],
        message: 'Provided {{ label }} value {{ value }} is not valid, please use any of: [{{ choices }}].'
    )]
    #[Groups(['Default'])]
    private ?string $pud = null;

    /**
     * @var string|null
     */
    #[Assert\When(
        expression: 'this.getPud() == "y"',
        constraints: [
            new Assert\NotBlank()
        ],
    )]
    #[Groups(['Default'])]
    private ?string $ancestry = null;

    /**
     * @return string|null
     */
    public function getCrop(): ?string
    {
        return $this->crop;
    }

    /**
     * @param string|null $crop
     * @return $this
     */
    public function setCrop(?string $crop): Material
    {
        $this->crop = $crop;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSampleID(): ?string
    {
        return $this->sampleID;
    }

    /**
     * @param string|null $sampleID
     * @return $this
     */
    public function setSampleID(?string $sampleID): Material
    {
        $this->sampleID = $sampleID;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPud(): ?string
    {
        return $this->pud;
    }

    /**
     * @param string|null $pud
     * @return $this
     */
    public function setPud(?string $pud): Material
    {
        $this->pud = $pud;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAncestry(): ?string
    {
        return $this->ancestry;
    }

    /**
     * @param string|null $ancestry
     * @return $this
     */
    public function setAncestry(?string $ancestry): Material
    {
        $this->ancestry = $ancestry;
        return $this;
    }
}
