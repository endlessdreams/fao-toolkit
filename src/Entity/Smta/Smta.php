<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Smta
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Smta;

use EndlessDreams\FaoToolkit\Entity\Base\Actor;
use EndlessDreams\FaoToolkit\Entity\Base\Entity;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Root entity in the Smta model
 * <smta>
 *   <symbol>[symbol]</symbol>
 *   <date>[date]</date>
 *   <type>[type]</type>
 *   <language>[language]</language>
 *   <provider>
 *     <type>[provType]</type>
 *     <pid>[PID]</pid>
 *     <name>[name]</name>
 *     <address>[address]</address>
 *     <country>[country]</country>
 *     <email>[email]</email>
 *   </provider>
 *   <recipient>
 *     <type>[recType]</type>
 *     <pid>[PID]</pid>
 *     <name>[name]</name>
 *     <address>[address]</address>
 *     <country>[country]</country>
 *   </recipient>
 *   <shipName>[shipName]</shipName>
 *   <annex1>
 *     <material>
 *       <crop>[crop]</crop>
 *       <sampleID>[sampleID]</sampleID>
 *       <pud>[PUD]</pud>
 *       <ancestry>[ancestry]</ancestry>
 *     </material> *
 *   </annex1>
 *   <document>
 *     <location>[location]</location>
 *     <retInfo>[retInfo]</retInfo>
 *     <pdf>[pdf]</pdf>
 *   </document>
 * </smta>
 *
 */
class Smta extends Entity
{
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Symbol cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    private ?string $symbol = null;
    /**
     * @var string|null
     */

    #[Assert\NotBlank]
    #[Assert\Length(
        max: 10,
        maxMessage: 'Date cannot be longer than {{ limit }} characters.',
    )]
    #[Assert\Regex(
        pattern: '/^\d{4}[-]([0][1-9]|[1][0-2])[-]([0][1-9]|[12]\d|[3][01])$/',
        message: 'Date was not written in format YYYY-MM-DD.'
    )]
    #[Groups(['Default'])]
    private ?string $date = null;
    /**
     * @var string|null
     */
    #[Assert\Choice(
        callback: [ChoiceHelper::class ,'getSmtaTypeChoices'],
        message: 'Provided {{label}} value {{value}} is not valid, please use any of: [{{ choices }}].'
    )]
    #[Groups(['Default'])]
    private ?string $type = null;

    /**
     * @var string|null
     */
    #[Assert\Choice(
        callback: [ChoiceHelper::class ,'getSmtaLanguageChoices'],
        message: 'Provided language value {{ value }} is not valid, please use any of: [{{ choices }}].'
    )]
    #[Assert\NotBlank]
    #[Groups(['Default'])]
    private ?string $language = null;

    /*
         # [Assert\Expression(
        "this.getProvider()?.getEmail() != null",
        message: 'Email is required for provider.',
    )]
     */
    /**
     * @var Actor|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Actor $provider = null;

    /**
     * @var Actor|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Actor $recipient = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'ShipName cannot be longer than {{ limit }} characters',
    )]
    #[Assert\When(
        expression: 'this.getType() == "sw"',
        constraints: [
            new Assert\NotBlank(
                message: 'ShipName should not be blank, when the type is "sw".',
            )
        ],
    )]
    #[Groups(['Default'])]
    #[SerializedName('shipName')]
    private ?string $shipName = null;

    /**
     * @var Materials|null
     */
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Materials $annex1 = null;

    /**
     * @var Document|null
     */
    #[Assert\NotBlank]
    #[Assert\Valid]
    #[Groups(['Default'])]
    private ?Document $document = null;

    /**
     * @return string|null
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * @param string|null $symbol
     * @return $this
     */
    public function setSymbol(?string $symbol): Smta
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return $this
     */
    public function setDate(?string $date): Smta
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return $this
     */
    public function setType(?string $type): Smta
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     * @return $this
     */
    public function setLanguage(?string $language): Smta
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return Actor|null
     */
    public function getProvider(): ?Actor
    {
        return $this->provider;
    }

    /**
     * @param Actor|null $provider
     * @return $this
     */
    public function setProvider(?Actor $provider): Smta
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return Actor|null
     */
    public function getRecipient(): ?Actor
    {
        return $this->recipient;
    }

    /**
     * @param Actor|null $recipient
     * @return $this
     */
    public function setRecipient(?Actor $recipient): Smta
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShipName(): ?string
    {
        return $this->shipName;
    }

    /**
     * @param string|null $shipName
     * @return $this
     */
    public function setShipName(?string $shipName): Smta
    {
        $this->shipName = $shipName;
        return $this;
    }

    /**
     * @return Materials|null
     */
    public function getAnnex1(): ?Materials
    {
        return $this->annex1;
    }

    /**
     * @param Materials|null $annex1
     * @return $this
     */
    public function setAnnex1(?Materials $annex1): Smta
    {
        $this->annex1 = $annex1;
        return $this;
    }

    /**
     * @return Document|null
     */
    public function getDocument(): ?Document
    {
        return $this->document;
    }

    /**
     * @param Document|null $document
     * @return $this
     */
    public function setDocument(?Document $document): Smta
    {
        $this->document = $document;
        return $this;
    }
}
