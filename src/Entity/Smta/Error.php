<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Smta
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Smta;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Child and collection entity Error in the Smta:Reponse model
 * <error>
 *   <code>[code]</code>
 *   <msg>[message]</msg>
 * </error>
 */
class Error
{
    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $code = null;

    /**
     * @var string|null
     */
    #[Groups(['Default'])]
    private ?string $msg = null;

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return $this
     */
    public function setCode(?string $code): Error
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMsg(): ?string
    {
        return $this->msg;
    }

    /**
     * @param string|null $msg
     * @return $this
     */
    public function setMsg(?string $msg): Error
    {
        $this->msg = $msg;
        return $this;
    }
}
