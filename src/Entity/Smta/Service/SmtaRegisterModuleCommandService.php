<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Smta
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Smta\Service;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\Smta\Response;
use EndlessDreams\FaoToolkit\Entity\Smta\Smta;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandService;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use Throwable;
use Yiisoft\Db\Command\CommandInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;

/**
 *
 */
final class SmtaRegisterModuleCommandService extends ModuleCommandService implements ModuleCommandServiceInterface
{
    /**
     * @var CommandInterface|null
     */
    private ?CommandInterface $itemsCommand = null;
    /**
     * @var string|null
     */
    private ?string $providerInstituteColumnName = null;

    /**
     *
     */
    public function __construct()
    {
        $this->entityClass = Smta::class;
        $this->entityXmlRootName = 'smta';

        $this->parserGroups = ['Default','Smta'];
        $this->validatorGroups = ['Default','Smta'];

        $this->entityResponseClass = Response::class;
        $this->responseXmlRootName = 'smta';

        $this->faoConfigInternalTableName = 'table_order';
        $this->uniqueKeyInInternalTable = 'symbol';

        $this->logMsgBeginning = 'SMTA order';

        $this->faoInstituteCodeQueryColumn = 'fao_institute_code';

        $this->rowPk = 'id';
        $this->dateRangeColumn = 'date';
    }

    /**
     * @param DbMapService $dbMapService
     * @return void
     * @throws Exception
     * @throws DbMapException
     * @throws InvalidConfigException
     */
    public function init(DbMapService $dbMapService): void
    {
        parent::init($dbMapService);
        $this->itemsCommand = $this->dbMapService
            ?->getTableQuery('table_item', ['orderid'])
            ?->andWhere('[[orderid]] = :orderid')->createCommand();
        $this->providerInstituteColumnName = StringHelper::camelize($this->faoInstituteCodeQueryColumn);
    }

    /**
     * @param array $row
     * @param int|null $index
     * @param array $context
     * @return void
     * @throws Throwable
     * @throws Exception
     */
    public function parseRow(array &$row, int|null $index, array &$context): void
    {
        /** @var CommandInterface $itemsCommand */
        $itemsCommand = $context['itemsCommand'];

        /** @var ?Provider $provider */
        $provider = isset($this->providerInstituteColumnName) ? $this->dbMapService?->faoConfig?->getProviders()
            ?->findProvider((string)$row[$this->providerInstituteColumnName]) : null;

        $row['provider'] = [
            'type' => $provider?->getType(),
            'pid' => $provider?->getPid($provider->getSelectedFaoEnvironment()),
            'name' => $provider?->getName(),
            'address' => $provider?->getAddress(),
            'country' => $provider?->getCountry(),
            'email' => $provider?->getEmail(),
        ];
        $row['recipient'] = [
            'type' => $row['recipientType'],
            'name' => $row['recipientName'],
            'address' => $row['recipientAddress'],
            'country' => $row['recipientCountry'],
        ];
        unset($row['recipientType']);
        unset($row['recipientName']);
        unset($row['recipientAddress']);
        unset($row['recipientCountry']);

        $absoluteDocumentPdfUrl = $this->resolveAbsolutePath((string)$row['documentPdf']);

        $documentPdf = $this->isUrlValid($absoluteDocumentPdfUrl)
            ? StringHelper::getBase64EncodedInlineFile((string)$absoluteDocumentPdfUrl)
            : null;

        $row['document'] = [
            'location' => $row['documentLocation'],
            'retInfo' => $row['documentRetinfo'],
            'pdf' => $documentPdf
        ];
        unset($row['documentLocation']);
        unset($row['documentRetinfo']);
        unset($row['documentPdf']);
        unset($row['faoInstituteCode']);

        $materials = $itemsCommand->queryAll();
        if (count($materials) > 0) {
            $row['annex1'] = ['material' => $materials];
        } else {
            $row['annex1'] = [];
            unset($materials);
        }
    }

    /**
     * @param array $row
     * @param array $context
     * @return array
     */
    protected function getParseAndDenormalizeContext(array $row, array &$context = []): array
    {
        return [
            'itemsCommand' => $this->itemsCommand?->bindValue(':orderid', (int)$row['id'])
        ];
    }

    /**
     * @param string|null $xml
     * @return void
     */
    public function filterXml(?string &$xml): void
    {
        if (isset($xml)) {
            $xml = str_replace("    <material></material>\n", '', $xml);
        }
    }

    /**
     * @param string $instituteCodeColumn
     * @return void
     */
    public function setFaoInstituteCodeQueryColumn(string $instituteCodeColumn): void
    {
        $this->faoInstituteCodeQueryColumn = $instituteCodeColumn;
    }

    /**
     * @param string|null $url
     * @param array|null $acceptedProtocols
     * @return bool
     */
    protected function isUrlValid(?string $url, ?array $acceptedProtocols = []): bool
    {
        return StringHelper::isUrlValid($url, $acceptedProtocols);
    }

    /**
     * @param string|null $url
     * @return string|null
     */
    protected function resolveAbsolutePath(?string $url): ?string
    {
        return StringHelper::resolveAbsolutePath($url, $this->dbMapService?->faoConfig?->getAliases());
    }
}
