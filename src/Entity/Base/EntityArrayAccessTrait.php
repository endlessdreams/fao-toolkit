<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Base
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Base;

use ArrayIterator;
use Symfony\Component\Serializer\Annotation\Ignore;
use Yiisoft\Arrays\ArrayAccessTrait;

/**
 *
 */
trait EntityArrayAccessTrait
{
    use ArrayAccessTrait {
        ArrayAccessTrait::getIterator as private internalIterator;
        ArrayAccessTrait::offsetExists as private internalOffsetExists;
        ArrayAccessTrait::offsetGet as private internalOffsetGet;
    }

    /**
     * @var array
     */
    #[Ignore]
    private array $data = [];

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array|null $data
     * @return $this
     */
    public function setData(?array $data): self
    {
        $this->data = $data ?? [];
        return $this;
    }

    /**
     * @return ArrayIterator
     */
    #[Ignore]
    public function getIterator(): ArrayIterator
    {
        return $this->internalIterator();
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    #[Ignore]
    public function offsetExists(mixed $offset): bool
    {
        return $this->internalOffsetExists($offset);
    }

    /**
     * @param mixed $offset
     * @return string|bool|null
     *
     * @psalm-suppress MixedInferredReturnType
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    #[Ignore]
    public function offsetGet(mixed $offset): string|bool|null
    {
        return $this->internalOffsetGet($offset);
    }
}
