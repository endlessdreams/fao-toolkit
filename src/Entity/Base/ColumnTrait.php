<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Base
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Base;

use Closure;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Serializer\Annotation\Ignore;
use Yiisoft\Arrays\ArrayHelper;

/**
 *
 */
trait ColumnTrait
{
    /**
     * @param bool|null $extraFields
     * @return array<array-key,mixed>
     *
     * @psalm-suppress MixedAssignment
     */
    protected function fetchFieldNamesReflexive(?bool $extraFields = false): array
    {
        $props = (new ReflectionClass($this))->getProperties();
        $arr = [];
        array_walk(
            $props,
            function (ReflectionProperty $prop) use ($extraFields, &$arr) {
                $ignoreCnt = count($prop->getAttributes(Ignore::class, ReflectionAttribute::IS_INSTANCEOF));
                if ($extraFields ? $ignoreCnt > 0 : $ignoreCnt === 0) {
                    /** @var array<array-key,mixed> $arr */
                    $arr[$prop->getName()] = $prop->getValue($this);
                }
            }
        );
        return $arr;
    }

    /**
     * @param bool|null $extraFields
     * @return array<array-key,mixed>
     */
    protected function fetchFields(?bool $extraFields = false): array
    {
        $fieldArray = $this->fetchFieldNamesReflexive($extraFields);

        return ArrayHelper::isAssociative($fieldArray)
            ? array_combine(($fields = array_keys($fieldArray)), array_map(function (string|int $value): Closure {
                $method = "get" . ucfirst((string)$value);
                return fn(): mixed => $this->$method();
            }, $fields))
            : [];
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @param array $exclude
     * @return array
     */
    public function toSelectArray(
        array $fields = [],
        array $expand = [],
        bool $recursive = true,
        array $exclude = []
    ): array {
        $arr = [];
        $keyVars = $this->toArray($fields, $expand, $recursive);
        $filtered = array_filter($keyVars, fn($key) => !in_array($key, $exclude), ARRAY_FILTER_USE_KEY);
        foreach ($filtered as $faoFieldName => $clientColumnName) {
            if (!isset($clientColumnName) || !is_string($clientColumnName)) {
                continue;
            }
            $arr[] = sprintf('[[%s]] as [[%s]]', $clientColumnName, $faoFieldName);
        }
        return $arr;
    }
}
