<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Base
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Base;

use EndlessDreams\FaoToolkit\Entity\Glis\Breeders;
use EndlessDreams\FaoToolkit\Entity\Glis\Collectors;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Yiisoft\Arrays\ArrayHelper;
use Yiisoft\Strings\StringHelper;

/**
 *
 */
class ActorsDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    /**
     * @inheritDoc
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        $obj = new $type();
        $key = $this->getSupportedKeys()[$type];
        codecept_debug('Denormalize actor:' . $key . ' - Begin');
        if (is_array($data)) {
            if (/*isset($key) && */!array_key_exists($key, $data)) {
                codecept_debug('key breeder does not exists.');
                codecept_debug($data);
                $data = [$key => $data];
                codecept_debug($data);
            } else {
                codecept_debug('Denormalize actor:' . $key);

                /** @var array<array-key,mixed> $actors */
                $actors = $data[$key];

                if (ArrayHelper::isAssociative($actors)) {
                    $data = [$key => [$actors]];
                }
            }

            if (
                is_array($data[$key])
                && !in_array(
                    false,
                    array_map(
                        fn(mixed $datum): bool => is_array($datum) && ArrayHelper::isAssociative($datum),
                        $data[$key]
                    )
                )
            ) {
                $arr = [];
                /** @var array $datum */
                foreach ($data[$key] as $datum) {
                    /** @var Actor $actor */
                    $actor = $this->denormalizer->denormalize($datum, Actor::class);
                    $arr[] = $actor;
                }

                if (method_exists($obj, $setMethod = "set" . StringHelper::baseName($type))) {
                    $obj->$setMethod($arr);
                }
            }
        }
        codecept_debug('Denormalize actor:' . $key . ' - End');
        return $obj;
    }

    /**
     * @return string[]
     */
    private function getSupportedKeys(): array
    {
        return [
            Breeders::class => 'breeder',
            Collectors::class => 'collector'
        ];
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return in_array($type, array_keys($this->getSupportedKeys()));
    }

    /**
     * @param string|null $format
     * @return array
     */
    public function getSupportedTypes(?string $format): array
    {
        $arr = [
            'object' => null, // Doesn't support any classes or interfaces
            '*' => false, // Supports any other types, but the result is not cacheable
        ];
        foreach ($this->getSupportedKeys() as $key => $value) {
            $arr[$key] = true;
        }
        return $arr;
    }
}
