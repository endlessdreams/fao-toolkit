<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Base
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Base;

use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Attribute\Ignore;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Generic Entity Actor
 * <Actor>
 *   <type>[type]</type>?
 *   <wiews>[wiews]</wiews>
 *   <pid>[pid]</pid>
 *   <name>[name]</name>
 *   <address>[address]</address>
 *   <country>[country]</country>
 *   <email>[email]</email>?
 * </Actor>
 */
class Actor
{

    /**
     * @var string|null
     */
    #[Assert\Choice(
        callback: [ChoiceHelper::class ,'getActorTypeChoices'],
        message: 'This value is not a valid actor type.',
        groups: ['Smta'],
    )]
    #[Assert\NotBlank(allowNull: true, groups: ['register'],)]
    #[Groups(['Smta'])]
    protected ?string $type = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 16,
        maxMessage: 'Wiews code cannot be longer than {{ limit }} characters.',
        groups: ['Glis'],
    )]
    #[Assert\Regex(
        pattern: '/^[A-Z][A-Z][A-Z]\d{3,4}/',
        message: "Wiews code doesn't meet expected format: {{ pattern }}.",
        groups: ['Glis'],
    )]
    #[Assert\NotBlank(allowNull: true, groups: ['Smta'],)]
    #[Groups(['Default'])]
    protected ?string $wiews = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 16,
        maxMessage: 'Pid cannot be longer than {{ limit }} characters.',
    )]
    #[Assert\Regex(
        pattern: '/^\d\d[A-Z][A-Z]\d\d/',
        message: "Pid doesn't meet expected format: {{ pattern }}."
    )]
    #[Groups(['Default'])]
    protected ?string $pid = null;

    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Name cannot be longer than {{ limit }} characters.',
    )]
    #[Groups(['Default'])]
    protected ?string $name = null;
    /**
     * @var string|null
     */
    #[Assert\Length(
        max: 128,
        maxMessage: 'Address cannot be longer than {{ limit }} characters.',
        groups: ['Glis']
    )]
    #[Assert\Length(
        max: 65536,
        maxMessage: 'Address cannot be longer than {{ limit }} characters.',
        groups: ['Smta']
    )]
    #[Groups(['Default'])]
    protected ?string $address = null;

    /**
     * @var string|null
     */
    #[Assert\AtLeastOneOf(
        constraints: [
            new Assert\Country(
                message: 'This value {{ value }} is not a valid country.' . PHP_EOL,
                alpha3: true
            ),
            new Assert\Choice(
                callback: [ChoiceHelper::class, 'getExtraCountryCodeChoices'],
                message: 'This value {{ value }} is not a valid FAO extended country.' . PHP_EOL
            ),
            new Assert\Blank(
                message: 'This value should be blank.' . PHP_EOL
            ),
        ],
        message: "Country value should satisfy at least one of the following constraints:" . PHP_EOL,
    )]
    #[Groups(['Default'])]
    protected ?string $country = null;

    /**
     * @var string|null
     */
    #[Assert\Email(
        message: 'The email {{ value }} is not a valid email.',
        groups: ['Smta'],
    )]
    #[Assert\NotBlank(allowNull: true, groups: ['register'],)]
    #[Groups(['Smta'])]
    protected ?string $email = null;

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return $this
     */
    public function setType(?string $type): Actor
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWiews(): ?string
    {
        return $this->wiews;
    }

    /**
     * @param string|null $wiews
     * @return Actor
     */
    public function setWiews(?string $wiews): Actor
    {
        $this->wiews = $wiews;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPid(): ?string
    {
        return $this->pid;
    }

    /**
     * @param string|null $pid
     * @return Actor
     */
    public function setPid(?string $pid): Actor
    {
        $this->pid = $pid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Actor
     */
    public function setName(?string $name): Actor
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return Actor
     */
    public function setAddress(?string $address): Actor
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return Actor
     */
    public function setCountry(?string $country): Actor
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return $this
     */
    public function setEmail(?string $email): Actor
    {
        $this->email = $email;
        return $this;
    }


    /**
     * @return bool
     */
    #[Ignore]
    public function isAllEmpty(): bool
    {
        return StringHelper::isAllEmpty([$this->pid,$this->wiews,$this->name,$this->address,$this->country]);
        /*
        return ($this->pid === null || $this->pid === '')
            && ($this->wiews === null || $this->wiews === '')
            && ($this->name === null || $this->name === '')
            && ($this->address === null || $this->address === '')
            && ($this->country === null || $this->country === '');
        */
    }

    /**
     * @param ExecutionContextInterface $context
     * @param mixed $payload
     * @return void
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, mixed $payload): void
    {
        $IsNameEtcValid = true;
        $IsNameEtcValidCnt = 0;


        if (!StringHelper::isEmpty($this->name)) {
            $IsNameEtcValidCnt++;
        }
        if (!StringHelper::isEmpty($this->address)) {
            $IsNameEtcValidCnt++;
        }
        if (!StringHelper::isEmpty($this->country)) {
            $IsNameEtcValidCnt++;
        }

        $IsNameEtcValid = $IsNameEtcValidCnt === 0 || $IsNameEtcValidCnt === 3;

        $qualityOfActor = match (true) {
            (
                !StringHelper::isEmpty($this->pid)
                && $context
                    ->getValidator()
                    ->validateProperty($this, 'pid', ['Default','Actor','Glis'])->count() === 0
            )
                => 3,
            (
                ($context->getRoot() instanceof Glis)
                && !StringHelper::isEmpty($this->wiews)
                && $context->getValidator()->validateProperty($this, 'wiews', ['Default','Actor'])->count() === 0
            )
                => 2,
            (
                $IsNameEtcValidCnt === 3
                && $context->getValidator()->validateProperty($this, 'name', ['Default','Actor'])->count() === 0
                && $context->getValidator()->validateProperty($this, 'address', ['Default','Actor'])->count() === 0
                && $context->getValidator()->validateProperty($this, 'country', ['Default','Actor'])->count() === 0
            )
                => 1,
            default => 0
        };

        if ($qualityOfActor === 0 && !$this->isAllEmpty()) {
            //codecept_debug('Either Wiews, Pid or all Name, Address and Country together be valid.');
            $context->buildViolation('Either Wiews, Pid or all Name, Address and Country together be valid.')
                ->atPath('Actor')
                ->addViolation();
        }

        if (!$IsNameEtcValid && $qualityOfActor === 0) {
            $context->buildViolation('Actor name, address or country must either all be in use or not.')
                ->atPath('name-address-country')
                ->addViolation();
        }
    }
}

