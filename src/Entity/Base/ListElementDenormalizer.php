<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Entity\Base
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Entity\Base;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credential;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credentials;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Options;
use EndlessDreams\FaoToolkit\Entity\Glis\Acquisition;
use EndlessDreams\FaoToolkit\Entity\Glis\Dois;
use EndlessDreams\FaoToolkit\Entity\Glis\Id;
use EndlessDreams\FaoToolkit\Entity\Glis\Ids;
use EndlessDreams\FaoToolkit\Entity\Glis\Kw;
use EndlessDreams\FaoToolkit\Entity\Glis\Kws;
use EndlessDreams\FaoToolkit\Entity\Glis\Name;
use EndlessDreams\FaoToolkit\Entity\Glis\Names;
use EndlessDreams\FaoToolkit\Entity\Glis\Target;
use EndlessDreams\FaoToolkit\Entity\Glis\Targets;
use EndlessDreams\FaoToolkit\Entity\Smta\Error;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Yiisoft\Arrays\ArrayHelper;
use Yiisoft\Strings\StringHelper;

/**
 *
 */
class ListElementDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    /**
     * @var array<string,array<string,string|bool|null>>|null
     */
    protected ?array $supportedLeafListElements = null;

    /**
     * @var string[]
     */
    private array $supportedKeys = [
        Names::class => 'name',
        Kws::class => 'kw',
        Dois::class => '*', //'doi'
        Ids::class => 'id',
        Targets::class => 'target',
        Kw::class => '#',
        Name::class => '#',
        Options::class => '#',
        //Id::class => '*',
        Credentials::class => 'credential',
        Credential::class => '*',
        /*Error::class => '*',*/
        /*Acquisition::class => '*',*/
    ];

    /**
     * @inheritDoc
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        codecept_debug('Inside ' . $type . ' List denormalize - Begin');
        $obj = new $type();

        if (empty($data)) {
            return $obj;
        }

        $key = $this->getSupportedKeys()[$type];

        if ($key == '*' && is_array($data)) {

            /** @var Error|Credential|Dois $obj */
            (match ($type) {
                Error::class => fn(Error $obj) => $obj
                    ->setCode((string)$data['code'])
                    ->setMsg((string)$data['msg']),
                Credential::class => fn(Credential $obj) => $obj
                    ->setEnv((string)$data['@env'])
                    ->setUsername((string)$data['username'])
                    ->setPassword((string)$data['password']),
                Dois::class => fn(Dois $obj) => $obj
                    ->setData($this->convertDoisIfNeeded($data['dois'])),
            })($obj);
            codecept_debug('Inside ' . $type . ' List denormalize - Leaving case *');
            return $obj;
        }

        if ($key === '#') {

            /** @var array $data */
            $data = is_string($data) ? ['#' => $data] : $data;
            if (ArrayHelper::isAssociative($data) && !ArrayHelper::keyExists($data, '#')) {
                $data = ['#' => $data];
            }

            if (method_exists($obj, 'setValue')) {
                $obj->setValue($data['#']);
            }

            codecept_debug('Inside ' . $type . ' List denormalize - Leaving case #');
            return $obj;
        }

        $names = (is_array($data) && array_key_exists($key, $data))
            ? $data[$key]
            : $data;

        if (is_string($names) || is_array($names) && ArrayHelper::isAssociative($names)) {
            $data = [$key => [$names]];
        }

        // Temporary remove /*'id','target',*/ from this if case
        if (
            is_array($names)
            && ArrayHelper::isIndexed($names)
            && array_key_exists($key, $this->getSupportedLeafElements())
        ) {
            $data = [ $key => []];

            /** @var array{ class: string|null, isLeaf: string|false } $supportedLeafTuple  */
            $supportedLeafTuple = ArrayHelper::getValue(
                $this->getSupportedLeafElements(),
                $key,
                ['class' => null, 'isLeaf' => false]
            );
            ['class' => $elementType, 'isLeaf' => $isLeaf] = $supportedLeafTuple;

            $leafs = $this->supportedLeafListElements['#'] ?? [];
            $cLeafs = $this->supportedLeafListElements['*'] ?? [];

            /** @var array $name */
            foreach ($names as $name) {
                if (array_key_exists($key, $leafs) || array_key_exists($key, $cLeafs)) {
                    $data[$key][] = $this->denormalize($name, (string)$elementType);
                } else {
                    switch ($key) {
                        case 'target':
                            $target = new Target();

                            /** @var Kws $kws */
                            $kws = $this->denormalize($name['kws'], Kws::class);
                            $target->setValue((string)$name['value'])?->setKws($kws);
                            $data[$key][] = $target;
                            break;
                        case 'id':
                            $id = new Id();
                            $id->setType((string)$name['@type'])->setValue((string)$name['#']);
                            $data[$key][] = $id;
                            break;
                    }
                }
            }
        }

        if (method_exists($obj, $setMethod = "set" . StringHelper::baseName($type))) {
            $obj->$setMethod($data[$key]);
        }

        codecept_debug('Inside ' . $type . ' List denormalize - End');
        return $obj;
    }

    /**
     * @return string[]
     */
    public function getSupportedKeys(): array
    {
        return $this->supportedKeys;
    }

    /**
     * @param array<string,string> $supportedKeys
     * @return $this
     */
    public function setSupportedKeys(array $supportedKeys): ListElementDenormalizer
    {
        $this->supportedKeys = $supportedKeys;
        return $this;
    }

    /**
     * @return array<string,array<string,string|bool|null>>
     */
    protected function getSupportedLeafElements(): array
    {
        if (isset($this->supportedLeafListElements)) {
            return $this->supportedLeafListElements;
        }
        $this->supportedLeafListElements = [];

        foreach ($this->getSupportedKeys() as $key => $value) {
            /** @var string $key */
            if (
                !array_key_exists(
                    ($value),
                    $this->supportedLeafListElements
                )
            ) {
                $this->supportedLeafListElements[$value] = [];
            }
            $this->supportedLeafListElements[$value][strtolower(StringHelper::baseName($key))] = $key;
        }

        foreach ($this->supportedLeafListElements as $key => &$supportedLeafListElement) {
            if ($key === '#') {
                continue;
            }
            if (
                array_key_exists($key . 's', $supportedLeafListElement)
                && is_string($supportedLeafListElement[strtolower($key . 's')])
            ) {
                $supportedLeafListElement['class'] = StringHelper::substring(
                    $supportedLeafListElement[strtolower($key . 's')],
                    0,
                    -1
                );
                $supportedLeafListElement['isLeaf'] = array_key_exists($key, $this->supportedLeafListElements['#']);
            }
        }
        return $this->supportedLeafListElements;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return in_array($type, array_keys($this->getSupportedKeys()));
    }

    /**
     * @param string|null $format
     * @return array
     */
    public function getSupportedTypes(?string $format): array
    {
        $arr = [
            'object' => null, // Doesn't support any classes or interfaces
            '*' => false, // Supports any other types, but the result is not cacheable
        ];
        foreach ($this->getSupportedKeys() as $key => $value) {
            $arr[$key] = true;
        }
        return $arr;
    }

    /**
     * @param mixed $dois
     * @return string[]
     *
     * @psalm-suppress MixedReturnTypeCoercion
     */
    private function convertDoisIfNeeded(mixed $dois): array
    {
        if (!is_array($dois)) {
            $dois = [(string)$dois];
        }
        array_walk($dois, fn(mixed $doi): string => (string)$doi);

        return $dois;
    }
}
