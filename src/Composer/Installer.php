<?php

declare(strict_types=1);

namespace App\Composer;

use FilesystemIterator as FSIterator;
use RecursiveDirectoryIterator as DirIterator;
use RecursiveIteratorIterator as RIterator;

/**
 *
 */
final class Installer
{
    /**
     * @psalm-suppress UndefinedClass
     */
    public static function postUpdate(): void
    {
        self::chmodRecursive('runtime', 0777);
        if (!file_exists('runtime/tests/_output')) {
            mkdir('runtime/tests/_output', 0777, true);
        }
    }

    /**
     * @param string $path
     * @param int $mode
     * @return void
     */
    private static function chmodRecursive(string $path, int $mode): void
    {
        chmod($path, $mode);
        $iterator = new RIterator(
            new DirIterator($path, FSIterator::SKIP_DOTS | FSIterator::CURRENT_AS_PATHNAME),
            RIterator::SELF_FIRST
        );
        /** @var string $item */
        foreach ($iterator as $item) {
            chmod($item, $mode);
        }
    }

    /**
     * @return void
     */
    public static function copyEnvFile(): void
    {
        if (!file_exists('.env')) {
            copy('.env.example', '.env');
        }
        if (!file_exists('.env.test')) {
            copy('.env.test.example', '.env.test');
        }
    }
}
