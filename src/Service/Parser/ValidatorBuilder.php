<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service\Parser
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Parser;

use Exception;
use Symfony\Component\Validator\Constraints\EmailValidator;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * class ValidatorBuilder
 */
class ValidatorBuilder
{
    /**
     * @var LazyLoadingMetadataFactory|null
     */
    private ?LazyLoadingMetadataFactory $metadataFactory = null;

    /**
     *
     */
    public function __construct(/*private readonly LazyLoadingMetadataFactory $metadataFactory*/)
    {
    }

    /**
     * @param LazyLoadingMetadataFactory $metadataFactory
     * @return $this
     */
    public function setMetadataFactory(LazyLoadingMetadataFactory $metadataFactory): ValidatorBuilder
    {
        $this->metadataFactory = $metadataFactory;
        return $this;
    }

    /**
     * @return ValidatorInterface
     * @throws Exception
     *
     * @psalm-suppress PossiblyNullArgument
     */
    public function create(): ValidatorInterface
    {
        if (!isset($this->metadataFactory)) {
            throw new Exception('Validator builder is missing metadata factory.');
        }
        return Validation::createValidatorBuilder()
            ->setMetadataFactory($this->metadataFactory)
            ->setConstraintValidatorFactory(
                new ConstraintValidatorFactory(
                    [EmailValidator::class => new EmailValidator(Assert\Email::VALIDATION_MODE_HTML5)]
                )
            )
            ->getValidator();
    }
}
