<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service\Parser
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Parser;

use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandRegistryService;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use EndlessDreams\FaoToolkit\Service\Trait\ValidatorServiceTrait;
use ArrayObject;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\YamlEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\UidNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Yiisoft\Arrays\ArrayHelper;

/**
 * class ParserService
 */
class ParserService
{
    use ValidatorServiceTrait;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var array|DenormalizerAwareInterface[]
     */
    private array $denormalizers;

    /**
     * @var ClassMetadataFactory
     */
    private ClassMetadataFactory $classMetadataFactory;

    /**
     * @var array<string,array<string,mixed>>
     */
    private array $context;

    /**
     * @var Serializer
     */
    private Serializer $serializer;

    /**
     * @var ModuleCommandRegistryService
     */
    public readonly ModuleCommandRegistryService $moduleCommandRegister;

    /**
     * @param ValidatorInterface $validator
     * @param DenormalizerAwareInterface[] $denormalizers
     * @param array<string,ModuleCommandServiceInterface>|null $moduleCommandServices
     */
    public function __construct(
        ValidatorInterface $validator,
        ?array $denormalizers = [],
        ?array $moduleCommandServices = []
    ) {
        $this->validator = $validator;
        $this->denormalizers = $denormalizers ?? [];
        $this->classMetadataFactory = self::initClassMetadataFactory();
        $this->context = self::initContext();
        $this->serializer = self::initSerializer(
            $this->classMetadataFactory,
            $this->denormalizers
        );
        $this->moduleCommandRegister = new ModuleCommandRegistryService();

        if (count($moduleCommandServices ?? []) > 0) {
            /**
             * @var array<string,ModuleCommandServiceInterface> $moduleCommandServices
             * @var string $moduleCommand
             * @var ModuleCommandServiceInterface $service
             */
            foreach ($moduleCommandServices as $moduleCommand => $service) {
                if ($service instanceof ModuleCommandServiceInterface) {
                    $this->moduleCommandRegister->addService($moduleCommand, $service);
                }
            }
        }
    }

    /**
     * @return ClassMetadataFactory
     */
    protected static function initClassMetadataFactory(): ClassMetadataFactory
    {
        $attributeLoader = new AttributeLoader();
        return new ClassMetadataFactory($attributeLoader);
    }

    /**
     * @return array<string,array<string,mixed>>
     */
    protected static function initContext(): array
    {
        $context = [];

        $context['xml'] = [
            'xml_format_output' => true,
            'xml_encoding' => 'UTF-8',
            AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
            'load_options' => \LIBXML_NONET | \LIBXML_NOEMPTYTAG | \LIBXML_NOBLANKS,
            'save_options' => \LIBXML_NONET | \LIBXML_NOEMPTYTAG | \LIBXML_NOBLANKS/* | LIBXML_COMPACT */,
            XmlEncoder::CDATA_WRAPPING => true,
            //'decoder_ignored_node_types' => [\XML_PI_NODE, \XML_COMMENT_NODE],
            //'encoder_ignored_node_types' => [\XML_COMMENT_NODE],
            //AbstractObjectNormalizer::SKIP_UNINITIALIZED_VALUES => false
        ]; // Specify 'UTF-8' as the encoding in the context

        $context['csv'] = [
            'csv_delimiter' => "\t",
            AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
        ];

        return $context;
    }

    /**
     * @param ClassMetadataFactory $classMetadataFactory
     * @param array $denormalizers
     * @return Serializer
     */
    protected static function initSerializer(
        ClassMetadataFactory &$classMetadataFactory,
        array &$denormalizers
    ): Serializer {
        /** @var array<EncoderInterface|DecoderInterface> $encoders */
        $encoders = [
            'xml' => new XmlEncoder(),
            'json' => new JsonEncoder(),
            'csv' => new CsvEncoder(),
            'yaml' => new YamlEncoder()
        ];

        $metadataAwareNameConverter = new MetadataAwareNameConverter(
            $classMetadataFactory,
            new CamelCaseToSnakeCaseNameConverter()
        );
        $propertyTypeExtractor = new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]);

        /** @var array<NormalizerInterface|DenormalizerInterface> $normalizers */
        $normalizers = [
            //new GetSetMethodNormalizer(),
            new ArrayDenormalizer(),
            new UidNormalizer(),
            new BackedEnumNormalizer(),
            new DateTimeNormalizer(),
        ];

        /** @var array<NormalizerInterface|DenormalizerInterface|NormalizerAwareInterface|DenormalizerAwareInterface> $denormalizers */
        foreach ($denormalizers as $denormalizer) {
            $normalizers[] = $denormalizer;
        }

        $normalizers[] =
            new ObjectNormalizer(
                $classMetadataFactory,
                $metadataAwareNameConverter,
                null,
                $propertyTypeExtractor
            );

        /** @var non-empty-array<NormalizerInterface|DenormalizerInterface> $normalizers */
        return new Serializer($normalizers, $encoders);
    }

    /**
     * @return Serializer
     */
    public function getSerializer(): Serializer
    {
        return $this->serializer;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * @return array|DenormalizerAwareInterface[]
     */
    public function getDenormalizers(): array
    {
        return $this->denormalizers;
    }

    /**
     * @param array $denormalizers
     * @return $this
     */
    public function setDenormalizers(array $denormalizers): ParserService
    {
        $this->denormalizers = $denormalizers;
        return $this;
    }

    /**
     * @return ClassMetadataFactory
     */
    public function getClassMetadataFactory(): ClassMetadataFactory
    {
        return $this->classMetadataFactory;
    }

    /**
     * @param string $xml
     * @param string|null $xml_root_node_name
     * @param mixed[]|null $addContext
     * @return mixed
     */
    public function decodeXml(string $xml, ?string $xml_root_node_name = null, ?array $addContext = null): mixed
    {
        /** @var array<string,mixed> $context */
        $context = $this->context['xml'];
        if (isset($xml_root_node_name)) {
            $context['xml_root_node_name'] = $xml_root_node_name;
        }

        if (isset($addContext)) {
            $context = array_merge($context, $addContext);
        }

        return $this->serializer->decode($xml, XmlEncoder::FORMAT, $context);
    }

    /**
     * @param array $data
     * @param string|null $xml_root_node_name
     * @param array|null $addContext
     * @return string
     */
    public function encodeXml(array $data, ?string $xml_root_node_name = null, ?array $addContext = null): string
    {
        /** @var array<string,mixed> $context */
        $context = $this->context['xml'];
        if (isset($xml_root_node_name)) {
            $context['xml_root_node_name'] = $xml_root_node_name;
        }

        if (isset($addContext)) {
            $context = array_merge($context, $addContext);
        }

        return $this->serializer->encode($data, XmlEncoder::FORMAT, $context);
    }

    /**
     * @param array $data
     * @return string
     */
    public function encodeCsv(array $data): string
    {
        /** @var array<string,mixed> $context */
        $context = $this->context['csv'];
        //$context[AbstractObjectNormalizer::SKIP_NULL_VALUES] = false;

        return $this->serializer->encode($data, CsvEncoder::FORMAT, $context);
    }

    /**
     * @param array $data
     * @param string $class
     * @param string|null $format
     * @param array|null $addContext
     * @return mixed
     */
    public function denormalize(array $data, string $class, ?string $format = null, ?array $addContext = null): mixed
    {
        $addContext ??= [];

        return $this->serializer->denormalize($data, $class, $format, $addContext);
    }

    /**
     * @param mixed $object
     * @return array|ArrayObject|bool|float|int|null|string
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object): array|ArrayObject|bool|float|int|null|string
    {
        return $this->serializer->normalize($object);
    }

    /**
     * @param mixed $object
     * @return array|ArrayObject|bool|float|int|null|string
     * @throws ExceptionInterface
     */
    public function normalizeXml(mixed $object): array|ArrayObject|bool|float|int|null|string
    {
        /** @var array<string,mixed> $context */
        $context = $this->context['xml'];
        return $this->serializer->normalize($object, XmlEncoder::FORMAT, $context);
    }

    /**
     * @param mixed $object
     * @param string|null $xml_root_node_name
     * @param mixed[]|null $addContext
     * @return string
     */
    public function serializeXml(mixed $object, ?string $xml_root_node_name = null, ?array $addContext = null): string
    {
        /** @var array<string,mixed> $context */
        $context = $this->context['xml'];
        if (isset($xml_root_node_name)) {
            $context['xml_root_node_name'] = $xml_root_node_name;
        }

        if (isset($addContext)) {
            $context = array_merge($context, $addContext);
        }

        /** @var array<string,mixed> $context */
        return $this->serializer->serialize($object, XmlEncoder::FORMAT, $context);
    }

    /**
     * @param mixed $object
     * @param array<string,mixed>|null $addContext
     * @return string
     */
    public function serializeCsv(mixed $object, ?array $addContext = null): string
    {
        /** @var array<string,mixed> $context */
        $context = $this->context['csv'];
        if (isset($addContext)) {
            $context = array_merge($context, $addContext);
        }
        return $this->serializer->serialize($object, CsvEncoder::FORMAT, $context);
    }

    /**
     * @param string $xml
     * @param string $class
     * @param string|null $xml_root_node_name
     * @param array|null $addContext
     * @return mixed
     */
    public function deserializeXml(
        string $xml,
        string $class,
        ?string $xml_root_node_name = null,
        ?array $addContext = null
    ): mixed {
        /** @var array<string,mixed> $context */
        $context = $this->context['xml'];

        if (isset($xml_root_node_name)) {
            $context['xml_root_node_name'] = $xml_root_node_name;
        }

        if (isset($addContext) && ArrayHelper::isAssociative($addContext)) {
            $context = array_merge($context, $addContext);
        }

        /** @var array<string,mixed> $context */
        return $this->serializer->deserialize($xml, $class, 'xml', $context);
    }

    /**
     * @param mixed $root
     * @return ConstraintViolationListInterface
     */
    public function validateRoot(mixed $root): ConstraintViolationListInterface
    {
        return $this->validator->validate($root);
    }
}
