<?php

/**
 * FAO Command Line and Web Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service\Parser
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Parser;

use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 *
 */
class ParserServiceBuilder
{
    /**
     * @var DenormalizerAwareInterface[]
     */
    private array $denormalizers = [];

    /**
     * @var array<string,ModuleCommandServiceInterface> $moduleCommandServices
     */
    private array $moduleCommandServices = [];

    /**
     * @param ValidatorInterface $validator
     */
    public function __construct(private readonly ValidatorInterface $validator)
    {
    }

    /**
     * @param DenormalizerAwareInterface[] $denormalizers
     * @return $this
     */
    public function setDenormalizers(array $denormalizers): ParserServiceBuilder
    {
        $this->denormalizers = $denormalizers;
        return $this;
    }

    /**
     * @param array<string,ModuleCommandServiceInterface> $moduleCommandServices
     * @return $this
     */
    public function setModuleCommandServices(array $moduleCommandServices): ParserServiceBuilder
    {
        $this->moduleCommandServices = $moduleCommandServices;
        return $this;
    }

    /**
     * @return ParserService
     */
    public function build(): ParserService
    {
        return new ParserService($this->validator, $this->denormalizers, $this->moduleCommandServices);
    }
}
