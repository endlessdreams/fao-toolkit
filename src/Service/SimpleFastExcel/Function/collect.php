<?php

namespace App\Service\SimpleFastExcel;

use Generator;
use Yiisoft\Arrays\ArrayableInterface;
use Yiisoft\Arrays\ArrayHelper;
use Yiisoft\Db\Query\Query;

/**
 * Return app instance of FastExcel.
 *
 * @param array<array-key,GenericCollection|Generator|Query|ArrayableInterface|array> $data
 * @return GenericCollection
 */
function collect(array $data = []): GenericCollection
{
    return new GenericCollection($data);
}

