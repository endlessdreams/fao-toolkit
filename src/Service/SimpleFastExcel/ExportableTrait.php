<?php

declare(strict_types=1);

namespace App\Service\SimpleFastExcel;

use Generator;
use InvalidArgumentException;
use OpenSpout\Common\Entity\Row;
use OpenSpout\Common\Entity\Style\Style;
use OpenSpout\Common\Exception\IOException;
use OpenSpout\Reader\CSV\Options as CsvReaderOptions;
use OpenSpout\Writer\Common\AbstractOptions;
use OpenSpout\Writer\Common\Creator\WriterEntityFactory;
use OpenSpout\Writer\CSV\Writer as CsvWriterOptions;
use OpenSpout\Writer\Exception\InvalidSheetNameException;
use OpenSpout\Writer\Exception\WriterNotOpenedException;
use Throwable;
use Yiisoft\Arrays\ArrayHelper;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Db\Query\Query;
use Yiisoft\Files\FileHelper;
use Yiisoft\Strings\StringHelper;

use function App\Service\SimpleFastExcel\collect;

/**
 *
 */
trait ExportableTrait
{
    /**
     * @var Style|null
     */
    private ?Style $headerStyle = null;
    /**
     * @var Style|null
     */
    private ?Style $rowsStyle = null;

    /**
     * @param $options
     * @return void
     */
    abstract protected function setOptions(&$options): void;

    /**
     * @param string $path
     * @param callable|null $callback
     * @return string
     *
     * @throws Exception
     * @throws IOException
     * @throws InvalidConfigException
     * @throws InvalidSheetNameException
     * @throws Throwable
     * @throws WriterNotOpenedException
     * @psalm-suppress RiskyTruthyFalsyComparison
     */
    public function export(string $path, callable $callback = null): string
    {
        self::exportOrDownload($path, 'openToFile', $callback);

        return realpath($path) ?: $path;
    }

    /**
     * @param string $path
     * @param string $function
     * @param callable|null $callback
     * @return void
     * @throws Exception
     * @throws IOException
     * @throws InvalidConfigException
     * @throws InvalidSheetNameException
     * @throws Throwable
     * @throws WriterNotOpenedException
     */
    private function exportOrDownload(string $path, string $function, callable $callback = null)
    {
        $writer = match (pathinfo($path, PATHINFO_EXTENSION)) {
            'csv' => new \OpenSpout\Writer\CSV\Writer($this->initAndReturnOptions(new \OpenSpout\Writer\CSV\Options())),
            'ods' => new \OpenSpout\Writer\ODS\Writer($this->initAndReturnOptions(new \OpenSpout\Writer\ODS\Options())),
            default => new \OpenSpout\Writer\XLSX\Writer(
                $this->initAndReturnOptions(new \OpenSpout\Writer\XLSX\Options())
            ),
        };

        /* @var \OpenSpout\Writer\WriterInterface $writer */
        $writer->$function($path);


        $hasSheets = (
            $writer instanceof \OpenSpout\Writer\XLSX\Writer || $writer instanceof \OpenSpout\Writer\ODS\Writer
        );

        // It can export one sheet (Collection) or N sheets (SheetCollection)
        $data = $this->transpose
            ? $this->transposeData()
            : ($this->data instanceof SheetCollection ? $this->data : new SheetCollection([$this->data]));

        foreach ($data as $key => $collection) {
            if ($collection instanceof GenericCollection) {
                $this->writeRowsFromCollection($writer, $collection, $callback);
            } elseif ($collection instanceof Generator) {
                $this->writeRowsFromGenerator($writer, $collection, $callback);
            } elseif ($collection instanceof Query) {
                $this->writeRowsFromQuery($writer, $collection, $callback);
            } elseif (is_array($collection)) {
                $this->writeRowsFromArray($writer, $collection, $callback);
            } else {
                throw new InvalidArgumentException('Unsupported type for $data');
            }
            if (is_string($key)) {
                $writer->getCurrentSheet()->setName($key);
            }
            if ($hasSheets && $data->keys()->last() !== $key) {
                $writer->addNewSheetAndMakeItCurrent();
            }
        }
        $writer->close();
    }

    /**
     * @return SheetCollection
     */
    private function transposeData(): SheetCollection
    {
        $data = $this->data instanceof SheetCollection ? $this->data : new SheetCollection([$this->data]);
        $transposedData = [];

        foreach ($data as $key => $collection) {
            foreach ($collection as $row => $columns) {
                foreach ($columns as $column => $value) {
                    ArrayHelper::setValue(
                        $transposedData,
                        implode('.', [
                            $key,
                            $column,
                            $row,
                        ]),
                        $value
                    );
                }
            }
        }

        return new SheetCollection($transposedData);
    }

    /**
     * @param $writer
     * @param GenericCollection $collection
     * @param callable|null $callback
     * @return void
     */
    private function writeRowsFromCollection($writer, GenericCollection $collection, ?callable $callback = null): void
    {
        // Apply callback
        if ($callback) {
            $collection->transform(function ($value) use ($callback) {
                return $callback($value);
            });
        }
        // Prepare collection (i.e remove non-string)
        $this->prepareCollection($collection);
        // Add header row.
        if ($this->withHeader) {
            $this->writeHeader($writer, $collection->first());
        }

        // createRowFromArray works only with arrays
        if (!is_array($collection->first())) {
            $collection = $collection->map(function ($value) {
                return $value->toArray();
            });
        }

        $allRows = $collection->map(function ($value) {
            return Row::fromValues($value);
        })->toArray();
        if ($this->rowsStyle) {
            $this->addRowsWithStyle($writer, $allRows, $this->rowsStyle);
        } else {
            $writer->addRows($allRows);
        }
    }

    /**
     * @param $writer
     * @param $allRows
     * @param $rowStyle
     * @return void
     */
    private function addRowsWithStyle($writer, $allRows, $rowStyle): void
    {
        $styledRows = [];
        // Style rows one by one
        foreach ($allRows as $row) {
            $styledRows[] = Row::fromValues($row->toArray(), $rowStyle);
        }
        $writer->addRows($styledRows);
    }

    /**
     * @param $writer
     * @param Generator $generator
     * @param callable|null $callback
     * @return void
     */
    private function writeRowsFromGenerator($writer, Generator $generator, ?callable $callback = null): void
    {
        foreach ($generator as $key => $item) {
            // Apply callback
            if ($callback) {
                $item = $callback($item);
            }

            // Prepare row (i.e remove non-string)
            $item = $this->transformRow($item);

            // Add header row.
            if ($this->withHeader && $key === 0) {
                $this->writeHeader($writer, $item);
            }
            // Write rows (one by one).
            $writer->addRow(Row::fromValues($item->toArray(), $this->rowsStyle));
        }
    }

    /**
     * @param $writer
     * @param Query $collection
     * @param callable|null $callback
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function writeRowsFromQuery($writer, \Yiisoft\Db\Query\Query $collection, ?callable $callback = null): void
    {
        $this->writeRowsFromGenerator($writer, $this->queryToGenerator($collection), $callback);
    }

    /**
     * @param Query $query
     * @return Generator
     * @throws Throwable
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function queryToGenerator(Query $query): Generator
    {
        foreach ($query->createCommand()->query() as $index => $row) {
            yield $row;
        }
    }

    /**
     * @param $writer
     * @param array $array
     * @param callable|null $callback
     * @return void
     */
    private function writeRowsFromArray($writer, array $array, ?callable $callback = null): void
    {
        $collection = collect($array);

        if (is_object($collection->first()) || is_array($collection->first())) {
            // provided $array was valid and could be converted to a collection
            $this->writeRowsFromCollection($writer, $collection, $callback);
        }
    }

    /**
     * @param $writer
     * @param $firstRow
     * @return void
     */
    private function writeHeader($writer, $firstRow): void
    {
        if ($firstRow === null) {
            return;
        }

        $keys = array_keys(is_array($firstRow) ? $firstRow : $firstRow->toArray());
        $writer->addRow(Row::fromValues($keys, $this->headerStyle));
    }


    /**
     * @param GenericCollection $collection
     * @return void
     */
    protected function prepareCollection(GenericCollection $collection): void
    {
        $needConversion = false;
        $firstRow = $collection->first();

        if (!$firstRow) {
            return;
        }

        if (!is_array($firstRow)) {
            return;
        }
        foreach ($firstRow as $item) {
            if (!is_string($item)) {
                $needConversion = true;
            }
        }
        if ($needConversion) {
            $this->transform($collection);
        }
    }

    /**
     * @param GenericCollection $collection
     * @return void
     */
    private function transform(GenericCollection $collection): void
    {
        $collection->transform(function ($data) {
            return $this->transformRow($data);
        });
    }

    /**
     * @param $data
     * @return GenericCollection
     */
    private function transformRow($data): GenericCollection
    {
        return collect($data)->map(function ($value) {
            return is_null($value) ? (string) $value : $value;
        })->filter(function ($value) {
            return is_string($value) || is_int($value) || is_float($value);
        });
    }

    /**
     * @param Style $style
     * @return $this
     */
    public function headerStyle(Style $style)
    {
        $this->headerStyle = $style;

        return $this;
    }

    /**
     * @param Style $style
     * @return $this
     */
    public function rowsStyle(Style $style)
    {
        $this->rowsStyle = $style;

        return $this;
    }
}
