<?php

namespace App\Service\SimpleFastExcel;

use DateTime;
use DateTimeImmutable;
use OpenSpout\Reader\SheetInterface;
use Yiisoft\Arrays\ArrayHelper;

use function App\Service\SimpleFastExcel\collect;

/**
 *
 */
trait ImportableTrait
{
    /**
     * @var int
     */
    private int $sheetNumber = 1;

    /**
     * @param $options
     * @return void
     */
    abstract protected function setOptions(&$options): void;

    /**
     * @param $path
     * @param callable|null $callback
     * @return SheetCollection
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException|\OpenSpout\Common\Exception\IOException
     */
    public function import($path, callable $callback = null): SheetCollection
    {
        $reader = $this->reader($path);

        $collection = [];
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            if ($this->sheetNumber != $key) {
                continue;
            }
            $collection = $this->importSheet($sheet, $callback);
        }
        $reader->close();

        return new SheetCollection($collection);
    }

    /**
     * @param $path
     * @param callable|null $callback
     * @return SheetCollection
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     */
    public function importSheets($path, callable $callback = null): SheetCollection
    {
        $reader = $this->reader($path);

        $collections = [];
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            if ($this->withSheetsNames) {
                $collections[$sheet->getName()] = $this->importSheet($sheet, $callback);
            } else {
                $collections[] = $this->importSheet($sheet, $callback);
            }
        }
        $reader->close();

        return new SheetCollection($collections);
    }

    /**
     * @param $path
     * @return \OpenSpout\Reader\CSV\Reader|\OpenSpout\Reader\ODS\Reader|\OpenSpout\Reader\ReaderInterface|\OpenSpout\Reader\XLSX\Reader
     * @throws \OpenSpout\Common\Exception\IOException
     */
    private function reader($path): \OpenSpout\Reader\CSV\Reader|\OpenSpout\Reader\ODS\Reader
    |\OpenSpout\Reader\XLSX\Reader|\OpenSpout\Reader\ReaderInterface
    {
        $reader = match (pathinfo((string)$path, PATHINFO_EXTENSION)) {
            'csv' => new \OpenSpout\Reader\CSV\Reader(
                $this->initAndReturnOptions(
                    new \OpenSpout\Reader\CSV\Options()
                )
            ),
            'ods' => new \OpenSpout\Reader\ODS\Reader(
                $this->initAndReturnOptions(
                    new \OpenSpout\Reader\ODS\Options()
                )
            ),
            default => new \OpenSpout\Reader\XLSX\Reader(
                $this->initAndReturnOptions(
                    new \OpenSpout\Reader\XLSX\Options()
                )
            ),
        };

        /* @var \OpenSpout\Reader\ReaderInterface $reader */
        $reader->open((string)$path);

        return $reader;
    }

    /**
     * @param array $array
     * @return array
     */
    private function transposeCollection(array $array)
    {
        $collection = [];

        foreach ($array as $row => $columns) {
            foreach ($columns as $column => $value) {
                ArrayHelper::setValueByPath(
                    $collection,
                    implode('.', [
                        $column,
                        $row,
                    ]),
                    $value
                );
            }
        }

        return $collection;
    }

    /**
     * @param SheetInterface $sheet
     * @param callable|null $callback
     * @return array
     */
    private function importSheet(SheetInterface $sheet, callable $callback = null)
    {
        $headers = [];
        $collection = [];
        $countHeader = 0;

        /** @var int $k */
        foreach ($sheet->getRowIterator() as $k => $rowAsObject) {
            $row = $rowAsObject?->toArray() ?? [];
            if ($k >= $this->startRow) {
                if ($this->withHeader) {
                    if ($k == $this->startRow) {
                        $headers = $this->toStrings($row);
                        $countHeader = count($headers);
                        continue;
                    }
                    if ($countHeader > $countRow = count($row)) {
                        $row = array_merge($row, array_fill(0, $countHeader - $countRow, null));
                    } elseif ($countHeader < $countRow = count($row)) {
                        $row = array_slice($row, 0, $countHeader);
                    }
                }
                if (is_callable($callback)) {
                    if ($result = (array) ($callback(empty($headers) ? $row : array_combine($headers, $row)))) {
                        $collection[] = $result;
                    }
                } else {
                    $collection[] = empty($headers) ? $row : array_combine($headers, $row);
                }
            }
        }

        if ($this->transpose) {
            return $this->transposeCollection($collection);
        }

        return $collection;
    }

    /**
     * @param $values
     * @return string[]
     *
     * @psalm-suppress MixedAssignment
     * @psalm-suppress MissingParamType
     * @psalm-suppress MixedInferredReturnType
     */
    private function toStrings($values): array
    {
        foreach ($values as &$value) {
            if ($value instanceof DateTime) {
                $value = $value->format('Y-m-d H:i:s');
            } elseif ($value instanceof DateTimeImmutable) {
                $value = $value->format('Y-m-d H:i:s');
            } else /*if ($value)*/ {
                $value = (string) $value;
            }
        }

        return $values;
    }
}
