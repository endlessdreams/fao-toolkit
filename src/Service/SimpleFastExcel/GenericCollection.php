<?php

namespace App\Service\SimpleFastExcel;

use ArrayAccess;
use Closure;
use Countable;
use IteratorAggregate;
use Yiisoft\Arrays\ArrayableInterface;
use Yiisoft\Arrays\ArrayAccessTrait;
use Yiisoft\Arrays\ArrayHelper;

/**
 *  @template TValue
 *  @template-implements IteratorAggregate<array-key, TValue>
 *  @template-implements ArrayAccess<array-key, TValue>
 */
class GenericCollection implements IteratorAggregate, ArrayAccess, Countable
{
    use ArrayAccessTrait;

    /**
     * @var int
     */
    private int $position = 0;

    /**
     * @param array $data
     */
    public function __construct(protected array $data = [])
    {
        reset($this->data);
        $isAssociative = ArrayHelper::isAssociative($this->data);
        foreach ($data as $k => $v) {
            if ($isAssociative) {
                $this->data[$k] = $v;
            } else {
                $this->data[] = $v;
            }
        }
    }

    /**
     * @return GenericCollection
     */
    public function keys(): GenericCollection
    {
        return new GenericCollection(array_keys($this->data));
    }

    /**
     * @return int|string|null
     */
    public function first(): int|string|null
    {
        return array_key_first($this->data);
    }

    /**
     * @return string|int|null
     */
    public function last(): string|int|null
    {
        return array_key_last($this->data);
    }

    /**
     * @param Closure $closure
     * @return $this
     */
    public function transform(Closure $closure): GenericCollection
    {
        foreach ($this->getIterator() as $key => $item) {
            $this->offsetSet($key, call_user_func($closure,$item));
        }
        return $this;
    }

    /**
     * @param Closure $closure
     * @return GenericCollection
     */
    public function map(Closure $closure): GenericCollection
    {
        return new GenericCollection(array_map($closure, $this->data));
    }

    /**
     * @param Closure $closure
     * @return GenericCollection
     */
    public function filter(Closure $closure): GenericCollection
    {
        return new GenericCollection(array_filter($this->data, $closure));
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->data;
    }

    /**
     * @param GenericCollection<array-key, TValue>|iterable<array-key, TValue> $compareWithItems
     * @return TValue|null
     */
    public function offsetGetByRecursiveKeys(GenericCollection|iterable $compareWithItems): mixed
    {
        $keyPaths = $this->recursiveKeysInternal($compareWithItems);
        foreach ($this->data as $item) {
            if (!empty($ret = $this->intersectAssocRecursiveInternally($this->data, $compareWithItems))) {
                $valid = true;
                foreach ($keyPaths as $keyPath) {
                    if (!($valid = ArrayHelper::getValueByPath($ret, $keyPath))) {
                        break;
                    }
                }
                if ($valid) {
                    return $item;
                }
            }
        }

        return null;
    }

    /**
     * @param array $keyPathsWithValues
     * @return mixed
     */
    public function offsetGetByKeypaths(array $keyPathsWithValues): mixed
    {
        foreach ($this->data as $item) {
            $valid = true;
            foreach ($keyPathsWithValues as $keyPath => $expectedValue) {
                if (!($valid = (ArrayHelper::getValueByPath($item, $keyPath) === $expectedValue))) {
                    break;
                }
            }
            if ($valid) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @param GenericCollection|ArrayableInterface|iterable<array-key, TValue>|mixed $items1
     * @param GenericCollection|ArrayableInterface|iterable<array-key, TValue>|mixed $items2
     *
     * @return array|bool|ArrayableInterface
     *
     * @psalm-return array<array-key, array<array-key, array<array-key, App\Service\SimpleFastExcel\GenericCollection|Yiisoft\Arrays\ArrayableInterface|bool|(iterable<array-key, TValue:App\Service\SimpleFastExcel\GenericCollection as mixed>)>|bool>|bool>|bool
     */
    protected function intersectAssocRecursiveInternally(
        GenericCollection|ArrayableInterface|iterable|string|int|bool $items1,
        GenericCollection|ArrayableInterface|iterable|string|int|bool $items2
    ): array|bool|ArrayableInterface {
        if (!is_array($items1) || !is_array($items2)) {
            return (string) $items1 == (string) $items2;
        }
        $commonKeys = array_intersect(array_keys($items1), array_keys($items2));
        $ret = [];
        foreach ($commonKeys as $key) {
            $ret[$key] = $this->intersectAssocRecursiveInternally($items1[$key], $items2[$key]);
        }
        return $ret;
    }

    /**
     * @return GenericCollection
     */
    public function recursiveKeys(): GenericCollection
    {
        return collect($this->data);
    }

    /**
     * @param array $array
     * @param array $path
     * @return GenericCollection|iterable
     *
     * @psalm-return GenericCollection<array-key, TValue:App\Service\SimpleFastExcel\GenericCollection as mixed>)|(iterable<array-key, TValue:App\Service\SimpleFastExcel\GenericCollection as mixed>
     */
    private function recursiveKeysInternal(array $array, array $path = []): array
    {
        $result = [];
        foreach ($array as $key => $val) {
            $currentPath = array_merge($path, array($key));
            if (is_array($val)) {
                $result = array_merge($result, $this->recursiveKeysInternal($val, $currentPath));
            } else {
                $result[] = implode('.', $currentPath);
            }
        }
        return $result;
    }
}
