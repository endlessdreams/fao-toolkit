<?php

namespace App\Service\SimpleFastExcel;

use Generator;
use OpenSpout\Writer\Common\AbstractOptions as AbstractWriterOptions;
use OpenSpout\Reader\CSV\Options as CsvReaderOptions;
use OpenSpout\Reader\ODS\Options as OdsReaderOptions;
use OpenSpout\Reader\XLSX\Options as XlsxReaderOptions;
use OpenSpout\Writer\CSV\Options as CsvWriterOptions;
use OpenSpout\Writer\ODS\Options as OdsWriterOptions;
use OpenSpout\Writer\XLSX\Options as XlsxWriterOptions;

/**
 * Class wrapper based on openSpout inspired of FastExcel.
 * Will probably migrate to use FastExcel.
 */
class SimpleFastExcel
{
    use ImportableTrait;
    use ExportableTrait;

    /**
     * @var SheetCollection|GenericCollection|Generator|array|null
     */
    protected SheetCollection|GenericCollection|Generator|array|null $data;

    /**
     * @var bool
     */
    private bool $withHeader = true;

    /**
     * @var bool
     */
    private bool $withSheetsNames = false;

    /**
     * @var int
     */
    private int $startRow = 1;

    /**
     * @var bool
     */
    private bool $transpose = false;

    /**
     * @var array<string,string|bool>
     */
    private array $csvConfiguration = [
        'delimiter' => ',',
        'enclosure' => '\"',
        'encoding'  => 'UTF-8',
        'bom'       => true,
    ];

    /**
     * @var callable|null
     */
    protected $optionsConfigurator = null;

    /**
     * FastExcel constructor.
     *
     * @param SheetCollection|GenericCollection|Generator|array|null $data
     */
    public function __construct(SheetCollection|GenericCollection|Generator|array|null $data = null)
    {
        $this->data = $data;
    }

    /**
     * Manually set data apart from the constructor.
     *
     * @param array|Generator|GenericCollection|SheetCollection $data
     *
     * @return SimpleFastExcel
     */
    public function data(GenericCollection|array|SheetCollection|Generator $data): SimpleFastExcel
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param int $sheetNumber
     *
     * @return $this
     */
    public function sheet(int $sheetNumber): SimpleFastExcel
    {
        $this->sheetNumber = $sheetNumber;

        return $this;
    }

    /**
     * @return $this
     */
    public function withoutHeaders(): SimpleFastExcel
    {
        $this->withHeader = false;

        return $this;
    }

    /**
     * @return $this
     */
    public function withSheetsNames(): SimpleFastExcel
    {
        $this->withSheetsNames = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function startRow(int $row): SimpleFastExcel
    {
        $this->startRow = $row;

        return $this;
    }

    /**
     * @return $this
     */
    public function transpose(): SimpleFastExcel
    {
        $this->transpose = true;

        return $this;
    }

    /**
     * @param string $delimiter
     * @param string $enclosure
     * @param string $encoding
     * @param bool $bom
     *
     * @return $this
     */
    public function configureCsv(
        string $delimiter = ',',
        string $enclosure = '"',
        string $encoding = 'UTF-8',
        bool $bom = false
    ): SimpleFastExcel {
        $this->csvConfiguration = compact('delimiter', 'enclosure', 'encoding', 'bom');

        return $this;
    }

    /**
     * Configure the underlying Spout Reader options using a callback.
     *
     * @param callable|null $callback
     *
     * @return $this
     */
    public function configureOptionsUsing(?callable $callback = null): SimpleFastExcel
    {
        $this->optionsConfigurator = $callback;

        return $this;
    }

    /**
     * @param CsvReaderOptions|CsvWriterOptions|OdsReaderOptions|OdsWriterOptions|XlsxReaderOptions|XlsxWriterOptions $options
     */
    protected function setOptions(&$options): void
    {
        if ($options instanceof CsvReaderOptions || $options instanceof CsvWriterOptions) {
            $options->FIELD_DELIMITER = is_string($this->csvConfiguration['delimiter'])
                ? $this->csvConfiguration['delimiter'] : '';
            $options->FIELD_ENCLOSURE = is_string($this->csvConfiguration['enclosure'])
                ? $this->csvConfiguration['enclosure'] : '';
            if ($options instanceof CsvReaderOptions) {
                $options->ENCODING = is_string($this->csvConfiguration['encoding'])
                    ? $this->csvConfiguration['encoding'] : '';
            }
            if ($options instanceof CsvWriterOptions) {
                $options->SHOULD_ADD_BOM = is_bool($this->csvConfiguration['bom']) && $this->csvConfiguration['bom'];
            }

            if (is_callable($this->optionsConfigurator)) {
                call_user_func(
                    $this->optionsConfigurator,
                    $options
                );
            }
        }
    }

    /**
     * @param CsvWriterOptions|OdsWriterOptions|XlsxWriterOptions|CsvReaderOptions|OdsReaderOptions|XlsxReaderOptions $options
     * @return CsvWriterOptions|OdsWriterOptions|XlsxWriterOptions|CsvReaderOptions|OdsReaderOptions|XlsxReaderOptions
     */
    protected function initAndReturnOptions(
        CsvWriterOptions|OdsWriterOptions|XlsxWriterOptions|CsvReaderOptions|OdsReaderOptions|XlsxReaderOptions $options
    ): CsvWriterOptions|OdsWriterOptions|XlsxWriterOptions|CsvReaderOptions|OdsReaderOptions|XlsxReaderOptions {
        $this->setOptions($options);
        return $options;
    }
}
