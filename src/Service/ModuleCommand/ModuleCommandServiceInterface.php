<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\ModuleCommand;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use Symfony\Component\Validator\Constraints\GroupSequence;

/**
 *
 */
interface ModuleCommandServiceInterface
{
    /**
     * @return string
     */
    public function getEntityClass(): string;

    /**
     * @return string
     */
    public function getResponseXmlRootName(): string;

    /**
     * @return GroupSequence|array<array-key, GroupSequence|string>|null|string
     */
    public function getParserGroups(): GroupSequence|array|null|string;

    /**
     * @return GroupSequence|array<array-key, GroupSequence|string>|null|string
     */
    public function getValidatorGroups(): GroupSequence|array|null|string;

    /**
     * @return string
     */
    public function getEntityXmlRootName(): string;

    /**
     * @return string
     */
    public function getEntityResponseClass(): string;

    /**
     * @return string
     */
    public function getFaoConfigInternalTableName(): string;

    /**
     * @return string
     */
    public function getUniqueKeyInInternalTable(): string;

    /**
     * @return string
     */
    public function getLogMsgBeginning(): string;

    /**
     * @return string
     */
    public function getFaoInstituteCodeQueryColumn(): string;

    /**
     * @param string $instituteCodeColumn
     * @return void
     */
    public function setFaoInstituteCodeQueryColumn(string $instituteCodeColumn): void;

    /**
     * @return string
     */
    public function getRowPk(): string;

    /**
     * @param DbMapService $dbMapService
     * @return void
     */
    public function init(DbMapService $dbMapService): void;

    /**
     * @param array $row
     * @param int $index
     * @param array $context
     * @return void
     */
    public function parseRow(array &$row, int $index, array &$context): void;

    /**
     * @param array $row
     * @param int $index
     * @param array $context
     * @return array
     */
    public function parseMapRow(array $row, int $index, array &$context): array;

    /**
     * @param array $row
     * @param int|null $index
     * @param array $context
     * @return mixed
     */
    public function parseAndDenormalize(array $row, int|null $index, array &$context): mixed;

    /**
     * @param string|null $xml
     * @return void
     */
    public function filterXml(?string &$xml): void;
}
