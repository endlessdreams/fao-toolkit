<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\ModuleCommand;

use ArrayAccess;
use Countable;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use IteratorAggregate;
use Yiisoft\Arrays\ArrayAccessTrait;
use Yiisoft\Arrays\ArrayHelper;

/**
 *
 * @template TKey of array-key
 * @template T
 * @implements IteratorAggregate<TKey,T>
 * @implements ArrayAccess<TKey|null,T>
 */
class ModuleCommandRegistryService implements IteratorAggregate, Countable, ArrayAccess
{
    use ArrayAccessTrait;

    /**
     * @var array<string,ModuleCommandServiceInterface>
     */
    private array $data = [];

    /**
     * @var string|null
     */
    private ?string $selectedModuleCommand = null;

    /**
     * @param string $moduleCommand
     * @return ModuleCommandServiceInterface|null
     */
    public function getService(string $moduleCommand): ?ModuleCommandServiceInterface
    {
        /** @var ModuleCommandServiceInterface $moduleCommandService */
        $moduleCommandService = ArrayHelper::getValue($this->data, $moduleCommand);
        return ($moduleCommandService instanceof ModuleCommandServiceInterface)
            ? $moduleCommandService
            : null;
    }

    /**
     * @param string $moduleCommand
     * @param ModuleCommandServiceInterface $service
     * @return $this
     */
    public function addService(
        string $moduleCommand,
        ModuleCommandServiceInterface $service
    ): ModuleCommandRegistryService {
        ArrayHelper::setValue($this->data, $moduleCommand, $service);
        return $this;
    }

    /**
     * @param string $moduleCommand
     * @return bool
     */
    public function hasService(string $moduleCommand): bool
    {
        return ArrayHelper::keyExists($this->data, $moduleCommand);
    }

    /**
     * @return string|null
     */
    public function getSelectedModuleCommand(): ?string
    {
        return $this->selectedModuleCommand;
    }

    /**
     * @param string $selectedModuleCommand
     * @return $this
     */
    public function setSelectedModuleCommand(string $selectedModuleCommand): ModuleCommandRegistryService
    {
        $this->selectedModuleCommand = $selectedModuleCommand;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasSelected(): bool
    {
        return isset($this->selectedModuleCommand)
            && !StringHelper::isEmpty($this->selectedModuleCommand)
            && $this->hasService($this->selectedModuleCommand);
    }

    /**
     * @return ModuleCommandServiceInterface|null
     */
    public function getSelectedService(): ?ModuleCommandServiceInterface
    {
        return $this->hasSelected() ? $this->getService($this->selectedModuleCommand ?? '') : null;
    }
}
