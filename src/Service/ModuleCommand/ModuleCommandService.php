<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\ModuleCommand;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use Symfony\Component\Validator\Constraints\GroupSequence;

/**
 * ModuleCommandService handles the different expected db and parsing actions during the process loop of a command call
 * with different parameters and different domains over a common interface.
 */
abstract class ModuleCommandService implements ModuleCommandServiceInterface
{
    /**
     * @var string
     */
    protected string $entityClass;
    /**
     * @var string
     */
    protected string $entityXmlRootName;
    /**
     * @var null|string|string[]|GroupSequence|GroupSequence[]
     */
    protected GroupSequence|array|null|string $parserGroups;
    /**
     * @var null|string|string[]|GroupSequence|GroupSequence[]
     */
    protected GroupSequence|array|null|string $validatorGroups;

    /**
     * @var string
     */
    protected string $entityResponseClass;
    /**
     * @var string
     */
    protected string $responseXmlRootName;

    /**
     * @var string
     */
    protected string $faoConfigInternalTableName;

    /**
     * @var string
     */
    protected string $uniqueKeyInInternalTable;

    /**
     * @var string
     */
    protected string $faoInstituteCodeQueryColumn;

    /**
     * @var string
     */
    protected string $dateRangeColumn;


    /**
     * @var DbMapService|null
     */
    protected ?DbMapService $dbMapService = null;
    /**
     * @var string
     */
    protected string $logMsgBeginning;

    /**
     * @var string
     */
    protected string $rowPk;

    /**
     *
     */
    abstract public function __construct();

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /**
     * @return string
     */
    public function getEntityXmlRootName(): string
    {
        return $this->entityXmlRootName;
    }

    /**
     * @return (GroupSequence|string)[]|GroupSequence|null|string
     *
     * @psalm-return GroupSequence|array<GroupSequence|string>|null|string
     */
    public function getParserGroups(): GroupSequence|array|null|string
    {
        return $this->parserGroups;
    }

    /**
     * @return (GroupSequence|string)[]|GroupSequence|null|string
     *
     * @psalm-return GroupSequence|array<GroupSequence|string>|null|string
     */
    public function getValidatorGroups(): GroupSequence|array|null|string
    {
        return $this->validatorGroups;
    }

    /**
     * @return string
     */
    public function getEntityResponseClass(): string
    {
        return $this->entityResponseClass;
    }

    /**
     * @return string
     */
    public function getResponseXmlRootName(): string
    {
        return $this->responseXmlRootName;
    }

    /**
     * @return string
     */
    public function getFaoConfigInternalTableName(): string
    {
        return $this->faoConfigInternalTableName;
    }

    /**
     * @return string
     */
    public function getUniqueKeyInInternalTable(): string
    {
        return $this->uniqueKeyInInternalTable;
    }

    /**
     * @return string
     */
    public function getLogMsgBeginning(): string
    {
        return $this->logMsgBeginning;
    }

    /**
     * @return string
     */
    public function getFaoInstituteCodeQueryColumn(): string
    {
        return $this->faoInstituteCodeQueryColumn;
    }

    /**
     * @return string
     */
    public function getRowPk(): string
    {
        return $this->rowPk;
    }

    /**
     * @param DbMapService $dbMapService
     * @return void
     */
    public function init(DbMapService $dbMapService): void
    {
        $this->dbMapService = $dbMapService;
        $this->dbMapService->setFaoInstituteCodeQueryColumn($this->faoInstituteCodeQueryColumn);
        $this->dbMapService->setUniqueKeyQueryColumn($this->uniqueKeyInInternalTable);
        $this->dbMapService->setDateRangeColumn($this->dateRangeColumn);
    }

    /**
     * @param array $row
     * @param int|null $index
     * @param array $context
     * @return void
     */
    abstract public function parseRow(array &$row, int|null $index, array &$context): void;

    /**
     * @param array $row
     * @param int $index
     * @param array $context
     * @return array
     */
    public function parseMapRow(array $row, int $index, array &$context): array
    {
        $this->parseRow($row, $index, $context);
        return $row;
    }

    /**
     * @param array $row
     * @param int|null $index
     * @param array $context
     * @return mixed
     */
    public function parseAndDenormalize(array $row, int|null $index, array &$context = []): mixed
    {
        // Get Context
        $context = $this->getParseAndDenormalizeContext($row, $context);

        // Convert
        $this->parseRow($row, $index, $context);

        return $this->denormalizeRowRowToObject($row);
    }

    /**
     * @param array $row
     * @param array $context
     * @return array
     */
    protected function getParseAndDenormalizeContext(array $row, array &$context = []): array
    {
        return $context;
    }

    /**
     * @param array $row
     * @return mixed
     */
    public function denormalizeRowRowToObject(array $row): mixed
    {
        return $this->dbMapService?->faoConfig->getParser()->denormalize(
            $row,
            $this->getEntityClass(),
            'xml',
            ['groups' => $this->getParserGroups()]
        );
    }

    /**
     * Override this method when it is necessary filter $xml.
     *
     * @param string|null $xml
     * @return void
     */
    public function filterXml(?string &$xml): void
    {
        // Do nothing.
    }
}
