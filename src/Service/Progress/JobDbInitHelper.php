<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service\Progress
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Progress;

use Throwable;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidArgumentException;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Db\Exception\NotSupportedException;

/**
 *
 */
class JobDbInitHelper
{
    /**
     *
     */
    public const JOB_STATUS_OPTIONS = [
        'initiated',
        'queued',
        'run',
        'completed',
        'failed',
    ];

    /**
     *
     */
    public const JOB_ERROR_TYPE_OPTIONS = [
        'Pre skipped violations',
        'Server errors',
    ];

    /**
     *
     */
    public const TABLE_JOB_STATUS = [
        'name' => '{{%job_status}}',
        'columns' => [
            'id' => 'pk',
            'status' => 'string(255) NOT NULL',
        ],
    ];

    /**
     *
     */
    public const TABLE_JOB_ERROR_TYPE = [
        'name' => '{{%job_error_type}}',
        'columns' => [
            'id' => 'pk',
            'type' => 'string(255) NOT NULL',
        ],
    ];

    /**
     *
     */
    public const TABLE_JOB = [
        'name' => '{{%job}}',
        'columns' => [
            'id' => 'pk',
            'run_as' => 'string(255) NOT NULL',
            'arguments' => 'string(255) NOT NULL',
            'status_id' => 'integer NOT NULL',
            'created_at' => 'datetime NOT NULL',
            'modified_at' => 'datetime NULL',
            'run_time' => 'integer NULL'
        ],
    ];

    /**
     *
     */
    public const TABLE_JOB_ERROR = [
        'name' => '{{%job_error}}',
        'columns' => [
            'id' => 'pk',
            'job_id' => 'integer NOT NULL',
            'type_id' => 'integer NOT NULL',
            'key' => 'string(255) NOT NULL',
            'group' => 'string(255) NOT NULL',
            'message' => 'string(255) NOT NULL',
            'created_at' => 'datetime NOT NULL',
        ],
    ];

    /**
     *
     */
    public const TABLE_JOB_COMPLETE_STATS = [
        'name' => '{{%job_complete_stats}}',
        'columns' => [
            'id' => 'pk',
            'job_id' => 'integer NOT NULL',
            'expected' => 'int NULL',
            'skipped' => 'int NOT NULL',
            'sent' => 'int NOT NULL',
            'rejected' => 'int NOT NULL',
            'accepted' => 'int NOT NULL',
            'created_at' => 'datetime NOT NULL',
        ],
    ];

    /**
     *
     */
    public const TABLE_DEFINITIONS = [
        self::TABLE_JOB_STATUS,
        self::TABLE_JOB_ERROR_TYPE,
        self::TABLE_JOB,
        self::TABLE_JOB_ERROR,
        self::TABLE_JOB_COMPLETE_STATS
    ];

    /**
     * @param ConnectionInterface $db
     * @return void
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws Throwable
     */
    public static function initDb(ConnectionInterface $db): void
    {
        if (!self::checkDbIsValid($db)) {
            self::dropTables($db);
            self::createDb($db);
        }
    }

    /**
     * @param ConnectionInterface $db
     * @return boolean
     * @throws NotSupportedException
     */
    private static function checkDbIsValid(ConnectionInterface $db): bool
    {
        foreach (self::TABLE_DEFINITIONS as $tableDef) {
            if (!self::checkIfTableExists($db, $tableDef)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param ConnectionInterface $db
     * @return void
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws Throwable
     */
    protected static function createDb(ConnectionInterface $db): void
    {
        self::createTable($db, self::TABLE_JOB_STATUS);
        self::batchInsertOptions(
            $db,
            self::TABLE_JOB_STATUS['name'],
            'status',
            self::JOB_STATUS_OPTIONS
        );
        self::createTable($db, self::TABLE_JOB_ERROR_TYPE);
        self::batchInsertOptions(
            $db,
            self::TABLE_JOB_ERROR_TYPE['name'],
            'type',
            self::JOB_ERROR_TYPE_OPTIONS
        );
        self::createTable($db, self::TABLE_JOB);
        self::createTable($db, self::TABLE_JOB_ERROR);
        self::createTable($db, self::TABLE_JOB_COMPLETE_STATS);
    }

    /**
     * @param ConnectionInterface $db
     * @return void
     * @throws \Exception
     */
    public static function dropTables(ConnectionInterface $db): void
    {
        self::dropTable($db, self::TABLE_JOB);
        self::dropTable($db, self::TABLE_JOB_STATUS);
        self::dropTable($db, self::TABLE_JOB_ERROR_TYPE);
        self::dropTable($db, self::TABLE_JOB_ERROR);
        self::dropTable($db, self::TABLE_JOB_COMPLETE_STATS);
    }

    /**
     * @param ConnectionInterface $db
     * @param array $tableDef
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws Throwable
     */
    private static function createTable(ConnectionInterface $db, array $tableDef): void
    {
        if (
            !(
                array_key_exists('name', $tableDef)
                && array_key_exists('columns', $tableDef)
            )
        ) {
            throw new \Exception('Table definition array was not properly defined.');
        }
        $tableName = is_string($tableDef['name'])
            ? $tableDef['name']
            : throw new \Exception('Table definition name was not properly defined.');
        $tableColumns = is_array($tableDef['columns'])
            ? $tableDef['columns']
            : throw new \Exception('Table definition columns was not properly defined.');
        $db->createCommand()->createTable(
            $tableName,
            $tableColumns,
        )->execute();
    }

    /**
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws InvalidArgumentException
     * @throws Exception
     */
    private static function batchInsertOptions(
        ConnectionInterface $db,
        string $tableName,
        string $columnName,
        array $options
    ): void {
        $db->createCommand()->batchInsert(
            $tableName,
            [$columnName],
            self::arrayify($options),
        )->execute();
    }

    /**
     * @throws Exception
     */
    private static function dropTable(ConnectionInterface $db, array $tableDef): void
    {
        if (
            !(
                array_key_exists('name', $tableDef)
            )
        ) {
            throw new Exception('Table definition array was not properly defined.');
        }
        $tableName = is_string($tableDef['name'])
            ? $tableDef['name']
            : throw new Exception('Table definition name was not properly defined.');
        try {
            $db->createCommand()->dropTable($tableName)->execute();
        } catch (InvalidConfigException | Exception | Throwable) {
        }
    }


    /**
     * @param ConnectionInterface $db
     * @param array<string,mixed> $tableDef
     * @return bool
     * @throws NotSupportedException
     */
    private static function checkIfTableExists(ConnectionInterface $db, array $tableDef): bool
    {
        $tableName = self::interpretTableName($db, $tableDef);
        if (!in_array($tableName, $db->getSchema()->getTableNames())) {
            return false;
        }
        $tableSchema = $db->getTableSchema((string)$tableDef['name'], true);
        $columnsNames = $tableSchema?->getColumnNames() ?? [];
        /** @var array<string,mixed> $tableColumnDef */
        $tableColumnDef = $tableDef['columns'];
        if (count(array_diff(array_keys($tableColumnDef), $columnsNames)) !== 0) {
            return false;
        }
        return true;
    }

    /**
     * @param ConnectionInterface $db
     * @param array $tableDef
     * @return string|null
     */
    private static function interpretTableName(ConnectionInterface $db, array $tableDef): ?string
    {
        $tableName = (string)$tableDef['name'];
        $re = '/{{%(\w+)}}/mu';
        if (($ret = preg_match($re, $tableName, $matches)) === false || $ret === 0) {
            return null;
        }
        $prefix = $db->getTablePrefix();
        return $prefix . $matches[1];
    }

    /**
     * @param array[]|string[]|array $arr
     * @return array<array-key,array<array-key, mixed>>
     */
    private static function arrayify(array $arr): array
    {
        $newArray = [];
        /** @var mixed $element */
        foreach ($arr as $element) {
            if (in_array(gettype($element), ['string','array'])) {
                $newArray[] = [$element];
            }
        }
        return count($newArray) === 0 ? [[]] : $newArray;
    }


    /**
     * @param string $status
     * @return int|false
     */
    public function getJobStatusId(string $status): int|false
    {
        $search = array_search($status, self::JOB_STATUS_OPTIONS);
        return $search !== false ? $search + 1 : false;
    }
}
