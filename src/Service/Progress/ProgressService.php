<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service\Progress
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Progress;

use EndlessDreams\FaoToolkit\Entity\Base\Entity;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credential;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\Glis\Get;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Entity\Smta\Smta;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use DateTime;
use Generator;
use Stringable;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\ProgressIndicator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidArgumentException;
use Yiisoft\Db\Exception\InvalidCallException;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Db\Exception\NotSupportedException;
use Yiisoft\Db\Query\Query;
use Yiisoft\Log\Logger;
use Yiisoft\Mailer\MailerInterface;

/**
 *
 * @phpstan-type ProgressServiceGeneratorReturnType array{xml: string|null, credentials: Credential|null,
 * uniqueKey: string, rowPkValue: int}
 */
class ProgressService
{
    /**
     * @var ProgressStatistics
     */
    private ProgressStatistics $statistics;

    /**
     * @var ProgressIndicator|ProgressBar|null
     */
    private ProgressIndicator|ProgressBar|null $progress = null;
    /**
     * @var Query
     */
    private Query $query;

    /**
     * @var array<string,mixed>
     */
    private array $currentViolations = [];
    /**
     * @var Provider|null
     */
    private ?Provider $apiProvider = null;
    /**
     * @var JobDbService
     */
    private JobDbService $jobDbService;
    /**
     * @var string
     */
    private string $logMsgBeginning;
    /**
     * @var string|null
     */
    private ?string $xml = null;
    /**
     * @var int|null
     */
    private ?int $index = null;
    /**
     * @var mixed|null
     */
    private mixed $object = null;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param DbMapService $dbMapService
     * @param ModuleCommandServiceInterface $moduleCommandService
     * @param Logger $logger
     * @param ConnectionInterface $db
     * @param Aliases $aliases
     * @param MailerInterface $mailer
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws Throwable
     */
    public function __construct(
        private readonly InputInterface $input,
        private readonly OutputInterface $output,
        private readonly DbMapService $dbMapService,
        private readonly ModuleCommandServiceInterface $moduleCommandService,
        private readonly Logger $logger,
        ConnectionInterface $db,
        Aliases $aliases,
        MailerInterface $mailer
    ) {
        if (($parser = $this->dbMapService->faoConfig->getParser()) === null) {
            throw new Exception('FaoConfig was not properly configured. No parser was created.');
        }

        $this->statistics = new ProgressStatistics();
        $this->jobDbService = (
            new JobDbService(
                $db,
                $input,
                $moduleCommandService,
                $this->statistics,
                $parser,
                $aliases,
                $mailer,
                $this->dbMapService->faoConfig->getEmailSender()
            )
        )->initDb();

        $this->logMsgBeginning = $this->moduleCommandService->getLogMsgBeginning();

        // Prepare row order
        $this->query = $this->dbMapService->getOrderQuery($this->input);

        $this->dbMapService->setRowQueryCommand(
            $this->dbMapService->generateRowQueryCommandByInput($this->input)
        );

        // Count expected amount, for Statistics and progress bar if verbose level === 0
        $cnt = $this->query->count();


        $this->statistics->noOfExpectedRows = is_int($cnt)
            ? $cnt
            : throw new \Exception('Could not estimate number of rows.');

        if ($cnt === 0) {
            throw new \Exception('No rows to process.');
        }


        if ($output->getVerbosity() === OutputInterface::VERBOSITY_NORMAL) {
            ProgressBar::setFormatDefinition(
                'custom',
                ' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s%  %memory:6s% -- '
                . 'Skipped: <comment>%skipped%</> | Sent: <fg=blue>%sent%</> | Rejected: <error>%rejected%</> '
                . '| Accepted: <info>%accepted%</>'
            );

            $this->progress = new ProgressBar($output, $this->statistics->noOfExpectedRows);
            $this->progress->setFormat('custom');
            $this->progress->setMessage((string)$this->statistics->noOfSkipped, 'skipped');
            $this->progress->setMessage((string)$this->statistics->noOfSent, 'sent');
            $this->progress->setMessage((string)$this->statistics->noOfRejected, 'rejected');
            $this->progress->setMessage((string)$this->statistics->noOfAccepted, 'accepted');
            $this->progress->start();
        }

        if ($output->getVerbosity() > OutputInterface::VERBOSITY_NORMAL) {
            $this->progress = new ProgressIndicator($output);
            $this->progress->start('Processing...');
        }
    }

    /**
     * @return void
     */
    public function increaseNoOfSkipped(): void
    {
        $this->statistics->noOfSkipped++;
        if ($this->progress instanceof ProgressBar) {
            $this->progress->setMessage((string)$this->statistics->noOfSkipped, 'skipped');
        }
    }

    /**
     * @return void
     */
    public function increaseNoOfSent(): void
    {
        $this->statistics->noOfSent++;
        if ($this->progress instanceof ProgressBar) {
            $this->progress->setMessage((string)$this->statistics->noOfSent, 'sent');
        }
    }

    /**
     * @return void
     */
    public function increaseNoOfRejected(): void
    {
        $this->statistics->noOfRejected++;
        if ($this->progress instanceof ProgressBar) {
            $this->progress->setMessage((string)$this->statistics->noOfRejected, 'rejected');
        }
    }

    /**
     * @return void
     */
    public function increaseNoOfAccepted(): void
    {
        $this->statistics->noOfAccepted++;
        if ($this->progress instanceof ProgressBar) {
            $this->progress->setMessage((string)$this->statistics->noOfAccepted, 'accepted');
        }
    }

    /**
     * @return OutputInterface
     */
    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    /**
     * @return Generator<int, ProgressServiceGeneratorReturnType, mixed, void>
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function getGenerator(): Generator
    {
        codecept_debug('GetGenerator - Begin');
        $context = [];
        //$this->progressStart();

        $expectedClass = $this->moduleCommandService->getEntityClass();

        /** @var array<string,mixed> $row */
        foreach ($this->dbMapService->getQueryGenerator($this->query) as $this->index => $row) {
            codecept_debug('Start parseAndDenormalize object');

            /**
             * @var Glis|Smta|Get|Entity $object
             */
            $object = $this->moduleCommandService->parseAndDenormalize($row, $this->index, $context);
            if (!($object instanceof Entity)) {
                yield;
            }
            $uniqueKey = (string)$row[$this->moduleCommandService->getUniqueKeyInInternalTable()];

            if ($this->onSkipInvalid($object, $uniqueKey)) {
                continue;
            }

            // Serialize, and post to server, and wait for response.
            codecept_debug('Start serialize xml');
            $this->xml = $this->dbMapService->faoConfig->getParser()?->serializeXml(
                $object,
                $this->moduleCommandService->getEntityXmlRootName()
            );

            codecept_debug('Start filter xml');
            $this->moduleCommandService->filterXml($this->xml);

            if ($this->onDryRun($uniqueKey)) {
                continue;
            }

            yield [
                'xml' => $this->xml,
                'credentials' => $this->getCredentials($row),
                'uniqueKey' => $uniqueKey,
                'rowPkValue' => (int)$row[$this->moduleCommandService->getRowPk()]
            ];
            codecept_debug('Past yield');

            $this->progressAdvance();
        }
        $this->progressFinish();
        codecept_debug('GetGenerator - End');
    }


    /**
     * @param mixed $object
     * @param string $uniqueKey
     * @return bool
     * @throws Exception
     * @throws InvalidCallException
     * @throws InvalidConfigException
     * @throws Throwable
     */
    protected function onSkipInvalid(mixed $object, string $uniqueKey): bool
    {
        if ($this->input->getOption('skip-invalid')) {
            // Get new validation error results
            if (
                count($this->currentViolations = $this->doValidateEntity($object)) > 0
            ) {
                $this->storeCurrentSkippedViolations($uniqueKey, $this->currentViolations);
                $this->object = $object;
                $this->logViolation();
                $this->object = null;
                //$this->log($uniqueKey,'contained violations',['violations' => $this->currentViolations]);
                if (
                    $this->output->isVerbose()
                ) {
                    $this->output->writeln(PHP_EOL . $this->prepareLastStatus($uniqueKey));
                    $this->progressSetMessage($uniqueKey);
                }
            } else {
                $this->currentViolations = [];
            }

            // Check input parameters if --skip-invalid is set.
            // If so,
            // Collect all errors, save all errors in a log file.
            // then skip to next row
            if (count($this->currentViolations) > 0) {
                $this->increaseNoOfSkipped();
                if (!$this->output->isQuiet()) {
                    $this->progress?->advance();
                }

                $this->logger->flush();
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $uniqueKey
     * @return bool
     */
    protected function onDryRun(string $uniqueKey): bool
    {
        if ($this->input->getOption('dry-run')) {
            switch ($this->output->getVerbosity()) {
                case OutputInterface::VERBOSITY_QUIET:
                    //Silent mode. Keep quiet...
                    break;
                case OutputInterface::VERBOSITY_NORMAL:
                    $this->progress?->advance();
                    break;
                case OutputInterface::VERBOSITY_VERBOSE: // verbose
                    $this->progress?->advance();
                    break;
                case OutputInterface::VERBOSITY_VERY_VERBOSE: // very verbose
                    $this->output->writeln(PHP_EOL . (string)$this->xml);
                    $this->progressSetMessage($uniqueKey);
                    $this->progress?->advance();
                    break;
                case OutputInterface::VERBOSITY_DEBUG: // Debug
                    $this->output->writeln(PHP_EOL . (string)$this->xml);
                    $this->progress?->advance();
                    break;
            }
            return true;
        }
        return false;
    }

    /**
     * @param mixed $object
     * @return array<string,array<int,string>>
     * @throws \Exception
     */
    private function doValidateEntity(mixed $object): array
    {
        return $this->dbMapService->faoConfig->getParser()
            ?->validateEntity(
                $object,
                $this->moduleCommandService->getValidatorGroups()
            ) ?? [];
    }

    /**
     * @param string $uniqueKey
     * @param array<string,array<int,string>> $currentViolations
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws InvalidCallException
     */
    private function storeCurrentSkippedViolations(string $uniqueKey, array $currentViolations): void
    {
        foreach ($currentViolations as $label => $currentViolation) {
            $this->jobDbService->createCurrentJobError(
                1,
                $uniqueKey,
                $label,
                implode(PHP_EOL, $currentViolation)
            );
        }
    }

    /**
     * @param string $uniqueKey
     * @param string $code
     * @param array<array-key, null|Stringable|scalar> $message
     * @return void
     * @throws Exception
     * @throws InvalidCallException
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function storeReceivedErrors(string $uniqueKey, string $code, array $message): void
    {
        $this->jobDbService->createCurrentJobError(2, $uniqueKey, $code, implode(PHP_EOL, $message));
    }

    /**
     * @param array $row
     * @return Credential|null
     */
    protected function getCredentials(array $row): ?Credential
    {
        $this->apiProvider ??= $this->dbMapService->faoConfig->getSelectedProvider();
        return $this->apiProvider?->getCredentials()->getSelected();
    }

    /**
     * @return void
     *
     * @psalm-suppress MixedAssignment
     */
    protected function logViolation(): void
    {
        $context = ['violations' => $this->currentViolations];
        if ($this->output->isVeryVerbose()) {
            if (isset($this->xml)) {
                $context['xml'] = $this->xml;
            } elseif (isset($this->object)) {
                $context['object'] = $this->object;
            }
        }
        if (isset($this->object) && is_object($this->object)) {
            $this->log(
                'warning',
                match (true) {
                    ($class = get_class($this->object)) === Smta::class
                    && method_exists($this->object, 'getSymbol') => (string)$this->object->getSymbol(),
                    $class === Glis::class
                    && method_exists($this->object, 'getSampleid') => (string)$this->object->getSampleid(),
                    default => 'N/A',
                },
                'contained violations',
                $context
            );
        }
    }

    /**
     * @param string $uniqueKey
     * @param array|null $context
     * @param int|null $verboseLevel
     * @return void
     */
    public function logSuccess(string $uniqueKey, ?array $context = null, ?int $verboseLevel = null): void
    {
        $context ??= [];
        $this->log('info', $uniqueKey, 'was accepted', $context, $verboseLevel);
    }

    /**
     * @param string $uniqueKey
     * @param string $code
     * @param array $message
     * @param int|null $verboseLevel
     * @return void
     */
    public function logError(string $uniqueKey, string $code, array $message, ?int $verboseLevel = null): void
    {
        $this->log(
            'error',
            $uniqueKey,
            'contained error(s)',
            ['code' => $code, 'error' => $message],
            $verboseLevel
        );
    }

    /**
     * @param string $level
     * @param string $uniqueKey
     * @param string $message
     * @param array|null $context
     * @param int|null $verboseLevel
     * @return void
     */
    public function log(
        string $level,
        string $uniqueKey,
        string $message,
        ?array $context = null,
        ?int $verboseLevel = OutputInterface::VERBOSITY_NORMAL
    ): void {
        if ($this->output->getVerbosity() >= $verboseLevel) {
            $context ??= [];
            $template = '[command][beginning uniqueKey message.]';
            $templateContext = [
                'timestamp' => (new DateTime())->format('Y-m-d H:i:s'),
                'command' => $this->input->getArgument('command'),
                'uniqueKey' => $uniqueKey,
                'beginning' => $this->logMsgBeginning,
                'message' => $message,
            ];
            $this->logger->log(
                $level,
                strtr($template, $templateContext),
                $context
            );
        }
    }

    /**
     * @return JobDbService
     */
    public function getJobDbService(): JobDbService
    {
        return $this->jobDbService;
    }


    public function setCurrentUniqueKey(string $uniqueKey): void
    {
        $this->statistics->currentUniqueKey = $uniqueKey;
    }

    /**
     * @return void
     */
    private function progressStart(): void
    {
        if ($this->progress instanceof ProgressIndicator) {
            $this->progress->start('Processing...');
        }
    }

    /**
     * @return void
     */
    private function progressAdvance(): void
    {
        $this->progress?->advance();
        $this->logger->flush();
    }

    /**
     * @return void
     */
    private function progressFinish(): void
    {
        if ($this->progress instanceof ProgressBar) {
            $this->progress->finish();
        } elseif ($this->progress instanceof ProgressIndicator) {
            $this->progress->finish(
                "Finished -- {$this->prepareLastStatus('Last status')} -- " .  $this->prepareIndicatorStatusMsg()
            );
        }

        $this->logger->flush(true);

        try {
            $this->jobDbService->jobCompleted();
        } catch (InvalidCallException | InvalidConfigException | Exception | Throwable $e) {
            $this->log('error', '---', 'JobDbService: ' . $e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @return string
     */
    private function prepareIndicatorStatusMsg(): string
    {
        $template = 'Processed: %s | Skipped: <comment>%s</> | Sent: <fg=blue>%s</> | Rejected: <error>%s</> '
            . '| Accepted: <info>%s</>';
        return sprintf(
            $template,
            (int)$this->index + 1,
            $this->statistics->noOfSkipped,
            $this->statistics->noOfSent,
            $this->statistics->noOfRejected,
            $this->statistics->noOfAccepted
        );
    }

    /**
     * @param string $keyValue
     * @return string
     */
    private function prepareLastStatus(string $keyValue): string
    {
        return $keyValue . ': ' . ((count($this->currentViolations) > 0) ? 'Invalid' : 'Passed');
    }

    /**
     * @param string $keyValue
     * @return void
     */
    private function progressSetMessage(string $keyValue): void
    {
        $status = ((count($this->currentViolations) > 0) ? 'Invalid' : 'Passed');
        $this->progress?->setMessage(
            "Last item... {$this->prepareLastStatus($keyValue)} -- "
            . $this->prepareIndicatorStatusMsg()
        );
    }
}
