<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service\Progress
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Progress;

use DateTimeImmutable;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use App\Service\SimpleFastExcel\SheetCollection;
use App\Service\SimpleFastExcel\SimpleFastExcel;
use EndlessDreams\FaoToolkit\Service\Trait\CommandOptionsAndArgumentsTrait;
use OpenSpout\Common\Exception\IOException;
use OpenSpout\Writer\Exception\InvalidSheetNameException;
use OpenSpout\Writer\Exception\WriterNotOpenedException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidArgumentException;
use Yiisoft\Db\Exception\InvalidCallException;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Db\Exception\NotSupportedException;
use Yiisoft\Db\Expression\Expression;
use Yiisoft\Db\Query\Query;
use Yiisoft\Mailer\File;
use Yiisoft\Mailer\MailerInterface;

/**
 *
 */
class JobDbService
{
    use CommandOptionsAndArgumentsTrait;

    /**
     * @var int|null
     */
    private ?int $currentJobId = null;
    /**
     * @var object[]|null
     */
    private ?array $outputExports = null;
    /**
     * @var string|null
     */
    private ?string $runAs = null;

    /**
     * @var string|null
     */
    private ?string $outputPath = null;
    /**
     * @var mixed|null
     */
    private ?string $emailTo = null;

    /**
     * @param ConnectionInterface $db
     * @param InputInterface $input
     * @param ModuleCommandServiceInterface $moduleCommandService
     * @param ProgressStatistics $statistics
     * @param ParserService $parserService
     * @param Aliases $aliases
     * @param MailerInterface $mailer
     * @param ?string $emailSender
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws Throwable
     */
    public function __construct(
        private readonly ConnectionInterface $db,
        private readonly InputInterface $input,
        private readonly ModuleCommandServiceInterface $moduleCommandService,
        private readonly ProgressStatistics $statistics,
        private readonly ParserService $parserService,
        private readonly Aliases $aliases,
        private readonly MailerInterface $mailer,
        private readonly ?string $emailSender = null
    ) {
        JobDbInitHelper::initDb($db);
        $this->outputPath =
            $this->input->hasOption('output-path')
                ? (string)($this->input->getOption('output-path') ?? '/tmp/jobstats.xlsx')
                : '/tmp/jobstats.xlsx';

        $oldPath = pathinfo($this->aliases->get($this->outputPath));
        if (
            array_key_exists('dirname', $oldPath)
            && array_key_exists('filename', $oldPath)
            && array_key_exists('extension', $oldPath)
        ) {
            $this->outputPath = $oldPath['dirname'] . DIRECTORY_SEPARATOR
                . $oldPath['filename'] . '_' . date('Y-m-d') . '.' . $oldPath['extension'];
        }

        $this->emailTo = $this->input->hasOption('email-to')
            ? $this->input->getOption('email-to')
            : null;
    }

    /**
     *
     * @throws Throwable
     */
    public function __destruct()
    {
        // Check if service was terminated abruptly,
        // by checking if status have reached 'completed' state.
        // If so, then set the job to status 'failed'
        $currentJobId = $this->getCurrentJobId();
        if (
            ($status = $this->getJobStatus($currentJobId)) !== 'completed'
            && $status !== 'queued'
        ) {
            $this->createJobError($currentJobId,2, $this->statistics->currentUniqueKey ?? 'Unknown', 'fatal', 'An unexpected fatal error has occurred.');
            $this->statistics->noOfRejected+=1;
            $this->setJobStatus($currentJobId, 'failed');
            $this->jobCompleted();
            $status = 'failed';
            codecept_debug(__METHOD__ . ':' . $currentJobId . ':' . $status);
            codecept_debug($this->statistics);
        }

        // Flush to files if it was requested...
        if (in_array($status, ['completed','failed'])) {
            $this->exportToFiles();
            if (
                !StringHelper::isEmpty((string)$this->emailTo)
                && !StringHelper::isEmpty($this->outputPath)
                && file_exists((string)$this->outputPath)
                && isset($this->emailSender)
            ) {
                $message = $this->mailer
                    ->compose()
                    ->withFrom($this->emailSender)
                    ->withTo((string)$this->emailTo)
                    ->withSubject('Fao-Toolkit command run - ' . $status)
                    ->withTextBody('Read the attached report')
                    ->withHtmlBody('<b>Read the attached report</b>')
                    ->withAttached(File::fromPath((string)$this->outputPath))
                ;
                $this->mailer->send($message);
            }
        }
    }


    /**
     * @return JobDbService
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws InvalidArgumentException
     * @throws NotSupportedException
     */
    public function initDb(): JobDbService
    {
        $this->getCurrentJobId();
        $this->initOutputExports();
        return $this;
    }

    /**
     * @return void
     */
    protected function initOutputExports(): void
    {
        if (!isset($this->currentJobId)) {
            return;
        }
        $this->outputExports = [
            'violations' => (object)[
                'filePath' => '/tmp/fao-toolkit-violations.csv',
                'aliasedColumns' => $this->getErrorHeadersByType(1),
                'query' => $this->getJobErrorsQuery($this->currentJobId, 1),
            ] ,
            'errors' => (object)[
                'filePath' => '/tmp/fao-toolkit-errors.csv',
                'aliasedColumns' => ($headers = $this->getErrorHeadersByType(2)),
                'query' => $this->getJobErrorsQuery($this->currentJobId, 2),
            ],
            'jobstats' => (object)[
                'filePath' => '/tmp/fao-toolkit-jobstats.csv',
                'aliasedColumns' => null,
                'query' => $this->getJobExtendedWithStatsQuery($this->currentJobId),
            ]
        ];
    }

    /**
     * @param string $runAs
     * @param string $arguments
     * @return int
     * @throws Throwable
     * @throws Exception
     * @throws InvalidCallException
     * @throws InvalidConfigException
     */
    public function createJob(string $runAs, string $arguments): int
    {
        $statusId = 1;

        $pk = $this->db->createCommand()->insertWithReturningPks(
            '{{%job}}',
            [
                'run_as' => $runAs,
                'arguments' => $arguments,
                'status_id' => $statusId,
                'created_at' => $this->getCurrentDateTime(),
                'modified_at' => null,
            ]
        );

        if ($pk === false) {
            throw new \Exception('Could not create a new job.');
        }

        return (int)$pk['id'];
    }

    /**
     * @return string
     */
    private function getCurrentDateTime(): string
    {
        $timestamp = time(); // get the current timestamp
        return date('Y-m-d H:i:s', $timestamp);
    }

    /**
     * @param int $jobId
     * @param int $statusId
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws InvalidArgumentException
     */
    public function changeJobStatus(int $jobId, int $statusId): void
    {
        $this->db->createCommand()->update(
            '{{%job}}',
            [
                'status_id' => $statusId,
                'modified_at' => $this->getCurrentDateTime()
            ],
            [
                'id' => $jobId
            ]
        )->execute();
    }

    /**
     * @param int $jobId
     * @param int $typeId
     * @param string $key
     * @param string $group
     * @param string $message
     * @return int|false
     * @throws Exception
     * @throws InvalidCallException
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function createJobError(int $jobId, int $typeId, string $key, string $group, string $message): int|false
    {
        $pk = $this->db->createCommand()->insertWithReturningPks(
            '{{%job_error}}',
            [
                'job_id' => $jobId,
                'type_id' => $typeId,
                'key' => $key,
                'group' => $group,
                'message' => $message,
                'created_at' => $this->getCurrentDateTime(),
            ]
        );

        return is_array($pk) ? (int)$pk['id'] : $pk;
    }

    /**
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws Exception
     * @throws InvalidCallException
     */
    public function createCurrentJobError(int $typeId, string $key, string $group, string $message): int|false
    {
        return $this->createJobError($this->getCurrentJobId(), $typeId, $key, $group, $message);
    }

    /**
     * @param int $jobId
     * @param int $typeId
     * @return Query
     */
    public function getJobErrorsQuery(int $jobId, int $typeId): Query
    {
        $orderedColumns = ['job_id','type_id','key','group','message'];
        return (new Query($this->db))
            ->select(['key','group','message'])
            ->from('{{%job_error}}')
            ->where(['job_id' => $jobId, 'type_id' => $typeId])
            ->groupBy($orderedColumns)
            ->orderBy(implode(', ', $orderedColumns));
    }

    /**
     * @param int $jobId
     * @return Query
     */
    public function getJobExtendedWithStatsQuery(int $jobId): Query
    {
        $orderedColumns = [
            'job_id' => 'j.id',
            'command' => new Expression("SUBSTR({{j}}.[[arguments]], CAST('1' AS INTEGER), "
                . "INSTR({{j}}.[[arguments]], ' '))"),
            'arguments' => new Expression("SUBSTR({{j}}.[[arguments]], INSTR({{j}}.[[arguments]], ' ')+1)"),
            'run_as' => 'run_as', 'status' => '{{s}}.[[status]]',
            'total' => '{{cs}}.[[expected]]', 'skipped' => '{{cs}}.[[skipped]]', 'sent' => '{{cs}}.[[sent]]',
            'rejected' => '{{cs}}.[[rejected]]', 'accepted' => '{{cs}}.[[accepted]]',
            'run_at' => new Expression("CASE WHEN {{cs}}.[[created_at]] IS NULL THEN NULL ELSE "
                . "datetime({{j}}.[[modified_at]], '-' || CAST(COALESCE({{j}}.[[run_time]],0) AS STRING) || ' second') "
                . "END"),
            'run_time' => new Expression("time({{j}}.[[run_time]], 'unixepoch')")
        ];
        return (new Query($this->db))
            ->select($orderedColumns)
            ->from(['j' => '{{%job}}'])
            ->leftJoin(['s' => '{{%job_status}}'], 's.id = status_id')
            ->leftJoin(['cs' => '{{%job_complete_stats}}'], 'cs.job_id = j.id')
            ->where(['j.id' => $jobId])
            ->groupBy(array_values($orderedColumns));
    }

    /**
     * @return int
     * @throws Exception
     * @throws InvalidCallException
     * @throws InvalidConfigException
     * @throws Throwable
     */
    private function getCurrentJobId(): int
    {
        /** @var string|null $opt */
        $opt = $this->input->getOption('run-as');
        $this->runAs ??= $opt;
        if (!(isset($this->runAs) && is_string($this->runAs))) {
            codecept_debug('Run As has to be set.');
            throw new \Exception('Run As has to be set.');
        }
        $this->currentJobId ??= $this->createJob(
            $this->runAs,
            $this->serializeCommandOptionsAndArguments($this->input)
        );
        return $this->currentJobId;
    }

    /**
     * @return void
     * @throws Exception
     * @throws InvalidCallException
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function jobCompleted(): void
    {
        $transaction = $this->db->beginTransaction();

        try {
            $fetchedStatus = (new Query($this->db))
                ->select('status_id')
                ->from('{{%job}}')
                ->where(['id' => $this->getCurrentJobId()])
                ->scalar();

            $this->db->createCommand()->insert(
                '{{%job_complete_stats}}',
                [
                    'job_id' => $this->getCurrentJobId(),
                    'expected' => $this->statistics->noOfExpectedRows,
                    'skipped' => $this->statistics->noOfSkipped,
                    'sent' => $this->statistics->noOfSent,
                    'rejected' => $this->statistics->noOfRejected,
                    'accepted' => $this->statistics->noOfAccepted,
                    'created_at' => ($stopTime = $this->getCurrentDateTime()),
                ]
            )->execute();

            $startTime =  new DateTimeImmutable((new Query($this->db))
                ->select(['result' =>'COALESCE(modified_at,created_at)'])
                ->from(['j' => '{{%job}}'])
                ->where(['id' => $this->getCurrentJobId()])
                ->scalar());

            // Statistics was successfully written, then update job status to 'completed'.
            $this->db->createCommand()
                ->update(
                    '{{%job}}',
                    [
                        // If it is already failed or initiated, keep it 'failed' otherwise 'completed'
                        'status_id' => $fetchedStatus == 5 ? 5 : 4,
                        'modified_at' => $stopTime,
                        'run_time' => ((new DateTimeImmutable($stopTime))->format('U'))
                            - (int)$startTime->format('U')
                        /*$startTime->diff(new DateTimeImmutable($stopTime))->s*/
                    ],
                    ['id' => $this->getCurrentJobId()]
                )->execute();
            $transaction->commit();
        } catch (InvalidCallException | InvalidConfigException | Exception | Throwable $e) {
            $transaction->rollBack();
            // to get a slightest info about the Exception
            $this->db->createCommand()
                ->update(
                    '{{%job}}',
                    [
                        'status_id' => 5, // = 'failed'
                        'modified_at' => $this->getCurrentDateTime()
                    ],
                    ['id' => $this->getCurrentJobId()]
                )->execute();

            throw $e;
        }
    }

    /**
     * @return void
     * @throws Exception
     * @throws InvalidCallException
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws InvalidArgumentException
     * @throws NotSupportedException
     */
    public function jobStartRunning(): void
    {
        $transaction = $this->db->beginTransaction();


        try {
            $this->db->createCommand()
                ->update(
                    '{{%job}}',
                    [
                        'status_id' => 3, // = 'run'
                        'modified_at' => $this->getCurrentDateTime()
                    ],
                    ['id' => $this->getCurrentJobId()]
                )->execute();
        } catch (InvalidCallException | InvalidConfigException | Exception | Throwable $e) {
            $transaction->rollBack();
            // to get a slightest info about the Exception
            $this->db->createCommand()
                ->update(
                    '{{%job}}',
                    [
                        'status_id' => 5, // = 'failed'
                        'modified_at' => $this->getCurrentDateTime()
                    ],
                    ['id' => $this->getCurrentJobId()]
                )->execute();
            throw $e;
        }
    }

    /**
     * @param int $type
     * @return string[]
     */
    protected function getErrorHeadersByType(int $type): array
    {
        return match ($type) {
            1 => [
                ucfirst($this->moduleCommandService->getUniqueKeyInInternalTable()) => 'key',
                'Field' => 'group',
                'Violations' => 'message'
            ],
            2 => [
                ucfirst($this->moduleCommandService->getUniqueKeyInInternalTable()) => 'key',
                'Code' => 'group',
                'Message' => 'message'
            ],
        };
    }

    /**
     * @param int $type
     * @param OutputInterface $output
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function outputCurrentJobErrorsAsTable(int $type, OutputInterface $output): void
    {
        $title = (new Query($this->db))
            ->select('type')
            ->from('{{%job_error_type}}')
            ->where(['id' => $type])->scalar();
        $headers = $this->getErrorHeadersByType($type);

        $rowErrorsQuery = (new Query($this->db))
            ->select(array_values($headers))
            ->from('{{%job_error}}')
            ->where(
                [
                    'job_id' => $this->currentJobId,
                    'type_id' => $type
                ]
            );


        if (($rowErrorsQuery->count()) > 0) {
            $table = new Table($output);
            $table->setHeaderTitle((string)$title);
            $table->setHeaders(array_keys($headers));

            $firstRow = true;
            /**
             * @var int|string $index
             * @var array $rowError
             */
            foreach ($rowErrorsQuery->createCommand()->query() as $index => $rowError) {
                if ($firstRow === true) {
                    $firstRow = false;
                } else {
                    $table->addRow(new TableSeparator());
                }
                $table->addRow([$rowError['key'], $rowError['group'], $rowError['message']]);
            }
            $table->render();
        }
    }

    /**
     * @param OutputInterface $output
     * @param int|null $jobId
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function outputJobstatsAsTable(OutputInterface $output, ?int $jobId = null): void
    {
        $jobId ??= $this->currentJobId;
        if (isset($jobId)) {
            $tableQuery = $this->getJobExtendedWithStatsQuery($jobId);
            $columns = $tableQuery->getSelect();
            unset($columns['arguments']);
            (new Table($output))
                ->setHeaderTitle('Job stats')
                ->setHeaders(array_keys($columns))
                ->setRows($tableQuery->select($columns)->all())
                ->render();
        }
    }

    /**
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws IOException
     * @throws InvalidSheetNameException
     * @throws WriterNotOpenedException
     */
    public function exportToFiles(): void
    {
        foreach ($this->outputExports ?? [] as $output) {
            $this->exportToFileInternal($output);
        }

        if (isset($this->outputPath)) {
            $exportData = [];
            foreach ($this->outputExports ?? [] as $sheet => $object) {
                /** @var Query $query */
                $query = $object->query;
                /** @var array<array-key,array<array-key,mixed>> $batchedRows */
                $aliasedQuery = (empty((array)$object->aliasedColumns))
                    ? $query
                    : $query->select((array)$object->aliasedColumns);
                $exportData[$sheet] = $aliasedQuery;
            }
            $sheets = new SheetCollection($exportData);
            (new SimpleFastExcel($sheets))->export($this->outputPath);
        }
    }

    /**
     * @param object $exportData
     * @return void
     */
    protected function exportToFileInternal(object $exportData): void
    {
        $myfile = fopen((string)$exportData->filePath, "w");

        /** @var Query $query */
        $query = $exportData->query;
        /** @var array<array-key,array<array-key,mixed>> $batchedRows */
        $aliasedQuery = (empty((array)$exportData->aliasedColumns))
            ? $query
            : $query->select((array)$exportData->aliasedColumns)->orderBy('id');

        /**
         * @var int|null|string $batch
         * @var mixed[] $rows
         */
        foreach ($aliasedQuery->batch() as $batch => $rows) {
            $csvRows = $this->parserService->getSerializer()->encode(
                $rows,
                'csv',
                [
                    'csv_delimiter' => "\t",
                    'csv_headers' => array_keys((array)$exportData->aliasedColumns),
                    'no_headers' => $batch > 0
                ]
            );
            fwrite($myfile, $csvRows);
        }
        fclose($myfile);
    }

    /**
     * @param int $jobId
     * @return string
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    private function getJobStatus(int $jobId): string
    {
        $query = new Query($this->db);

        /** @var string $jobStatus */
        $jobStatus = $query
            ->from(['j' => '{{%job}}'])
            ->leftJoin(['js' => '{{%job_status}}'], 'j.status_id = js.id')
            ->select(['status' => 'js.status'])
            ->where(['j.id' => $jobId])
            ->scalar()
        ;
        return $jobStatus;
    }

    /**
     * @throws InvalidConfigException
     * @throws InvalidArgumentException
     * @throws Exception
     * @throws Throwable
     */
    private function setJobStatus(int $jobId, string $status): void
    {
        $query = new Query($this->db);
        $statusId = (int)$query
            ->from(['js' => '{{%job_status}}'])
            ->select(['statusid' => 'js.id'])
            ->where(['js.status' => $status])
            ->scalar();
        $this->db->createCommand()
            ->update('{{%job}}', ['status_id' => $statusId], 'id = :id', [':id' => $jobId])
            ->execute();
    }
}
