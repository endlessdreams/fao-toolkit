<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Trait;

use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use Exception;
use Generator;
use Iterator;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\ConstraintViolation;

/**
 *
 */
trait ValidatorServiceTrait
{
    /**
     * @param Iterator $iterator
     * @param bool $removeEmptyKeys
     * @param array<array-key,string>|null $keysToUnset
     * @param bool $skipMapParsing
     * @return Generator
     */
    public function validateAllRows(
        Iterator $iterator,
        bool $removeEmptyKeys = false,
        array|null $keysToUnset = null,
        bool $skipMapParsing = false
    ): Generator {
        $class = Glis::class;

        codecept_debug($removeEmptyKeys);
        codecept_debug($keysToUnset);

        $context = [];

        /**
         * @var int $index
         * @var array<array-key,mixed> $row
         */
        foreach ($iterator as $index => $row) {
            if (isset($keysToUnset)) {
                foreach ($keysToUnset as $key) {
                    unset($row[$key]);
                }
            }

            if ($removeEmptyKeys) {
                array_walk($row, function (mixed $value, string $key) use (&$row) {
                    if (empty($row[$key])) {
                        unset($row[$key]);
                    }
                });
            }

            $moduleCommand = $this->moduleCommandRegister->getService('glis:register');

            // Prepare sources
            if (!$skipMapParsing) {
                $moduleCommand?->parseRow($row, $index, $context);
            }
            // Yield
            yield ['sampleid' => $row['sampleid'], 'error' => $this->validateRow($row, $class)];
        }
    }

    /**
     * @param array $row
     * @param string $class
     * @return array<string,array<int,string>>
     */
    public function validateRow(array $row, string $class): array
    {
        codecept_debug('      enter: ' . __METHOD__);
        try {
            return $this->validateEntity($this->denormalize($row, $class));
        } catch (Exception $e) {
            return ['fatal' => [$e->getMessage()]];
        }
    }

    /**
     * @param mixed $object
     * @param GroupSequence|array<array-key, GroupSequence|string>|null|string $groups
     * @return array<string,array<int,string>>
     * @throws Exception
     */
    public function validateEntity(mixed $object, string|GroupSequence|array $groups = null): array
    {
        if (isset($object)) {
            $resultedErrorMsgs = [];
            $errors = $this->validator->validate($object, null, $groups);

            if ($errors->count()) {
                foreach ($errors as $error) {
                    if ($error instanceof ConstraintViolation) {
                        if (!array_key_exists($error->getPropertyPath(), $resultedErrorMsgs)) {
                            $resultedErrorMsgs[$error->getPropertyPath()] = [];
                        }
                        if (
                            !in_array(
                                $msg = (string)$error->getMessage(),
                                $resultedErrorMsgs[$error->getPropertyPath()]
                            )
                        ) {
                            $resultedErrorMsgs[$error->getPropertyPath()][] = $msg;
                        }
                    }
                }
            }

            return $resultedErrorMsgs;
        }
        throw new Exception('ParserService was not able to denormalize array.');
    }
}
