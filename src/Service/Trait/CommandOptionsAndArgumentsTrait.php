<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Trait;

use Symfony\Component\Console\Input\InputInterface;

/**
 *
 */
trait CommandOptionsAndArgumentsTrait
{
    /**
     * @param InputInterface $input
     * @return string
     */
    protected function serializeCommandOptionsAndArguments(InputInterface $input): string
    {
        $options = [];
        foreach ($input->getOptions() as $option => $optionValue) {
            if (isset($optionValue)) {
                $eq = is_bool($optionValue) && $optionValue ? '' : '=';
                $val = is_bool($optionValue) && $optionValue ? '' : $optionValue;
                $val = is_array($val)
                    ? implode(',', array_map(fn($item): string => is_scalar($item) ? (string)$item : '', $val))
                    : (string)$val;

                if (!(is_bool($optionValue) && $optionValue === false)) {
                    $options[] = "--$option$eq$val";
                }
            }
        }
        /** @var array<array-key, null|scalar> $arguments */
        $arguments = $input->getArguments();
        unset($arguments['command']);
        $action = array_key_exists(
            'action',
            $arguments
        ) && isset($arguments['action']) ? ' ' . $arguments['action'] : '';
        unset($arguments['action']);
        return trim(((string)$input->getArgument('command')) . $action . ' '
            . implode(' ', $options) . ' '  . implode(' ', $arguments));
    }
}
