<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Helper;

use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use Exception;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Panther\Client;
use Symfony\Component\Serializer\Encoder\YamlEncoder;

/**
 * Class OrganizationFetchHelper
 * @package endlessdreams\faoToolkit\Wiews
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v1.0.0
 */
class OrganizationFetchHelper
{
    /**
     * @param string|string[] $wiewscode
     * @return array
     *
     * @throws DriverMissingException
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public static function lookupByWiewsCode(string|array $wiewscode): array
    {
        try {
            $wiewscodes = is_string($wiewscode) ? [$wiewscode] : $wiewscode;
            $organizations = [];

            /**
             * @see Client
             **/
            $httpClient = Client::createFirefoxClient(dirname(__DIR__,3).'/drivers/geckodriver');

            foreach ($wiewscodes as $wiewscode) {
                $response = $httpClient->request(
                    'GET',
                    "https://www.fao.org/wiews/data/organizations/en/?instcode={$wiewscode}#details"
                );

                // Wait for the "table" to appear, it may take some time because it's done in JS
                $httpClient->waitFor('#organizations');

                $organization = [];

                $response->filter('#organizations div#containerGPA div.GPA-row div.GPA-cell.txt')
                    ->each(
                        function (Crawler $node) use (&$organization) {
                            $attrGpaindex = $node->attr('data-gpaindex');
                            $spanList = $node->filter('span[data-gpaindex]');
                            $spanAttrGpaindex = $spanList->count() > 0 ? $spanList->first()->attr(
                                'data-gpaindex'
                            ) : null;

                            if (isset($attrGpaindex) && !StringHelper::isEmpty($attrGpaindex)) {
                                $organization[$attrGpaindex] = $node->text();
                            } elseif (isset($spanAttrGpaindex)) {
                                $a = $node->filter('a[data-gpaindex]')->first();
                                //$aAttrGpaindex = $a->attr('data-gpaindex');
                                $aAttrHref = $a->attr('href');
                                if (
                                    isset($aAttrHref)
                                    && $aAttrHref != '/wiews/data/organizations/en/?instcode=null#details'
                                ) {
                                    $organization[$spanAttrGpaindex] = [
                                        "@href" => 'https://www.fao.org' . $aAttrHref,
                                        "#" => $spanList->first()->text()
                                    ];
                                }
                            } else {
                                $parent = $node->ancestors()->first();
                                $elemLbl = $parent->filter('div.GPA-cell.lbl')->first();
                                if ($elemLbl->text() === 'Organization role categories') {
                                    $name = 'roles';
                                    $roles = [];
                                    $node->filter('li.GPAListElement')->each(
                                        function (Crawler $node) use (&$roles) {
                                            if (!in_array('hiddenflag', explode(' ', $node->attr('class') ?? ''))) {
                                                $roles[] = $node->text();
                                            }
                                        }
                                    );
                                    $organization[$name] = implode(',', $roles);
                                }
                            }
                        }
                    );
                $organizations[] = $organization;
            }

            return $organizations;
        } catch (Exception $e) {
            if (\Yiisoft\Strings\StringHelper::endsWith(trim($e->getMessage()), 'drivers/geckodriver: not found')) {
                throw new DriverMissingException();
            }
            throw $e;
        }
    }

    /**
     * @param array $data
     * @param string $format
     * @param ParserService $parser
     * @return bool|string
     */
    public static function encodeOrganizationData(array $data, string $format, ParserService $parser): bool|string
    {
        return match ($format) {
            'json_old' => json_encode(
                $data,
                JSON_PRETTY_PRINT
            ),
            'json' => $parser->getSerializer()->encode(
                $data,
                'json',
                [ 'json_encode_options' => JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ]
            ),
            'yaml' => $parser->getSerializer()->encode(
                [ 'organizations' => $data ],
                'yaml',
                [ YamlEncoder::YAML_INLINE => 4 ]
            ),
            'csv' => $parser->getSerializer()->encode(
                $data,
                'csv'
            ),
            'xml' => $parser->encodeXml(
                [ 'organization' => $data ],
                'organizations'
            ),
            'var_dump' => ArrayJsonHelper::toSerializedArray(
                $data,
                true,
                ArrayJsonHelper::VAR_EXPORT_SHORT_ARRAY | ArrayJsonHelper::VAR_EXPORT_NO_INDEX
            ) ?? false,
            default => print_r(
                $data,
                true
            ),
        };
    }

    /**
     * @return string[]
     */
    public static function encodeOptions(): array
    {
        return ['json_old','json','yaml','csv','xml','var_dump','print_r'];
    }
}
