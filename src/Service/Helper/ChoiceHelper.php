<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Helper;

use Yiisoft\Arrays\ArrayHelper;

/**
 *
 */
class ChoiceHelper
{
    /**
     * @var array<string,string>
     */
    private static array $cropOptions = [
        'Agropyron' => 'Agropyron',
        'Agrostis' => 'Agrostis',
        'Alopecurus' => 'Alopecurus',
        'Andropogon' => 'Andropogon',
        'Apple' => 'Apple',
        'Arrhenatherum' => 'Arrhenatherum',
        'Asparagus' => 'Asparagus',
        'Astragalus' => 'Astragalus',
        'Atriplex' => 'Atriplex',
        'BananaPlantain' => 'Banana/Plantain',
        'Barley' => 'Barley',
        'Beans' => 'Beans',
        'Beet' => 'Beet',
        'BrassicaComplex' => 'Brassica complex',
        'Breadfruit' => 'Breadfruit',
        'Canavalia' => 'Canavalia',
        'Carrot' => 'Carrot',
        'Cassava' => 'Cassava',
        'Chickpea' => 'Chickpea',
        'Citrus' => 'Citrus',
        'Coconut' => 'Coconut',
        'Coronilla' => 'Coronilla',
        'CowpeaEtAl' => 'Cowpea et al.',
        'Dactylis' => 'Dactylis',
        'Eggplant' => 'Eggplant',
        'FabaBeanVetch' => 'Faba Bean/Vetch',
        'Festuca' => 'Festuca',
        'FingerMillet' => 'Finger Millet',
        'Grasspea' => 'Grass pea',
        'Hedysarum' => 'Hedysarum',
        'Lathyrus' => 'Lathyrus',
        'Lentil' => 'Lentil',
        'Lespedeza' => 'Lespedeza',
        'Lolium' => 'Lolium',
        'Lotus' => 'Lotus',
        'Lupinus' => 'Lupinus',
        'Maize' => 'Maize',
        'Majoraroids' => 'Major aroids',
        'Medicago' => 'Medicago',
        'Melilotus' => 'Melilotus',
        'Oat' => 'Oat',
        'Onobrychis' => 'Onobrychis',
        'Ornithopus' => 'Ornithopus',
        'Pea' => 'Pea',
        'PearlMillet' => 'Pearl Millet',
        'Phalaris' => 'Phalaris',
        'Phleum' => 'Phleum',
        'PigeonPea' => 'Pigeon Pea',
        'Poa' => 'Poa',
        'Potato' => 'Potato',
        'Prosopis' => 'Prosopis',
        'Pueraria' => 'Pueraria',
        'Rice' => 'Rice',
        'Rye' => 'Rye',
        'Salsola' => 'Salsola',
        'Sorghum' => 'Sorghum',
        'Strawberry' => 'Strawberry',
        'Sunflower' => 'Sunflower',
        'SweetPotato' => 'Sweet Potato',
        'Trifolium' => 'Trifolium',
        'Tripsacum' => 'Tripsacum',
        'Triticale' => 'Triticale',
        'Wheat' => 'Wheat',
        'Yams' => 'Yams',
    ];

    /**
     * @var array<string,string>
     */
    private static array $actorTypeOptions = [
        'in' => 'individual',
        'or' => 'organization',
    ];

    /**
     * @var array<string,string>
     */
    private static array $extraCountryCodeOptions = [
        'XAL' => 'Alpine',
        'XAN' => 'Andes',
        'XAR' => 'Arabia',
        'XAA' => 'Australasia',
        'XAZ' => 'Australia & New Zealand',
        'XBE' => 'Benelux',
        'XBN' => 'Bengal',
        'XCH' => 'Cape Horn',
        'XCR' => 'Caribbean',
        'XCP' => 'Caspian',
        'XCF' => 'Central Africa',
        'XCA' => 'Central America',
        'CSK' => 'Czechoslovakia',
        'XEF' => 'East Africa',
        'XEE' => 'Eastern Europe',
        'XFE' => 'Far East',
        'DDR' => 'German Democratic Rep.',
        'BRD' => 'Germany, Federal Rep. of',
        'XHM' => 'Himalaya',
        'XIB' => 'Iberia',
        'XMD' => 'Mediterranean',
        'XME' => 'Middle East',
        'NHB' => 'New Hebrides',
        'XNF' => 'North Africa',
        'XNA' => 'North America',
        'XNE' => 'North-East Asia',
        'XPO' => 'Pacific Ocean',
        'PCZ' => 'Panama Canal Zone',
        'XSH' => 'Sahara',
        'XSC' => 'Scandinavia',
        'XSJ' => 'Sea of Japan',
        'SCG' => 'Serbia and Montenegro',
        'XSA' => 'South America',
        'XAS' => 'South East Asia',
        'XSF' => 'Southern Africa',
        'XSE' => 'Southern Europe',
        'SUN' => 'Union of Soviet Soc. Rep.',
        'HVO' => 'Upper Volta',
        'XWF' => 'West Africa',
        'XWE' => 'Western Europe',
        'YMD' => 'Yemen, Democratic',
        'YUG' => 'Yugoslavia',
        'XAB' => 'Africa Rice',
        'XAC' => 'Bioversity International',
        'XAD' => 'CIAT',
        'XAE' => 'CIMMYT',
        'XAF' => 'CIP',
        'XAG' => 'ICARDA',
        'XAH' => 'ICRAF',
        'XAI' => 'ICRISAT',
        'XAJ' => 'IITA',
        'XAK' => 'ILRI',
        'XAM' => 'IRRI',
    ];

    /**
     * @var array<string,string>
     */
    private static array $glisMethodOptions = [
        'acqu' => 'Acquisition',
        'ihcp' => 'In-house copy',
        'ihva' => 'In-house variant',
        'nodi' => 'Novel distinct PGRFA',
        'obna' => 'Observation - Natural',
        'obin' => 'Inherited',
    ];

    /**
     * @var array<array-key,string>
     */
    private static array $glisTkwOptions = [
        '1' => 'Passport data',
        '1.1' => 'Genealogy',
        '1.2' => 'Collection documents',
        '2' => 'Characterization',
        '3' => 'Evaluation',
        '3.1' => 'Chemical analysis',
        '3.2' => 'Abiotic stress',
        '3.3' => 'Biotic stress',
        '3.4' => 'Biochemical markers',
        '3.5' => 'Molecular markers',
        '3.6' => 'Cytological characters',
        '3.7' => 'Genomics',
        '3.8' => 'Phenomics',
        '4' => 'Environments',
        '5' => 'Multimedia',
    ];

    /**
     * @var array<array-key,string>
     */
    private static array $glisSampstatOptions = [
        '100' => 'Wild',
        '110' => 'Natural',
        '120' => 'Semi-natural/wild',
        '130' => 'Semi-natural/sown',
        '200' => 'Weedy',
        '300' => 'Traditional cultivar/landrace',
        '400' => 'Breeding/research material',
        '410' => 'Breeder’s line',
        '411' => 'Synthetic population',
        '412' => 'Hybrid',
        '413' => 'Founder stock/base population',
        '414' => 'Inbred line (parent of hybrid cultivar)',
        '415' => 'Segregating population',
        '416' => 'Clonal selection',
        '420' => 'Genetic stock',
        '421' => 'Mutant',
        '422' => 'Cytogenetic stocks',
        '423' => 'Other genetic stocks',
        '500' => 'Advanced or improved cultivar',
        '600' => 'GMO',
        '999' => 'Other',
    ];

    /**
     * @var array<string,string>
     */
    private static array $glisITypeOptions = [
        'ark' => 'ARK',
        'genesysuuid' => 'Genesys UUID',
        'gmsid' => 'GMS ID',
        'lsid' => 'LSID',
        'purl' => 'PURL',
        'sgsvid' => 'Global Seed Vault ID',
        'n/a' => 'Other',
    ];

    /**
     * @var array<array-key,string>
     */
    private static array $glisMlsStatusOptions = [
        '0' => 'Not available under the MLS',
        '1' => 'Available under the MLS',
        '11' => 'The PGRFA is of a crop listed in Annex I and is under the management '
            . 'and control of a Contracting Party to the Treaty and in the public domain',
        '12' => 'The sample is in an international collection under Article 15 of the Treaty',
        '13' => 'The holder received the PGRFA with an SMTA',
        '14' => 'The holder has voluntarily placed the PGRFA in the MLS',
        '15' => 'The PGRFA is derived from, and distinct from, material previously received from the MLS, '
            . 'is still under development and not yet ready for commercialization, and may be made available '
            . 'at the discretion of the developer, with an SMTA',
    ];

    /**
     * @var array<array-key,string>
     */
    private static array $glisSourceOptions = [
        '10' => 'Wild habitat',
        '11' => 'Forest or woodland',
        '12' => 'Shrubland',
        '13' => 'Grassland',
        '14' => 'Desert or tundra',
        '15' => 'Acquatic habitat',
        '20' => 'Farm or cultivated habitat',
        '21' => 'Field',
        '22' => 'Orchard',
        '23' => 'Backyard, kitchen or home garden (urban, peri-urban or rural)',
        '24' => 'Fallow land',
        '25' => 'Pasture',
        '26' => 'Farm store',
        '27' => 'Threshing floor',
        '28' => 'Park',
        '30' => 'Market or shop',
        '40' => 'Institute, Experimental station, Research organization, Genebank',
        '50' => 'Seed company',
        '60' => 'Weedy, disturbed or ruderal habitat',
        '61' => 'Roadside',
        '62' => 'Field margin',
        '99' => 'Other',
    ];

    /**
     * @var array<string,string>
     */
    private static array $smtaTypeOptions = [
        'cw' => 'click-wrap',
        'sw' => 'shrink-wrap',
        'si' => 'signed',
    ];

    /**
     * @var array<string,string>
     */
    private static array $smtaLanguageOptions = [
        'ar' => 'arabic',
        'en' => 'english',
        'es' => 'spanish',
        'fr' => 'french',
        'ru' => 'russian',
        'zh' => 'chinese'
    ];

    /**
     * @var array<string,string>
     */
    private static array $smtaLocationOptions = [
        'p' => 'with the Provider',
        's' => 'with the Secretariat',
        'o' => 'elsewhere',
    ];

    /**
     * @var array|string[]
     */
    private static array $yesOrNoOptions = [
        'y' => 'yes',
        'n' => 'no',
    ];

    /**
     * @return array<string,string>
     */
    public static function getCropOptions(): array
    {
        return self::$cropOptions;
    }

    /**
     * @return string[]
     */
    public static function getCropChoices(): array
    {
        return array_map(callback: fn($key): string => $key, array: array_keys(self::$cropOptions));
    }

    /**
     * @param string $crop
     * @return mixed
     */
    public static function getCropChoiceDescription(string $crop): mixed
    {
        return ArrayHelper::getValue(self::$cropOptions, $crop);
    }

    /**
     * @return array|string[]
     */
    public static function getActorTypeOptions(): array
    {
        return self::$actorTypeOptions;
    }

    /**
     * @return string[]
     */
    public static function getActorTypeChoices(): array
    {
        return array_keys(self::$actorTypeOptions);
    }

    /**
     * @param string $type
     * @return mixed
     */
    public static function getActorTypeChoiceDescription(string $type): mixed
    {
        return ArrayHelper::getValue(self::$actorTypeOptions, $type);
    }

    /**
     * @return string[]
     */
    public static function getExtraCountryCodeOptions(): array
    {
        return self::$extraCountryCodeOptions;
    }

    /**
     * @return string[]
     */
    public static function getExtraCountryCodeChoices(): array
    {
        return array_keys(self::$extraCountryCodeOptions);
    }

    /**
     * @param string $country
     * @return mixed
     */
    public static function getExtraCountryCodeChoiceDescription(string $country): mixed
    {
        return ArrayHelper::getValue(self::$extraCountryCodeOptions, $country);
    }

    /**
     * @return string[]
     */
    public static function getGlisMethodOptions(): array
    {
        return self::$glisMethodOptions;
    }

    /**
     * @return string[]
     */
    public static function getGlisMethodChoices(): array
    {
        return array_keys(self::$glisMethodOptions);
    }

    /**
     * @param string $method
     * @return mixed
     */
    public static function getGlisMethodChoiceDescription(string $method): mixed
    {
        return ArrayHelper::getValue(self::$glisMethodOptions, $method);
    }

    /**
     * @return string[]
     */
    public static function getGlisTkwOptions(): array
    {
        return self::$glisTkwOptions;
    }

    /**
     * @return string[]
     */
    public static function getGlisTkwChoices(): array
    {
        return array_map(callback: fn($key): string => (string)$key, array: array_keys(self::$glisTkwOptions));
    }

    /**
     * @param string $tkw
     * @return mixed
     */
    public static function getGlisTkwChoiceDescription(string $tkw): mixed
    {
        return ArrayHelper::getValue(self::$glisTkwOptions, $tkw);
    }

    /**
     * @return string[]
     */
    public static function getGlisSampstatOptions(): array
    {
        return self::$glisSampstatOptions;
    }

    /**
     * @return string[]
     */
    public static function getGlisSampstatChoices(): array
    {
        return array_map(fn(mixed $item): string => (string)$item, array_keys(self::$glisSampstatOptions));
    }

    /**
     * @return array
     */
    public static function getGlisSampstatOptionalChoices(): array
    {
        return array_merge(self::getGlisSampstatChoices(), ['', null]);
    }


    /**
     * @param string $sampstat
     * @return mixed
     */
    public static function getGlisSampstatChoiceDescription(string $sampstat): mixed
    {
        return ArrayHelper::getValue(self::$glisSampstatOptions, $sampstat);
    }

    /**
     * @return string[]
     */
    public static function getGlisITypeOptions(): array
    {
        return self::$glisITypeOptions;
    }

    /**
     * @return string[]
     */
    public static function getGlisITypeChoices(): array
    {
        return array_keys(self::$glisITypeOptions);
    }

    /**
     * @return string[]
     */
    public static function getGlisITypeOptionalChoices(): array
    {
        return array_map(
            fn(mixed $item): string => (string)$item,
            array_merge(array_keys(self::$glisITypeOptions), ['', null])
        );
    }

    /**
     * @param string $itype
     * @return mixed
     */
    public static function getGlisITypeChoiceDescription(string $itype): mixed
    {
        return ArrayHelper::getValue(self::$glisITypeOptions, $itype);
    }

    /**
     * @return string[]
     */
    public static function getGlisMlsStatusOptions(): array
    {
        return self::$glisMlsStatusOptions;
    }

    /**
     * @return string[]
     */
    public static function getGlisMlsStatusChoices(): array
    {
        return array_map(callback: fn($key): string => (string)$key, array: array_keys(self::$glisMlsStatusOptions));
    }

    /**
     * @param string $mls
     * @return mixed
     */
    public static function getGlisMlsStatusChoiceDescription(string $mls): mixed
    {
        return ArrayHelper::getValue(self::$glisMlsStatusOptions, $mls);
    }

    /**
     * @return string[]
     */
    public static function getGlisSourceOptions(): array
    {
        return self::$glisSourceOptions;
    }

    /**
     * @return string[]
     */
    public static function getGlisSourceChoices(): array
    {
        return array_map(callback: fn($key): string => (string)$key, array: array_keys(self::$glisSourceOptions));
    }

    /**
     * @param string $source
     * @return mixed
     */
    public static function getGlisSourceChoiceDescription(string $source): mixed
    {
        return ArrayHelper::getValue(self::$glisSourceOptions, $source);
    }

    /**
     * @return string[]
     */
    public static function getSmtaTypeOptions(): array
    {
        return self::$smtaTypeOptions;
    }

    /**
     * @return string[]
     */
    public static function getSmtaTypeChoices(): array
    {
        return array_keys(self::$smtaTypeOptions);
    }

    /**
     * @param string $type
     * @return mixed
     */
    public static function getSmtaTypeChoiceDescription(string $type): mixed
    {
        return ArrayHelper::getValue(self::$smtaTypeOptions, $type);
    }

    /**
     * @return string[]
     */
    public static function getSmtaLanguageOptions(): array
    {
        return self::$smtaLanguageOptions;
    }

    /**
     * @return string[]
     */
    public static function getSmtaLanguageChoices(): array
    {
        return array_keys(self::$smtaLanguageOptions);
    }

    /**
     * @param string $language
     * @return mixed
     */
    public static function getSmtaLanguageChoiceDescription(string $language): mixed
    {
        return ArrayHelper::getValue(self::$smtaLanguageOptions, $language);
    }

    /**
     * @return string[]
     */
    public static function getSmtaLocationOptions(): array
    {
        return self::$smtaLocationOptions;
    }

    /**
     * @return string[]
     */
    public static function getSmtaLocationChoices(): array
    {
        return array_keys(self::$smtaLocationOptions);
    }

    /**
     * @param string $location
     * @return mixed
     */
    public static function getSmtaLocationChoiceDescription(string $location): mixed
    {
        return ArrayHelper::getValue(self::$smtaLocationOptions, $location);
    }

    /**
     * @return array|string[]
     */
    public static function getYesOrNoOptions(): array
    {
        return self::$yesOrNoOptions;
    }

    /**
     * @return string[]
     *
     * @psalm-suppress MixedReturnTypeCoercion
     */
    public static function getYesOrNoChoices(): array
    {
        return array_keys(self::$yesOrNoOptions);
    }

    /**
     * @param string $option
     * @return mixed
     */
    public static function getYesOrNoChoiceDescription(string $option): mixed
    {
        return ArrayHelper::getValue(self::$yesOrNoOptions, $option);
    }
}
