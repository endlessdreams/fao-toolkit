<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Helper;

use Exception;
use Yiisoft\Arrays\ArrayHelper;

/**
 *
 */
final class ArrayJsonHelper
{
    public const VAR_EXPORT_SHORT_ARRAY = 1;
    public const VAR_EXPORT_NO_INDEX = 2;
    public const VAR_EXPORT_COMPACT = 4;

    /**
     * @param array $row
     * @param non-empty-string $key_sep
     * @return void
     * @throws Exception
     *
     * @psalm-suppress MixedAssignment
     * @psalm-suppress MixedArrayAssignment
     * @psalm-suppress MixedArgument
     */
    public static function createSubArrayIfNeeded(array &$row, string $key_sep = '.'): void
    {
        foreach ($row as $key => $value) {
            if (is_string($key) && str_contains($key, $key_sep)) {
                $new_key_arr = explode($key_sep, $key);
                $new_key_head = array_shift($new_key_arr);
                $new_key_tails = implode('.', $new_key_arr);
                if (!array_key_exists($new_key_head, $row)) {
                    $row[$new_key_head] = [];
                }
                $row[$new_key_head][$new_key_tails] = $value;
                self::createSubArrayIfNeeded($row[$new_key_head]);
                unset($row[$key]);
            }
        }
    }

    /**
     * @param array $row
     * @param string[] $keysToTest
     * @return void
     *
     * @psalm-suppress MixedAssignment
     */
    public static function parseJsonKeyValues(array &$row, array $keysToTest = []): void
    {
        foreach ($keysToTest as $jsonfield) {
            if (!array_key_exists($jsonfield, $row)) {
                continue;
            }
            /** @var array<string,mixed> $assocArr */
            if (
                is_array($row[$jsonfield])
                && ArrayHelper::isAssociative($row[$jsonfield])
                && count($assocArr = $row[$jsonfield]) === 1
                && array_key_exists($firstChildKey = self::getFirstElementInArray($assocArr), $assocArr)
                && is_array($firstChild = $assocArr[$firstChildKey])
                && ArrayHelper::isIndexed($firstChild)
            ) {
                // Is already populated by a sub table
                continue;
            }
            if (self::isJson(($json = $row[$jsonfield]))) {
                // Remove all shortcut keys.
                array_walk(
                    $row,
                    function (mixed $value, string $key) use (&$row, $jsonfield) {
                        if (str_starts_with($key, "$jsonfield.")) {
                            unset($row["$key"]);
                        }
                    }
                );

                /** @var string $json */
                $row["$jsonfield"] = json_decode($json, true);
            } else {
                unset($row[$jsonfield]);
            }
        }
    }


    /**
     * @param non-empty-array<array-key,mixed> $assocArr
     * @return string|int
     */
    protected static function getFirstElementInArray(array $assocArr): string|int
    {
        $keys = array_keys($assocArr);
        return $keys[0];
    }

    /**
     * @param mixed $data
     * @return bool
     */
    public static function isJson(mixed $data): bool
    {
        if (!empty($data)) {
            return is_string($data) &&
            is_array(json_decode($data, true));
        }
        return false;
    }

    /**
     * @param array $arr
     * @param bool|null $return
     * @param int $flags
     * @return string|void
     */
    public static function toSerializedArray(array $arr, ?bool $return = false, int $flags = 0)
    {
        $output = var_export($arr, true);
        $arrayBegin = 'array \(';
        $arrayEnd = ')';
        if (($flags & self::VAR_EXPORT_SHORT_ARRAY) === self::VAR_EXPORT_SHORT_ARRAY) {
            $patterns = [
                "/array \(/" => '[',
                "/^([ ]*)\)(,?)$/m" => '$1]$2',
            ];
            $output = preg_replace(array_keys($patterns), array_values($patterns), $output);
            $arrayBegin = '\[';
            $arrayEnd = ']';
        }
        if (($flags & self::VAR_EXPORT_NO_INDEX) === self::VAR_EXPORT_NO_INDEX) {
            //$noIndexPattern = '/^(\s+\d+\s+=>\s+?\n)(?=\s*'.$arrayBegin.')/m';
            $noIndexPattern = '/^((\s+?)\d+\s+=>\s+)/m';
            $output = preg_replace($noIndexPattern, '$2', $output);
        }
        if (($flags & self::VAR_EXPORT_COMPACT) === self::VAR_EXPORT_COMPACT) {
            $compactPattern = '/(([\[,])\s*\n)/m';
            $output = preg_replace($compactPattern, '$2', $output);
            $compactPattern = '/^\s+/m';
            $output = preg_replace($compactPattern, ' ', $output);
        }
        if ($return ?? false) {
            return $output;
        } else {
            echo $output;
        }
    }
}
