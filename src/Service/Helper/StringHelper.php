<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Service
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Service\Helper;

use finfo;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Strings\StringHelper as YiiStringHelper;

/**
 *
 */
class StringHelper
{
    /**
     * @param string $str
     * @return string
     */
    public static function pascalize(string $str): string
    {
        return str_replace(
            ' ',
            '',
            YiiStringHelper::uppercaseFirstCharacterInEachWord(
                preg_replace('/[^\pL\pN]+/u', ' ', $str)
            )
        );
    }

    /**
     * @param string $str
     * @return string
     */
    public static function camelize(string $str): string
    {
        $pascal = self::pascalize($str);
        return YiiStringHelper::lowercase(YiiStringHelper::substring($pascal, 0, 1))
            . YiiStringHelper::substring($pascal, 1);
    }

    /**
     * @param string|null $str
     * @return string|null
     */
    public static function nullif(?string $str): ?string
    {
        return self::isEmpty($str) ? null : $str;
    }

    /**
     * @param string|null $str
     * @return bool
     */
    public static function isEmpty(?string $str): bool
    {
        return $str === null || $str === '';
    }

    /**
     * @param array<array-key,string|null> $stringArr
     * @return bool
     */
    public static function isAllEmpty(array $stringArr): bool
    {
        foreach ($stringArr as $item) {
            if (!static::isEmpty($item)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string $xml
     * @return string|null
     */
    public static function getRootTagFromXml(string $xml): ?string
    {
        $re = '/^\<\?xml .+?\?>\s(\s+)?\<(?P<root>\w+[^>]+)>/u';
        if (($result = preg_match($re, $xml, $matches)) === 0 || !$result) {
            return null;
        }
        return explode(' ', $matches['root'])[0];
    }

    /**
     * @param string $xml
     *
     * @return string|null
     */
    public static function removeEmptyXmlTagFromXml(string $xml): ?string
    {
        $re = '/<\S+\/>/im';
        return preg_filter($re, '', $xml);
    }

    /**
     * @param string|null $docUrl
     * @param array $allowedMimes
     * @return string|null
     */
    public static function getBase64EncodedInlineFile(
        ?string $docUrl,
        array $allowedMimes = ['application/pdf; charset=binary']
    ): ?string {
        $matchResult = match (true) {
            isset($docUrl) => function () use ($docUrl, $allowedMimes) {
                switch (parse_url($docUrl, PHP_URL_SCHEME)) {
                    // HTTP or HTTPS
                    case 'http' || 'https' || 'file':
                        $contextOptions = [
                            'ssl' => [
                                'verify_peer' => true,
                                'cafile' => '/etc/pki/tls/cacert.pem',
                            ]
                        ];
                        $context = stream_context_create($contextOptions);

                        if (($data = file_get_contents($docUrl, false, $context)) === false) {
                            // Resource did not exist.
                            return null;
                        }

                        if (in_array((new finfo(FILEINFO_MIME))->buffer($data), $allowedMimes)) {
                            return $data;
                        }
                        break;
                    default:
                        return null;
                }
                return null;
            },
            default => null,
        };

        if (isset($matchResult) && ($data = $matchResult()) !== null) {
            return chunk_split(base64_encode($data));
        }
        return null;
    }

    /**
     * @param string|null $url
     * @param array|null $acceptedProtocols
     * @return bool
     */
    public static function isUrlValid(?string $url, ?array $acceptedProtocols = []): bool
    {
        // Make sure that it is not null...
        $acceptedProtocols ??= [];

        if (!self::isEmpty($url) && isset($url)) {
            $protocol = parse_url($url, PHP_URL_SCHEME);
            // If $protocol is any of the valid protocols (Url Schemes), then return true.
            return !is_bool($protocol) && in_array($protocol ?? null, $acceptedProtocols);
        }

        // If $url is empty or not recognizable by parse_url, then false.
        return false;
    }

    /**
     * @param string|null $url
     * @param Aliases|null $aliases
     * @return string|null
     */
    public static function resolveAbsolutePath(?string $url, ?Aliases $aliases): ?string
    {
        if (self::isEmpty($url) || !isset($url)) {
            return null;
        }
        /** @var ?Aliases $aliases */
        return StringHelper::isUrlValid($url, ['file'])
        && !YiiStringHelper::startsWith(strtolower($url), 'file://')
            ? 'file://' . match (true) {
                YiiStringHelper::startsWith(
                    ($file = str_ireplace('file:', '', $url)),
                    '.'
                )
                    => realpath($file),
                YiiStringHelper::startsWith(
                    $file,
                    '@'
                ) && $aliases !== null
                    => $aliases->get($file),
                default
                    => $file,
            }
            : $url;
    }
}
