<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Command\Glis
 */

declare(strict_types=1);

namespace App\Command;

use EndlessDreams\FaoToolkit\Command\Base\RequestCommand;
use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyErrorInterface;
use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyInterface;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credential;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\HelpCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Log\Target\File\FileTarget;
use Yiisoft\Mailer\MailerInterface;
use Yiisoft\Strings\StringHelper;

/**
 *
 */
#[AsCommand(name: 'glis:add-targets', description: '<bg=yellow;options=bold>TBA. '
    . 'Part of API but not implemented yet.</> Command to add targets to GLIS.')]
final class GlisAddTargetsCommand extends RequestCommand
{
    /**
     * @param FaoConfig $faoConfig
     * @param Aliases $aliases
     * @param ConnectionInterface $db
     * @param FileTarget $fileTarget
     * @param MailerInterface $mailer
     */
    public function __construct(
        FaoConfig $faoConfig,
        Aliases $aliases,
        ConnectionInterface $db,
        FileTarget $fileTarget,
        MailerInterface $mailer
    ) {
        parent::__construct($faoConfig, $aliases, $db, $fileTarget, $mailer);
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setHelp(
            'TBA. Future feature. Add targets to GLIS.'
        );
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Symfony\Component\Console\Exception\ExceptionInterface
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);
        $style->title(StringHelper::uppercaseFirstCharacter($this->getName() ?? ''));
        $style->warning('This command is not implemented yet.');

        $help = new HelpCommand();
        $help->setCommand($this);
        return $help->run($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function initExecute(InputInterface &$input, OutputInterface &$output): void
    {
        parent::initExecute($input, $output);
    }

    /**
     * @param string $xml
     * @param Credential $credential
     * @return array
     */
    protected function prepareHeaderAndBodyToRequest(string $xml, Credential $credential): array
    {
        // Setting headers and body according to GLIS
        /** @var array<array-key,mixed> */
        return [[], $xml];
    }
}
