<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Command\Glis
 */

declare(strict_types=1);

namespace App\Command;

use App\Entity\Base\ResponseBodyInterface;
use EndlessDreams\FaoToolkit\Command\Base\RequestCommand;
use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyErrorInterface;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credential;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\Glis\Response;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Log\Target\File\FileTarget;
use Yiisoft\Mailer\MailerInterface;

/**
 *
 */
#[AsCommand(name: 'glis:update', description: 'Command to update GLISs.')]
final class GlisUpdateCommand extends RequestCommand
{
    /**
     * @param FaoConfig $faoConfig
     * @param Aliases $aliases
     * @param ConnectionInterface $db
     * @param FileTarget $fileTarget
     * @param MailerInterface $mailer
     */
    public function __construct(
        FaoConfig $faoConfig,
        Aliases $aliases,
        ConnectionInterface $db,
        FileTarget $fileTarget,
        MailerInterface $mailer
    ) {
        parent::__construct($faoConfig, $aliases, $db, $fileTarget, $mailer);
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setHelp(
            'Update GLISs using db config file to connect to database and retrieves GLIS passport data rows. '
        );
        parent::configure();
        $this->addOption(
            'sampledoi',
            null,
            InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
            'Request update of Sample DOI or DOIs'
        );
        $this->addOption(
            'sampleid',
            null,
            InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
            'Request update of Sample Id or Ids'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function initExecute(InputInterface &$input, OutputInterface &$output): void
    {
        parent::initExecute($input, $output);
    }

    /**
     * @param string $xml
     * @param Credential $credential
     * @return array
     */
    protected function prepareHeaderAndBodyToRequest(string $xml, Credential $credential): array
    {
        // Setting headers and body according to GLIS
        /** @var array<array-key,mixed> */
        return [[], $xml];
    }
}
