<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Command\Smta
 */

declare(strict_types=1);

namespace App\Command;

use EndlessDreams\FaoToolkit\Command\Base\RequestCommand;
use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyInterface;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credential;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use RingCentral\Psr7\MultipartStream;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use Yiisoft\Db\Command\CommandInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;

use function React\Async\{await};

/**
 *
 */
#[AsCommand(name: 'smta:get', description: 'Command to fetch a SMTA')]
final class SmtaGetCommand extends RequestCommand
{
    /**
     * @var CommandInterface|null
     */
    protected ?CommandInterface $itemsCommand = null;

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setHelp(
            'Fetch a SMTA by symbol.'
        );
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function initExecute(InputInterface &$input, OutputInterface &$output): void
    {
        parent::initExecute($input, $output);
    }

    /**
     * @param string $xml
     * @param Credential $credential
     * @return array
     */
    protected function prepareHeaderAndBodyToRequest(string $xml, Credential $credential): array
    {
        // Setting headers and body according to SMTA
        // TODO: Implements feature to send pdf
        //$doCompress = false;
        $content = [
            'username' => $credential->getUsername(),
            'password' => $credential->getPassword(),
            'compressed' => /*$doCompress ? 'y' : */'n',
            'xml' => $xml,
        ];
        $multipart = [];
        foreach ($content as $name => $value) {
            $multipart[] = ['name' => $name, 'contents' => $value];
        }

        /** @var MultipartStream $body */
        $body = new MultipartStream($multipart);
        $headers = [];
        $headers['Content-Type'] = 'multipart/form-data; boundary=' . $body->getBoundary();
        /** @var array<array-key,mixed> */
        return [$headers, $body];
    }

    /**
     * @param ResponseBodyInterface $responseBody
     * @param int $pkValue
     * @return void
     */
    protected function processRequestSuccess(ResponseBodyInterface $responseBody, int $pkValue): void
    {
        // TODO: Output recieved Smta.
    }
}
