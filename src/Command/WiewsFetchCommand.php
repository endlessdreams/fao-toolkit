<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Command\Wiews
 */

declare(strict_types=1);

namespace App\Command;

use EndlessDreams\FaoToolkit\Service\Helper\DriverMissingException;
use EndlessDreams\FaoToolkit\Service\Helper\OrganizationFetchHelper;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

/**
 *
 */
#[AsCommand(name: 'wiews:fetch', description: 'FAO WIEWS code services. Fetch organization data.')]
final class WiewsFetchCommand extends Command
{
    public function __construct(
        private readonly ParserService $parser
    ) {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setHelp(
            'FAO WIEWS code services. Command to fetch organization data for submitted wiews code'
        );

        $this->addOption(
            'format',
            null,
            InputOption::VALUE_OPTIONAL,
            'Output format, any of: [\'json_old\',\'json\',\'yaml\',\'csv\',\'xml\',\'var_dump\',\'print_r\']',
            'print_r',
        );

        $this->addArgument('wiews_code', InputArgument::REQUIRED, 'Wiews code to look up');

        //.ArrayJsonHelper::toSerializedArray(OrganizationFetchHelper::encodeOptions(),true,ArrayJsonHelper::VAR_EXPORT_SHORT_ARRAY|ArrayJsonHelper::VAR_EXPORT_NO_INDEX|ArrayJsonHelper::VAR_EXPORT_COMPACT)
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input Symfony Input object.
     * @param OutputInterface $output Symfony Output object.
     *
     * @return int
     *
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $wiewsCodes = array_map(
                fn(string $item): string => trim($item),
                explode(',', (string)$input->getArgument('wiews_code'))
            );

            $encodedData = count($resultedArray = OrganizationFetchHelper::lookupByWiewsCode($wiewsCodes)) === 0
                ? false
                : OrganizationFetchHelper::encodeOrganizationData(
                    $resultedArray,
                    $input->getOption('format') !== null
                        ? (string)$input->getOption('format')
                        : 'print_r',
                    $this->parser
                );

            if (is_bool($encodedData)) {
                $output->writeln('Unable to get any organization data.');
                return Command::FAILURE;
            }
            $output->writeln($encodedData);
            return Command::SUCCESS;
        } catch (DriverMissingException) {
            $missingDriverText = <<<TXT
If 'Composer bdi' doesn't manage to install drivers, then you have to do it manually

Alternatively, you can use the package manager of your operating system to install them.

On Ubuntu, run:
apt-get install firefox-geckodriver

On Mac, using Homebrew:
brew install geckodriver

On Windows, using chocolatey:
choco install selenium-gecko-driver

TXT;

            $output->writeln('<error>Missing geckodriver</error>');
            $output->writeln('<info>It seems like geckodriver has not been installed.</info>');
            $output->writeln('<info>Try to run: composer bdi</info>');
            $output->writeln('<info>' . $missingDriverText . '</info>');
        }
        return Command::FAILURE;
    }
}
