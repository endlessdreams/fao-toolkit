<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Command\Base
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Command\Base;

use EndlessDreams\FaoToolkit\Entity\Base\ResponseBodyInterface;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Credential;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\FaoConfigException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use EndlessDreams\FaoToolkit\Service\Progress\ProgressService;
use Error;
use React\Http\Browser;
use React\Http\Message\Response;
use React\Http\Message\ResponseException;
use React\Stream\ReadableStreamInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidCallException;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Log\Logger;
use Yiisoft\Log\Target\File\FileTarget;
use Yiisoft\Mailer\MailerInterface;
use Yiisoft\Yii\Console\ExitCode;

use function React\Async\{await};

/**
 *
 */
abstract class RequestCommand extends Command
{
    /**
     * @var DbMapService|null
     */
    protected ?DbMapService $dbMapService = null;

    /**
     * @var ModuleCommandServiceInterface|null
     */
    protected ?ModuleCommandServiceInterface $moduleServiceClass = null;

    /**
     * @var ProgressService|null
     */
    private ?ProgressService $progressService = null;
    /**
     * @var Logger
     */
    private Logger $logger;

    private ?string $userAgent = null;

    /**
     * @param FaoConfig $faoConfig
     * @param Aliases $aliases
     * @param ConnectionInterface $db
     * @param FileTarget $fileTarget
     * @param MailerInterface $mailer
     */
    public function __construct(
        private readonly FaoConfig $faoConfig,
        private readonly Aliases $aliases,
        private readonly ConnectionInterface $db,
        private readonly FileTarget $fileTarget,
        private readonly MailerInterface $mailer
    ) {
        $this->logger = new Logger([$this->fileTarget]);
        $version = str_replace('dev-master#', '', (string)$this->faoConfig->getClientVersion());
        $version = str_replace('dev-master', '3', $version);
        $version = str_replace('v', '', $version);
        $this->userAgent = 'Fao-Toolkit/' . $version;
        parent::__construct();
    }

    /**
     *
     */
    public function __destruct()
    {
        $this->logger->flush(true);
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDefinition(
            new InputDefinition([
                new InputOption(
                    'run-as',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Wiews code of institute which accessions to fetch and export as xml'
                ),
                new InputOption(
                    'dry-run',
                    null,
                    InputOption::VALUE_NONE,
                    'Make a dry run. This run do not contact FAO\'s Glis API. It just generates the XML and outputs it'
                ),
                new InputOption(
                    'skip-invalid',
                    null,
                    InputOption::VALUE_NONE,
                    'Skip sending each row that is validated invalid by the client'
                ),
                new InputOption(
                    'test',
                    null,
                    InputOption::VALUE_NONE,
                    'Connect to the test server and make the SMTA registration.'
                ),
                new InputOption(
                    'interactive',
                    null,
                    InputOption::VALUE_NONE,
                    'Prompts a query when client validator finds errors, whatever to skip or not, ' . PHP_EOL
                    . 'and if server returns a error response stops the process with a prompt.' . PHP_EOL
                    . 'It prompts you if there is no credentials.'
                ),
                new InputOption(
                    'from',
                    'f',
                    InputOption::VALUE_REQUIRED,
                    'From which SMTA date to start report'
                ),
                new InputOption(
                    'to',
                    't',
                    InputOption::VALUE_REQUIRED,
                    'To which SMTA date to end report'
                ),
                new InputOption(
                    'filter',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'filter expression for symbol matching. '
                    . 'Like expression for SqlSrv/MsSql and Regexp for Sqlite, MySql, Postgres and Oracle.'
                ),
                new InputOption(
                    'offset',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Offset the report to specified numbers of records'
                ),
                new InputOption(
                    'limit',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Limit the report to specified numbers of records'
                ),
                new InputOption(
                    'order-by',
                    null,
                    InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                    'Order of records by column name'
                ),
                new InputOption(
                    'fao-username',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Fao account username'
                ),
                new InputOption(
                    'fao-password',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Fao account password'
                ),
                new InputOption(
                    'output-path',
                    'o',
                    InputOption::VALUE_REQUIRED,
                    'Filename to extra job stat xlsx-file.'
                ),
                new InputOption(
                    'email-to',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Email receiver to extra job stat xlsx-file.'
                ),

            ])
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        codecept_debug(__METHOD__ . ':enter');
        try {
            $this->initExecute($input, $output);
        } catch (DbMapException | InvalidConfigException $e) {
            $output->writeln('<fg=red>' . $e->getMessage() . '</>');
            //$output->writeln(print_r($this->faoConfig, true));
            return ExitCode::CONFIG;
        } catch (Exception | Throwable $e) {
            $output->writeln('<fg=red>' . $e->getMessage() . '</>');
            //throw $e;
            return ExitCode::UNSPECIFIED_ERROR;
        }

        // Iterate for each row, collect violations, send data to server, collect errors, movve progressbar
        /**
         * @var string $xml
         * @var Credential $credentials
         * @var string $uniqueKey
         * @var int $pkValue
         */
        foreach (
            $this->progressService?->getGenerator()
            ?? [] as ['xml' => $xml, 'credentials' => $credentials, 'uniqueKey' => $uniqueKey, 'rowPkValue' => $pkValue]
        ) {
            if ($output->isVeryVerbose()) {
                $output->writeln($xml);
                $url = (string)$this->faoConfig->getServerUrl();
                $output->writeln($url);
            }

            codecept_debug('ProcessRequest - Begin');
            $this->processRequest($xml, $credentials, $uniqueKey, $pkValue);
            codecept_debug('ProcessRequest - End');
        }
        $output->writeln('');

        // After progress bar
        $this->progressService?->getJobDbService()?->outputCurrentJobErrorsAsTable(1, $output);
        $this->progressService?->getJobDbService()?->outputCurrentJobErrorsAsTable(2, $output);
        $this->progressService?->getJobDbService()?->outputJobstatsAsTable($output);
        codecept_debug(__METHOD__ . ':leave');
        return ExitCode::OK;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws DbMapException
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    protected function initExecute(InputInterface &$input, OutputInterface &$output): void
    {
        // Check if no --run-as option is set, then set --run-as to default.
        if ($input->getOption('run-as') === null) {
            $input->setOption('run-as', $this->faoConfig->getProviders()?->getDefaultInstituteCode());
        }

        // Configure FaoConfig with console input
        $this->faoConfig->setConsoleInputState($input);
        $url = $this->faoConfig->getServerUrl();

        if (!isset($url)) {
            throw new FaoConfigException('No server address was set! Please update the configuration.');
        }

        // Set up DbMapService
        $this->dbMapService = new DbMapService($this->faoConfig);

        // Get service to convert the client table row array to module main table row.
        $this->moduleServiceClass = $this->faoConfig->getParser()
            ?->moduleCommandRegister
            ?->getService((string)$input->getArgument('command'));

        if ($this->moduleServiceClass === null) {
            throw new \Exception('A module command service was not defined.');
        }

        $this->moduleServiceClass->init($this->dbMapService);

        // Init Progress bar service
        $this->progressService = new ProgressService(
            $input,
            $output,
            $this->dbMapService,
            $this->moduleServiceClass,
            $this->logger,
            $this->db,
            $this->aliases,
            $this->mailer
        );
    }

    /**
     * @param string $xml
     * @param Credential $credential
     * @return array
     */
    abstract protected function prepareHeaderAndBodyToRequest(string $xml, Credential $credential): array;

    /**
     * @param string $xml
     * @param Credential $credential
     * @param string $uniqueKey
     * @param int $pkValue
     * @return void
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws InvalidCallException
     */
    protected function processRequest(
        string $xml,
        Credential $credential,
        string $uniqueKey,
        int $pkValue
    ): void {
        codecept_debug(__METHOD__ . ':enter');
        try {
            $this->progressService->setCurrentUniqueKey($uniqueKey);

            $url = (string)$this->faoConfig->getServerUrl();

            codecept_debug(__METHOD__ . ':before prepareHeaderAndBody');

            /**
             * @var array<array-key,mixed|array<array-key,mixed>> $headers
             * @var ReadableStreamInterface|string $body
             */
            [$headers, $body] = $this->prepareHeaderAndBodyToRequest($xml, $credential);

            codecept_debug(__METHOD__ . ':after prepareHeaderAndBody');

            $client = new Browser();

            if (isset($this->userAgent)) {
                //$client->withHeader('User-Agent',$this->userAgent);
                if (!array_key_exists('User-Agent', $headers) || !is_array($headers['User-Agent'])) {
                    $headers['User-Agent'] = [];
                }
                $headers['User-Agent'][] = $this->userAgent;
            }

            codecept_debug(__METHOD__ . ':before post');
            $promise = $client->post(
                $url,
                $headers,
                $body
            );
            $this->progressService?->increaseNoOfSent();
            /** @var Response $response */
            $response = await($promise);
            codecept_debug(__METHOD__ . ':after post');

            // response successfully received
            /** @var ResponseBodyInterface $responseBody */
            $responseBody = $this->faoConfig->getParser()?->deserializeXml(
                (string)$response->getBody(),
                $this->moduleServiceClass?->getEntityResponseClass()
                    ?? throw new \Exception('Entity response class was not defined.'),
                $this->moduleServiceClass?->getResponseXmlRootName(),
                ['groups' => ['Default']]
            );
            switch ($response->getStatusCode()) {
                case '200':
                    if (
                        $this->moduleServiceClass?->getResponseXmlRootName() === 'glis'
                        && $this->moduleServiceClass?->getEntityResponseClass() === Glis::class
                    ) {
                        $this->progressService?->increaseNoOfAccepted();
                        //$this->progressService?->getOutput()->writeln("\r". $xml);
                        $this->progressService?->getOutput()->writeln("\r" . (string)$response->getBody());

                        $this->progressService?->logSuccess(
                            $uniqueKey,
                            $this->progressService?->getOutput()?->isVeryVerbose() ? ['xml' => $responseBody] : null,
                            OutputInterface::VERBOSITY_VERY_VERBOSE
                        );
                    } elseif ($responseBody->getError() === null) {
                        $this->progressService?->increaseNoOfAccepted();
                        $this->processRequestSuccess($responseBody, $pkValue);
                        $this->progressService?->logSuccess(
                            $uniqueKey,
                            $this->progressService?->getOutput()?->isVeryVerbose() ? ['xml' => $xml] : null,
                            OutputInterface::VERBOSITY_VERY_VERBOSE
                        );
                    } else {
                        $this->progressService?->increaseNoOfRejected();
                        $this->progressService
                            ?->storeReceivedErrors($uniqueKey, 'Server Processed', [$responseBody->getError()]);
                        $this->progressService?->logError($uniqueKey, 'Server Processed', [$responseBody->getError()]);
                    }
                    break;
                default:
                    $this->progressService?->increaseNoOfRejected();
                    $this->progressService
                        ?->storeReceivedErrors($uniqueKey, 'HTTP Server response code', [$response->getStatusCode()]);
                    $this->progressService
                        ?->logError($uniqueKey, 'HTTP Server response code', [$response->getStatusCode()]);
                    break;
            }
        } catch (ResponseException $e) {
            // a reponse exception occurred while performing the request
            codecept_debug($e->getMessage());
            $this->progressService?->increaseNoOfRejected();
            $this->progressService
                ?->storeReceivedErrors($uniqueKey, 'HTTP ' . (string)$e->getCode(), [$e->getMessage()]);
            $this->progressService?->logError($uniqueKey, (string)$e->getCode(), [$e->getMessage()]);
        } catch (Exception | Throwable $e) {
            // any other error occurred while performing the request
            codecept_debug($e->getMessage());
            $this->progressService?->increaseNoOfRejected();
            $this->progressService?->storeReceivedErrors($uniqueKey, (string)$e->getCode(), [$e->getMessage()]);
            $this->progressService?->logError($uniqueKey, (string)$e->getCode(), [$e->getMessage()]);
        }
        codecept_debug(__METHOD__ . ':enter');
    }

    /**
     * @param ResponseBodyInterface $responseBody
     * @param int $pkValue
     * @return void
     */
    protected function processRequestSuccess(ResponseBodyInterface $responseBody, int $pkValue): void
    {
    }
}
