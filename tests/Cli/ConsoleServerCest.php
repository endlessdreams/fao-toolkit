<?php

declare(strict_types=1);

namespace App\Tests\Cli;

use App\Tests\Support\CliTester;
use Yiisoft\Strings\StringHelper;

final class ConsoleServerCest
{
    public function testCommandYii(CliTester $I): void
    {
        $command = dirname(__DIR__, 2) . '/yii';
        $I->runShellCommand($command);
        $I->seeInShellOutput('Fao-Toolkit');
    }

    public function testCommandServer(CliTester $I): void
    {
        $command = dirname(__DIR__, 2) . '/yii';
        $I->runShellCommand($command . ' server:ctrl start');
        $cnt = 1000;
        do {
            $I->runShellCommand($command . ' server:ctrl read-stats', false);
            $output = $I->grabShellOutput();
            time_nanosleep(1, 100000000);
            $I->comment('I want to make sure that server is running.');
        } while (
            !StringHelper::startsWith($output, 'Server is running at')
            && $cnt++ > 0
        );
        $I->seeInShellOutput('Server is running at');
        $I->runShellCommand($command . ' server:ctrl stop');

        $cnt = 1000;
        do {
            $I->runShellCommand($command . ' server:ctrl read-stats', false);
            $output = $I->grabShellOutput();
            time_nanosleep(1, 100000000);
            $I->comment('I want to make sure that server is not running.');
        } while ($output != 'Server is inactive' && $cnt++ > 0);
        $I->seeInShellOutput('Server is inactive');
    }
}
