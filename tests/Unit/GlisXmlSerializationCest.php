<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Entity\Glis\Ids;
use EndlessDreams\FaoToolkit\Entity\Glis\Kws;
use EndlessDreams\FaoToolkit\Entity\Glis\Names;
use EndlessDreams\FaoToolkit\Entity\Glis\Register;
use EndlessDreams\FaoToolkit\Entity\Glis\Service\GlisRegisterModuleCommandService;
use EndlessDreams\FaoToolkit\Entity\Glis\Targets;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use ErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Tests\Support\UnitTester;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Yii\Runner\Console\ConsoleApplicationRunner;

class GlisXmlSerializationCest
{
    use CestUtilTrait;

//    private ParserService $parser;
//    private ValidatorInterface $validator;

    private array $xmls = [
/*
        Targets::class => '<?xml version="1.0" encoding="UTF-8"?>
        <targets>
            <target>
                <value>https://nordic-baltic-genebanks.org/gringlobal/AccessionDetail.aspx?id=2230</value>
                <kws>
                    <kw>1</kw>
                </kws>
            </target>
            <target>
                <value>https://nordic-baltic-genebanks.org/gringlobal/method.aspx?id=342</value>
                <kws>
                    <kw>2</kw>
                    <kw>3</kw>
                </kws>
            </target>
        </targets>',*//*
        Target::class => '<?xml version="1.0" encoding="UTF-8"?>
        <target>
            <value>https://nordic-baltic-genebanks.org/gringlobal/AccessionDetail.aspx?id=2230</value>
            <kws>
                <kw>1</kw>
            </kws>
        </target>',
        Kws::class => '<?xml version="1.0" encoding="UTF-8"?>
        <kws>
            <kw>1</kw>
        </kws>',
        Actor::class => '<?xml version="1.0" encoding="UTF-8"?>
        <actor>
            <wiews>XXX001</wiews>
            <pid>AA00AA</pid>
            <name>ACME</name>
            <address>Main street 1, XX-0001 Creekvile</address>
            <country>XXX</country>
        </actor>',
        Acquisition::class => '
        <acquisition>
            <provider>
                <wiews>XXX002</wiews>
                <pid>AA00AB</pid>
                <name>NOT ACME</name>
                <address>Second street 1, XX-0001 Slipsvile</address>
                <country>XXX</country>
            </provider>
            <sampleid>ACME 1</sampleid>
            <provenance>XXX</provenance>
        </acquisition>',
        Breeding::class => '
        <breeding>
            <breeders>
                <breeder>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </breeder>
                <breeder>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </breeder>
            </breeders>
            <ancestry>------</ancestry>
        </breeding>
        ',
        Breeders::class => '
            <breeders>
                <breeder>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </breeder>
            </breeders>
        '
        ,
        Collectors::class => '
            <collectors>
                <collector>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </collector>
            </collectors>
        '
        ,
        Collection::class => '
            <collection>
                <collectors>
                    <collector>
                        <wiews>XXX003</wiews>
                        <pid>AA00AC</pid>
                        <name>NOT EVEN ACME</name>
                        <address>Third street 1, XX-0001 Slipsvile</address>
                        <country>XXX</country>
                    </collector>
                </collectors>
                <sampleid>NACME 2</sampleid>
                 <missid>[missid]</missid>
                 <site>[site]</site>
                 <lat>[clat]</lat>
                 <lon>[clon]</lon>
                 <uncert>[uncert]</uncert>
                 <datum>[datum]</datum>
                 <georef>[georef]</georef>
                 <elevation>[elevation]</elevation>
                 <date>[cdate]</date>
                 <source>[source]</source>
            </collection>
        '
        , Names::class => '
            <names>
                <name>[name]</name>
                <name>[name]</name>
            </names>
        '
        , Ids::class => '
            <ids>
                <id type="sgsvid">239741</id>
                <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
            </ids>
        '
        ,*/
        Glis::class => '<?xml version="1.0" encoding="UTF-8" ?>
            <register username="cgn" password="Passw0rd">
                <location>
                    <pid>00AC55</pid>
                </location>
                <sampledoi>[sampledoi]</sampledoi>
                <sampleid>CGN00001</sampleid>
                <date>1986-05-12</date>
                <method>acqu</method>
                <genus>Hordeum</genus>
                <cropnames>
                    <name>Barley</name>
                </cropnames>
                <targets>
                    <target>
                        <value>http://www.nordgen.org/sgsv/index.php?unit_id=239741</value>
                        <kws>
                            <kw>5</kw>
                        </kws>
                    </target>
                </targets>
                <biostatus>300</biostatus>
                <species>vulgare</species>
                <spauth>L.</spauth>
                <subtaxa>subsp. vulgare</subtaxa>
                <stauth>L.</stauth>
                <names>
                    <name>Maartsche Gerst</name>
                </names>
                <ids>
                    <id type="sgsvid">239741</id>
                    <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
                </ids>
                <mlsstatus>1</mlsstatus>
                <historical>n</historical>
                <acquisition>
                    <provider>
                        <wiews>NLD078</wiews>
                    </provider>
                    <sampleid>GZ A1</sampleid>
                    <provenance>NLD</provenance>
                </acquisition>
                <collection>
                    <collectors>
                        <collector>
                            <wiews>[cwiews]</wiews>
                            <pid>[cpid]</pid>
                            <name>[cname]</name>
                            <address>[caddress]</address>
                            <country>[ccountry]</country>
                        </collector>
                    </collectors>
                    <sampleid>[csampleid]</sampleid>
                    <missid>[missid]</missid>
                    <site>[site]</site>
                    <lat>[clat]</lat>
                    <lon>[clon]</lon>
                    <uncert>[uncert]</uncert>
                    <datum>[datum]</datum>
                    <georef>[georef]</georef>
                    <elevation>[elevation]</elevation>
                    <date>[cdate]</date>
                    <source>source</source>
                </collection>
                <breeding>
                    <breeders>
                        <breeder>
                            <wiews>[wiews]</wiews>
                            <pid>[pid]</pid>
                            <name>[name]</name>
                            <address>[address]</address>
                            <country>[country]</country>
                        </breeder>
                        <breeder>
                            <wiews>[wiews]</wiews>
                            <pid>[pid]</pid>
                            <name>[name]</name>
                            <address>[address]</address>
                            <country>[country]</country>
                        </breeder>
                    </breeders>
                    <ancestry>[ancestry]</ancestry>
                </breeding>
            </register>
        '
    ];

    private array $xmls2 = [
        Kws::class => '<?xml version="1.0" encoding="UTF-8"?>
        <kws>
            <kw>1</kw>
        </kws>'
        , Names::class => '
            <names>
                <name>[name]</name>
                <name>[name]</name>
            </names>
        '
        , Ids::class => '
            <ids>
                <id type="sgsvid">239741</id>
                <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
            </ids>
        '
    ];
//    private \Yiisoft\Config\ConfigInterface $config;
//    private \Psr\Container\ContainerInterface $container;


    /**
     * @throws ErrorException
     */
    public function _before(UnitTester $I)
    {
        if (!empty($this->table)) {
            return;
        }
        try {
            codecept_debug('Init validate helper');
            $this->setUpContainer();
            $this->setUpFaoConfig('glis', 'test', 'glis:register');
            $this->moduleServiceClass?->setFaoInstituteCodeQueryColumn('location.wiews');
            $this->aliases = $this->container->get(Aliases::class);
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface $e) {
            codecept_debug($e->getMessage());
        }
    }

    // tests


    public function testDeserializeThenSerialize(UnitTester $I): void
    {
        $I->expectTo('be able to deserialize and then serialize xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDeserializeThenSerializeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }

    private function testDeserializeThenSerializeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {
        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }

        $I->amGoingTo('deserialize xml to expected class ' . $class . '.');
        codecept_debug($expectedXml);
        $addContext = ["groups" => ["Default", "Glis", (string)$root]];
        $object = $this->parser->deserializeXml($expectedXml, $class, $root, $addContext);

        $I->amGoingTo('serialize object of ' . $object::class . ' class into an xml with root tag <' . $root . '>.');
        codecept_debug($object);
        $resultXml = $this->parser->serializeXml($object, $root, $addContext);
        codecept_debug($resultXml);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }

    public function testDecodeThenDenormalizeThenSerialize(UnitTester $I): void
    {
        $I->expectTo('be able to decode, denormalize and then serialize xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDecodeThenDenormalizeThenSerializeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }

    private function testDecodeThenDenormalizeThenSerializeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {
        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }

        $I->amGoingTo('decode xml to to an array.');
        codecept_debug($expectedXml);
        $addContext = ["groups" => ["Default", "Glis", (string)$root]];
        $array = $this->parser->decodeXml($expectedXml, $root, $addContext);

        $I->amGoingTo('denormalize array to expected class ' . $class . '.');
        codecept_debug($array);
        $object = $this->parser->denormalize($array, $class, 'xml', $addContext);

        $I->amGoingTo('serialize object of ' . $object::class . ' class into an xml with root tag <' . $root . '>.');
        codecept_debug($object);
        $resultXml = $this->parser->serializeXml($object, $root, $addContext);
        codecept_debug($resultXml);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }

    public function testDecodeThenDenormalizeThenNormalizeThenEncode(UnitTester $I): void
    {
        $I->expectTo('be able to decode, denormalize, normalize and then encode xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDecodeThenDenormalizeThenNormalizeThenEncodeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }

    private function testDecodeThenDenormalizeThenNormalizeThenEncodeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {
        codecept_debug($root);
        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }

        $I->amGoingTo('decode xml to to an array.');
        codecept_debug($expectedXml);
        $addContext = [
            "groups" => [
                "Default",
                "Glis",
                (string)$root
            ],
            "save_options" => LIBXML_NOBLANKS,
            "xml_root_node_name" => $root
        ];
        $array = $this->parser->decodeXml($expectedXml, $root, $addContext);

        $I->amGoingTo('denormalize array to expected class ' . $class . '.');
        codecept_debug($array);
        $object = $this->parser->denormalize($array, $class, 'xml', $addContext);

        $I->amGoingTo('normalize object of ' . $object::class . ' class into an array.');
        codecept_debug($object);
        //$array2 = $this->parser->normalizeXml($object);
        $array2 = $this->parser->getSerializer()->normalize($object, 'xml', $addContext);


        $I->amGoingTo('encode array into an xml with root tag <' . $root . '>.');
        codecept_debug($array2);

        $resultXml = $this->parser->encodeXml($array2, $root, $addContext);
        $resultXml = StringHelper::removeEmptyXmlTagFromXml($resultXml);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }

    public function testDeserializeThenNormalizeThenEncode(UnitTester $I): void
    {
        $I->expectTo('be able to deserialize, normalize and then encode xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDeserializeThenNormalizeThenEncodeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }

    private function testDeserializeThenNormalizeThenEncodeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {

        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }

        $I->amGoingTo('deserialize xml to expected class ' . $class . '.');
        codecept_debug($expectedXml);
        $addContext = [
            "groups" => [
                "Default",
                "Glis",
                (string)$root
            ],
            "save_options" => \LIBXML_NOEMPTYTAG | LIBXML_NOBLANKS,
            "xml_root_node_name" => $root
        ];
        $object = $this->parser->deserializeXml($expectedXml, $class, $root, $addContext);

        $I->amGoingTo('normalize object of ' . $object::class . ' class into an array.');
        codecept_debug($object);
        $array2 = $this->parser->normalizeXml($object);

        $I->amGoingTo('encode array into an xml with root tag <' . $root . '>.');
        codecept_debug($array2);
        $resultXml = $this->parser->encodeXml($array2, $root, $addContext);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }
}
