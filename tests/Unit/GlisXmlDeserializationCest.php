<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use ErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use App\Tests\Support\UnitTester;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 *
 */
class GlisXmlDeserializationCest
{
    use CestUtilTrait;

    /**
     * @var array|string[]
     */
    private array $xmls = [
        Glis::class => '<?xml version="1.0" encoding="UTF-8"?>
<register username="testuser" password="password">
  <location>
    <wiews>SWE054</wiews>
    <pid>00AA97</pid>
    <name>NORDGEN</name>
    <address>Växthusvägen 12, 234 56 Alnarp</address>
    <country>SWE</country>
  </location>
  <sampleid>NGB 20102</sampleid>
  <date>2023-01-22</date>
  <method>acqu</method>
  <genus>Leymus</genus>
  <cropnames>
    <name>european dune grass</name>
  </cropnames>
  <targets>
    <target>
      <value>https://nordic-baltic-genebanks.org/gringlobal/accessiondetail?id=37053</value>
      <kws>
        <kw>1</kw>
      </kws>
    </target>
  </targets>
  <biostatus>100</biostatus>
  <species>arenarius</species>
  <subtaxa></subtaxa>
  <names>
    <name>EYRARBAKKI AB0201</name>
  </names>
  <historical>n</historical>
  <acquisition>
    <provider/>
    <provenance>ISL</provenance>
  </acquisition>
  <collection>
    <site>Eyarbakki</site>
    <lat>63.85639</lat>
    <lon>-21.12972</lon>
    <datum>WGS84</datum>
    <georef>GPS</georef>
    <date>2008-09-02</date>
  </collection>
  <breeding/>
</register>'
    ];

    /**
     * @param UnitTester $I
     * @return void
     */
    public function _before(UnitTester $I): void
    {
        // Run these lines only once...
        if (!empty($this->table)) {
            return;
        }
        codecept_debug('_before');
        try {
            $this->setUpContainer();
            codecept_debug('Init validate helper');
            $this->parser = $this->container->get(ParserService::class);
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface | ErrorException $e) {
            codecept_debug($e->getMessage());
        }
    }

    // tests

    /**
     * @param UnitTester $I
     * @return void
     */
    public function testDeserializeThenSerialize(UnitTester $I): void
    {
        $I->expectTo('be able to deserialize and then serialize xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDeserializeThenSerializeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }

    /**
     * @param UnitTester $I
     * @param string $expectedXml
     * @param string $class
     * @param string|null $root
     * @return void
     */
    private function testDeserializeThenSerializeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {
        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }
        $addContext = [
            "groups" => [
                "Default",
                "Glis",
                (string)$root
            ],
            "save_options" => LIBXML_NOBLANKS,
            "xml_root_node_name" => $root
        ];

        $I->amGoingTo('deserialize xml to expected class ' . $class . '.');
        codecept_debug($expectedXml);
        $object = $this->parser->deserializeXml($expectedXml, $class, $root, $addContext);

        $I->amGoingTo('serialize object of ' . $object::class . ' class into an xml with root tag <' . $root . '>.');
        codecept_debug($object);
        $resultXml = $this->parser->serializeXml($object, $root, $addContext);
        codecept_debug($resultXml);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }

    /**
     * @param UnitTester $I
     * @return void
     */
    public function testDecodeThenDenormalizeThenSerialize(UnitTester $I): void
    {
        $I->expectTo('be able to decode, denormalize and then serialize xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDecodeThenDenormalizeThenSerializeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }

    /**
     * @param UnitTester $I
     * @param string $expectedXml
     * @param string $class
     * @param string|null $root
     * @return void
     */
    private function testDecodeThenDenormalizeThenSerializeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {
        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }
        $addContext = [
            "groups" => [
                "Default",
                "Glis",
                (string)$root
            ],
            "save_options" => LIBXML_NOBLANKS,
            "xml_root_node_name" => $root
        ];

        $I->amGoingTo('decode xml to to an array.');
        codecept_debug($expectedXml);
        $array = $this->parser->decodeXml($expectedXml, $root, $addContext);

        $I->amGoingTo('denormalize array to expected class ' . $class . '.');
        codecept_debug($array);
        $object = $this->parser->denormalize($array, $class, 'xml', $addContext);

        $I->amGoingTo('serialize object of ' . $object::class . ' class into an xml with root tag <' . $root . '>.');
        codecept_debug($object);
        $resultXml = $this->parser->serializeXml($object, $root, $addContext);
        codecept_debug($resultXml);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }


    /**
     * @param UnitTester $I
     * @return void
     * @throws ExceptionInterface
     */
    public function testDecodeThenDenormalizeThenNormalizeThenEncode(UnitTester $I): void
    {
        $I->expectTo('be able to decode, denormalize, normalize and then encode xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDecodeThenDenormalizeThenNormalizeThenEncodeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }


    /**
     * @param UnitTester $I
     * @param string $expectedXml
     * @param string $class
     * @param string|null $root
     * @return void
     * @throws ExceptionInterface
     */
    private function testDecodeThenDenormalizeThenNormalizeThenEncodeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {
        codecept_debug($root);
        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }
        $addContext = [
            "groups" => [
                "Default",
                "Glis",
                (string)$root
            ],
            "save_options" => LIBXML_NOBLANKS,
            "xml_root_node_name" => $root
        ];


        $I->amGoingTo('decode xml to to an array.');
        codecept_debug($expectedXml);
        $array = $this->parser->decodeXml($expectedXml, $root, $addContext);

        $I->amGoingTo('denormalize array to expected class ' . $class . '.');
        codecept_debug($array);
        $object = $this->parser->denormalize($array, $class, 'xml', $addContext);

        $I->amGoingTo('normalize object of ' . $object::class . ' class into an array.');
        codecept_debug($object);
        $array2 = $this->parser->normalizeXml($object);


        $I->amGoingTo('encode array into an xml with root tag <' . $root . '>.');
        codecept_debug($array2);

        $resultXml = $this->parser->encodeXml($array2, $root, $addContext);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }


    /**
     * @param UnitTester $I
     * @return void
     * @throws ExceptionInterface
     */
    public function testDeserializeThenNormalizeThenEncode(UnitTester $I): void
    {
        $I->expectTo('be able to deserialize, normalize and then encode xml with different root tags.');
        foreach ($this->xmls as $class => $expectedXml) {
            $this->testDeserializeThenNormalizeThenEncodeModel(
                $I,
                $expectedXml,
                $class,
                StringHelper::getRootTagFromXml($expectedXml)
            );
        }
    }

    /**
     * @param UnitTester $I
     * @param string $expectedXml
     * @param string $class
     * @param string|null $root
     * @return void
     * @throws ExceptionInterface
     */
    private function testDeserializeThenNormalizeThenEncodeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null
    ): void {

        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }
        $addContext = [
            "groups" => [
                "Default",
                "Glis",
                (string)$root
            ],
            "save_options" => LIBXML_NOBLANKS,
            "xml_root_node_name" => $root
        ];

        $I->amGoingTo('deserialize xml to expected class ' . $class . '.');
        codecept_debug($expectedXml);
        $object = $this->parser->deserializeXml($expectedXml, $class, $root, $addContext);

        $I->amGoingTo('normalize object of ' . $object::class . ' class into an array.');
        codecept_debug($object);
        $array2 = $this->parser->normalizeXml($object);

        $I->amGoingTo('encode array into an xml with root tag <' . $root . '>.');
        codecept_debug($array2);
        $resultXml = $this->parser->encodeXml($array2, $root, $addContext);

        $I->assertXmlStringEqualsXmlString($expectedXml, $resultXml);
    }
}
