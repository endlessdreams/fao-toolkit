<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use Codeception\Exception\InjectionException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Database;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Entity\Smta\Smta;
use EndlessDreams\FaoToolkit\Service\Helper\ArrayJsonHelper;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use ErrorException;
use Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Tests\Support\UnitTester;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Definitions\Exception\InvalidConfigException;
use Yiisoft\Yii\Runner\Console\ConsoleApplicationRunner;

/**
 *
 */
class SmtaCsvDenormalizationAndValidationCest
{
    use CestUtilTrait;

    /**
     * @var Aliases|null
     */
    private ?Aliases $aliases;


    /**
     * @var array|null
     */
    private ?array $table = null;

    /**
     * @var array|null
     */
    private ?array $tableIndex = null;

    /**
     * @var ModuleCommandServiceInterface|null
     */
    private ?ModuleCommandServiceInterface $moduleServiceClass;
    /**
     * @var string|null
     */
    private ?string $mainClass = null;

    /**
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws ErrorException
     * @throws InvalidConfigException
     */
    protected function _inject(): void
    {
        // Run these lines only once...
        if (!empty($this->table)) {
            return;
        }
        $this->setUpContainer();
        $this->setUpFaoConfig('smta', 'test', 'smta:register');
        //$this->moduleServiceClass?->setFaoInstituteCodeQueryColumn('location.wiews');
        $this->populateTableRows(filePath: './tests/Support/Data/smta/smtaWithValidationErrors.csv');
        $this->parseAndIndexTableRows();
        $this->mainClass = Smta::class;
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateNotApplicableDescriptors(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a SMTA entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier N/A - Such as whole expected
        valid SMTA and other that are marked N/A in specification document of SMTA.');

        $this->testDescriptorIdentifierGroup($I, 'N/A');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateMaterialDescriptors(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a SMTA entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier material - Child entity Material
        in specification document of SMTA.');

        $this->testDescriptorIdentifierGroup($I, 'material');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateDocumentDescriptors(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a SMTA entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier document - Child entity Document
        in specification document of SMTA.');

        $this->testDescriptorIdentifierGroup($I, 'document');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateDocumentUrlDescriptor(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a SMTA entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier document - Child entity Document
        element url in specification document of SMTA.');

        $this->testDescriptorIdentifierGroup($I, 'document', ['url']);
    }


    /**
     * @param UnitTester $I
     * @param string $did
     * @param array|null $groups
     * @param array|null $keysToUnset
     * @return void
     * @throws Exception
     */
    private function testDescriptorIdentifierGroup(
        UnitTester $I,
        string $did,
        ?array $groups = null,
        ?array $keysToUnset = []
    ): void {
        $firstGroup = empty($groups) ? null : $groups[0];

        $objs = $this->parser->getSerializer()->denormalize(
            $this->getTableRows($firstGroup, $did)/*$this->table*/,
            (string)$this->mainClass . '[]',
            'csv'
        );

        if (!isset($objs) || !is_array($objs)) {
            $I->fail('Failed to parse table rows to an array of Entities.');
        }

        foreach ($objs as $index => $obj) {

            /** @var array<string,array<int,string>> $validationResult */
            $validationResult = $this->parser->validateEntity($obj, $groups ?? ['Default','Smta']);

            foreach ($validationResult as $validationGroup => &$validationMessage) {
                $validationMessage = preg_replace('~[\r\n]+~', '', $validationMessage);
            }
            $error = &$this->tableIndex[$firstGroup][$did][$index];

            $error['actual'] = $validationResult;

            $I->amGoingTo($error['purpose']);
            $I->assertEquals($error['expected'], $error['actual']);
        }
    }

    /**
     * @param string|null $firstGroup
     * @param string $did
     * @return array
     */
    private function getTableRows(?string $firstGroup, string $did): array
    {
        $arr = empty($firstGroup) ? $this->tableIndex[null] : $this->tableIndex[$firstGroup];
        return array_filter(
            $this->table,
            fn($index) => array_key_exists($did, $arr) && array_key_exists($index, $arr[$did]),
            ARRAY_FILTER_USE_KEY
        );
    }


    /**
     * @param string $filePath
     * @return void
     */
    private function populateTableRows(string $filePath): void
    {
        $data = file_get_contents($filePath);
        $this->table = (array)$this->parser->getSerializer()->decode(
            $data,
            'csv',
            [
                CsvEncoder::DELIMITER_KEY => "\t",
            ]
        );
    }

    /**
     * @return void
     */
    private function parseAndIndexTableRows(): void
    {
        $this->tableIndex = [];

        foreach ($this->table ?? [] as $index => &$arr) {
            $group = array_key_exists('group', $arr) ? $arr['group'] : null;

            if (!array_key_exists($group, $this->tableIndex)) {
                $this->tableIndex[$group] = [];
            }
            $did = $arr['did'];
            if (!array_key_exists($did, $this->tableIndex[$group])) {
                $this->tableIndex[$group][$did] = [];
            }

            $expected = $arr['expected_validation_errors'];
            $purpose = $arr['purpose_of_test'];
            unset($arr['expected_validation_errors']);
            unset($arr['purpose_of_test']);
            $this->tableIndex[$group][$did][$index] = [
                'purpose' => $purpose,
                'expected' => $expected,
                'actual' => null
            ];
            ArrayJsonHelper::parseJsonKeyValues($this->tableIndex[$group][$did][$index], ['expected']);

            if (
                array_key_exists('annex1', $arr)
                && !empty($arr['annex1'])
            ) {
                ArrayJsonHelper::parseJsonKeyValues($this->table[$index], ['annex1']);
            }
            //else {
            //    unset($this->table[$index]['progdois']);
            //}
        }
    }
}
