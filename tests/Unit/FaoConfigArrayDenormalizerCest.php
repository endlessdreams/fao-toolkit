<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use ErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Tests\Support\UnitTester;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Definitions\Exception\InvalidConfigException;

/**
 *
 */
class FaoConfigArrayDenormalizerCest
{
    use CestUtilTrait;

    /**
     * @var array
     */
    private array $params;

    /**
     * @var array<string, array<array-key, mixed>>
     */
    private array $faoConfigArray;

    /**
     * @var array
     */
    private array $faoConfigTestArray;
    /**
     * @var true
     */
    private bool $isInjected = false;


    /**
     * @throws NotFoundExceptionInterface
     * @throws InvalidConfigException
     * @throws ContainerExceptionInterface
     * @throws ErrorException
     */
    public function _before(UnitTester $I)
    {
        if ($this->isInjected) {
            return;
        }
        codecept_debug('_before');
        try {
            $this->setUpContainer();
            $this->parser = $this->container->get(ParserService::class);
            $this->validator = $this->parser->getValidator();
            $this->aliases = $this->container->get(Aliases::class);
            $this->params = $this->config->get('params');
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface | ErrorException $e) {
            codecept_debug($e->getMessage());
        }
        $this->isInjected = true;

        $this->faoConfigArray = [];
        $this->faoConfigArray['default'] =
            array_key_exists('endlessdreams/fao-toolkit', $this->params)
            && array_key_exists('fao-config', $this->params['endlessdreams/fao-toolkit'])
            ? $this->params['endlessdreams/fao-toolkit']['fao-config']
            : [];
    }

    // tests

    /**
     * @param UnitTester $I
     * @return void
     */
    public function testExecute1(UnitTester $I): void
    {

        codecept_debug(__METHOD__);
        /** @var FaoConfig $faoConfig */
        $faoConfig = $this->parser->denormalize(
            $this->faoConfigArray['default'],
            FaoConfig::class,
            'xml',
            ['groups' => ['Default']]
        );
        //codecept_debug($faoConfig);

        /*
        codecept_debug($faoConfig->getOptions());

        codecept_debug($faoConfig->getOptions()->offsetGet('force_migration2') ?? false);
*/

        //codecept_debug($this->parser->getSerializer()->serialize($faoConfig,'yaml', [YamlEncoder::YAML_INLINE => 4]));
        codecept_debug(
            json_encode(
                json_decode($this->parser->getSerializer()->serialize($faoConfig, 'json')),
                JSON_PRETTY_PRINT
            )
        );
        //codecept_debug($this->parser->serializeXml($faoConfig,'faoconfig'));
        //codecept_debug($this->parser->getDenormalizers());

        //codecept_debug($this->parser->normalize($faoConfig));
        //codecept_debug($this->parser->normalize($this->faoConfig));
    }

    // TODO: Make tests that proves it is able to deserialize and serialize and get the same result.
    // TODO: Make tests that proves it is able to denormalize an array get expected object.
    // TODO: Make tests that proves it is able to denormalize an array,
    // and then normalize it back get get the same array.
    // TODO: Make tests that proves it is able to decode an XML, JSON, YAML, or CVS,
    // and then encode it back get get the same array.
}
