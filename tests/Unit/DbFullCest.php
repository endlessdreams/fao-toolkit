<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use App\Tests\src\Trait\DbSetupCestUtilTrait;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use App\Tests\Support\UnitTester;
use EndlessDreams\FaoToolkit\Tests\Db\CreateGlis;
use EndlessDreams\FaoToolkit\Tests\Db\CreateSmta;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Throwable;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Yii\Console\ExitCode;

/**
 *
 *
 */
final class DbFullCest
{
    use CestUtilTrait;
    use DbSetupCestUtilTrait;

    /**
     * @var array
     */
    private array $params;

    /**
     * @var bool
     */
    private bool $isInjected = false;

    /**
     * @var bool
     */
    private bool $forceMigration = false;

    /**
     * @var bool
     */
    private bool $hasErrorsDuringInject = false;

    /**
     * @var Aliases|null
     */
    private ?Aliases $aliases = null;
    /**
     * @var CreateSmta
     */
    private CreateSmta $createSmta;

    /**
     * @var CreateGlis
     */
    private CreateGlis $createGlis;

    /**
     * @var array<array-key,array<int,array<array-key,mixed>>>|null
     */
    private ?array $glisTableData = null;

    /**
     * @var int|null
     */
    private ?int $numberOfGlisToTest = null;

    /**
     * @var array<array-key,array<int,array<array-key,mixed>>>|null
     */
    private ?array $smtaTableData = null;

    /**
     * @var int|null
     */
    private ?int $numberOfSmtaToTest = null;

    /**
     * @var bool
     */
    private bool $doStartServer = true;

    /**
     * @param UnitTester $I
     * @return void
     */
    public function prepareDbAndStartUpAndTestTestServer(UnitTester $I): void
    {
        if ($this->doStartServer) {
            $I->amGoingTo('stop Server in background (If it is running).');
            $this->makeTestServerCtrlCommandCallAndFetchOutput('start');

            $I->amGoingTo('start Server in background.');
            $this->makeTestServerCtrlCommandCallAndFetchOutput('start');
        }

        $I->amGoingTo('test that Server is running.');

        $noOfTries = 0;
        do {
            sleep(1);
            $output = $this->makeTestServerCtrlCommandCallAndFetchOutput('status');
            $noOfTries++;
            codecept_debug($output);
        } while ($output !== "Server is running\n" && $noOfTries < 10);

        $I->assertEquals("Server is running\n", $output);

        $I->amGoingTo('check that Server has not processed any data yet.');
        $output = $this->makeTestServerCtrlCommandCallAndFetchOutput('read-stats');
        $I->assertStringContainsString('Proc:0', $output);
    }


    /**
     * @param UnitTester $I
     * @return void
     */
    public function testGlis(UnitTester $I): void
    {
        if (!$this->forceMigration) {
            $I->markTestSkipped('No test of Glis due to no migration');
            return;
        }

        $commandName = 'glis:register';
        $commandCreateGlisRegister = $this->createCommandTester($commandName);

        $numberOfrowsToTest = $this->numberOfGlisToTest ?? 0;

        $dryRunMatchesArray = $this->makeDryRunAndFetchXml(
            $I,
            $commandCreateGlisRegister,
            $commandName,
            'register',
            [
                '--order-by' => 'glisId',
                '--limit' => $numberOfrowsToTest,
            ]
        );

        $I->amGoingTo('run argument command glis:register call.');

        $input = [
            '--run-as' => 'XXX001',
            '--test' => true,
            '--order-by' => 'glisId',
            '--limit' => $numberOfrowsToTest,
            '--skip-invalid' => true,
        ];
        $opt = [
            'verbosity' => OutputInterface::VERBOSITY_NORMAL
        ];

        codecept_debug(__METHOD__ . ':' . __LINE__);
        $I->assertSame(ExitCode::OK, $commandCreateGlisRegister->execute($input, $opt));
        codecept_debug(__METHOD__ . ':' . __LINE__);

        $output = $commandCreateGlisRegister->getDisplay(true);
        codecept_debug($output);
        $I->assertStringContainsString('completed', $output);

        $expectedString = trim(
            implode(
                ' | ',
                [
                    null,
                    str_pad('glis:register', 14),
                    str_pad('XXX001', 6),
                    str_pad('completed', 9),
                    str_pad((string)$numberOfrowsToTest, 5),
                    str_pad('0', 7),
                    str_pad((string)$numberOfrowsToTest, 4),
                    str_pad('0', 8),
                    str_pad((string)$numberOfrowsToTest, 8),
                    null
                ]
            )
        );
        $I->assertStringContainsString($expectedString, $output);

        $I->amGoingTo(
            'check that Server has processed all ' . $numberOfrowsToTest . ' glis items and all ware created.'
        );

        $output = $this->makeTestServerCtrlCommandCallAndFetchOutput('read-stats');
        $I->assertStringContainsString('Proc:' . $numberOfrowsToTest . ' ', $output);
        $I->assertStringContainsString('Glis (C:' . $numberOfrowsToTest . ',', $output);

        $I->amGoingTo('check that Server has the same glis data as the local information server.');
        $I->amGoingTo('make the test by first retrieving the stored data by making a dry-run of the 3 glis.');

        $expectedGlisArrays = $this->makeDryRunAndFetchXml(
            $I,
            $commandCreateGlisRegister,
            $commandName,
            'register',
            [
                '--order-by' => 'glisId',
                '--limit' => $numberOfrowsToTest,
            ]
        );

        foreach ($expectedGlisArrays as $num => $expectedGlisArray) {
            $I->amGoingTo(
                'run argument command glis:get call on a sampledoi, and then compare with the dry-run result.'
            );
            $this->compareGlisData(
                $I,
                $expectedGlisArray,
                $this->createCommandTester('glis:get')
            );
        }
    }

    /**
     * @param UnitTester $I
     * @param array<array-key,mixed> $expectedGlis
     * @param CommandTester|null $commandCreateGlisGet
     * @return void
     */
    private function compareGlisData(
        UnitTester $I,
        array $expectedGlis,
        ?CommandTester $commandCreateGlisGet = null
    ): void {
        $commandCreateGlisGet ??= $this->createCommandTester('glis:get');
        $sampledoi = (string)$expectedGlis['sampledoi'];

        $input = [
            '--run-as' => 'XXX001',
            '--test' => true,
            '--order-by' => 'glisId',
            '--limit' => 1,
            '--skip-invalid' => true,
            '--sampledoi' =>  $sampledoi,
        ];
        $opt = [
            'verbosity' => OutputInterface::VERBOSITY_VERY_VERBOSE
        ];

        codecept_debug(__METHOD__ . ':' . __LINE__);
        $I->assertSame(ExitCode::OK, $commandCreateGlisGet->execute($input, $opt));
        codecept_debug(__METHOD__ . ':' . __LINE__);

        $output = $commandCreateGlisGet->getDisplay(true);
        codecept_debug($output);
        $I->assertStringContainsString('completed', $output);

        $actualGlisArray = $this->extractXmlFromOutput($output, 'glis');
        codecept_debug($actualGlisArray[0]);

        codecept_debug($expectedGlis);
        unset($expectedGlis['id']);
        unset($actualGlisArray[0]['id']);

        $I->assertEquals($expectedGlis, $actualGlisArray[0]);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws DbMapException
     * @throws Throwable
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function testSmta(UnitTester $I): void
    {
        if (!$this->forceMigration) {
            $I->markTestSkipped('No test of Smta due to no migration');
            return;
        }

        $commandName = 'smta:register';
        $commandCreateSmtaRegister = $this->createCommandTester($commandName);

        $I->comment('Validate that order and item rows are populated as expected.');

        $expectedOrderArr = is_array($this->smtaTableData) && array_key_exists('order', $this->smtaTableData)
            ? $this->smtaTableData['order']
            : [['id' => null]];
        $orderIds = array_map(fn($order) => (int)$order['id'], $expectedOrderArr);

        $dbOrderRows = $this->dbMapService->getTableQuery('table_order')
            ?->where(['id' => $orderIds])
            ->createCommand()->queryAll();

        $expOrd = array_map(fn($row) => array_values($row), $expectedOrderArr);
        $resOrd = array_map(fn($row) => array_values($row), $dbOrderRows);

        $I->assertEquals(
            $expOrd,
            $resOrd
        );

        $dbItemRows = $this->dbMapService->getTableQuery('table_item')
            ?->select('orderid, sampleID, crop, pud, ancestry')
            ?->where(['orderid' => $orderIds /*[1,2,3]*/])
            ->orderBy('id, orderid')
            ->createCommand()->queryAll();

        $expectedItemArr = is_array($this->smtaTableData) && array_key_exists('item', $this->smtaTableData)
            ? array_map(
                function (array $row): array {
                    unset($row['id']);
                    return $row;
                },
                $this->smtaTableData['item']
            )
            : [];
        $I->assertEquals(
            array_map(fn($row) => array_values($row), $expectedItemArr),
            array_map(fn($row) => array_values($row), $dbItemRows)
        );

        $dryRunMatchesArray = $this->makeDryRunAndFetchXml($I, $commandCreateSmtaRegister, $commandName, 'smta');

        $output = $this->makeTestServerCtrlCommandCallAndFetchOutput('read-stats');

        $re = '/\|\s+Proc:(?P<proc>\d+)\s+\|/m';
        preg_match($re, $output, $matches);
        $proc = (int)($matches['proc']);

        $I->amGoingTo('run argument command smta:register call.');

        $cntNoOfOrders = is_array($this->smtaTableData) && array_key_exists('order', $this->smtaTableData)
            ? count($this->smtaTableData['order'])
            : 0;

        $cntNoOfOrders = 3;

        $input = [
            '--run-as' => 'XXX001',
            '--test' => true,
            '--order-by' => 'date',
            '--limit' => $cntNoOfOrders,
        ];
        $opt = [
            'verbosity' => /*OutputInterface::VERBOSITY_NORMAL*/ OutputInterface::VERBOSITY_VERY_VERBOSE
        ];

        $I->assertSame(ExitCode::OK, $commandCreateSmtaRegister->execute($input, $opt));

        $output = $commandCreateSmtaRegister->getDisplay(true);
        codecept_debug($output);
        $I->assertStringContainsString('completed', $output);

        $I->assertStringContainsString(
            '| smta:register  | XXX001 | completed | 3     | 0       | 3    | 0        | 3        |',
            $output
        );

        $I->amGoingTo('check that Server has processed all 3 orders and all 3 ware created.');

        // Add the number of expected already processed GLIS.
        $proc += 3;

        $output = $this->makeTestServerCtrlCommandCallAndFetchOutput('read-stats');
        $I->assertStringContainsString('Proc:' . (string)$proc . ' ', $output);
        $I->assertStringContainsString('Smta (C:3,', $output);

        $I->amGoingTo(
            'check that Server has identified 8 items as general "under MLS", and 1 item specified as MLS 14.'
        );
        $I->assertStringContainsString('Mls: [0:0, 1:8, 11:0, 12:0, 13:0, 14:1, 15:0]', $output);
    }

    /**
     * @param UnitTester $I
     * @return void
     */
    public function testTestServerFinalTests(UnitTester $I): void
    {
        if ($this->doStartServer) {
            $I->amGoingTo('stop Server in background.');
            $output = $this->makeTestServerCtrlCommandCallAndFetchOutput('stop');

            $I->amGoingTo('test that Server is inactive.');
            $output = $this->makeTestServerCtrlCommandCallAndFetchOutput('status');

            $I->assertEquals("Server is inactive\n", $output);
        }
    }

    /**
     * @param string $commandName
     * @return CommandTester
     */
    private function createCommandTester(string $commandName): CommandTester
    {
        $module = explode(':', $commandName)[0];
        $this->faoConfig->getDatabases()->setSelectedModule($module);
        $app = new Application();
        $loader = new ContainerCommandLoader(
            $this->container,
            $this->params['yiisoft/yii-console']['commands']
        );
        $app->setCommandLoader($loader);
        $command = $app->find($commandName);
        return new CommandTester($command);
    }

    /**
     * @param UnitTester $I
     * @param CommandTester $commandTester
     * @param string $commandName
     * @param string $rootTag
     * @param array|null $input
     * @return array<int,array<array-key,mixed>>
     */
    private function makeDryRunAndFetchXml(
        UnitTester $I,
        CommandTester $commandTester,
        string $commandName,
        string $rootTag,
        ?array $input = []
    ): array {
        $I->amGoingTo("run argument command $commandName dry-run call.");

        $input = array_replace(
            [
                '--run-as' => 'XXX001',
                '--test' => true,
                '--dry-run' => true,
                '--order-by' => 'date',
                '--limit' => 3,
            ],
            $input ?? []
        );
        $opt = [
            'verbosity' => OutputInterface::VERBOSITY_VERY_VERBOSE
        ];

        $I->assertSame(ExitCode::OK, $commandTester->execute($input, $opt));

        $output = $commandTester->getDisplay(true);
        $I->assertStringContainsString('completed', $output);

        return $this->extractXmlFromOutput($output, $rootTag);
    }

    /**
     * @param string $output
     * @param string $rootTag
     * @return array<int,array<array-key,mixed>>
     */
    private function extractXmlFromOutput(string $output, string $rootTag): array
    {
        // Extract xml from output
        $re = '/(?P<xml><\?xml version="1.0" encoding="UTF-8"\?>\s<'
            . $rootTag . '[^>]+?>[\s\S]*?<\/' . $rootTag . '>)/m';
        preg_match_all($re, $output, $matches, PREG_SET_ORDER, 0);
        //preg_match($re, $output, $matches);

        $matchesXml = [];
        foreach ($matches as $index => $match) {
            /** @var array<array-key, mixed> $row */
            $row = $this->parser->decodeXml($match['xml']);
            $matchesXml[] = array_merge(['id' => ($index + 1)], $row);
        }
        unset($matches);
        return $matchesXml;
    }

    /**
     * @param string $commandArgument
     * @return string
     */
    private function makeTestServerCtrlCommandCallAndFetchOutput(string $commandArgument): string
    {
        $process = new Process(['./yii', 'server:ctrl', $commandArgument ,'--test', '--no-ansi']);
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output = $process->getOutput();
        codecept_debug($output);
        return $output;
    }
}
