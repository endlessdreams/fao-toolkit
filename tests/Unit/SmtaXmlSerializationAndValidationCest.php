<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use EndlessDreams\FaoToolkit\Entity\Base\Actor;
use EndlessDreams\FaoToolkit\Entity\Smta\Smta;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use DOMDocument;
use ErrorException;
use Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use App\Tests\Support\UnitTester;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validation;
use Yiisoft\Aliases\Aliases;

/**
 *
 */
class SmtaXmlSerializationAndValidationCest
{
    use CestUtilTrait;

    /**
     * @var bool
     */
    private bool $isInjected = false;

    /**
     * @var Aliases|null
     */
    private ?Aliases $aliases;

    /**
     * @param UnitTester $I
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws ErrorException
     * @throws \Yiisoft\Definitions\Exception\InvalidConfigException
     */
    public function _before(UnitTester $I): void
    {
//        $runner = new ConsoleApplicationRunner(
//            rootPath: dirname(__DIR__, 2),
//            environment: $_ENV['YII_ENV']
//        );
//        $this->config = $runner->getConfig();
//
//        $this->container = $runner->getContainer();
//
//        $this->params = $this->config->get('params');
//        $this->faoConfig = $this->container->get(FaoConfig::class);
//
//        $this->aliases = $this->container->get(Aliases::class);
//
//        $this->parser = $this->container->get(ParserService::class);
//
//        $this->validator = $this->parser->getValidator();

        if ($this->isInjected) {
            return;
        }
        codecept_debug('_before');
        try {
            $this->setUpContainer();
            $this->setUpFaoConfig('smta', 'test', 'smta:register');
            //$this->moduleServiceClass?->setFaoInstituteCodeQueryColumn('location.wiews');
            $this->aliases = $this->container->get(Aliases::class);
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface | ErrorException $e) {
            codecept_debug($e->getMessage());
        }
        $this->isInjected = true;
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testNormalizeArrayAndValidate(UnitTester $I): void
    {
        $arr = [
            'id' => '198',
            'symbol' => 'order-50',
            'date' => '2023-09-290',
            'type' => 'sw',
            'language' => 'xx',
            'shipName' => 'Valdemar Agard',
            'provider' =>
                [
                    'type' => 'or',
                    'pid' => '00AA00',
                    'name' => 'Acme',
                    'address' => 'Acme street',
                    'country' => 'XXX',
                    'email' => 'contact@acme.institute',
                ],
            'recipient' =>
                [
                    'type' => 'ori',
                    'name' => 'Birk Nilsen',
                    'address' => 'Magnes Gade 10,
8500 Roskilde',
                    'country' => 'DNK',
                ],
            'document' =>
                [
                    'location' => 's',
                    'retInfo' => 'Contact the Secreteriat to retrieve an SMTA',
                    'pdf' => null,
                ],
            'annex1' =>
                [
                    'material' =>
                        [
                            0 =>
                                [
                                    'crop' => 'Lespedeza',
                                    'sampleID' => 'BDF 475',
                                    'pud' => 'n',
                                    'ancestry' => null,
                                ],
                        ],
                ],
        ];


        /** @var Smta $obj */
        $obj = $this->faoConfig->getParser()?->denormalize($arr, Smta::class, 'xml');
        //codecept_debug($obj);

        //codecept_debug($this::class);


        /** @var array<string,array<int,string>> $validateArr */
        $validateArr = $this->faoConfig->getParser()?->validateEntity($obj);

        codecept_debug($validateArr);

        //codecept_debug(ChoiceHelper::getSmtaLanguageChoices());
        //codecept_debug($obj->getLanguage());

        $validateArr2 = $this->faoConfig->getParser()?->getValidator()
            ->validate(
                $obj,
                null
            );
        codecept_debug($validateArr2);
    }


    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testNormalizeArrayAndValidateWithPdfUrl(UnitTester $I): void
    {
        $arr = [
            'id' => '198',
            'symbol' => 'order-50',
            'date' => '2023-09-29',
            'type' => 'sw',
            'language' => 'en',
            'shipName' => 'Valdemar Agard',
            'provider' =>
                [
                    'type' => 'or',
                    'pid' => '00AA00',
                    'name' => 'Acme',
                    'address' => 'Acme street',
                    'country' => 'SWE',
                    'email' => 'contact@acme.institute',
                ],
            'recipient' =>
                [
                    'type' => 'ori',
                    'name' => 'Birk Nilsen',
                    'address' => 'Magnes Gade 10,
8500 Roskilde',
                    'country' => 'DNK',
                ],
            'document' =>
                [
                    'location' => 's',
                    'retInfo' => 'Contact the Secreteriat to retrieve an SMTA',
                    'pdfUrl' => 'file:@root/docs/pdfs/fao_smta_api_spec.pdf',
                ],
            'annex1' =>
                [
                    'materials' =>
                        [
                            0 =>
                                [
                                    'crop' => 'Lespedeza',
                                    'sampleID' => 'BDF 475',
                                    'pud' => 'n',
                                    'ancestry' => null,
                                ],
                        ],
                ],
        ];

        /** @var Smta $obj */
        $obj = $this->faoConfig->getParser()?->denormalize($arr, Smta::class, 'xml');

        $pdfUrl = $obj->getDocument()?->getPdfUrl();

        $documentPdfUri = StringHelper::resolveAbsolutePath($pdfUrl, $this->faoConfig->getAliases());

        $documentPdf = StringHelper::isUrlValid($documentPdfUri, ['http','https','file'])
            ? StringHelper::getBase64EncodedInlineFile((string)$documentPdfUri)
            : null;

        $obj->getDocument()?->setPdf($documentPdf);

        //XmlEncoder::CDATA_WRAPPING

        /** @var array<string,array<int,string>> $validateArr */
        $validateArr = $this->faoConfig->getParser()?->validateEntity($obj);

        codecept_debug($validateArr);

        //codecept_debug(ChoiceHelper::getSmtaLanguageChoices());
        //codecept_debug($obj->getLanguage());

        $validateArr2 = $this->faoConfig->getParser()?->getValidator()
            ->validate(
                $obj,
                null
            );
        codecept_debug($validateArr2);

        $serializedXml = $this->parser->serializeXml($obj, 'register', ['groups' => ['Default','register']]);

        $serializedCvs = $this->parser->serializeCsv($obj);
        $serializedCvs2 = $this->parser->serializeCsv($obj, ['groups' => ['Default','url']]);
        $serializedXml2 = $this->parser->getSerializer()->serialize(
            $obj,
            'xml',
            [
                'groups' => ['Default','register'],
                'xml_root_node_name' => 'register',
                XmlEncoder::CDATA_WRAPPING => true,
                'xml_format_output' => true,
                'xml_encoding' => 'UTF-8',
                AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
                'load_options' => \LIBXML_NONET | \LIBXML_NOEMPTYTAG | \LIBXML_NOBLANKS,
                'save_options' => \LIBXML_NONET | \LIBXML_NOEMPTYTAG | \LIBXML_NOBLANKS/* | LIBXML_COMPACT */,
            ]
        );
        codecept_debug($serializedXml);
    }

    /**
     * @param UnitTester $I
     * @return void
     */
    public function test1(UnitTester $I): void
    {

        $arr = [
            'pid' => 'x',
        ];

        $xml = <<<XML
<actor>
<pid>xxxxxxxxxxxxxxxxxxxx</pid>
</actor>
XML;

        // new AnnotationLoader(new AnnotationReader())
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());

        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        $serializer = new Serializer(
            [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)],
            [
                'json' => new JsonEncoder(),
                'xml' => new XmlEncoder(),
                'csv' => new CsvEncoder()
            ]
        );

        $classValidatorMetadataFactory =
            new LazyLoadingMetadataFactory(
                new \Symfony\Component\Validator\Mapping\Loader\AttributeLoader()
            );
        $validator = Validation::createValidatorBuilder()
            ->setMetadataFactory($classValidatorMetadataFactory)
            ->getValidator();

        /** @var Actor $obj */
        $obj = $this->faoConfig->getParser()
            ->deserializeXml($xml, Actor::class, 'actor');
        codecept_debug($obj);

        //$validateArr = $this->faoConfig?->getParser()?->validateEntity($obj);
        $validateArr = $this->faoConfig?->getParser()?->getValidator()
            ->validate($obj, null, ['Default']);

        codecept_debug($validateArr);
    }

    /**
     * @param UnitTester $I
     * @return void
     */
    public function testDeserialize(UnitTester $I): void
    {
        $expectedXml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<smta>
    <symbol>order-50</symbol>
    <date>2023-09-290</date>
    <type>sw</type>
    <language>xx</language>
    <shipName>Valdemar Agard</shipName>
    <provider>
        <type>or</type>
        <pid>00AA00</pid>
        <name>Acme</name>
        <address>Acme street</address>
        <country>XXX</country>
        <email>contact@acme.institute</email>
    </provider>
    <recipient>
        <type>ori</type>
        <name>Birk Nilsen</name>
        <address>Magnes Gade 10,
8500 Roskilde</address>
        <country>DNK</country>
    </recipient>
    <document>
        <location>s</location>
        <retInfo>Contact the Secreteriat to retrieve an SMTA</retInfo>
    </document>
    <annex1>
        <material>
            <crop>Lespedeza</crop>
            <sampleID>BDF 475</sampleID>
            <pud>n</pud>
        </material>
    </annex1>
</smta>
XML;

        $rootNode = 'smta';
        $groups = ['groups' => ['Default','Smta']];
        $obj = $this->parser->deserializeXml($expectedXml, Smta::class, $rootNode, $groups);

        $actualXml = $this->parser->serializeXml($obj, $rootNode, $groups);
        $expectedDomXml = new DOMDocument();
        $expectedDomXml->loadXML($expectedXml);
        $expectedXml = $expectedDomXml->saveXML();
        $actualDomXml = new DOMDocument();
        $actualDomXml->loadXML($expectedXml);
        $actualXml = $actualDomXml->saveXML();
        $I->assertXmlStringEqualsXmlString($expectedXml, $actualXml);
    }

    /**
     * @param UnitTester $I
     * @return void
     */
    public function testDecode(UnitTester $I): void
    {
        $expectedXml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<smta>
    <symbol>order-50</symbol>
    <date>2023-09-290</date>
    <type>sw</type>
    <language>xx</language>
    <shipName>Valdemar Agard</shipName>
    <provider>
        <type>or</type>
        <pid>00AA00</pid>
        <name>Acme</name>
        <address>Acme street</address>
        <country>XXX</country>
        <email>contact@acme.institute</email>
    </provider>
    <recipient>
        <type>ori</type>
        <name>Birk Nilsen</name>
        <address>Magnes Gade 10,
8500 Roskilde</address>
        <country>DNK</country>
    </recipient>
    <document>
        <location>s</location>
        <retInfo>Contact the Secreteriat to retrieve an SMTA</retInfo>
    </document>
    <annex1>
        <material>
            <crop>Lespedeza</crop>
            <sampleID>BDF 475</sampleID>
            <pud>n</pud>
        </material>
    </annex1>
</smta>
XML;

        $rootNode = 'smta';
        $groups = ['groups' => ['Default','Smta']];
        $arr = $this->parser->decodeXml($expectedXml, $rootNode, $groups);

        $actualXml = $this->parser->encodeXml($arr, $rootNode, $groups);
        $expectedDomXml = new DOMDocument();
        $expectedDomXml->loadXML($expectedXml);
        $expectedXml = $expectedDomXml->saveXML();
        $actualDomXml = new DOMDocument();
        $actualDomXml->loadXML($expectedXml);
        $actualXml = $actualDomXml->saveXML();

        $I->assertXmlStringEqualsXmlString($expectedXml, $actualXml);
    }
}
