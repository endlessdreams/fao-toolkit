<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use Codeception\Exception\InjectionException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Database;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Service\Helper\ArrayJsonHelper;
use EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use ErrorException;
use Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Tests\Support\UnitTester;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Definitions\Exception\InvalidConfigException;
use Yiisoft\Yii\Runner\Console\ConsoleApplicationRunner;

/**
 *
 */
class GlisCsvDenormalizationAndValidationCest
{
    use CestUtilTrait;

    /**
     * @var Aliases|null
     */
    private ?Aliases $aliases;

    /**
     * @var array|null
     */
    private ?array $table = null;

    /**
     * @var array|null
     */
    private ?array $tableIndex = null;

    /**
     * @var ModuleCommandServiceInterface|null
     */
    private ?ModuleCommandServiceInterface $moduleServiceClass;

    /**
     * @var string|null
     */
    private ?string $mainClass = null;

    /**
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws ErrorException
     * @throws InvalidConfigException
     */
    protected function _inject(): void
    {
        // Run these lines only once...
        if (!empty($this->table)) {
            return;
        }
        $this->setUpContainer();
        $this->setUpFaoConfig('glis', 'test', 'glis:register');
        $this->moduleServiceClass?->setFaoInstituteCodeQueryColumn('location.wiews');
        $this->populateTableRows(filePath: './tests/Support/Data/glis/glisWithValidationErrors.csv');
        $this->parseAndIndexTableRows();
        $this->mainClass = Glis::class;
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateNotApplicableDescriptors(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier N/A - Such as whole expected
        valid GLIS and other that are marked N/A in specification document of GLIS.');

        $this->testDescriptorIdentifierGroup($I, 'N/A');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateM01(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier M01 - location.Actor.');

        $this->testDescriptorIdentifierGroup($I, 'M01');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateM02(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier M02 - sampleid.');

        $this->testDescriptorIdentifierGroup($I, 'M02');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateM03(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier M03 - sampleid.');

        $this->testDescriptorIdentifierGroup($I, 'M03');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateM04(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier M04 - method.');

        $this->testDescriptorIdentifierGroup($I, 'M04');
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateM05(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier M05 - genus-species-cropname.');

        $this->testDescriptorIdentifierGroup($I, 'M05', null/*, [26 => 'cropnames', 27 => 'genus']*/);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR01(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R01 - target.');

        $this->testDescriptorIdentifierGroup($I, 'R01', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR02(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R02 - progdois.');

        $this->testDescriptorIdentifierGroup($I, 'R02', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR03(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R03 - biostatus.');

        $this->testDescriptorIdentifierGroup($I, 'R03', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR04(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R04 - spauth-subtaxa-stauth.');

        $this->testDescriptorIdentifierGroup($I, 'R04', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR05(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R05 - names.');

        $this->testDescriptorIdentifierGroup($I, 'R05', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR06(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R06 - ids');

        $this->testDescriptorIdentifierGroup($I, 'R06', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR07(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R07 - mlsstatus.');

        $this->testDescriptorIdentifierGroup($I, 'R07', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateR08(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier R08 - historical.');

        $this->testDescriptorIdentifierGroup($I, 'R08', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA01(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A01 - acquisition.provider.');

        $this->testDescriptorIdentifierGroup($I, 'A01', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA01Register(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A01 - acquisition.provider.');

        $this->testDescriptorIdentifierGroup($I, 'A01', ['register']);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA02(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A02 - acquisition.sampleid.');

        $this->testDescriptorIdentifierGroup($I, 'A02', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA03(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A03 - acquisition.provenance.');

        $this->testDescriptorIdentifierGroup($I, 'A03', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA04(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A04 - collection.collectors.');

        $this->testDescriptorIdentifierGroup($I, 'A04', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA05(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A05 - collection.sampleid.');

        $this->testDescriptorIdentifierGroup($I, 'A05', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA06(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A06 - collection.missid.');

        $this->testDescriptorIdentifierGroup($I, 'A06', null);
    }
    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA07(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A07 - collection.site.');

        $this->testDescriptorIdentifierGroup($I, 'A07', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA08(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A08 - collection.lat.');

        $this->testDescriptorIdentifierGroup($I, 'A08', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA09(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A09 - collection.lon.');

        $this->testDescriptorIdentifierGroup($I, 'A09', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA10(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A10 - collection.uncert.');

        $this->testDescriptorIdentifierGroup($I, 'A10', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA11(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A11 - collection.datum.');

        $this->testDescriptorIdentifierGroup($I, 'A11', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA12(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A12 - collection.georef.');

        $this->testDescriptorIdentifierGroup($I, 'A12', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA13(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A13 - collection.elevation.');

        $this->testDescriptorIdentifierGroup($I, 'A13', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA14(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A14 - collection.date.');

        $this->testDescriptorIdentifierGroup($I, 'A14', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA15(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A15 - collection.source.');

        $this->testDescriptorIdentifierGroup($I, 'A15', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA16(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A16 - breeding.breeders.');

        $this->testDescriptorIdentifierGroup($I, 'A16', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateA17(UnitTester $I): void
    {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier A17 - breeding.ancestry.');

        $this->testDescriptorIdentifierGroup($I, 'A17', null);
    }

    /**
     * @param UnitTester $I
     * @return void
     * @throws Exception
     */
    public function testDeserializeCSVWithExpectedErrorsThenValidateNotApplicableDescriptorsWhenGroupRegister(
        UnitTester $I
    ): void {
        $I->comment('This test will read a CSV file that each row represent a GLIS entity and two extra
        columns with expected validation errors and one with the purpose.');
        $I->comment('The asserts will be between expected and actual validation errors.');
        $I->comment('This test will make asserts on Descriptor Identifier N/A with group register is forced.');

        $this->testDescriptorIdentifierGroup($I, 'N/A', ['register']);
    }

    /**
     * @param UnitTester $I
     * @param string $did
     * @param array|null $groups
     * @param array|null $keysToUnset
     * @return void
     * @throws Exception
     */
    private function testDescriptorIdentifierGroup(
        UnitTester $I,
        string $did,
        ?array $groups = null,
        ?array $keysToUnset = []
    ): void {
        $class = Glis::class;

        $firstGroup = empty($groups) ? null : $groups[0];

        $objs = $this->parser->getSerializer()->denormalize(
            $this->getTableRows($firstGroup, $did)/*$this->table*/,
            $class . '[]',
            'csv'
        );

        if (!isset($objs) || !is_array($objs)) {
            $I->fail('Failed to parse table rows to an array of Entities.');
        }

        foreach ($objs as $index => $obj) {

            /** @var array<string,array<int,string>> $validationResult */
            $validationResult = $this->parser->validateEntity($obj, $groups ?? ['Default','Glis']);

            foreach ($validationResult as $validationGroup => &$validationMessage) {
                $validationMessage = preg_replace('~[\r\n]+~', '', $validationMessage);
            }
            $error = &$this->tableIndex[$firstGroup][$did][$index];

            $error['actual'] = $validationResult;

            $I->amGoingTo($error['purpose']);
            $I->assertEquals($error['expected'], $error['actual']);
        }
    }

    /**
     * @param string|null $firstGroup
     * @param string $did
     * @return array
     */
    private function getTableRows(?string $firstGroup, string $did): array
    {
        $arr = empty($firstGroup) ? $this->tableIndex[null] : $this->tableIndex[$firstGroup];
        return array_filter(
            $this->table,
            fn($index) => array_key_exists($did, $arr) && array_key_exists($index, $arr[$did]),
            ARRAY_FILTER_USE_KEY
        );
    }

    /**
     * @param string $filePath
     * @return void
     */
    private function populateTableRows(string $filePath): void
    {
        $data = file_get_contents($filePath);
        $this->table = (array)$this->parser->getSerializer()->decode(
            $data,
            'csv',
            [
                CsvEncoder::DELIMITER_KEY => "\t",
            ]
        );
    }

    /**
     * @return void
     */
    private function parseAndIndexTableRows(): void
    {
        $this->tableIndex = [];

        foreach ($this->table ?? [] as $index => &$arr) {
            $group = array_key_exists('group', $arr) ? $arr['group'] : null;

            if (!array_key_exists($group, $this->tableIndex)) {
                $this->tableIndex[$group] = [];
            }
            $did = $arr['did'];
            if (!array_key_exists($did, $this->tableIndex[$group])) {
                $this->tableIndex[$group][$did] = [];
            }
            //$this->tableIndex[$group][$did][$index] = [];

            if (
                array_key_exists('targets', $arr)
                && array_key_exists('target', $arr['targets'])
                && count($arr['targets']['target']) === 1
                && array_key_exists('value', ($target = $arr['targets']['target'][0]))
                && empty($target['value'])
                && array_key_exists('kws', $target)
                && array_key_exists('kw', $target['kws'])
                && count($kw = $target['kws']['kw']) === 1
                && array_key_exists('#', $kw[0])
                && empty($kw[0]['#'])
            ) {
                unset($this->table[$index]['targets']);
            }
            $expected = $arr['expected_validation_errors'];
            $purpose = $arr['purpose_of_test'];
            unset($arr['expected_validation_errors']);
            unset($arr['purpose_of_test']);
            $this->tableIndex[$group][$did][$index] = [
                'purpose' => $purpose,
                'expected' => $expected,
                'actual' => null
            ];
            ArrayJsonHelper::parseJsonKeyValues($this->tableIndex[$group][$did][$index], ['expected']);

            if (
                array_key_exists('progdois', $arr)
                && !empty($arr['progdois'])
            ) {
                ArrayJsonHelper::parseJsonKeyValues($this->table[$index], ['progdois']);
            } else {
                unset($this->table[$index]['progdois']);
            }
            if (
                array_key_exists('collection', $arr)
                && !empty($arr['collection'])
            ) {
                ArrayJsonHelper::parseJsonKeyValues($this->table[$index], ['collection']);
            } else {
                unset($this->table[$index]['collection']);
            }
            if (
                array_key_exists('breeding', $arr)
                && !empty($arr['breeding'])
            ) {
                ArrayJsonHelper::parseJsonKeyValues($this->table[$index], ['breeding']);
            } else {
                unset($this->table[$index]['breeding']);
            }
        }
    }
}
