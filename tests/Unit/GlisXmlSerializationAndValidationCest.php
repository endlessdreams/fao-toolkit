<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\src\Trait\CestUtilTrait;
use EndlessDreams\FaoToolkit\Entity\Glis\Acquisition;
use EndlessDreams\FaoToolkit\Entity\Base\Actor;
use EndlessDreams\FaoToolkit\Entity\Glis\Breeders;
use EndlessDreams\FaoToolkit\Entity\Glis\Breeding;
use EndlessDreams\FaoToolkit\Entity\Glis\Collection;
use EndlessDreams\FaoToolkit\Entity\Glis\Collectors;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Entity\Glis\Ids;
use EndlessDreams\FaoToolkit\Entity\Glis\Kw;
use EndlessDreams\FaoToolkit\Entity\Glis\Kws;
use EndlessDreams\FaoToolkit\Entity\Glis\Names;
use EndlessDreams\FaoToolkit\Entity\Glis\Register;
use EndlessDreams\FaoToolkit\Entity\Glis\Target;
use EndlessDreams\FaoToolkit\Entity\Glis\Targets;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use ErrorException;
use Exception;
use App\Tests\Support\UnitTester;
use Generator;
use Iterator;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Definitions\Exception\InvalidConfigException;

/**
 *
 */
class GlisXmlSerializationAndValidationCest
{
    use CestUtilTrait;

    /**
     * @var mixed|Aliases
     */
    private mixed $aliases;

    /**
     * @var array|string[]
     */
    private array $xmls = [
        Targets::class => '<?xml version="1.0" encoding="UTF-8"?>
        <targets>
            <target>
                <value>https://nordic-baltic-genebanks.org/gringlobal/AccessionDetail.aspx?id=2230</value>
                <kws>
                    <kw>1</kw>
                </kws>
            </target>
            <target>
                <value>https://nordic-baltic-genebanks.org/gringlobal/method.aspx?id=342</value>
                <kws>
                    <kw>2</kw>
                    <kw>3</kw>
                </kws>
            </target>
        </targets>',
        Target::class => '<?xml version="1.0" encoding="UTF-8"?>
        <target>
            <value>https://nordic-baltic-genebanks.org/gringlobal/AccessionDetail.aspx?id=2230</value>
            <kws>
                <kw>1</kw>
            </kws>
        </target>',
        Kw::class => '<?xml version="1.0" encoding="UTF-8"?>
            <kw>1</kw>
        ',
        Kws::class => '<?xml version="1.0" encoding="UTF-8"?>
        <kws>
            <kw>1</kw>
        </kws>',
        Actor::class => '<?xml version="1.0" encoding="UTF-8"?>
        <actor>
            <wiews>XXX001</wiews>
            <pid>AA00AA</pid>
            <name>ACME</name>
            <address>Main street 1, XX-0001 Creekvile</address>
            <country>XXX</country>
        </actor>',
        Acquisition::class => '
        <acquisition>
            <provider>
                <wiews>XXX002</wiews>
                <pid>AA00AB</pid>
                <name>NOT ACME</name>
                <address>Second street 1, XX-0001 Slipsvile</address>
                <country>XXX</country>
            </provider>
            <sampleid>ACME 1</sampleid>
            <provenance>XXX</provenance>
        </acquisition>',
        Breeding::class => '
        <breeding>
            <breeders>
                <breeder>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </breeder>
                <breeder>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </breeder>
            </breeders>
            <ancestry>------</ancestry>
        </breeding>
        ',
        Breeders::class => '
            <breeders>
                <breeder>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </breeder>
            </breeders>
        '
        ,
        Collectors::class => '
            <collectors>
                <collector>
                    <wiews>XXX003</wiews>
                    <pid>AA00AC</pid>
                    <name>NOT EVEN ACME</name>
                    <address>Third street 1, XX-0001 Slipsvile</address>
                    <country>XXX</country>
                </collector>
            </collectors>
        '
        ,
        Collection::class => '
            <collection>
                <collectors>
                    <collector>
                        <wiews>XXX003</wiews>
                        <pid>AA00AC</pid>
                        <name>NOT EVEN ACME</name>
                        <address>Third street 1, XX-0001 Slipsvile</address>
                        <country>XXX</country>
                    </collector>
                </collectors>
                <sampleid>NACME 2</sampleid>
                 <missid>[missid]</missid>
                 <site>[site]</site>
                 <lat>[clat]</lat>
                 <lon>[clon]</lon>
                 <uncert>[uncert]</uncert>
                 <datum>[datum]</datum>
                 <georef>[georef]</georef>
                 <elevation>[elevation]</elevation>
                 <date>[cdate]</date>
                 <source>[source]</source>
            </collection>
        '
        , Names::class => '
            <names>
                <name>name1</name>
                <name>name2</name>
            </names>
        '
        , Names::class . '2' => '
            <names>
                <name>[name]</name>
                <name>[name]</name>
            </names>
        '
        , Ids::class => '
            <ids>
                <id type="sgsvid">239741</id>
                <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
            </ids>
        '
        ,
        Register::class . '1' => '<?xml version="1.0" encoding="UTF-8" ?>
            <register username="cgn" password="Passw0rd">
                <location>
                    <pid>00AC55</pid>
                </location>
                <sampledoi>[sampledoi]</sampledoi>
                <sampleid>CGN00001</sampleid>
                <date>1986-05-12</date>
                <method>acqu</method>
                <genus>Hordeum</genus>
                <cropnames>
                    <name>Barley</name>
                </cropnames>
                <targets>
                    <target>
                        <value>http://www.nordgen.org/sgsv/index.php?unit_id=239741</value>
                        <kws>
                            <kw>5</kw>
                        </kws>
                    </target>
                </targets>
                <biostatus>300</biostatus>
                <species>vulgare</species>
                <spauth>L.</spauth>
                <subtaxa>subsp. vulgare</subtaxa>
                <stauth>L.</stauth>
                <names>
                    <name>Maartsche Gerst</name>
                </names>
                <ids>
                    <id type="sgsvid">239741</id>
                    <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
                </ids>
                <mlsstatus>1</mlsstatus>
                <historical>n</historical>
                <acquisition>
                    <provider>
                        <wiews>NLD078</wiews>
                    </provider>
                    <sampleid>GZ A1</sampleid>
                    <provenance>NLD</provenance>
                </acquisition>
                <collection>
                    <collectors>
                        <collector>
                            <wiews>[cwiews]</wiews>
                            <pid>[cpid]</pid>
                            <name>[cname]</name>
                            <address>[caddress]</address>
                            <country>[ccountry]</country>
                        </collector>
                    </collectors>
                    <sampleid>[csampleid]</sampleid>
                    <missid>[missid]</missid>
                    <site>[site]</site>
                    <lat>[clat]</lat>
                    <lon>[clon]</lon>
                    <uncert>[uncert]</uncert>
                    <datum>[datum]</datum>
                    <georef>[georef]</georef>
                    <elevation>[elevation]</elevation>
                    <date>[cdate]</date>
                    <source>source</source>
                </collection>
                <breeding>
                    <breeders>
                        <breeder>
                            <wiews>[wiews]</wiews>
                            <pid>[pid]</pid>
                            <name>[name]</name>
                            <address>[address]</address>
                            <country>[country]</country>
                        </breeder>
                        <breeder>
                            <wiews>[wiews]</wiews>
                            <pid>[pid]</pid>
                            <name>[name]</name>
                            <address>[address]</address>
                            <country>[country]</country>
                        </breeder>
                    </breeders>
                    <ancestry>[ancestry]</ancestry>
                </breeding>
            </register>
        '
        ,
        Register::class => '<?xml version="1.0" encoding="UTF-8" ?>
            <register username="cgn" password="Passw0rd">
                <location>
                    <pid>00AC55</pid>
                </location>
                <sampledoi>[sampledoi]</sampledoi>
                <sampleid>CGN00001</sampleid>
                <date>1986-05-12</date>
                <method>acqu</method>
                <genus>Hordeum</genus>
                <cropnames>
                    <name>Barley</name>
                </cropnames>
                <targets>
                    <target>
                        <value>http://www.nordgen.org/sgsv/index.php?unit_id=239741</value>
                        <kws>
                            <kw>5</kw>
                        </kws>
                    </target>
                </targets>
                <biostatus>300</biostatus>
                <species>vulgare</species>
                <spauth>L.</spauth>
                <subtaxa>subsp. vulgare</subtaxa>
                <stauth>L.</stauth>
                <names>
                    <name>Maartsche Gerst</name>
                </names>
                <ids>
                    <id type="sgsvid">239741</id>
                    <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
                </ids>
                <mlsstatus>1</mlsstatus>
                <historical>n</historical>
                </register>
        '
    ];
    private ?\EndlessDreams\FaoToolkit\Service\ModuleCommand\ModuleCommandServiceInterface $moduleServiceClass;

    /**
     * @param UnitTester $I
     * @return void
     * @throws ErrorException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws InvalidConfigException
     */
    public function _before(UnitTester $I): void
    {
        if (!empty($this->table)) {
            return;
        }
        $this->setUpContainer();
        $this->setUpFaoConfig('glis', 'test', 'glis:register');
        $this->moduleServiceClass = $this->faoConfig->getParser()
            ?->moduleCommandRegister
            ?->getService('glis:register');
        $this->moduleServiceClass?->setFaoInstituteCodeQueryColumn('location.wiews');
        $this->aliases = $this->container->get(Aliases::class);
    }

    // tests

/*
    public function testDeserializeThenValidate(UnitTester $I): void
    {
        $I->expectTo('be able to deserialize and then validate.');
        //$filepath = realpath('./tests/Support/Data/glis/validation1.xml');

        //$xml = file_get_contents($filepath);
        //foreach ($this->xmls as $class => $expectedXml) {
        $xml = $this->xmls[Register::class];

        $this->testDeserializeModel($I, $xml, Register::class);
        //}

    }
*/

    /**
     * @var array|\array[][]
     */
    private array $entityTestData = [
        [
            Actor::class => [
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
        <actor>
            <wiews></wiews>
            <pid></pid>
            <name></name>
            <address></address>
            <country></country>
        </actor>',
                    'expectedErrors' => [
                        /*"Either Wiews, Pid or all Name, Address and Country together be valid"*/
                    ]
                ],
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
        <actor>
            <wiews></wiews>
            <pid></pid>
            <name>NOT EVEN ACME</name>
            <address></address>
            <country></country>
        </actor>',
                    'expectedErrors' => [
                        "Actor" => ["Either Wiews, Pid or all Name, Address and Country together be valid."],
                        "name-address-country" => ["Actor name, address or country must either all be in use or not."]
                    ]
                ],
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
        <actor>
            <wiews></wiews>
            <pid></pid>
            <name>NOT EVEN ACME</name>
            <address>Third street 1, XX-0001 Slipsvile</address>
            <country></country>
        </actor>',
                    'expectedErrors' => [
                        "Actor" => ["Either Wiews, Pid or all Name, Address and Country together be valid."],
                        "name-address-country" => ["Actor name, address or country must either all be in use or not."]
                    ]
                ],
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
        <actor>
            <wiews></wiews>
            <pid></pid>
            <name>NOT EVEN ACME</name>
            <address>Third street 1, XX-0001 Slipsvile</address>
            <country>GBR</country>
        </actor>',
                    'expectedErrors' => []
                ],
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
        <actor>
            <wiews></wiews>
            <pid></pid>
            <name>NOT EVEN ACME</name>
            <address>Third street 1, XX-0001 Slipsvile</address>
            <country>XAL</country>
        </actor>',
                    'expectedErrors' => []
                ],
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
        <actor>
            <wiews></wiews>
            <pid></pid>
            <name>NOT EVEN ACME</name>
            <address>Third street 1, XX-0001 Slipsvile</address>
            <country>XXX</country>
        </actor>',
                    'expectedErrors' => [
                        "Actor" => ["Either Wiews, Pid or all Name, Address and Country together be valid."],
                        "country" => [
                            "Country value should satisfy at least one of the following constraints: "
                            . "[1] This value \"XXX\" is not a valid country. "
                            . "[2] This value \"XXX\" is not a valid FAO extended country. "
                            . "[3] This value should be blank."
                        ]
                    ]
                ]
            ]
        ],
        [
            Actor::class . '[]' => [
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
                            <actor>
                                <wiews></wiews>
                                <pid></pid>
                                <name>NOT EVEN ACME</name>
                                <address>Third street 1, XX-0001 Slipsvile</address>
                                <country>GBR</country>
                            </actor>
                            <actor>
                                <wiews></wiews>
                                <pid></pid>
                                <name>NOT EVEN ACME</name>
                                <address>Third street 1, XX-0001 Slipsvile</address>
                                <country>XAL</country>
                            </actor>',
                    'expectedErrors' => []
                ]
            ]
        ],
        [
            Glis::class => [
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
<register username="cgn" password="Passw0rd">
    <location>
        <pid>00AC55</pid>
    </location>
    <sampleid>CGN00001</sampleid>
    <date>1986-05-12</date>
    <method>acqu</method>
    <genus>Hordeum</genus>
    <cropnames>
        <name>Barley</name>
    </cropnames>
    <targets>
        <target>
            <value>http://www.nordgen.org/sgsv/index.php?unit_id=239741</value>
            <kws>
                <kw>5</kw>
            </kws>
        </target>
    </targets>
    <biostatus>300</biostatus>
    <species>vulgare</species>
    <spauth>L.</spauth>
    <subtaxa>subsp. vulgare</subtaxa>
    <stauth>L.</stauth>
    <names>
        <name>Maartsche Gerst</name>
    </names>
    <ids>
        <id type="sgsvid">239741</id>
        <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
    </ids>
    <mlsstatus>1</mlsstatus>
    <historical>n</historical>
    <acquisition>
        <provider>
            <wiews>NLD078</wiews>
        </provider>
        <sampleid>GZ A1</sampleid>
        <provenance>NLD</provenance>
    </acquisition>
</register>',
                    'expectedErrors' => []
                ]

            ]
        ],
        [
            Glis::class => [
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
<register username="cgn" password="Passw0rd">
    <location>
        <pid>00AC55</pid>
    </location>
    <sampleid>CGN00001</sampleid>
    <date>1986-05-12</date>
    <method>acqu</method>
    <genus>Hordeum</genus>
    <cropnames>
        <name>Barley</name>
    </cropnames>
    <targets>
        <target>
            <value>http://www.nordgen.org/sgsv/index.php?unit_id=239741</value>
            <kws>
                <kw>5</kw>
            </kws>
        </target>
    </targets>
    <biostatus>300</biostatus>
    <species>vulgare</species>
    <spauth>L.</spauth>
    <subtaxa>subsp. vulgare</subtaxa>
    <stauth>L.</stauth>
    <names>
        <name>Maartsche Gerst</name>
    </names>
    <ids>
        <id type="sgsvid">239741</id>
        <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
    </ids>
    <mlsstatus>1</mlsstatus>
    <historical>n</historical>
    <acquisition>
        <provider>
            <wiews>NLD078</wiews>
        </provider>
        <sampleid>GZ A1</sampleid>
        <provenance>NLD</provenance>
    </acquisition>
</register>',
                    'expectedErrors' => []
                ],
                [
                    'xml' => '<?xml version="1.0" encoding="UTF-8" ?>
<register username="cgn" password="Passw0rd">
    <location>
        <pid>00AC55</pid>
    </location>
    <sampleid>CGN00001</sampleid>
    <date>1986-05-12</date>
    <method>acqu</method>
    <genus>Hordeum</genus>
    <cropnames>
        <name>Barley</name>
    </cropnames>
    <targets>
        <target>
            <value>http://www.nordgen.org/sgsv/index.php?unit_id=239741</value>
            <kws>
                <kw>5</kw>
            </kws>
        </target>
    </targets>
    <biostatus>300</biostatus>
    <species>vulgare</species>
    <spauth>L.</spauth>
    <subtaxa>subsp. vulgare</subtaxa>
    <stauth>L.</stauth>
    <names>
        <name>Maartsche Gerst</name>
    </names>
    <ids>
        <id type="sgsvid">239741</id>
        <id type="genesysuuid">urn:uuid:89e8a52d-5b79-4510-b209-af0f48254cea</id>
    </ids>
    <mlsstatus>1</mlsstatus>
    <historical>n</historical>
    <acquisition>
        <provider>
            <wiews>NLD078</wiews>
        </provider>
        <sampleid>GZ A1</sampleid>
        <provenance>NLD</provenance>
    </acquisition>
</register>',
                    'expectedErrors' => []
                ]
            ]
        ]
    ];

    /**
     * @throws Exception
     */
    public function testEntityDeserializeThenValidate(UnitTester $I): void
    {
        $class = Actor::class;
        foreach ($this->entityTestData[0][$class] as $actorXmlArray) {
            $I->expectTo('be able to deserialize and then validate.');
            $this->testDeserializeModel(
                $I,
                $actorXmlArray['xml'],
                $class,
                StringHelper::getRootTagFromXml((string)$actorXmlArray['xml']),
                $actorXmlArray['expectedErrors']
            );
        }

        $class = Actor::class . '[]';
        foreach ($this->entityTestData[1][$class] as $actorXmlArray) {
            $I->expectTo('be able to deserialize and then validate.');
            $this->testDeserializeModel(
                $I,
                $actorXmlArray['xml'],
                $class,
                StringHelper::getRootTagFromXml((string)$actorXmlArray['xml']),
                $actorXmlArray['expectedErrors']
            );
        }

        $class = Glis::class;
        foreach ($this->entityTestData[2][$class] as $actorXmlArray) {
            $I->expectTo('be able to deserialize and then validate.');
            codecept_debug($actorXmlArray);
            $this->testDeserializeModel(
                $I,
                $actorXmlArray['xml'],
                $class,
                StringHelper::getRootTagFromXml((string)$actorXmlArray['xml']),
                $actorXmlArray['expectedErrors']
            );
        }

        //$class = Glis::class;
        foreach ($this->entityTestData[3][$class] as $actorXmlArray) {
            $I->expectTo('be able to deserialize and then validate.');
            codecept_debug($actorXmlArray);
            $this->testDeserializeModel(
                $I,
                $actorXmlArray['xml'],
                $class,
                StringHelper::getRootTagFromXml((string)$actorXmlArray['xml']),
                $actorXmlArray['expectedErrors']
            );
        }
    }

    /**
     * @throws Exception
     */
    private function testDeserializeModel(
        UnitTester $I,
        string $expectedXml,
        string $class,
        ?string $root = null,
        ?array $expectedErrors = []
    ): void {
        $class_parts = explode('\\', $class);
        $entity = end($class_parts);
        if (!isset($root)) {
            $class_parts = explode('\\', $class);
            $root = strtolower(end($class_parts));
        }

        $addContext = [
            "groups" => (strtolower($entity) !== $root)
            ? [
                "Default",
                $entity,
                (string)$root
            ]
            : ["Default"],
            "save_options" => LIBXML_NOBLANKS,
            "xml_root_node_name" => $root
        ];

        $I->amGoingTo('deserialize xml to expected class ' . $class . '.');
        //codecept_debug($expectedXml);

        if (!$expectedXml) {
            throw new Exception('Empty body.');
        }

        try {
            codecept_debug('--------');
            $object = $this->parser->deserializeXml($expectedXml, $class, $root, $addContext);
            codecept_debug('--------');
        } catch (Exception $e) {
            codecept_debug($e->getTraceAsString());
            codecept_debug($e->getMessage());
        }

        codecept_debug('--------');
        if (isset($object)) {
            codecept_debug($object);
            codecept_debug('--------');

            /** @var array<string,array<int,string>> $resultedErrorMsgs */
            $resultedErrorMsgs = $this->parser->validateEntity($object, $addContext['groups']);

            foreach ($resultedErrorMsgs as $validationGroup => &$validationMessage) {
                $validationMessage = preg_replace('~[\r\n]+~', '', $validationMessage);
            }

            $I->assertEquals($expectedErrors, $resultedErrorMsgs);
        }
    }

/*
    public function testDenormalizeArrayOfArraysThenSerializeAndValidate(UnitTester $I): void
    {
        $class = Register::class;
        //$csv = new \ParseCsv\Csv();
        //$csv->delimiter = "\t";
        //$csv->parseFile('./tests/Support/Data/glis/glisWithValidationErrors2.csv');

        //$csv = Reader::createFromPath('./tests/Support/Data/glis/glisWithValidationErrors4.csv', 'r');
        $csv->setDelimiter("\t");
        $csv->includeEmptyRecords();
        $csv->setHeaderOffset(0);



        $header = $csv->getHeader(); //returns the CSV header record
        $records = $this->transformEmptyToNull($csv->getRecords()); //returns all the CSV records as an Iterator object

/ *
        foreach ($records as $index => $row)
        {
            codecept_debug($row);
            $expectedValidationerrors = $row['expected_validation_errors'];
            unset($row['expected_validation_errors']);


            array_walk($row, function ($value, $key) use (&$row) {
               //codecept_debug($key);
               if (empty($row[$key])) {
                   unset($row[$key]);
               }
            });

            // Prepare sources

            codecept_debug("CVS row: $index");
            $this->arrayJsonHelper->parseRow($row);
            codecept_debug("parsed row $index");
            codecept_debug($row);
            codecept_debug('expectedValidationerrors');
            codecept_debug($expectedValidationerrors);
            $expectedValidationerrorsArr = json_decode($expectedValidationerrors, true);
            codecept_debug($expectedValidationerrorsArr);

            $I->amGoingTo('denormalise array to expected class ' . $class . ', then try to return validation array.');
            $validationErrors = $this->validateHelper->validateRow($row, $class);
            codecept_debug($validationErrors);
            $I->amGoingTo("test if expected result occur in test row $index.");
            $I->assertEquals($validationErrors, $expectedValidationerrorsArr);
        }
* /

        //$records->rewind();
        $validateAllGenerator = $this->parser->validateAllRows($records,false, ['expected_validation_errors']);


        $validatedRows = [];
        foreach ($validateAllGenerator as $index => $errors)
        {
            $validatedRows[] =$errors;
        }
        codecept_debug("\$validatedRows");
        codecept_debug($validatedRows);


/ *        foreach ($this->transformEmptyToNull($records) as $index => $row)
        {
            codecept_debug($index);
            foreach ($row as $key => $value) {
                if (is_null($value))
                codecept_debug("$key is null");
            }
        } * /

        //$res = iterator_to_array($this->transformEmptyToNull($csv), true);

        //codecept_debug($res);


    }
*/


/*
    public function testDenormalizeArrayOfArraysThenSerializeAndValidate2(UnitTester $I): void
    {
        $class = Glis::class;
        //$csv = new \ParseCsv\Csv();
        //$csv->delimiter = "\t";
        //$csv->parseFile('./tests/Support/Data/glis/glisWithValidationErrors2.csv');

        $csv = Reader::createFromPath('./tests/Support/Data/glis/glisWithValidationErrors5.csv', 'r');
        $csv->setDelimiter("\t");
        $csv->includeEmptyRecords();
        $csv->setHeaderOffset(0);



        $header = $csv->getHeader(); //returns the CSV header record
        $records = $this->transformEmptyToNull($csv->getRecords()); //returns all the CSV records as an Iterator object

        //$records->rewind();
        //$validateAllGenerator = $this->parser->validateAllRows($records,false, ['expected_validation_errors']);
        $validateAllGenerator = $this->parser->validateAllRows($records,false, ['expected_validation_errors'], true);


        $validatedRows = [];
        foreach ($validateAllGenerator as $index => $errors)
        {
            $validatedRows[] =$errors;
        }
        codecept_debug("\$validatedRows");
        codecept_debug($validatedRows);


//                foreach ($this->transformEmptyToNull($records) as $index => $row)
//                {
//                    codecept_debug($index);
//                    foreach ($row as $key => $value) {
//                        if (is_null($value))
//                        codecept_debug("$key is null");
//                    }
//                }

        //$res = iterator_to_array($this->transformEmptyToNull($csv), true);

        //codecept_debug($res);


    }*/

    /**
     * @param Iterator $iterator
     * @return Generator
     */
    private function transformEmptyToNull(Iterator $iterator): Generator
    {
        foreach ($iterator as $index => $row) {
            foreach ($row as $key => $value) {
                if (empty($value)) {
                    $row[$key] = null;
                }
            }
            yield $row;
        }
    }
}
