<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Trait
 */

declare(strict_types=1);

namespace App\Tests\src\Trait;

use App\Tests\Support\UnitTester;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use EndlessDreams\FaoToolkit\Tests\Db\CreateGlis;
use EndlessDreams\FaoToolkit\Tests\Db\CreateSmta;
use EndlessDreams\FaoToolkit\Tests\Faker\FakerHelper;
use ErrorException;
use Exception;
use Faker\Factory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;
use Yiisoft\Db\Command\CommandInterface;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\NotSupportedException;
use Yiisoft\Definitions\Exception\InvalidConfigException;
use Yiisoft\Strings\StringHelper;

trait DbSetupCestUtilTrait
{
    /**
     * @param UnitTester $I
     * @return void
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws DbMapException
     * @throws ErrorException
     * @throws Throwable
     * @throws InvalidConfigException
     *
     * @psalm-suppress MixedAssignment
     */
    public function _before(UnitTester $I): void
    {
        if ($this->hasErrorsDuringInject || $this->isInjected) {
            return;
        }

        codecept_debug('_before');


        try {
            $I->amGoingTo('get DI components.');
            $module = 'smta';
            $this->setUpContainer();
            $this->setUpFaoConfig($module, 'test', 'smta:register');
            $this->aliases = $this->faoConfig->getAliases();
            $this->params = $this->config->get('params');

            $this->forceMigration = (
                /** @var false|string|bool|int|float|null $forceMigration */
                is_bool(($forceMigration = ($this->faoConfig->getOptions()?->offsetGet('force_migration') ?? false)))
                && $forceMigration
            );

            $module2 = 'glis';
            $this->faoConfig->getDatabases()->setSelectedModule($module2);
            $db = $this->faoConfig->getDbConnection($module2);
            $I->amGoingTo('prepare a ' . ($db->getDriverName() ?? '') . ' database');

            if (!isset($db)) {
                throw new Exception('Unable to get db connection.');
            }
            $mapService2 = new DbMapService($this->faoConfig);
            $provider = $mapService2->faoConfig->getSelectedProvider();

            if (
                $this->forceMigration
                || !($this->tableExist($db, $mapService2->getTableName('table_glis')))
            ) {
                $faker = Factory::create();
                $fakerHelper = new FakerHelper($faker);
                $glisIndex = 0;
                $glisData = [
                    [
                        'glis_id' => ++$glisIndex,
                        'lwiews' => $provider->getInstituteCode(),
                        'lpid' => $provider->getPid(),
                        'lname' => $provider->getName(),
                        'laddress' => $provider->getAddress(),
                        'lcountry' => $provider->getCountry(),
                        'sampledoi' => null,
                        'sampleid' => 'XXX ' . str_pad((string)$glisIndex, 5, '0', STR_PAD_LEFT) ,
                        'date' => ($acqdate = '2010-01-01'),
                        'method' => 'acqu',
                        'genus' => 'Linum',
                        'species' => 'usitatissimume',
                        'cropname' => 'Flax',
                        'cropnames' => null,
                        'tvalue' => $faker->url(),
                        'tkw' => '1',
                        'targets' => null,
                        'progdoi' => null,
                        'biostatus' => (string)$faker->randomElement(
                            ChoiceHelper::getGlisSampstatChoices()
                        ),
                        'spauth' => 'L.',
                        'subtaxa' => null,
                        'stauth' => null,
                        'nvalue' => $faker->word(),
                        'names' => null,
                        'itype' => 'genesysuuid',
                        'ivalue' => $faker->uuid(),
                        'ids' => null,
                        'mlsstatus' => '14' /*[0, 1, 11, 12, 13, 14, 15]*/,
                        'hist' => $faker->optional(0.9, 'y')->randomElement(['n']),
                        'pwiews' => null,
                        'ppid' => null,
                        'pname' => null,
                        'paddress' => null,
                        'pcountry' => null,
                        'psampleid' => null,
                        'provenance' => null,
                        'cwiews' => $faker->numerify((string)$fakerHelper->getACountryCode() . '###'),
                        'cpid' => null,
                        'cname' => null,
                        'caddress' => null,
                        'ccountry' => null,
                        'collectors' => null,
                        'csampleid' => $faker->regexify('[A-Z]{3} [1-9][0-9]{0,5}'),
                        'missid' => $faker->regexify('[A-Z]{5}[0-4]{3}'),
                        'site' => $faker->sentence(),
                        'clat' => number_format($faker->latitude(), 5),
                        'clon' => number_format($faker->longitude(), 5),
                        'uncert' => (string)$faker->randomNumber(3),
                        'datum' => 'WGS84',
                        'georef' => (string)$faker->optional(0.3)->randomElement(
                            ['GPS', 'determined from map', 'gazetteer', 'estimated using software']
                        ),
                        'elevation' => (string)$faker->randomNumber(3),
                        'cdate' => $faker->dateTimeBetween(
                            '-10 years',
                            '-5 years'
                        )->format('Y-m-d'),
                        'source' => $faker->randomElement(
                            ChoiceHelper::getGlisSourceChoices()
                        ),
                        'bwiews' => null,
                        'bpid' => null,
                        'bname' => null,
                        'baddress' => null,
                        'bcountry' => null,
                        'breeders' => null,
                        'ancestry' => null,
                        'last_update' => $faker->dateTimeBetween('-1 week')->format('Y-m-d')
                    ],
                    [
                        'glis_id' => ++$glisIndex,
                        'lwiews' => $provider->getInstituteCode(),
                        'lpid' => $provider->getPid(),
                        'lname' => $provider->getName(),
                        'laddress' => $provider->getAddress(),
                        'lcountry' => $provider->getCountry(),
                        'sampledoi' => null,
                        'sampleid' => 'XXX ' . str_pad((string)$glisIndex, 5, '0', STR_PAD_LEFT) ,
                        'date' => ($acqdate = '2010-01-01'),
                        'method' => 'acqu',
                        'genus' => 'Triticum',
                        'species' => 'aestivum',
                        'cropname' => 'Wheat',
                        'cropnames' => null,
                        'tvalue' => $faker->url(),
                        'tkw' => '1',
                        'targets' => null,
                        'progdoi' => null,
                        'biostatus' => (string)$faker->randomElement(
                            ChoiceHelper::getGlisSampstatChoices()
                        ),
                        'spauth' => 'L.',
                        'subtaxa' => null,
                        'stauth' => null,
                        'nvalue' => $faker->word(),
                        'names' => null,
                        'itype' => 'genesysuuid',
                        'ivalue' => $faker->uuid(),
                        'ids' => null,
                        'mlsstatus' => '11' /*[0, 1, 11, 12, 13, 14, 15]*/,
                        'hist' => $faker->optional(0.9, 'y')->randomElement(['n']),
                        'pwiews' => null,
                        'ppid' => null,
                        'pname' => null,
                        'paddress' => null,
                        'pcountry' => null,
                        'psampleid' => null,
                        'provenance' => null,
                        'cwiews' => $faker->numerify((string)$fakerHelper->getACountryCode() . '###'),
                        'cpid' => null,
                        'cname' => null,
                        'caddress' => null,
                        'ccountry' => null,
                        'collectors' => null,
                        'csampleid' => $faker->regexify('[A-Z]{3} [1-9][0-9]{0,5}'),
                        'missid' => $faker->regexify('[A-Z]{5}[0-4]{3}'),
                        'site' => $faker->sentence(),
                        'clat' => number_format($faker->latitude(), 5),
                        'clon' => number_format($faker->longitude(), 5),
                        'uncert' => (string)$faker->randomNumber(3),
                        'datum' => 'WGS84',
                        'georef' => (string)$faker->optional(0.3)->randomElement(
                            ['GPS', 'determined from map', 'gazetteer', 'estimated using software']
                        ),
                        'elevation' => (string)$faker->randomNumber(3),
                        'cdate' => $faker->dateTimeBetween(
                            '-10 years',
                            '-5 years'
                        )->format('Y-m-d'),
                        'source' => $faker->randomElement(
                            ChoiceHelper::getGlisSourceChoices()
                        ),
                        'bwiews' => null,
                        'bpid' => null,
                        'bname' => null,
                        'baddress' => null,
                        'bcountry' => null,
                        'breeders' => null,
                        'ancestry' => null,
                        'last_update' => $faker->dateTimeBetween('-1 week')->format('Y-m-d')
                    ],
                    [
                        'glis_id' => ++$glisIndex,
                        'lwiews' => $provider->getInstituteCode(),
                        'lpid' => $provider->getPid(),
                        'lname' => $provider->getName(),
                        'laddress' => $provider->getAddress(),
                        'lcountry' => $provider->getCountry(),
                        'sampledoi' => null,
                        'sampleid' => 'XXX ' . str_pad((string)$glisIndex, 5, '0', STR_PAD_LEFT) ,
                        'date' => ($acqdate = '2010-01-01'),
                        'method' => 'acqu',
                        'genus' => 'Solanum',
                        'species' => 'tuberosum',
                        'cropname' => 'Potato',
                        'cropnames' => null,
                        'tvalue' => $faker->url(),
                        'tkw' => '1',
                        'targets' => null,
                        'progdoi' => null,
                        'biostatus' => (string)$faker->randomElement(
                            ChoiceHelper::getGlisSampstatChoices()
                        ),
                        'spauth' => 'L.',
                        'subtaxa' => null,
                        'stauth' => null,
                        'nvalue' => $faker->word(),
                        'names' => null,
                        'itype' => 'genesysuuid',
                        'ivalue' => $faker->uuid(),
                        'ids' => null,
                        'mlsstatus' => '11' /*[0, 1, 11, 12, 13, 14, 15]*/,
                        'hist' => $faker->optional(0.9, 'y')->randomElement(['n']),
                        'pwiews' => null,
                        'ppid' => null,
                        'pname' => null,
                        'paddress' => null,
                        'pcountry' => null,
                        'psampleid' => null,
                        'provenance' => null,
                        'cwiews' => $faker->numerify((string)$fakerHelper->getACountryCode() . '###'),
                        'cpid' => null,
                        'cname' => null,
                        'caddress' => null,
                        'ccountry' => null,
                        'collectors' => null,
                        'csampleid' => $faker->regexify('[A-Z]{3} [1-9][0-9]{0,5}'),
                        'missid' => $faker->regexify('[A-Z]{5}[0-4]{3}'),
                        'site' => $faker->sentence(),
                        'clat' => number_format($faker->latitude(), 5),
                        'clon' => number_format($faker->longitude(), 5),
                        'uncert' => (string)$faker->randomNumber(3),
                        'datum' => 'WGS84',
                        'georef' => (string)$faker->optional(0.3)->randomElement(
                            ['GPS', 'determined from map', 'gazetteer', 'estimated using software']
                        ),
                        'elevation' => (string)$faker->randomNumber(3),
                        'cdate' => $faker->dateTimeBetween(
                            '-10 years',
                            '-5 years'
                        )->format('Y-m-d'),
                        'source' => $faker->randomElement(
                            ChoiceHelper::getGlisSourceChoices()
                        ),
                        'bwiews' => null,
                        'bpid' => null,
                        'bname' => null,
                        'baddress' => null,
                        'bcountry' => null,
                        'breeders' => null,
                        'ancestry' => null,
                        'last_update' => $faker->dateTimeBetween('-1 week')->format('Y-m-d')
                    ],
                    [
                        'glis_id' => ++$glisIndex,
                        'lwiews' => $provider->getInstituteCode(),
                        'lpid' => $provider->getPid(),
                        'lname' => $provider->getName(),
                        'laddress' => $provider->getAddress(),
                        'lcountry' => $provider->getCountry(),
                        'sampledoi' => null,
                        'sampleid' => 'XXX ' . str_pad((string)$glisIndex, 5, '0', STR_PAD_LEFT) ,
                        'date' => ($acqdate = '2010-01-01'),
                        'method' => 'acqu',
                        'genus' => '[genus' . $glisIndex . ']',
                        'species' => '[species' . $glisIndex . ']',
                        'cropname' => '[cropname' . $glisIndex . ']',
                        'cropnames' => null,
                        'tvalue' => $faker->url(),
                        'tkw' => '1',
                        'targets' => null,
                        'progdoi' => null,
                        'biostatus' => (string)$faker->randomElement(
                            ChoiceHelper::getGlisSampstatChoices()
                        ),
                        'spauth' => '[spauth' . $glisIndex . ']',
                        'subtaxa' => null,
                        'stauth' => null,
                        'nvalue' => $faker->word(),
                        'names' => null,
                        'itype' => 'genesysuuid',
                        'ivalue' => $faker->uuid(),
                        'ids' => null,
                        'mlsstatus' => $faker->randomElement(
                            ChoiceHelper::getGlisMlsStatusChoices() /*[0, 1, 11, 12, 13, 14, 15]*/
                        ),
                        'hist' => $faker->optional(0.9, 'y')->randomElement(['n']),
                        'pwiews' => null,
                        'ppid' => null,
                        'pname' => null,
                        'paddress' => null,
                        'pcountry' => null,
                        'psampleid' => null,
                        'provenance' => null,
                        'cwiews' => $faker->numerify((string)$fakerHelper->getACountryCode() . '###'),
                        'cpid' => null,
                        'cname' => null,
                        'caddress' => null,
                        'ccountry' => null,
                        'collectors' => null,
                        'csampleid' => $faker->regexify('[A-Z]{3} [1-9][0-9]{0,5}'),
                        'missid' => $faker->regexify('[A-Z]{5}[0-4]{3}'),
                        'site' => $faker->sentence(),
                        'clat' => number_format($faker->latitude(), 5),
                        'clon' => number_format($faker->longitude(), 5),
                        'uncert' => (string)$faker->randomNumber(3),
                        'datum' => 'WGS84',
                        'georef' => (string)$faker->optional(0.3)->randomElement(
                            ['GPS', 'determined from map', 'gazetteer', 'estimated using software']
                        ),
                        'elevation' => (string)$faker->randomNumber(3),
                        'cdate' => $faker->dateTimeBetween(
                            '-10 years',
                            '-5 years'
                        )->format('Y-m-d'),
                        'source' => $faker->randomElement(
                            ChoiceHelper::getGlisSourceChoices()
                        ),
                        'bwiews' => null,
                        'bpid' => null,
                        'bname' => null,
                        'baddress' => null,
                        'bcountry' => null,
                        'breeders' => null,
                        'ancestry' => null,
                        'last_update' => $faker->dateTimeBetween('-1 week')->format('Y-m-d')
                    ],
                ];

                $this->numberOfGlisToTest = count($glisData);
                $this->glisTableData = [
                    'glis' => &$glisData,
                ];

                $opt = [
                    'number_of_rows_to_generate' => 3,
                    'provider' => $provider,
                    'institute_index' => null,
                    'table_data' => [
                        'glis' => &$glisData,
                    ],
                ];
                $this->createGlis = new CreateGlis(
                    $mapService2,
                    [
                        'destroyOnDestruct' => true /*true*/,
                        'default' => $opt,

                    ]
                );
            }

            $module = 'smta';
            //$this->setUpContainer();
            $this->setUpFaoConfig($module, 'test', 'smta:register');
            //this->aliases = $this->faoConfig->getAliases();
            //$this->params = $this->config->get('params');

            $db = $this->faoConfig->getDbConnection($module);

            if (!isset($db)) {
                throw new Exception('Unable to get db connection.');
            }

            //$mapService = new DbMapService($this->faoConfig);

            $tablenameOrder = $this->dbMapService->getTableName('table_order');
            $tablenameItem = $this->dbMapService->getTableName('table_item');


            if (
                $this->forceMigration
                || !($this->tableExist($db, $tablenameOrder) && $this->tableExist($db, $tablenameItem))
            ) {
                $faker = Factory::create();
                $fakerHelper = new FakerHelper($faker);
                $orderIndex = 0;
                $itemIndex = 0;

                $map = $this->dbMapService->faoConfig->getMap();
                $mapOrder = $map?->getColumnsOrder();
                $mapItem = $map?->getColumnsItem();

                $orderColumnNamesDef = [
                    'id' => (string)$mapOrder?->getId(),
                    'symbol' => (string)$mapOrder?->getSymbol(),
                    'date' => (string)$mapOrder?->getDate(),
                    'type' => (string)$mapOrder?->getType(),
                    'language' => (string)$mapOrder?->getLanguage(),
                    'shipName' => (string)$mapOrder?->getShipName(),
                    'recipient_type' => (string)$mapOrder?->getRecipientType(),
                    'recipient_name' => (string)$mapOrder?->getRecipientName(),
                    'recipient_address' => (string)$mapOrder?->getRecipientAddress(),
                    'recipient_country' => (string)$mapOrder?->getRecipientCountry(),
                    'document_location' => (string)$mapOrder?->getDocumentLocation(),
                    'document_retinfo' => (string)$mapOrder?->getDocumentRetinfo(),
                    'document_pdf' => (string)$mapOrder?->getDocumentPdf(),
                    'fao_institute_code' => (string)$mapOrder?->getFaoInstituteCode(),
                ];

                $itemColumnNamesDef = [
                    'id' => 'id',
                    'orderid' => 'orderid',
                    'sampleID' => (string)$mapItem?->getSampleID(),
                    'crop' => (string)$mapItem?->getCrop(),
                    'pud' => (string)$mapItem?->getPud(),
                    'ancestry' => (string)$mapItem?->getAncestry(),
                ];

                $expectedRowOrderValues = [
                    [
                        ++$orderIndex,
                        'order1',
                        '2021-05-10',
                        'sw',
                        'en',
                        'Marty McFly',
                        'or',
                        'Doc Brown',
                        '1640 Riverside Drive, Hill Valley, California',
                        'USA',
                        'p',
                        'https:///domain.com/smtaid=1',
                        null,
                        'XXX001',
                    ],
                    [
                        ++$orderIndex,
                        'order2',
                        '2021-05-10',
                        'cw',
                        'en',
                        'Willow Rosenberg',
                        'or',
                        'Buffy Summers',
                        '1630 Revello Drive, Sunnydale, California',
                        'USA',
                        'p',
                        'https:///domain.com/smtaid=2',
                        null,
                        'XXX001',
                    ],
                    [
                        ++$orderIndex,
                        'order3',
                        '2021-06-20',
                        'si',
                        'en',
                        'Laura Kinney',
                        'in',
                        'Henry Philip McCoy',
                        '1407 Graymalkin Lane, Salem Center, New York',
                        'USA',
                        'p',
                        'https:///domain.com/smtaid=3',
                        null,
                        'XXX001',
                    ],
                ];

                $orderData = [];
                foreach ($expectedRowOrderValues as $index => $rowData) {
                    $orderData[] = array_combine(
                        array_values($orderColumnNamesDef),
                        $rowData
                    );
                }

                $expectedRowItemValues = [
                    [
                        1,
                        'Accession1',
                        'Potato',
                        'n',
                        null
                    ],
                    [
                        1,
                        'Accession2',
                        'Trifolium',
                        'n',
                        null
                    ],
                    [
                        1,
                        'XXX 00001',
                        'Flax',
                        'n',
                        null
                    ],
                    [
                        2,
                        'Accession1',
                        'Beet',
                        'n',
                        null
                    ],
                    [
                        2,
                        'Accession2',
                        'Trifolium',
                        'n',
                        null
                    ],
                    [
                        2,
                        'Accession3',
                        'Barley',
                        'n',
                        null
                    ],
                    [
                        3,
                        'Accession1',
                        'Wheat',
                        'n',
                        null
                    ],
                    [
                        3,
                        'Accession2',
                        'Wheat',
                        'n',
                        null
                    ],
                    [
                        3,
                        'Accession3',
                        'Wheat',
                        'y',
                        'Wheat:Accession1,Accession2'
                    ]
                ];

                $itemData = [];

                $itemColumnList = array_values($itemColumnNamesDef);
                array_shift($itemColumnList);
                foreach ($expectedRowItemValues as $index => $rowData) {
                    $itemData[] = array_combine(
                        [
                            'orderid',
                            'sampleID',
                            'crop',
                            'pud',
                            'ancestry'
                        ],
                        //$itemColumnList,
                        $rowData
                    );
                }

                $this->numberOfSmtaToTest = count($orderData);
                $this->smtaTableData = [
                    'order' => &$orderData,
                    'item' => &$itemData,
                ];

                $opt = [
                    'number_of_rows_to_generate' => 3,
                    'provider' => $provider,
                    'institute_index' => null,
                    'table_data' => [
                        'order' => &$orderData,
                        'item' => &$itemData,
                    ],
                ];

                $this->createSmta = new CreateSmta(
                    $this->dbMapService,
                    [
                        'destroyOnDestruct' => true /*true*/,
                        'default' => $opt,
                    ]
                );
            }
            $this->faoConfig->getDatabases()->setSelectedModule($module);

            $this->isInjected = true;
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface | ErrorException | Exception | Throwable $e) {
            codecept_debug($e->getMessage());
            $this->hasErrorsDuringInject = true;
            $I->fail($e->getMessage());
            throw $e;
        }
    }


        /**
     * @param DbMapService $mapService
     * @return void
     * @throws DbMapException
     * @throws Throwable
     * @throws \Yiisoft\Db\Exception\Exception
     * @throws \Yiisoft\Db\Exception\InvalidConfigException
     * @throws NotSupportedException
     */
    private function createSmtaTables(DbMapService $mapService): void
    {
        codecept_debug(__METHOD__ . __LINE__);
        /** @var ConnectionInterface $db */
        $db = $mapService->faoConfig->getDbConnection('smta');

        $tablenameItem = $mapService->getTableName('table_item');
        $tablenameOrder = $mapService->getTableName('table_order');

        $transaction = $db->beginTransaction();
        try {
            if ($this->tableExist($db, $tablenameItem)) {
                codecept_debug("{ucfirst($tablenameItem)} exists, drop it before create.");
                $db->createCommand()->dropTable("{{%{$tablenameItem}}}")->execute();
            }

            if ($this->tableExist($db, $tablenameOrder)) {
                codecept_debug("{ucfirst($tablenameOrder)} exists, drop it before create.");
                $db->createCommand()->dropTable("{{%{$tablenameOrder}}}")->execute();
            }

            $db->createCommand()->createTable(
                "{{%{$tablenameOrder}}}",
                [
                    'id' => 'pk',
                    'symbol' => 'string(128) NOT NULL',
                    'date' => 'string(10) NOT NULL',
                    'type' => 'string(2) NOT NULL',
                    'language' => 'string(2) NOT NULL',
                    'shipname' => 'string(128) NOT NULL',
                    'recipient_type' => 'string(2) NOT NULL',
                    'recipient_name' => 'string(128) NOT NULL',
                    'recipient_address' => 'string(65536) NOT NULL',
                    'recipient_country' => 'string(3) NOT NULL',
                    'document_location' => 'string(1) NOT NULL',
                    'document_retinfo' => 'string(128) NULL',
                    'document_pdf' => 'string(255) NULL',
                    'fao_institute_code' => 'string(7) NOT NULL',
                ]
            )->execute();

            $db->createCommand()->createTable(
                "{{%{$tablenameItem}}}",
                [
                    'id' => 'pk',
                    'orderid' => 'integer NOT NULL',
                    'crop' => 'string(64) NOT NULL',
                    'sampleid' => 'string(256) NOT NULL',
                    'pud' => 'string(1)',
                    'ancestry' => 'string(65536)',
                ]
            )->execute();


            /** @var ConnectionInterface $db */
            $db->createCommand()->batchInsert(
                "{{%{$tablenameOrder}}}",
                [
                    'symbol',
                    'date',
                    'type',
                    'language',
                    'shipname',
                    'recipient_type',
                    'recipient_name',
                    'recipient_address',
                    'recipient_country',
                    'document_location',
                    'document_retinfo',
                    'document_pdf',
                    'fao_institute_code',],
                [
                    [
                        'NGB-1-O-2023-01-01',
                        '2023-01-01',
                        'cw',
                        'en',
                        'Mr. Smith',
                        'or',
                        'Acme',
                        'Acme street 1',
                        'XXX',
                        'o',
                        'https://printservice.nordgen.org/gringlobal/smta?oid=1',
                        null,
                        'XXX001',
                        ],
                    [
                        'NGB-1-O-2023-06-01',
                        '2023-06-01',
                        'cw',
                        'en',
                        'Mr. Q',
                        'or',
                        'Macmed',
                        'Acme street 10',
                        'XXX',
                        'o',
                        'https://printservice.nordgen.org/gringlobal/smta?oid=2',
                        null,
                        'XXX001',
                        ],
                    [
                        'NGB-1-O-2023-09-01',
                        '2023-09-01',
                        'cw',
                        'en',
                        'Mr. Smith',
                        'or',
                        'Acme',
                        'Acme street 1',
                        'XXX',
                        'o',
                        'https://printservice.nordgen.org/gringlobal/smta?oid=1',
                        null,
                        'XXX001',
                    ],
                ]
            )->execute();


            /** @var ConnectionInterface $db */
            $db->createCommand()->batchInsert(
                "{{%{$tablenameItem}}}",
                ['orderid','crop','sampleid','pud','ancestry',],
                [
                    [1,'Wheat','NGB 1','n',null,],
                    [1,'Wheat','NGB 2','n',null,],
                    [1,'Wheat','NGB 3','n',null,],
                    [1,'Wheat','NGB 4','n',null,],
                    [1,'Wheat','NGB 5','n',null,],
                    [1,'Wheat','NGB 6','n',null,],
                    [2,'BrassicaComplex','NGB 13000','n',null,],
                    [2,'BrassicaComplex','NGB 13001','n',null,],
                    [3,'Wheat','NGB 7','n',null,],
                    [3,'Wheat','NGB 8','n',null,],
                    [3,'Wheat','NGB 9','n',null,],
                    [3,'Wheat','NGB 10','n',null,],
                ]
            )->execute();


            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            // to get the slightest info about the Exception
            codecept_debug($e->getMessage());
            codecept_debug('Rollback was performed');
        }
        codecept_debug(__METHOD__ . __LINE__);
    }

    /**
     * @param ConnectionInterface $db
     * @param string $table
     * @return bool
     * @throws Throwable
     */
    private function tableExist(ConnectionInterface $db, string $table): bool
    {
        try {
            $db->createCommand("select 1 FROM {{%{$table}}}")->queryOne();
            codecept_debug(__METHOD__ . __LINE__);
        } catch (\Yiisoft\Db\Exception\Exception $e) {
            codecept_debug($e->errorInfo);
            if (StringHelper::startsWith($e->errorInfo[2], 'no such table:')) {
                codecept_debug('No such table...');
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $row
     * @param Provider $provider
     * @param CommandInterface $itemsCommand
     * @return void
     * @throws Throwable
     * @throws \Yiisoft\Db\Exception\Exception
     */
    private function mapSmtaRow(
        array &$row,
        Provider $provider,
        CommandInterface $itemsCommand
    ): void {
        $row['provider'] = [
            'type' => $provider->getType(),
            'pid' => $provider->getPid($provider->getSelectedFaoEnvironment()),
            'name' => $provider->getName(),
            'address' => $provider->getAddress(),
            'country' => $provider->getCountry(),
            'email' => $provider->getEmail(),
        ];
        $row['recipient'] = [
            'type' => $row['recipientType'],
            'name' => $row['recipientName'],
            'address' => $row['recipientAddress'],
            'country' => $row['recipientCountry'],
        ];
        unset($row['recipientType']);
        unset($row['recipientName']);
        unset($row['recipientAddress']);
        unset($row['recipientCountry']);
        $row['document'] = [
            'location' => $row['documentLocation'],
            'retInfo' => $row['documentRetinfo'],
            'pdf' => $row['documentPdf']
        ];
        unset($row['documentLocation']);
        unset($row['documentRetinfo']);
        unset($row['documentPdf']);
        unset($row['faoInstituteCode']);

        $row['annex1'] = ['material' => $itemsCommand->queryAll()];
    }
}
