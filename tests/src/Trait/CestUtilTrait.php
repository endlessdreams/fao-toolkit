<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Trait
 */

declare(strict_types=1);

namespace App\Tests\src\Trait;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Database;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use ErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Yiisoft\Config\ConfigInterface;
use Yiisoft\Definitions\Exception\InvalidConfigException;
use Yiisoft\Yii\Runner\Console\ConsoleApplicationRunner;

/**
 *
 */
trait CestUtilTrait
{
    /**
     * @var ConfigInterface
     */
    private ConfigInterface $config;

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var array
     */
    private array $params;

    /**
     * @var FaoConfig
     */
    private FaoConfig $faoConfig;

    /**
     * @var ParserService
     */
    private ParserService $parser;
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var DbMapService
     */
    private DbMapService $dbMapService;

    /**
     * @throws ErrorException
     * @throws InvalidConfigException
     */
    protected function setUpContainer(): void
    {
        $runner = new ConsoleApplicationRunner(
            rootPath: dirname(__DIR__, 3),
            environment: $_ENV['YII_ENV']
        );
        $this->config = $runner->getConfig();
        $this->container = $runner->getContainer();
        $this->params = $this->config->get('params');
        if (!isset($this->container)) {
            throw new InvalidConfigException('Unable to fetch container.');
        }
    }

    /**
     * @param string $module
     * @param string $env
     * @param string|null $moduleCommand
     * @return void
     * @throws ContainerExceptionInterface
     * @throws InvalidConfigException
     * @throws NotFoundExceptionInterface
     */
    protected function setUpFaoConfig(string $module, string $env, ?string $moduleCommand = null): void
    {
        $this->faoConfig = $this->container->get(FaoConfig::class);

        $defaultInstituteCode = (string)$this->faoConfig->getProviders()->getDefaultInstituteCode();
        $this->faoConfig->setSelectedFaoEnvironment($env);
        $this->faoConfig->setSelectedProviderCode($defaultInstituteCode);

        if (! ($this->faoConfig->getDatabases()->getDatabase($module) instanceof Database)) {
            $module = $this->faoConfig->getDatabases()->getDefaultModule();
        }
        $this->faoConfig->getDatabases()->setSelectedModule($module);

        if (!isset($this->faoConfig)) {
            throw new InvalidConfigException('Unable to fetch container FaoConfig.');
        }

        $this->parser = $this->faoConfig->getParser();

        if (!isset($this->parser)) {
            throw new InvalidConfigException('Unable to fetch container parser.');
        }

        $this->validator = $this->parser->getValidator();

        if (!isset($this->validator)) {
            throw new InvalidConfigException('Unable to fetch container validator.');
        }

        $this->dbMapService = new DbMapService($this->faoConfig);

        if (isset($moduleCommand)) {
            $this->initFaoConfig($moduleCommand);
        }
    }

    /**
     * @param string $moduleCommand
     * @return void
     * @throws InvalidConfigException
     */
    protected function initFaoConfig(string $moduleCommand): void
    {
        // Get service to convert the client table row array to module main table row.
        $this->moduleServiceClass = $this->parser
            ?->moduleCommandRegister
            ?->getService($moduleCommand);

        if ($this->moduleServiceClass === null) {
            throw new InvalidConfigException('A module command service was not defined.');
        }

        $this->moduleServiceClass->init($this->dbMapService);
    }
}
