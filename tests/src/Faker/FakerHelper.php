<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Faker
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Faker;

use Faker\Factory;
use Faker\Generator;
use League\ISO3166\ISO3166;

/**
 * Class FakerHelper
 * @package endlessdreams\faoToolkit\Tests\Faker
 */
class FakerHelper
{
    /**
     * @var Generator
     */
    private Generator $faker;

    /**
     * FakerHelper constructor.
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * @return string[]
     */
    public function cropCodeToCrop(): array
    {
        // 'Crop Code' => 'English name',
        return [
            'Agropyron' => 'Agropyron',
            'Agrostis' => 'Agrostis',
            'Alopecurus' => 'Alopecurus',
            'Andropogon' => 'Andropogon',
            'Apple' => 'Apple',
            'Arrhenatherum' => 'Arrhenatherum',
            'Asparagus' => 'Asparagus',
            'Astragalus' => 'Astragalus',
            'Atriplex' => 'Atriplex',
            'BananaPlantain' => 'Banana/Plantain',
            'Barley' => 'Barley',
            'Beans' => 'Beans',
            'Beet' => 'Beet',
            'BrassicaComplex' => 'Brassica complex',
            'Breadfruit' => 'Breadfruit',
            'Canavalia' => 'Canavalia',
            'Carrot' => 'Carrot',
            'Cassava' => 'Cassava',
            'Chickpea' => 'Chickpea',
            'Citrus' => 'Citrus',
            'Coconut' => 'Coconut',
            'Coronilla' => 'Coronilla',
            'CowpeaEtAl' => 'Cowpea et al.',
            'Dactylis' => 'Dactylis',
            'Eggplant' => 'Eggplant',
            'FabaBeanVetch' => 'Faba Bean/Vetch',
            'Festuca' => 'Festuca',
            'FingerMillet' => 'Finger Millet',
            'Grasspea' => 'Grass pea',
            'Hedysarum' => 'Hedysarum',
            'Lathyrus' => 'Lathyrus',
            'Lentil' => 'Lentil',
            'Lespedeza' => 'Lespedeza',
            'Lolium' => 'Lolium',
            'Lotus' => 'Lotus',
            'Lupinus' => 'Lupinus',
            'Maize' => 'Maize',
            'Majoraroids' => 'Major aroids',
            'Medicago' => 'Medicago',
            'Melilotus' => 'Melilotus',
            'Oat' => 'Oat',
            'Onobrychis' => 'Onobrychis',
            'Ornithopus' => 'Ornithopus',
            'Pea' => 'Pea',
            'PearlMillet' => 'Pearl Millet',
            'Phalaris' => 'Phalaris',
            'Phleum' => 'Phleum'
        ];
    }

    /**
     * @return array{wiews:string|null, pid:string|null, name: string|null, address:string|null, country:mixed|null}
     */
    public function getFakedInstitute(): array
    {
        [$faker, $countryCode] = $this->getFakerAndCountryCode();
        return [
            'wiews' => $faker->numerify($countryCode . '###') ?? null,
            'pid' => strtoupper($faker->bothify('##??##')) ?? null,
            'name' => $faker->company() ?? null,
            'address' => $faker->address() ?? null,
            'country' => $countryCode ?? null,
        ];
    }

    /**
     * @return array
     */
    public function getFakerAndCountryCode(): array
    {
        $countryCode = $this->getACountryCode();
        $faker = Factory::create($this->getLocaleFromCountryCode($countryCode));
        return [$faker, $countryCode];
    }

    /**
     * @return mixed
     */
    public function getACountryCode(): mixed
    {
        return $this->faker->randomElement(
            ['SWE', 'DNK', 'NOR', 'FIN', 'ISL', 'EST', 'LVA', 'LTU', 'DEU', 'NLD', 'FRA', 'GBR', 'USA', 'RUS', 'POL']
        );
    }

    /**
     * @param string $countryCode
     * @return string
     */
    public function getLocaleFromCountryCode(string $countryCode): string
    {
        return $this->countryCodeToLocale((new ISO3166())->alpha3($countryCode)['alpha2']);
    }

    /**
     * Returns a locale from a country code that is provided.
     *
     *
     *
     * @param string $country_code ISO 3166-2-alpha 2 country code
     * @param string $language_code ISO 639-1-alpha 2 language code
     * @return false|string|null a locale, formatted like en_US, or null if not found
     */
    public function countryCodeToLocale(string $country_code, string $language_code = ''): false|string|null
    {
        // Locale list taken from:
        // http://stackoverflow.com/questions/3191664/
        // list-of-all-locales-and-their-short-codes
        $locales = array(
            'af-ZA',
            'am-ET',
            'ar-AE',
            'ar-BH',
            'ar-DZ',
            'ar-EG',
            'ar-IQ',
            'ar-JO',
            'ar-KW',
            'ar-LB',
            'ar-LY',
            'ar-MA',
            'arn-CL',
            'ar-OM',
            'ar-QA',
            'ar-SA',
            'ar-SY',
            'ar-TN',
            'ar-YE',
            'as-IN',
            'az-Cyrl-AZ',
            'az-Latn-AZ',
            'ba-RU',
            'be-BY',
            'bg-BG',
            'bn-BD',
            'bn-IN',
            'bo-CN',
            'br-FR',
            'bs-Cyrl-BA',
            'bs-Latn-BA',
            'ca-ES',
            'co-FR',
            'cs-CZ',
            'cy-GB',
            'da-DK',
            'de-AT',
            'de-CH',
            'de-DE',
            'de-LI',
            'de-LU',
            'dsb-DE',
            'dv-MV',
            'el-GR',
            'en-029',
            'en-AU',
            'en-BZ',
            'en-CA',
            'en-GB',
            'en-IE',
            'en-IN',
            'en-JM',
            'en-MY',
            'en-NZ',
            'en-PH',
            'en-SG',
            'en-TT',
            'en-US',
            'en-ZA',
            'en-ZW',
            'es-AR',
            'es-BO',
            'es-CL',
            'es-CO',
            'es-CR',
            'es-DO',
            'es-EC',
            'es-ES',
            'es-GT',
            'es-HN',
            'es-MX',
            'es-NI',
            'es-PA',
            'es-PE',
            'es-PR',
            'es-PY',
            'es-SV',
            'es-US',
            'es-UY',
            'es-VE',
            'et-EE',
            'eu-ES',
            'fa-IR',
            'fi-FI',
            'fil-PH',
            'fo-FO',
            'fr-BE',
            'fr-CA',
            'fr-CH',
            'fr-FR',
            'fr-LU',
            'fr-MC',
            'fy-NL',
            'ga-IE',
            'gd-GB',
            'gl-ES',
            'gsw-FR',
            'gu-IN',
            'ha-Latn-NG',
            'he-IL',
            'hi-IN',
            'hr-BA',
            'hr-HR',
            'hsb-DE',
            'hu-HU',
            'hy-AM',
            'id-ID',
            'ig-NG',
            'ii-CN',
            'is-IS',
            'it-CH',
            'it-IT',
            'iu-Cans-CA',
            'iu-Latn-CA',
            'ja-JP',
            'ka-GE',
            'kk-KZ',
            'kl-GL',
            'km-KH',
            'kn-IN',
            'kok-IN',
            'ko-KR',
            'ky-KG',
            'lb-LU',
            'lo-LA',
            'lt-LT',
            'lv-LV',
            'mi-NZ',
            'mk-MK',
            'ml-IN',
            'mn-MN',
            'mn-Mong-CN',
            'moh-CA',
            'mr-IN',
            'ms-BN',
            'ms-MY',
            'mt-MT',
            'nb-NO',
            'ne-NP',
            'nl-BE',
            'nl-NL',
            'nn-NO',
            'nso-ZA',
            'oc-FR',
            'or-IN',
            'pa-IN',
            'pl-PL',
            'prs-AF',
            'ps-AF',
            'pt-BR',
            'pt-PT',
            'qut-GT',
            'quz-BO',
            'quz-EC',
            'quz-PE',
            'rm-CH',
            'ro-RO',
            'ru-RU',
            'rw-RW',
            'sah-RU',
            'sa-IN',
            'se-FI',
            'se-NO',
            'se-SE',
            'si-LK',
            'sk-SK',
            'sl-SI',
            'sma-NO',
            'sma-SE',
            'smj-NO',
            'smj-SE',
            'smn-FI',
            'sms-FI',
            'sq-AL',
            'sr-Cyrl-BA',
            'sr-Cyrl-CS',
            'sr-Cyrl-ME',
            'sr-Cyrl-RS',
            'sr-Latn-BA',
            'sr-Latn-CS',
            'sr-Latn-ME',
            'sr-Latn-RS',
            'sv-FI',
            'sv-SE',
            'sw-KE',
            'syr-SY',
            'ta-IN',
            'te-IN',
            'tg-Cyrl-TJ',
            'th-TH',
            'tk-TM',
            'tn-ZA',
            'tr-TR',
            'tt-RU',
            'tzm-Latn-DZ',
            'ug-CN',
            'uk-UA',
            'ur-PK',
            'uz-Cyrl-UZ',
            'uz-Latn-UZ',
            'vi-VN',
            'wo-SN',
            'xh-ZA',
            'yo-NG',
            'zh-CN',
            'zh-HK',
            'zh-MO',
            'zh-SG',
            'zh-TW',
            'zu-ZA',
        );

        foreach ($locales as $locale) {
            $locale_region = locale_get_region($locale);
            $locale_language = locale_get_primary_language($locale);
            $locale_array = array(
                'language' => $locale_language,
                'region' => $locale_region
            );

            if (
                strtoupper($country_code) == $locale_region &&
                $language_code == ''
            ) {
                return locale_compose($locale_array);
            } elseif (
                strtoupper($country_code) == $locale_region &&
                strtolower($language_code) == $locale_language
            ) {
                return locale_compose($locale_array);
            }
        }

        return null;
    }
}
