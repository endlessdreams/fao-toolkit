<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Db
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Exception;
use Throwable;

/**
 * Trait CreateGlisGlisTrait
 *
 * @package endlessdreams\faoToolkit\Tests\Db
 * @property DbMapService $mapService
 */
trait CreateGlisGlisTrait
{
    /**
     * @return void
     * @throws Exception|Throwable
     */
    private function createGlis(): void
    {
        $columnsNames = $this->mapService->faoConfig->getMap()->getColumnsGlis();

        /**************************************
         * Create table and columns           *
         **************************************/
        $tableName = $this->mapService->getTableName('table_glis');

        $columns = [
            $columnsNames->getGlisId() => 'pk',
            $columnsNames->getLwiews() => 'string(16) NULL',
            $columnsNames->getLpid() => 'string(16) NULL',
            $columnsNames->getLname() => 'string(128) NULL',
            $columnsNames->getLaddress() => 'string(128) NULL',
            $columnsNames->getLcountry() => 'string(3) NULL',
            $columnsNames->getSampledoi() => 'string(128) NULL',
            $columnsNames->getSampleid() => 'string(128) NULL',
            $columnsNames->getDate() => 'string(10) NULL',
            $columnsNames->getMethod() => 'string(4) NULL',
            $columnsNames->getGenus() => 'string(64) NULL',
            $columnsNames->getSpecies() => 'string(128) NULL',
            $columnsNames->getCropname() => 'string(128) NULL',
            $columnsNames->getCropnames() => 'string(4000) NULL',
            $columnsNames->getTvalue() => 'string(256) NULL',
            $columnsNames->getTkw() => 'string(16) NULL',
            $columnsNames->getTargets() => 'text NULL',
            $columnsNames->getProgdoi() => 'string(128) NULL',
            $columnsNames->getProgdois() => 'text NULL',
            $columnsNames->getBiostatus() => 'string(3) NULL',
            $columnsNames->getSpauth() => 'string(64) NULL',
            $columnsNames->getSubtaxa() => 'string(128) NULL',
            $columnsNames->getStauth() => 'string(64) NULL',
            $columnsNames->getNvalue() => 'string(128) NULL',
            $columnsNames->getNames() => 'text NULL',
            $columnsNames->getItype() => 'string(16) NULL',
            $columnsNames->getIvalue() => 'string(128) NULL',
            $columnsNames->getIds() => 'text NULL',
            $columnsNames->getMlsstatus() => 'string(2) NULL',
            $columnsNames->getHist() => 'string(1) NULL',
            $columnsNames->getPwiews() => 'string(16) NULL',
            $columnsNames->getPpid() => 'string(16) NULL',
            $columnsNames->getPname() => 'string(128) NULL',
            $columnsNames->getPaddress() => 'string(128) NULL',
            $columnsNames->getPcountry() => 'string(3) NULL',
            $columnsNames->getPsampleid() => 'string(128) NULL',
            $columnsNames->getProvenance() => 'string(3) NULL',
            $columnsNames->getCwiews() => 'string(16) NULL',
            $columnsNames->getCpid() => 'string(16) NULL',
            $columnsNames->getCname() => 'string(128) NULL',
            $columnsNames->getCaddress() => 'string(128) NULL',
            $columnsNames->getCcountry() => 'string(3) NULL',
            $columnsNames->getCollectors() => 'text NULL',
            $columnsNames->getCsampleid() => 'string(128) NULL',
            $columnsNames->getMissid() => 'string(128) NULL',
            $columnsNames->getSite() => 'string(128) NULL',
            $columnsNames->getClat() => 'string(10) NULL',
            $columnsNames->getClon() => 'string(10) NULL',
            $columnsNames->getUncert() => 'string(16) NULL',
            $columnsNames->getDatum() => 'string(16) NULL',
            $columnsNames->getGeoref() => 'string(128) NULL',
            $columnsNames->getElevation() => 'string(16) NULL',
            $columnsNames->getCdate() => 'string(10) NULL',
            $columnsNames->getSource() => 'string(2) NULL',
            $columnsNames->getBwiews() => 'string(16) NULL',
            $columnsNames->getBpid() => 'string(16) NULL',
            $columnsNames->getBname() => 'string(128) NULL',
            $columnsNames->getBaddress() => 'string(128) NULL',
            $columnsNames->getBcountry() => 'string(3) NULL',
            $columnsNames->getBreeders() => 'text NULL',
            $columnsNames->getAncestry() => 'text NULL',
            $columnsNames->getLastUpdate() => 'datetime NULL',
        ];

        /**************************************
         * Set initial data                    *
         **************************************/

        $this->institutes = [];
        $instituteIndex = 0;

        $opts = $this?->opt ?? [];

        // Create data
        $glisArr = [];
        $offset = 0;

        foreach ($opts as $key => &$opt) {
            if ($key === 'destroyOnDestruct') {
                continue;
            }
            if (array_key_exists('provider', $opt)) {
                /** @var Provider $provider */
                $provider = $opt['provider'];
                $this->institutes[] = [
                    'wiews' => $provider->getInstituteCode() ?? false,
                    'pid' => $provider->getPid() ?? false,
                    'name' => $provider->getName() ?? false,
                    'address' => $provider->getAddress() ?? false,
                    'country' => $provider->getCountry() ?? false,
//            'email' => 'contact@acme.institute'
                ];
            }
            if (array_key_exists('institute_index', $opt)) {
                $opt['institute_index'] = $instituteIndex++;
            }
            if (array_key_exists('table_data', $opt)) {
                $tableData = $opt['table_data'];
                $glisArr = &$tableData['glis'];
                $offset = count($glisArr);
            }
        }

        for ($i = 0; $i < 50; $i++) {
            $this->institutes[] = $this->fakerHelper->getFakedInstitute();
        }




        foreach ($opts as $opt2Name => $opt2) {
            if ($opt2Name === 'destroyOnDestruct') {
                continue;
            }
            for ($i = 1; $i <= $opt2['number_of_rows_to_generate']; $i++) {
                [$newGlis] = $this->populateGlis(
                    $offset + $i,
                    $this->institutes[$opt2['institute_index']],
                    $this->institutes
                );
                $glisArr[] = $newGlis;
            }
            $offset = $offset + $i;
        }

        $this->populateTables(
            [
                $tableName => [
                    'columns' => &$columns,
                    'values' => &$glisArr,
                    'hasPk' => true,
                ],
            ]
        );

        $this->addDestroyDbTable(fn() => $this->dropTables([$tableName]));
    }

    /**
     * @param int $newId
     * @param array $l Legal entity / Actor or location element
     * @param array $institutes
     * @param string $accessionFormat
     * @return array [$smta, $smtaItems]
     */
    protected function populateGlis(
        int $newId,
        array $l,
        array $institutes,
        string $accessionFormat = '[A-Z]{3} [1-9][0-9]{0,5}'
    ): array {
        $p = $this->faker->randomElement($institutes);
        $c = $this->faker->randomElement($institutes);
        $b = $this->faker->randomElement($institutes);

        $w1 = $this->faker->randomNumber(2);

        $w2 = $w1 + $this->faker->randomNumber(2);

        $w3 = $w2 + $this->faker->randomNumber(2);

        $fCropnames = ['name' => []];
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fCropnames['name'][] = $this->faker->randomElement(array_keys($this->fakerHelper->cropCodeToCrop()));
        }
        $fCropnamesJson = (count($fCropnames['name']) > 0) ? json_encode($fCropnames) : null;
        codecept_debug('cropnames=' . $fCropnamesJson);

        $fNames = ['name' => []];
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fNames['name'][] = $this->faker->word();
        }
        $fNamesJson = (count($fNames['name']) > 0) ? json_encode($fNames) : null;
        codecept_debug('names=' . $fNamesJson);


        $ftargerts = ['target' => []];
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fTarget = ['value' => $this->faker->url()];

            $fKws = ['kw' => ['1']];

            for ($j = 0; $j < $this->faker->randomNumber(1); $j++) {
                $fKws['kw'][] = $this->faker->randomElement(
                    ChoiceHelper::getGlisTkwChoices()
                    /*['1', '1.1', '1.2', '2', '3', '3.1', '3.2', '3.3', '3.4', '3.5', '3.6', '3.7', '3.8', '4', '5']*/
                );
            }
            $fTarget['kws'] = $fKws;

            $ftargerts['target'][] = $fTarget;
        }
        $fTargertsJson = (count($ftargerts['target']) > 0) ? json_encode($ftargerts) : null;

        $fIds = ['id' => []];
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fIds['id'][] = [
                    '@type' => $this->faker->randomElement(
                        ChoiceHelper::getGlisITypeChoices()
                        /*['ark', 'genesysuuid', 'gmsid', 'lsid', 'purl', 'sgsvid', 'n/a']*/
                    ),
                    '#' => $this->faker->uuid(),
                ];
        }
        $fIdsJson = (count($fIds['id']) > 0) ? json_encode($fIds) : null;

        $fCollectors = ['collector' => []];
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fCollectors['collector'][] = $this->faker->randomElement($institutes);
        }
        $fCollectorsJson = (count($fCollectors['collector']) > 0) ? json_encode($fCollectors) : null;


        $fBreeders = ['breeder' => []];
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fBreeders['breeder'][] = $this->faker->randomElement($institutes);
        }
        $fBreedersJson = (count($fBreeders['breeder']) > 0) ? json_encode($fBreeders) : null;

        $glis = [
            'glis_id' => $newId,
            'lwiews' => $l['wiews'],
            'lpid' => $l['pid'],
            'lname' => $l['name'],
            'laddress' => $l['address'],
            'lcountry' => $l['country'],

            'sampledoi' => $this->faker->optional(0.3)->regexify('10\.[0-9]{4}/[A-Z0-9]{10}'),
            'sampleid' => $this->faker->regexify($accessionFormat),
            'date' => $this->faker->dateTimeBetween(
                '-' . $w2 . ' week',
                '-' . $w1 . ' week'
            )->format('Y-m-d'),
            'method' => 'acqu',
            'genus' => ucfirst($this->faker->word()),
            'species' => $this->faker->word(),
            'cropname' => $this->faker->randomElement(array_keys($this->fakerHelper->cropCodeToCrop())),

            'cropnames' => $fCropnamesJson,

            'tvalue' => $this->faker->url(),
            'tkw' => '1',
            'targets' => $fTargertsJson,

            'progdoi' => null,
            'biostatus' => (string)$this->faker->randomElement(
                ChoiceHelper::getGlisSampstatChoices()
                /*[
                100,
                110,
                120,
                130,
                200,
                300,
                400,
                410,
                411,
                412,
                413,
                414,
                415,
                416,
                420,
                421,
                422,
                423,
                500,
                600,
                999
                ]*/
            ),
            'spauth' => $this->faker->lastName(),
            'subtaxa' => $this->faker->randomElement(['subsp. ', 'var. ']) . $this->faker->word(),
            'stauth' => $this->faker->lastName(),
            'nvalue' => $this->faker->word(),
            'names' => $fNamesJson,

            'itype' => 'genesysuuid',
            'ivalue' => $this->faker->uuid(),
            'ids' => $fIdsJson,

            'mlsstatus' => $this->faker->randomElement(
                ChoiceHelper::getGlisMlsStatusChoices() /*[0, 1, 11, 12, 13, 14, 15]*/
            ),
            'hist' => $this->faker->optional(0.9, 'y')->randomElement(['n']),

            'pwiews' => $p['wiews'],
            'ppid' => $p['pid'],
            'pname' => $p['name'],
            'paddress' => $p['address'],
            'pcountry' => $p['country'],

            'psampleid' => $this->faker->regexify($accessionFormat),

            'provenance' => $this->faker->countryISOAlpha3(),

            'cwiews' => $c['wiews'],
            'cpid' => $c['pid'],
            'cname' => $c['name'],
            'caddress' => $c['address'],
            'ccountry' => $c['country'],
            'collectors' => $fCollectorsJson,

            'csampleid' => $this->faker->regexify($accessionFormat),

            'missid' => $this->faker->regexify('[A-Z]{5}[0-4]{3}'),

            'site' => $this->faker->sentence(),
            'clat' => number_format($this->faker->latitude(), 5),
            'clon' => number_format($this->faker->longitude(), 5),
            'uncert' => (string)$this->faker->randomNumber(3),
            'datum' => 'WGS84',
            'georef' => (string)$this->faker->optional(0.3)->randomElement(
                ['GPS', 'determined from map', 'gazetteer', 'estimated using software']
            ),
            'elevation' => (string)$this->faker->randomNumber(3),

            'cdate' => $this->faker->dateTimeBetween(
                '-' . $w3 . ' week',
                '-' . $w2 . ' week'
            )->format('Y-m-d'),
            'source' => $this->faker->randomElement(
                ChoiceHelper::getGlisSourceChoices()
                /*[10, 11, 12, 13, 14, 15, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 40, 50, 60, 61, 62, 99]*/
            ),

            'bwiews' => $b['wiews'],
            'bpid' => $b['pid'],
            'bname' => $b['name'],
            'baddress' => $b['address'],
            'bcountry' => $b['country'],
            'breeders' => $fBreedersJson,

            'ancestry' => $this->faker->sentence(),
            'last_update' => $this->faker->dateTimeBetween('-' . $w1 . ' week')->format('Y-m-d')

        ];
        codecept_debug($glis);
        return [$glis];
    }
}
