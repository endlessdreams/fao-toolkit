<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Db
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;

/**
 * interface CreateTableInterface
 *
 * @package endlessdreams\faoToolkit\Tests\Db
 */
interface CreateTableInterface
{
    /**
     * CreateTableInterface constructor.
     * @param DbMapService $mapService
     * @param array $params
     */
    public function __construct(DbMapService $mapService, array $params);

    /**
     * CreateTableInterface destructor.
     */
    public function __destruct();
}
