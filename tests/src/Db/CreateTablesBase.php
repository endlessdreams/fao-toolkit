<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Command
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use Closure;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use Exception as StdException;
use Throwable;
use Yiisoft\Db\Connection\ConnectionInterface;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidArgumentException;
use Yiisoft\Db\Exception\InvalidConfigException;
use Yiisoft\Db\Exception\NotSupportedException;
use Yiisoft\Db\Query\Query;
use Yiisoft\Db\Schema\TableSchemaInterface;
use Yiisoft\Strings\StringHelper;

/**
 * Class CreateTablesBase
 *
 * @package endlessdreams\faoToolkit\Tests\Db
 */
abstract class CreateTablesBase
{
    /**
     * @var ConnectionInterface|null
     */
    protected ?ConnectionInterface $db;

    /**
     * @var array|null
     */
    protected ?array $opt;

    /**
     * @var Closure[]
     */
    protected array $destroyDbTables = [];

    /**
     * @var DbMapService
     */
    protected DbMapService $mapService;

    /**
     * CreateSmta constructor.
     * @param DbMapService $mapService
     * @param array|null $opt
     * @throws StdException
     */
    public function __construct(DbMapService $mapService, ?array $opt = [])
    {
        $this->mapService = $mapService;
        $this->db = $mapService->faoConfig->getDbConnection();
        $this->opt = $opt ?? [];
        $this->createTablesAndInsertData();
    }

    /**
     * To be overridden to define tables
     */
    abstract protected function createTablesAndInsertData(): void;

    /**
     * @param string $subTableName
     * @param array $subTableColumns
     * @param callable $callback
     * @param string $mainTable
     * @param string $mainTablePrimaryKey
     * @param string|null $subTableMainTableForeignKey
     * @throws Throwable
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function createPopulateAndAddToDestroySingleSubTable(
        string $subTableName,
        array $subTableColumns,
        callable $callback,
        string $mainTable,
        string $mainTablePrimaryKey,
        ?string $subTableMainTableForeignKey = null
    ): void {
        $subTableMainTableForeignKey = $subTableMainTableForeignKey ?? $mainTablePrimaryKey;

        /**************************************
         * Set initial data                    *
         **************************************/
        if (!isset($this->db)) {
            throw new StdException('Data connection was not set in table creation class');
        }

        // Create data
        $query = new Query($this->db);

        $result = $query->select($mainTablePrimaryKey)->from($mainTable)->all();
        $glisSubArr = [];

        foreach ($result as $row) {
            $callback((int)$row[$subTableMainTableForeignKey], $glisSubArr);
        }

        /**************************************
         * Create tables and populate tables  *
         **************************************/

        $this->populateTables(
            [
                $subTableName => [
                    'columns' => &$subTableColumns,
                    'values' => &$glisSubArr,
                ],
            ]
        );

        $this->addDestroyDbTable(fn() => $this->dropTables([$subTableName]));
    }

    /**
     * @param array $tableDefs
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws InvalidArgumentException
     * @throws NotSupportedException
     */
    protected function populateTables(array $tableDefs): void
    {
        if (!isset($this->db) || !($this->db instanceof ConnectionInterface)) {
            throw new StdException('Data connection was not set in table creation class');
        }
        $driver = $this->db->getDriverName();
        foreach ($tableDefs as $tableName => $data) {
            // Create table
            if (
                $this->dropAndCreateTable($tableName, $data['columns'])
            ) {
                $hasPrivateKeyAndDriverIsMssql = $driver === 'sqlsrv'
                    && array_key_exists('hasPk', $data)
                    && is_bool($data['hasPk'])
                    && $data['hasPk'];

                if ($hasPrivateKeyAndDriverIsMssql) {
                    // Execute SET IDENTITY_INSERT IdentityTable ON, when driver is sqlsrv
                    $sql = $this->db->createCommand("SET IDENTITY_INSERT {{%$tableName}} ON;")->getRawSql() . "\n";
                    foreach ($data['values'] as $rowValues) {
                        $sql .= $this->db->createCommand()->insert("{{%$tableName}}", $rowValues)->getRawSql() . "\n";
                    }
                    $sql .= $this->db->createCommand("SET IDENTITY_INSERT {{%$tableName}} ON;")->getRawSql() . "\n";
                    $this->db->createCommand($sql)->execute();
                    continue;
                }

                // Populate table with data
                foreach ($data['values'] as $rowValues) {
                    $this->db->createCommand()->insert("{{%$tableName}}", $rowValues)->execute();
                }
            }
        }
    }

    /**
     * @param String $tableName
     * @param array $columnDefinitions
     * @return bool
     * @throws Throwable
     */
    protected function dropAndCreateTable(string $tableName, array $columnDefinitions): bool
    {
        try {
            if ($this->checkIfTableExists($tableName)) {
                $this->db->createCommand()->dropTable("{{%$tableName}}")->execute();
            }
            $command = $this->db->createCommand()->createTable("{{%$tableName}}", $columnDefinitions);
            $command->execute();
        } catch (StdException) {
            return false;
        }
        return true;
    }

    /**
     * @param string $tableName
     * @return bool
     * @throws Throwable
     */
    private function checkIfTableExists(string $tableName): bool
    {
        try {
            return  match ($this->db?->getDriverName()) {
                'sqlite' => $this->db?->createCommand("select 1 FROM {{%{$tableName}}}")?->queryScalar() === '1',
                'sqlsrv' => $this->db?->createCommand(
                        "SELECT count(*) FROM {{INFORMATION_SCHEMA.TABLES}} WHERE [[TABLE_TYPE]] = "
                        . $this->db->getQuoter()->quoteValue('BASE TABLE')
                        . " AND [[TABLE_NAME]] = "
                        . $this->db->getQuoter()->quoteValue($tableName)
                )?->queryScalar() > 0,
                default => $this->db?->createCommand(
                    "SELECT count(*) FROM information_schema.tables WHERE table_name = '$tableName';"
                )?->queryScalar() > 0,
            };
        } catch (\Yiisoft\Db\Exception\Exception $e) {
            //codecept_debug($e->errorInfo);
            if (
                StringHelper::startsWith($e->errorInfo[2], 'no such table:')
                || substr_count((string)$e->errorInfo[2], 'Invalid object name') === 1
            ) {
                //codecept_debug('No such table...');
                return false;
            }
        }
        return true;
    }

    /**
     * @param Closure $destroyDbTable
     */
    protected function addDestroyDbTable(Closure $destroyDbTable): void
    {
        $this->destroyDbTables[] = $destroyDbTable;
    }

    /**
     * @param array $tableNames
     * @throws Exception
     * @throws InvalidConfigException
     * @throws Throwable
     */
    protected function dropTables(array $tableNames): void
    {
        foreach ($tableNames as $tableName) {
            if (
                is_string($tableName)
                && isset($this->db)
                && $this->db->getTableSchema($tableName) instanceof TableSchemaInterface
            ) {
                $this->db->createCommand()->dropTable($tableName)->execute();
            }
        }
    }
}
