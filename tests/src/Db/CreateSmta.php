<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Command
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Exception;
use Faker\Factory;
use League\ISO3166\ISO3166;
use Throwable;
use Yiisoft\Arrays\ArrayHelper;
use Yiisoft\Db\Exception\Exception as YiiSoftException;
use Yiisoft\Db\Exception\InvalidConfigException;

/**
 * Class CreateSmta
 * @package endlessdreams\faoToolkit\Tests\Db
 * @property DbMapService $mapService
 */
class CreateSmta extends CreateTablesBase implements CreateTableInterface
{
    private ?string $tableOrderName = null;
    private ?string $tableItemName = null;
    private array $institutes = [];

    /**
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    protected function createTablesAndInsertData(): void
    {
        /**************************************
         * Create tables                       *
         **************************************/
        $this->tableOrderName = $this->mapService->getTableName('table_order') ?? 'order';
        $this->tableItemName = $this->mapService->getTableName('table_item') ?? 'item';
        $map = $this->mapService->faoConfig->getMap();
        $mapOrder = $map?->getColumnsOrder();
        $mapItem = $map?->getColumnsItem();

        $orderColumnNamesDef = [
            'id' => (string)$mapOrder?->getId(),
            'symbol' => (string)$mapOrder?->getSymbol(),
            'date' => (string)$mapOrder?->getDate(),
            'type' => (string)$mapOrder?->getType(),
            'language' => (string)$mapOrder?->getLanguage(),
            'shipName' => (string)$mapOrder?->getShipName(),
            'recipient_type' => (string)$mapOrder?->getRecipientType(),
            'recipient_name' => (string)$mapOrder?->getRecipientName(),
            'recipient_address' => (string)$mapOrder?->getRecipientAddress(),
            'recipient_country' => (string)$mapOrder?->getRecipientCountry(),
            'document_location' => (string)$mapOrder?->getDocumentLocation(),
            'document_retinfo' => (string)$mapOrder?->getDocumentRetinfo(),
            'document_pdf' => (string)$mapOrder?->getDocumentPdf(),
            'fao_institute_code' => (string)$mapOrder?->getFaoInstituteCode(),
        ];

        $itemColumnNamesDef = [
            'id' => 'id',
            'orderid' => 'orderid',
            'sampleID' => (string)$mapItem?->getSampleID(),
            'crop' => (string)$mapItem?->getCrop(),
            'pud' => (string)$mapItem?->getPud(),
            'ancestry' => (string)$mapItem?->getAncestry(),
        ];

        $orderColumnsDef = [
            $orderColumnNamesDef['id'] => 'pk',
            $orderColumnNamesDef['symbol'] => 'string(128) NOT NULL',
            $orderColumnNamesDef['date'] => 'string(10) NOT NULL',
            $orderColumnNamesDef['type'] => 'string(2) NOT NULL',
            $orderColumnNamesDef['language'] => 'string(2) NOT NULL',
            $orderColumnNamesDef['shipName'] => 'string(128) NOT NULL',
            $orderColumnNamesDef['recipient_type'] => 'string(2) NOT NULL',
            $orderColumnNamesDef['recipient_name'] => 'string(128) NOT NULL',
            $orderColumnNamesDef['recipient_address'] => 'string(4000) NOT NULL',
            $orderColumnNamesDef['recipient_country'] => 'string(3) NOT NULL',
            $orderColumnNamesDef['document_location'] => 'string(1) NOT NULL',
            $orderColumnNamesDef['document_retinfo'] => 'string(128) NULL',
            $orderColumnNamesDef['document_pdf'] => 'string(255) NULL',
            $orderColumnNamesDef['fao_institute_code'] => 'string(7) NOT NULL',
        ];

        $itemColumnsDef = [
            $itemColumnNamesDef['id'] => 'pk',
            $itemColumnNamesDef['orderid'] => 'integer NOT NULL',
            $itemColumnNamesDef['sampleID'] => 'string(256) NOT NULL',
            $itemColumnNamesDef['crop'] => 'string(64) NOT NULL',
            $itemColumnNamesDef['pud'] => 'string(1)',
            $itemColumnNamesDef['ancestry'] => 'string(4000)',
        ];

        /**************************************
         * Set initial data                    *
         **************************************/
        $instituteIndex = 0;
        $opts = $this?->opt ?? [];

        // Create data
        $orders = null;
        $offset = 0;
        $items = null;

        foreach ($opts as $key => &$opt) {
            if ($key === 'destroyOnDestruct') {
                continue;
            }
            if (array_key_exists('provider', $opt)) {
                /** @var Provider $provider */
                $provider = $opt['provider'];
                $this->institutes[] = [
                    'wiews' => $provider->getInstituteCode() ?? false,
                    'pid' => $provider->getPid() ?? false,
                    'name' => $provider->getName() ?? false,
                    'address' => $provider->getAddress() ?? false,
                    'country' => $provider->getCountry() ?? false,
//            'email' => 'contact@acme.institute'
                ];
            }
            if (array_key_exists('institute_index', $opt)) {
                $opt['institute_index'] = $instituteIndex++;
            }
            if (array_key_exists('table_data', $opt)) {
                $tableData = $opt['table_data'];
                $orders = &$tableData['order'];
                $offset = count($orders);
                $items = &$tableData['item'];
            }
        }

        // Array with some test data to insert to database

        /* @var array<int,array<string,mixed>> $orders */
        $orders ??= array(
            array(
                $orderColumnNamesDef['id'] => 1,
                $orderColumnNamesDef['symbol'] => 'order1',
                $orderColumnNamesDef['date'] => '2021-05-10',
                $orderColumnNamesDef['type'] => 'sw',
                $orderColumnNamesDef['language'] => 'en',
                $orderColumnNamesDef['shipName'] => 'Marty McFly',
                $orderColumnNamesDef['recipient_type'] => 'or',
                $orderColumnNamesDef['recipient_name'] => 'Doc Brown',
                $orderColumnNamesDef['recipient_address'] => '1640 Riverside Drive, Hill Valley, California',
                $orderColumnNamesDef['recipient_country'] => 'USA',
                $orderColumnNamesDef['document_location'] => 'p',
                $orderColumnNamesDef['document_retinfo'] => 'https:///domain.com/smtaid=1',
                //$orderColumnNamesDef['document_pdf'] => 'http://www.fao.org/fileadmin/user_upload/faoweb/plant-treaty/GLIS/GLIS_XML_integration_protocol_3.6.pdf',
                $orderColumnNamesDef['document_pdf'] => null,
                $orderColumnNamesDef['fao_institute_code'] => 'XXX001'
            ),
            array(
                $orderColumnNamesDef['id'] => 2,
                $orderColumnNamesDef['symbol'] => 'order2',
                $orderColumnNamesDef['date'] => '2021-05-10',
                $orderColumnNamesDef['type'] => 'cw',
                $orderColumnNamesDef['language'] => 'en',
                $orderColumnNamesDef['shipName'] => 'Willow Rosenberg',
                $orderColumnNamesDef['recipient_type'] => 'or',
                $orderColumnNamesDef['recipient_name'] => 'Buffy Summers',
                $orderColumnNamesDef['recipient_address'] => '1630 Revello Drive, Sunnydale, California',
                $orderColumnNamesDef['recipient_country'] => 'USA',
                $orderColumnNamesDef['document_location'] => 'p',
                $orderColumnNamesDef['document_retinfo'] => 'https:///domain.com/smtaid=2',
                $orderColumnNamesDef['document_pdf'] => null,
                $orderColumnNamesDef['fao_institute_code'] => 'XXX001'
            ),
            array(
                $orderColumnNamesDef['id'] => 3,
                $orderColumnNamesDef['symbol'] => 'order3',
                $orderColumnNamesDef['date'] => '2021-06-20',
                $orderColumnNamesDef['type'] => 'si',
                $orderColumnNamesDef['language'] => 'en',
                $orderColumnNamesDef['shipName'] => 'Laura Kinney',
                $orderColumnNamesDef['recipient_type'] => 'in',
                $orderColumnNamesDef['recipient_name'] => 'Henry Philip McCoy',
                $orderColumnNamesDef['recipient_address'] => '1407 Graymalkin Lane, Salem Center, New York',
                $orderColumnNamesDef['recipient_country'] => 'USA',
                $orderColumnNamesDef['document_location'] => 'p',
                $orderColumnNamesDef['document_retinfo'] => 'https:///domain.com/smtaid=3',
                $orderColumnNamesDef['document_pdf'] => null,
                $orderColumnNamesDef['fao_institute_code'] => 'XXX001'
            )
        );



        // Array with some test data to insert to database
        $items ??= array(
            array(
                $itemColumnNamesDef['orderid'] => 1,
                $itemColumnNamesDef['sampleID'] => 'Accession1',
                $itemColumnNamesDef['crop'] => 'Potato',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 1,
                $itemColumnNamesDef['sampleID'] => 'Accession2',
                $itemColumnNamesDef['crop'] => 'Trifolium',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 1,
                $itemColumnNamesDef['sampleID'] => 'XXX 00001',
                $itemColumnNamesDef['crop'] => 'Flax',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 2,
                $itemColumnNamesDef['sampleID'] => 'Accession1',
                $itemColumnNamesDef['crop'] => 'Beet',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 2,
                $itemColumnNamesDef['sampleID'] => 'Accession2',
                $itemColumnNamesDef['crop'] => 'Trifolium',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 2,
                $itemColumnNamesDef['sampleID'] => 'Accession3',
                $itemColumnNamesDef['crop'] => 'Barley',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 3,
                $itemColumnNamesDef['sampleID'] => 'Accession1',
                $itemColumnNamesDef['crop'] => 'Wheat',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 3,
                $itemColumnNamesDef['sampleID'] => 'Accession2',
                $itemColumnNamesDef['crop'] => 'Wheat',
                $itemColumnNamesDef['pud'] => 'n',
                $itemColumnNamesDef['ancestry'] => null
            ),
            array(
                $itemColumnNamesDef['orderid'] => 3,
                $itemColumnNamesDef['sampleID'] => 'Accession3',
                $itemColumnNamesDef['crop'] => 'Wheat',
                $itemColumnNamesDef['pud'] => 'y',
                $itemColumnNamesDef['ancestry'] => 'Wheat:Accession1,Accession2'
            )
        );

        // Create data
        $noOfSmtasToGenerate = 200;

        codecept_debug(__METHOD__ . ':' . __LINE__);

        for ($i = intval($orders[count($orders) - 1]['id']) + 1; $i <= $noOfSmtasToGenerate; $i++) {

            /**
             * @var array<string,mixed> $newOrder
             * @var array<int,array<string,mixed>> $newItems
             */
            [$newOrder, $newItems] = $this->createSmtaOrderAndSmtaItems($i, $orderColumnNamesDef, $itemColumnNamesDef);
            $orders[] = $newOrder;
            $items += array_merge($items, $newItems);
        }

        /**************************************
         * Play with databases and tables      *
         **************************************/
        $this->populateTables(
            [
                $this->tableOrderName => [
                    'columns' => &$orderColumnsDef,
                    'values' => &$orders,
                    'hasPk' => true,
                ],
                $this->tableItemName => [
                    'columns' => &$itemColumnsDef,
                    'values' => &$items,
                ],
            ]
        );
    }

    /**
     * @param int $newId
     * @param string[] $orderColumns
     * @param string[] $itemColumns
     * @param string $symbolFormat
     * @param string $accessionFormat
     * @return array<array<string,mixed>,array<int,array<string,mixed>>> [$order, $items]
     */
    private function createSmtaOrderAndSmtaItems(
        int $newId,
        array $orderColumns,
        array $itemColumns,
        string $symbolFormat = 'order-[1-9][0-9]{0,5}',
        string $accessionFormat = '[A-Z]{3} [1-9][0-9]{0,5}'
    ): array {
        $preFaker = Factory::create();
        $countryCode = $preFaker->randomElement(
            ['SWE', 'DNK', 'NOR', 'FIN', 'ISL', 'EST', 'LVA', 'LTU', 'DEU', 'NLD', 'FRA', 'GBR', 'USA', 'RUS', 'POL']
        );
        $countryCodeData = (new ISO3166())->alpha3($countryCode);
        $countryCode2 = $countryCodeData['alpha2'];
        $locale = $this->countryCodeToLocale($countryCode2);

        $faker = Factory::create($locale);

        $order = [
            $orderColumns['id'] => $newId,
            $orderColumns['symbol'] => $faker->regexify($symbolFormat),
            $orderColumns['date'] => $faker->dateTimeThisYear()->format('Y-m-d'),
            $orderColumns['type'] => $faker->randomElement(['si', 'sw', 'cw']),
            $orderColumns['language'] => 'en',
            $orderColumns['shipName'] => $faker->name(),
            $orderColumns['recipient_type'] => 'or',
            $orderColumns['recipient_name'] => $faker->name(),
            $orderColumns['recipient_address'] => $faker->address(),
            $orderColumns['recipient_country'] => $countryCode,
            $orderColumns['document_location'] => ($documentLocation = $faker->randomElement(['p', 's', 'o'])),
            $orderColumns['document_retinfo'] => match ($documentLocation) {
                'p' => 'SMTA can be retrieved by the Provider',
                's' => 'Contact the Secretariat to retrieve an SMTA',
                'o' => mb_substr($faker->sentence(), 0, 100),
                default => '',
            },
            $orderColumns['document_pdf'] => null,
            $orderColumns['fao_institute_code'] => 'XXX001',
        ];

        $items = [];

        $noOfItems = $faker->biasedNumberBetween(1, 20, fn($x) => 1 - sqrt($x));

        for ($i = 0; $i < $noOfItems; $i++) {
            $items[] = [
                $itemColumns['orderid'] => $newId,
                $itemColumns['sampleID'] => $faker->regexify($accessionFormat),
                $itemColumns['crop'] => $faker->randomElement(array_keys($this->cropCodeToCrop())),
                $itemColumns['pud'] => 'n',
                $itemColumns['ancestry'] => null
            ];
        }

        return [$order, $items];
    }

    /**
     * Returns a locale from a country code that is provided.
     *
     *
     *
     * @param string $country_code ISO 3166-2-alpha 2 country code
     * @param string $language_code ISO 639-1-alpha 2 language code
     * @return false|string|null a locale, formatted like en_US, or null if not found
     */
    private function countryCodeToLocale(string $country_code, string $language_code = ''): false|string|null
    {
        // Locale list taken from:
        // http://stackoverflow.com/questions/3191664/
        // list-of-all-locales-and-their-short-codes
        $locales = array(
            'af-ZA',
            'am-ET',
            'ar-AE',
            'ar-BH',
            'ar-DZ',
            'ar-EG',
            'ar-IQ',
            'ar-JO',
            'ar-KW',
            'ar-LB',
            'ar-LY',
            'ar-MA',
            'arn-CL',
            'ar-OM',
            'ar-QA',
            'ar-SA',
            'ar-SY',
            'ar-TN',
            'ar-YE',
            'as-IN',
            'az-Cyrl-AZ',
            'az-Latn-AZ',
            'ba-RU',
            'be-BY',
            'bg-BG',
            'bn-BD',
            'bn-IN',
            'bo-CN',
            'br-FR',
            'bs-Cyrl-BA',
            'bs-Latn-BA',
            'ca-ES',
            'co-FR',
            'cs-CZ',
            'cy-GB',
            'da-DK',
            'de-AT',
            'de-CH',
            'de-DE',
            'de-LI',
            'de-LU',
            'dsb-DE',
            'dv-MV',
            'el-GR',
            'en-029',
            'en-AU',
            'en-BZ',
            'en-CA',
            'en-GB',
            'en-IE',
            'en-IN',
            'en-JM',
            'en-MY',
            'en-NZ',
            'en-PH',
            'en-SG',
            'en-TT',
            'en-US',
            'en-ZA',
            'en-ZW',
            'es-AR',
            'es-BO',
            'es-CL',
            'es-CO',
            'es-CR',
            'es-DO',
            'es-EC',
            'es-ES',
            'es-GT',
            'es-HN',
            'es-MX',
            'es-NI',
            'es-PA',
            'es-PE',
            'es-PR',
            'es-PY',
            'es-SV',
            'es-US',
            'es-UY',
            'es-VE',
            'et-EE',
            'eu-ES',
            'fa-IR',
            'fi-FI',
            'fil-PH',
            'fo-FO',
            'fr-BE',
            'fr-CA',
            'fr-CH',
            'fr-FR',
            'fr-LU',
            'fr-MC',
            'fy-NL',
            'ga-IE',
            'gd-GB',
            'gl-ES',
            'gsw-FR',
            'gu-IN',
            'ha-Latn-NG',
            'he-IL',
            'hi-IN',
            'hr-BA',
            'hr-HR',
            'hsb-DE',
            'hu-HU',
            'hy-AM',
            'id-ID',
            'ig-NG',
            'ii-CN',
            'is-IS',
            'it-CH',
            'it-IT',
            'iu-Cans-CA',
            'iu-Latn-CA',
            'ja-JP',
            'ka-GE',
            'kk-KZ',
            'kl-GL',
            'km-KH',
            'kn-IN',
            'kok-IN',
            'ko-KR',
            'ky-KG',
            'lb-LU',
            'lo-LA',
            'lt-LT',
            'lv-LV',
            'mi-NZ',
            'mk-MK',
            'ml-IN',
            'mn-MN',
            'mn-Mong-CN',
            'moh-CA',
            'mr-IN',
            'ms-BN',
            'ms-MY',
            'mt-MT',
            'nb-NO',
            'ne-NP',
            'nl-BE',
            'nl-NL',
            'nn-NO',
            'nso-ZA',
            'oc-FR',
            'or-IN',
            'pa-IN',
            'pl-PL',
            'prs-AF',
            'ps-AF',
            'pt-BR',
            'pt-PT',
            'qut-GT',
            'quz-BO',
            'quz-EC',
            'quz-PE',
            'rm-CH',
            'ro-RO',
            'ru-RU',
            'rw-RW',
            'sah-RU',
            'sa-IN',
            'se-FI',
            'se-NO',
            'se-SE',
            'si-LK',
            'sk-SK',
            'sl-SI',
            'sma-NO',
            'sma-SE',
            'smj-NO',
            'smj-SE',
            'smn-FI',
            'sms-FI',
            'sq-AL',
            'sr-Cyrl-BA',
            'sr-Cyrl-CS',
            'sr-Cyrl-ME',
            'sr-Cyrl-RS',
            'sr-Latn-BA',
            'sr-Latn-CS',
            'sr-Latn-ME',
            'sr-Latn-RS',
            'sv-FI',
            'sv-SE',
            'sw-KE',
            'syr-SY',
            'ta-IN',
            'te-IN',
            'tg-Cyrl-TJ',
            'th-TH',
            'tk-TM',
            'tn-ZA',
            'tr-TR',
            'tt-RU',
            'tzm-Latn-DZ',
            'ug-CN',
            'uk-UA',
            'ur-PK',
            'uz-Cyrl-UZ',
            'uz-Latn-UZ',
            'vi-VN',
            'wo-SN',
            'xh-ZA',
            'yo-NG',
            'zh-CN',
            'zh-HK',
            'zh-MO',
            'zh-SG',
            'zh-TW',
            'zu-ZA',
        );

        foreach ($locales as $locale) {
            $locale_region = locale_get_region($locale);
            $locale_language = locale_get_primary_language($locale);
            $locale_array = array(
                'language' => $locale_language,
                'region' => $locale_region
            );

            if (
                strtoupper($country_code) == $locale_region &&
                $language_code == ''
            ) {
                return locale_compose($locale_array);
            } elseif (
                strtoupper($country_code) == $locale_region &&
                strtolower($language_code) == $locale_language
            ) {
                return locale_compose($locale_array);
            }
        }

        return null;
    }

    /**
     * @return string[]
     */
    private function cropCodeToCrop(): array
    {
        // 'Crop Code' => 'English name',
        return ChoiceHelper::getCropOptions();
    }


    /**
     * @throws Throwable
     * @throws YiiSoftException
     * @throws InvalidConfigException
     */
    public function __destruct()
    {
        /**************************************
         * Drop tables                         *
         **************************************/

        if (!ArrayHelper::getValue($this->opt, 'destroyOnDestruct')) {
            return;
        }
        // Drop table easysmta and easysmtaitem
        $this->dropTables(
            [
                $this->tableOrderName,
                $this->tableItemName,
            ]
        );
        /**************************************
         * Close db connections                *
         **************************************/
    }
}
