<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Db
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use Closure;
use EndlessDreams\FaoToolkit\Tests\Faker\FakerHelper;
use Faker\Factory;
use Faker\Generator;
use Throwable;
use Yiisoft\Arrays\ArrayHelper;

/**
 * Class CreateGlis
 *
 * @package endlessdreams\faoToolkit\Tests\Db
 */
class CreateGlis extends CreateTablesBase implements CreateTableInterface
{
    use CreateGlisGlisTrait;
    use CreateGlisCropnameTrait;
    use CreateGlisTargetTrait;
    use CreateGlisNameTrait;
    use CreateGlisIdTrait;
    use CreateGlisCollectorTrait;
    use CreateGlisBreederTrait;

    /**
     * @var Generator
     */
    private Generator $faker;
    /**
     * @var FakerHelper
     */
    private FakerHelper $fakerHelper;

    /**
     * @var array
     */
    private array $institutes = [];

    /**
     * Migration statements
     * @throws Throwable
     */
    protected function createTablesAndInsertData(): void
    {
        $this->faker = Factory::create();
        $this->fakerHelper = new FakerHelper($this->faker);

        $this->createGlis();
        $this->createGlisCropname();
        $this->createGlisTarget();
        $this->createGlisName();
        $this->createGlisId();
        $this->createGlisCollector();
        $this->createGlisBreeder();
    }

    /**
     * Call all callables to destroy all tables
     */
    public function __destruct()
    {
        /**************************************
         * Drop tables                         *
         **************************************/

        if (!ArrayHelper::getValue($this->opt, 'destroyOnDestruct')) {
            return;
        }
        foreach ($this->destroyDbTables as $fn) {
            $fn();
        }
        /**************************************
         * Close db connections                *
         **************************************/
    }
}
