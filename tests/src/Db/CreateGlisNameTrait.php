<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Db
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use Throwable;
use Yiisoft\Db\Exception\Exception as YiiSoftException;
use Yiisoft\Db\Exception\InvalidConfigException;

/**
 * Trait CreateGlisNameTrait
 *
 * @package endlessdreams\faoToolkit\Tests\Db
 * @property DbMapService $mapService
 */
trait CreateGlisNameTrait
{
    /**
     * @return void
     * @throws Throwable
     * @throws YiiSoftException
     * @throws InvalidConfigException
     */
    private function createGlisName(): void
    {
        $mapService = $this->mapService;
        $glisGlisId = $mapService->faoConfig->getMap()->getColumnsGlis()->getGlisId();
        $columns = $mapService->faoConfig->getMap()->getColumnsGlisCropname();
        $subTableName = $mapService->getTableName('table_glis_name');
        if (!isset($subTableName)) {
            return;
        }
        /**************************************
         * Create table and columns           *
         **************************************/
        $this->createPopulateAndAddToDestroySingleSubTable(
            $subTableName,
            [
                'glis_name_id' => 'pk',
                $columns->getGlisId() => 'integer',
                $columns->getName() => 'string(128) NOT NULL',
            ],
            [$this, 'populateGlisName'],
            $mapService->getTableName('table_glis'),
            $glisGlisId,
            $columns->getGlisId()
        );
    }

    /**
     * @param int $glis_id
     * @param array $fNames
     * @return array
     */
    protected function populateGlisName(int $glis_id, array &$fNames = []): array
    {
        $glisId = $this->mapService->faoConfig->getMap()->getColumnsGlisBreeder()->getGlisId();
        $name = $this->mapService->faoConfig->getMap()->getColumnsGlisBreeder()->getName();
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fNames[] = [
                $glisId => $glis_id,
                $name => $this->faker->word()
            ];
        }

        return $fNames;
    }
}
