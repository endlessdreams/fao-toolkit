<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Db
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Exception;
use Throwable;

/**
 * Trait CreateGlisTargetTrait
 *
 * @package endlessdreams\faoToolkit\Tests\Db
 * @property DbMapService $mapService
 */
trait CreateGlisTargetTrait
{
    /**
     * @return void
     * @throws Exception|Throwable
     */
    private function createGlisTarget(): void
    {
        $mapService = $this->mapService;
        $glisGlisId = $mapService->faoConfig->getMap()->getColumnsGlis()->getGlisId();
        $columns = $mapService->faoConfig->getMap()->getColumnsGlisTarget();
        $subTableName = $mapService->getTableName('table_glis_target');
        if (!isset($subTableName)) {
            return;
        }
        /**************************************
         * Create table and columns           *
         **************************************/
        $this->createPopulateAndAddToDestroySingleSubTable(
            $subTableName,
            [
                'glis_name_id' => 'pk',
                $columns->getGlisId() => 'integer',
                $columns->getValue() => 'string(256) NOT NULL',
                $columns->getKw() => 'string(16) NOT NULL',
            ],
            [$this, 'populateGlisTarget'],
            $mapService->getTableName('table_glis'),
            $glisGlisId,
            $columns->getGlisId()
        );
    }

    /**
     * @param int $glis_id
     * @param array $glisTargetArr
     * @return array
     */
    protected function populateGlisTarget(int $glis_id, array &$glisTargetArr = []): array
    {
        $columns = $this->mapService->faoConfig->getMap()->getColumnsGlisTarget();
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $value = $this->faker->url();
            $glisTargetArr[] = [
                $columns->getGlisId() => $glis_id,
                $columns->getValue() => $value,
                $columns->getKw() => '1'
            ];
            for ($j = 0; $j < $this->faker->randomNumber(1); $j++) {
                $glisTargetArr[] = [
                    $columns->getGlisId() => $glis_id,
                    $columns->getValue() => $value,
                    $columns->getKw() => $this->faker->randomElement(
                        ChoiceHelper::getGlisTkwChoices()
                        /*
                        * [
                        * '1', '1.1', '1.2', '2', '3', '3.1', '3.2', '3.3', '3.4', '3.5', '3.6', '3.7', '3.8', '4', '5'
                        * ]
                        * */
                    )
                ];
            }
        }

        return $glisTargetArr;
    }
}
