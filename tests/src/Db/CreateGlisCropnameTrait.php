<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Db
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Exception\DbMapException;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use Throwable;
use Yiisoft\Db\Exception\Exception;
use Yiisoft\Db\Exception\InvalidConfigException;

/**
 * Trait CreateGlisCropnameTrait
 *
 * @package EndlessDreams\FaoToolkit\Tests\Db
 * @property DbMapService $mapService
 */
trait CreateGlisCropnameTrait
{
    /**
     * @return void
     * @throws DbMapException
     * @throws Throwable
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function createGlisCropname(): void
    {
        $mapService = $this->mapService;
        $glisGlisId = $mapService->faoConfig->getMap()->getColumnsGlis()->getGlisId();
        $columns = $mapService->faoConfig->getMap()->getColumnsGlisCropname();
        $subTableName = $mapService->getTableName('table_glis_cropname');
        if (!isset($subTableName)) {
            return;
        }

        /**************************************
         * Create table and columns           *
         **************************************/
        $this->createPopulateAndAddToDestroySingleSubTable(
            $subTableName,
            [
                'glis_cropname_id' => 'pk',
                $columns->getGlisId() => 'integer',
                $columns->getName() => 'string(128) NOT NULL',
            ],
            [$this, 'populateGlisCropname'],
            $mapService->getTableName('table_glis'),
            $glisGlisId,
            $columns->getGlisId()
        );
    }

    /**
     * @param int $glis_id
     * @param array $fCropnames
     * @return array
     */
    protected function populateGlisCropname(int $glis_id, array &$fCropnames = []): array
    {
        $glisId = $this->mapService->faoConfig->getMap()->getColumnsGlisCropname()->getGlisId();
        $name = $this->mapService->faoConfig->getMap()->getColumnsGlisCropname()->getName();
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fCropnames[] = [
                $glisId => $glis_id,
                $name => $this->faker->randomElement(array_keys($this->fakerHelper->cropCodeToCrop()))
            ];
        }

        return $fCropnames;
    }
}
