<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dreams(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Db
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Db;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Service\DbMapService;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use Exception;
use Throwable;

/**
 * Trait CreateGlisIdTrait
 *
 * @package endlessdreams\faoToolkit\Tests\Db
 * @property DbMapService $mapService
 */
trait CreateGlisIdTrait
{
    /**
     * @return void
     * @throws Exception|Throwable
     */
    private function createGlisId(): void
    {
        $mapService = $this->mapService;
        $glisGlisId = $mapService->faoConfig->getMap()->getColumnsGlis()->getGlisId();
        $columns = $mapService->faoConfig->getMap()->getColumnsGlisId();
        $subTableName = $mapService->getTableName('table_glis_id');
        if (!isset($subTableName)) {
            return;
        }
        /**************************************
         * Create table and columns           *
         **************************************/
        $this->createPopulateAndAddToDestroySingleSubTable(
            $subTableName,
            [
                'glis_id_id' => 'pk',
                $columns->getGlisId() => 'integer',
                $columns->getType() => 'string(16) NOT NULL',
                $columns->getValue() => 'string(128) NOT NULL',
            ],
            [$this, 'populateGlisId'],
            $mapService->getTableName('table_glis'),
            $glisGlisId,
            $columns->getGlisId()
        );
    }

    /**
     * @param int $glis_id
     * @param array $fIds
     * @return array
     */
    protected function populateGlisId(int $glis_id, array &$fIds = []): array
    {
        $glisId = $this->mapService->faoConfig->getMap()->getColumnsGlisId()->getGlisId();
        $type = $this->mapService->faoConfig->getMap()->getColumnsGlisId()->getType();
        $value = $this->mapService->faoConfig->getMap()->getColumnsGlisId()->getValue();
        for ($i = 0; $i < $this->faker->randomNumber(1); $i++) {
            $fIds[] = [
                $glisId => $glis_id,
                $type => $this->faker->randomElement(
                    ChoiceHelper::getGlisITypeChoices()
                    /*['ark', 'genesysuuid', 'gmsid', 'lsid', 'purl', 'sgsvid', 'n/a']*/
                ),
                $value => $this->faker->uuid(),
            ];
        }

        return $fIds;
    }
}
