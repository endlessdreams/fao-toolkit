<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Registry
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Registry;

use Exception;

/**
 * trait RegistryInstanceTrait
 *
 * @template-covariant T of RegistryInterface
 */
trait RegistryInstanceTrait
{
    /**
     * @var T|null
     */
    protected static $registry = null;

    /**
     * @return void
     */
    protected static function init(): void
    {
        self::setInstance(new (self::class)());
    }

    /**
     * @return T
     * @throws Exception
     */
    public static function getInstance()
    {
        if (self::$registry === null) {
            self::init();
        }

        return self::$registry ?? throw new Exception('Could not get an instance of GlisRegistry.');
    }

    /**
     * @param T $registryInterface
     */
    protected static function setInstance($registryInterface): void
    {
        self::$registry = $registryInterface;
    }
}
