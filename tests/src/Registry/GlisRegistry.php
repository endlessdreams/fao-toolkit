<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Registry
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Registry;

use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Entity\Glis\Target;
use App\Service\SimpleFastExcel\GenericCollection;
use Exception;
use SimpleXMLElement;

/**
 * Class GlisRegistry
 * @package endlessdreams\faoToolkit\Tests\Registry
 * @extends GenericCollection<Glis>
 * @implements RegistryInterface<array-key, Glis, GlisRegistry>
 */
class GlisRegistry extends GenericCollection implements RegistryInterface
{
    /**
     * @use RegistryInstanceTrait<GlisRegistry>
     */
    use RegistryInstanceTrait;

    use RegistryParserServiceTrait;

    /**
     * @param string $doi
     * @param Glis|string|SimpleXMLElement $glis
     * @param bool $allowDefaultTarget
     * @throws Exception
     */
    public static function set(string $doi, Glis|string|SimpleXMLElement $glis, bool $allowDefaultTarget = false): void
    {
        if ($glis instanceof SimpleXMLElement) {
            $glis = $glis->asXML();
        }
        if (is_string($glis)) {
            $re = '/(<\?xml(\s+version\+?=\+?\"1\.0\")?(\s+encoding\s*=\s*\"[\w-]+\")?\?>\s?)?'
                . '<(register|update|glis)[^>]*>((\s|.)+)(?=<\/(register|update|glis)>)<\/(register|update|glis)>'
                . '/mi';
            $glis = preg_replace($re, '<glis>$5</glis>', $glis);

            $instance = self::getInstance();

            /** @var Glis $glis */
            $glis = $instance->getParserService()?->deserializeXml(
                $glis,
                Glis::class,
                'glis',
                ['groups' => ['Default']]
            );
        }

        if ($allowDefaultTarget) {
            $oldTargets = self::isRegistered($doi) ? (self::get($doi))?->getTargets()->getTargets() : null;

            /** @var Target[] $newTargets */
            $newTargets = $glis->getTargets()?->getTargets() ?? [];
            if (
                count($newTargets) === 0
            ) {
                self::addTargets($glis, $oldTargets ?? []);
            }
        }

        if ($glis instanceof Glis) {
            $instance = self::getInstance();
            $instance->offsetSet($doi, $glis);
        }
    }

    /**
     * @param string $doi
     * @return bool
     * @throws Exception
     */
    public static function isRegistered(string $doi): bool
    {
        if (self::$registry === null) {
            return false;
        }
        //return self::$registry->offsetExists($doi);
        return self::getInstance()->offsetExists($doi);
    }

    /**
     * @param string|null $doi
     * @param bool $throwException
     * @return Glis|null
     * @throws Exception
     */
    public static function get(?string $doi, bool $throwException = true): ?Glis
    {
        if (!isset($doi)) {
            return null;
        }
        $instance = self::getInstance();

        /** @var Glis|null $glis */
        $glis = $instance->offsetExists($doi) ? $instance->offsetGet($doi) : null;

        return $glis ?? ($throwException ? throw new Exception("No entry is registered for doi '$doi'") : null);
    }

    /**
     * @param Glis $glis
     * @param Target[] $targets
     * @return void
     */
    private static function addTargets(Glis $glis, array $targets): void
    {
        $glis->getTargets()?->setTargets($targets);
    }

    /**
     * @param array $needle
     * @return Glis|null
     * @throws Exception
     */
    public static function getAlternative(array $needle): ?Glis
    {
        $instance = self::getInstance();
        return $instance->offsetGetAlternative($needle);
    }

    /**
     * @param array $needle ['pid', 'sampleid', 'genus']
     * @return Glis|null
     * @throws Exception
     */
    public function offsetGetAlternative(array $needle): ?Glis
    {
        if (!array_key_exists('pid', $needle)) {
            throw new Exception("Needle array does not contain a pid key.");
        }
        if (!array_key_exists('sampleid', $needle)) {
            throw new Exception("Needle array does not contain a sampleid key.");
        }
        if (!array_key_exists('genus', $needle)) {
            throw new Exception("Needle array does not contain a genus key.");
        }

        /** @var Glis|null $glis */
        $glis = $this->offsetGetByKeypaths(
            [
                'getLocation().getPid()' => $needle['pid'],
                'getSampleid()' => $needle['sampleid'],
                'getGenus()' => $needle['genus']
            ]
        );
        return $glis;
    }

    /**
     * @param string $providerPid
     * @param string $crop
     * @param string $sampleid
     * @return int|null
     * @throws Exception
     */
    public function getMlsStatusWithSmtaStyledIdentifier(string $providerPid, string $crop, string $sampleid): ?int
    {
        /** @var Glis|null $glis */
        $glis = $this->offsetGetByKeypaths(
            [
                'getLocation().getPid()' => $providerPid,
                'getCropnames().getNames().0.getValue()' => $crop,
                'getSampleid()' => $sampleid
            ]
        );

        $mlsStatus = (int)$glis?->getMlsstatus();

        return isset($glis) ? $mlsStatus : null;
    }
}
