<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Registry
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Registry;

use App\Service\SimpleFastExcel\GenericCollection;
use Exception;

/**
 * Class GlisRegistry
 * @package endlessdreams\faoToolkit\Tests\Registry
 * @extends GenericCollection<User>
 * @implements RegistryInterface<array-key,User,UserRegistry>
 */
class UserRegistry extends GenericCollection implements RegistryInterface
{
    /**
     * @use RegistryInstanceTrait<UserRegistry>
     */
    use RegistryInstanceTrait;

    /**
     * @param string $username
     * @param User $user
     * @throws Exception
     */
    public static function set(string $username, User $user): void
    {
        $instance = self::getInstance();
        $instance->offsetSet($username, $user);
    }

    /**
     * @param string $username
     * @return bool
     * @throws Exception
     */
    public static function isRegistered(string $username): bool
    {
        if (static::$registry === null) {
            return false;
        }
        return static::getInstance()->offsetExists($username);
    }

    /**
     * @param string $username
     * @param string $password
     * @return User
     * @throws Exception
     */
    public static function login(string $username, string $password): User
    {
        $user = static::get($username);

        if ($user?->getPassword() !== $password) {
            throw new Exception("Unable to complete action since user or password was not correct.");
        }

        return $user ?? throw new Exception("Unable to complete action since user or password was not correct.");
    }

    /**
     * @param string $username
     * @return User|null
     * @throws Exception
     */
    public static function get(string $username): ?User
    {
        /** @var User|null $user */
        $user = static::getInstance()->offsetGet($username);
        return $user
            ?? throw new Exception("No entry is registered for username '$username'");
    }
}
