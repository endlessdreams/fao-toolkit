<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Registry
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Registry;

use EndlessDreams\FaoToolkit\Entity\Smta\Materials;
use EndlessDreams\FaoToolkit\Entity\Smta\Smta;
use EndlessDreams\FaoToolkit\Service\Helper\ChoiceHelper;
use App\Service\SimpleFastExcel\GenericCollection;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Command\MlsDistribution;
use Exception;

/**
 * Class SmtaRegistry
 *
 * @package endlessdreams\faoToolkit\Tests\Registry
 * @extends GenericCollection<Smta>
 * @implements RegistryInterface<array-key,Smta,SmtaRegistry>
 */
class SmtaRegistry extends GenericCollection implements RegistryInterface
{
    /**
     * @use RegistryInstanceTrait<SmtaRegistry>
     */
    use RegistryInstanceTrait;
    use RegistryParserServiceTrait;

    /**
     * @param string $providerPid
     * @param string $crop
     * @param string $sampleid
     * @return int|null
     * @throws Exception
     */
    public function getMlsStatusFromGlisIfExist(string $providerPid, string $crop, string $sampleid): ?int
    {
        $glisRegistry = GlisRegistry::getInstance();

        try {
            /** @var int|null $mlsStatus */
            $mlsStatus = $glisRegistry->getMlsStatusWithSmtaStyledIdentifier($providerPid, $crop, $sampleid);
            return $mlsStatus;
        } catch (Exception $e) {
            throw  $e;
            //return null;
        }
    }

    /**
     * @param string $providerPid
     * @param Materials $materials
     * @return MlsDistribution
     * @throws Exception
     */
    public function getMaterialsMlsStatusDistribution(string $providerPid, Materials $materials): MlsDistribution
    {
        $mlsDist = new MlsDistribution();

        foreach ($materials->getMaterials() as $material) {
            if (in_array($crop = $material->getCrop(), ChoiceHelper::getCropChoices())) {
                $mlsDist->mls1++;
            } else {
                $mls = "mls"
                    . (string)(
                    $this->getMlsStatusFromGlisIfExist(
                        $providerPid,
                        $crop ?? '',
                        $material->getSampleID() ?? ''
                    ) ?? '0'
                    );
                $mlsDist->{$mls}++;
            }
        }
        return $mlsDist;
    }
}
