<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Registry
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Registry;

/**
 * Class User
 * @package endlessdreams\faoToolkit\Tests\Registry
 */
class User
{
    /**
     * @var string|null
     */
    protected ?string $user = null;
    /**
     * @var string|null
     */
    protected ?string $password = null;
    /**
     * @var string|null
     */
    protected ?string $pid = null;
    /**
     * @var string|null
     */
    protected ?string $wiews = null;
    /**
     * @var array
     */
    protected array $providingFor = [];

    /**
     * @return string|null
     */
    public function getUser(): ?string
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return User
     */
    public function setUser(string $user): User
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPid(): ?string
    {
        return $this->pid;
    }

    /**
     * @param string $pid
     * @return User
     */
    public function setPid(string $pid): User
    {
        $this->pid = $pid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWiews(): ?string
    {
        return $this->wiews;
    }

    /**
     * @param string $wiews
     * @return User
     */
    public function setWiews(string $wiews): User
    {
        $this->wiews = $wiews;
        return $this;
    }

    /**
     * @return array
     */
    public function getProvidingFor(): array
    {
        return $this->providingFor;
    }

    /**
     * @param string[]|string $providingFor
     * @return User
     */
    public function setProvidingFor(string|array $providingFor): User
    {
        if (is_string($providingFor)) {
            $providingFor = explode(',', $providingFor);
        }
        $providingFor = array_map(
            fn($a) => trim($a, "'"),
            $providingFor
        );
        $this->providingFor = $providingFor;
        return $this;
    }

    /**
     * @param string $wiews
     * @return bool
     */
    public function isProvidingFor(string $wiews): bool
    {
        return in_array($wiews, $this->providingFor);
    }
}
