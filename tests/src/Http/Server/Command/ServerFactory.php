<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Command;

use EndlessDreams\FaoToolkit\Tests\Http\Server\Router\RouteData;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Router\Router;
use FastRoute\DataGenerator\GroupCountBased;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std;
use React\EventLoop\LoopInterface;
use React\Http\HttpServer;
use React\Http\Middleware\LimitConcurrentRequestsMiddleware;
use React\Http\Middleware\RequestBodyBufferMiddleware;
use React\Http\Middleware\RequestBodyParserMiddleware;
use React\Http\Middleware\StreamingRequestMiddleware;
use React\Socket\SocketServer;

/**
 * Class ServerFactory
 * @package endlessdreams\faoToolkit\Tests\Http\Server
 */
class ServerFactory
{
    /**
     * @param string $host
     * @param int $port
     * @param LoopInterface $loop
     * @param RouteData[] $routeDataArray
     * @param Stats $stats
     * @return HttpServer
     */
    public static function create(
        string $host,
        int $port,
        LoopInterface $loop,
        array $routeDataArray,
        Stats $stats
    ): HttpServer {
        $routes = new RouteCollector(new Std(), new GroupCountBased());
        foreach ($routeDataArray as $routeData) {
            switch ($routeData->getAction()) {
                case 'get':
                    $routes->get($routeData->getPath(), $routeData->getRequestToResponseCallback());
                    break;
                case 'post':
                    $routes->post($routeData->getPath(), $routeData->getRequestToResponseCallback());
                    break;
            }
        }

        $http = new HttpServer(
            new StreamingRequestMiddleware(),
            new LimitConcurrentRequestsMiddleware(100), // 100 concurrent buffering handlers
            new RequestBodyBufferMiddleware(2 * 1024 * 1024), // 2 MiB per request
            new RequestBodyParserMiddleware(),
            new Router($routes),
        );
        $hostIp = gethostbyname($host);
        $socket = new SocketServer($hostIp . ':' . $port, [], $loop);
        $http->listen($socket);
        $stats->setPromptMessage("Server is running at http(s)://$host:$port");
        $stats->startProgress();
        $loop->addPeriodicTimer(0.5, fn() => $stats->progressIndicator());
        return $http;
    }
}
