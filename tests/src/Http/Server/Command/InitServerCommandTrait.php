<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Command;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\Provider;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\Providers;
use EndlessDreams\FaoToolkit\Tests\Registry\User;
use EndlessDreams\FaoToolkit\Tests\Registry\UserRegistry;
use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yiisoft\Arrays\ArrayHelper;

/**
 *
 */
trait InitServerCommandTrait
{
    /**
     * @param Providers $providers
     * @param OutputInterface $output
     * @return void
     * @throws Exception
     */
    private function initProviderRegistry(Providers $providers, OutputInterface $output): void
    {
        /** @var string[] $providerNames */
        $providerNames = ArrayHelper::getColumn(
            $providers->getProviders(),
            fn(Provider $provider) => (string)$provider->getInstituteCode()
        );

        $setUsers = function (string $wiews) use ($providers, $output) {
            $selectedProvider = $providers->findProvider($wiews);

            $testUserCred = $providers->findProvider($wiews)->getCredentials()->getSelected();

            if (!empty($testUserCred->getUsername())) {
                UserRegistry::set(
                    $testUserCred->getUsername(),
                    (new User())
                        ->setUser($testUserCred->getUsername())
                        ->setPassword($testUserCred->getPassword())
                        ->setPid($selectedProvider->getPid())
                        ->setWiews($wiews)
                        ->setProvidingFor($selectedProvider->getProvidingFor())
                );
            }
            unset($testUserCred);
        };

        foreach ($providerNames as $intituteCode) {
            $setUsers($intituteCode);
        }
    }

    /**
     * @param InputInterface $input
     * @return void
     */
    private function initFaoConfig(InputInterface $input): void
    {
        $this->faoConfig->setConsoleInputState($input);
        $this->faoConfig->getDatabases()->setSelectedModule($this->faoConfig->getDatabases()?->getDefaultModule());
    }
}
