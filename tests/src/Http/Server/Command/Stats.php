<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Command;

use EndlessDreams\FaoToolkit\Tests\Registry\SmtaRegistry;
use EndlessDreams\FaoToolkit\Tests\Registry\GlisRegistry;
use Exception;
use Symfony\Component\Console\Helper\ProgressIndicator;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 */
class Stats
{
    /**
     * @var ProgressIndicator
     */
    private ProgressIndicator $progress;

    /**
     * @var string
     */
    private string $promptMessage = '';

    /**
     * @var GlisRegistry
     */
    private GlisRegistry $glisRegistry;

    /**
     * @var SmtaRegistry
     */
    private SmtaRegistry $smtaRegistry;

    /**
     * @param OutputInterface $output
     * @param int $requestCnt
     * @param int $glisUpd
     * @param MlsDistribution $mlsDist
     * @throws Exception
     */
    public function __construct(
        OutputInterface $output,
        public int $requestCnt = 0,
        public int $glisUpd = 0,
        public MlsDistribution $mlsDist = new MlsDistribution()
    ) {
        $this->progress = new ProgressIndicator($output, 'verbose', 100, ['⠏', '⠛', '⠹', '⢸', '⣰', '⣤', '⣆', '⡇']);

        $this->glisRegistry = GlisRegistry::getInstance();
        $this->smtaRegistry = SmtaRegistry::getInstance();
    }

    /**
     * @return void
     */
    public function startProgress(): void
    {
        $this->progress->start('running...');
        $this->progress->setMessage($this->promptMessage . ' | ' . $this->prepareIndicatorStatusMsg());
    }

    /**
     * @return void
     */
    public function progressAdvance(): void
    {
        $this->requestCnt++;
        $this->progressIndicator();
    }

    /**
     * @return int
     */
    public function getCurrentRequestCnt(): int
    {
        return $this->requestCnt;
    }

    /**
     * @return void
     */
    public function progressIndicator(): void
    {
        $this->progress->setMessage($this->promptMessage . ' | ' . $this->prepareIndicatorStatusMsg());
        $this->progress->advance();
    }

    /**
     * @return string
     */
    private function prepareIndicatorStatusMsg(): string
    {
        return sprintf(
            implode(
                ' | ',
                [
                    'Proc:<info>%s</>',
                    $this->getGlisTemplate(),
                    $this->getSmtaTemplate()
                ]
            ),
            $this->requestCnt,
            $this->glisRegistry->count(),
            $this->glisUpd,
            $this->smtaRegistry->count(),
            $this->mlsDist->mls0,
            $this->mlsDist->mls1,
            $this->mlsDist->mls11,
            $this->mlsDist->mls12,
            $this->mlsDist->mls13,
            $this->mlsDist->mls14,
            $this->mlsDist->mls15,
        );
    }

    /**
     * @return string
     */
    private function getGlisTemplate(): string
    {
        return 'Glis (C:<comment>%s</>, U:<comment>%s</>)';
    }

    /**
     * @return string
     */
    private function getSmtaTemplate(): string
    {
        return 'Smta (C:<fg=blue>%s</>, ' . $this->mlsDist->getMlsTemplate() . ')';
    }

    /**
     * @param string $message
     * @return void
     */
    public function setPromptMessage(string $message): void
    {
        $this->promptMessage = $message;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->promptMessage . ' | ' . $this->prepareIndicatorStatusMsg();
    }
}
