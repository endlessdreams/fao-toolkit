<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Command;

use Stringable;

/**
 *
 */
class MlsDistribution implements Stringable
{
    /**
     * @var int
     */
    public int $mls0 = 0;

    /**
     * @var int
     */
    public int $mls1 = 0;

    /**
     * @var int
     */
    public int $mls11 = 0;

    /**
     * @var int
     */
    public int $mls12 = 0;

    /**
     * @var int
     */
    public int $mls13 = 0;

    /**
     * @var int
     */
    public int $mls14 = 0;

    /**
     * @var int
     */
    public int $mls15 = 0;

    /**
     * @param MlsDistribution $otherMlsDistribution
     * @return void
     */
    public function add(MlsDistribution $otherMlsDistribution): void
    {
        $this->mls0 += $otherMlsDistribution->mls0;
        $this->mls1 += $otherMlsDistribution->mls1;
        $this->mls11 += $otherMlsDistribution->mls11;
        $this->mls12 += $otherMlsDistribution->mls12;
        $this->mls13 += $otherMlsDistribution->mls13;
        $this->mls14 += $otherMlsDistribution->mls14;
        $this->mls15 += $otherMlsDistribution->mls15;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return vsprintf($this->getMlsTemplate(), $this->toArray());
    }

    /**
     * @return string
     */
    public function getMlsTemplate(): string
    {
        return 'Mls: [0:<fg=blue>%d</>, 1:<fg=blue>%d</>, 11:<fg=blue>%d</>, 12:<fg=blue>%d</>, 13:<fg=blue>%d</>, '
            . '14:<fg=blue>%d</>, 15:<fg=blue>%d</>]';
    }

    /**
     * @return int[]
     */
    public function toArray(): array
    {
        return [$this->mls0, $this->mls1, $this->mls11, $this->mls12, $this->mls13, $this->mls14, $this->mls15];
    }
}
