<?php

/**
 * FAO Command Line and Web Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Command;

use Cocur\BackgroundProcess\BackgroundProcess;
use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Service\Trait\CommandOptionsAndArgumentsTrait;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Controller\GlisController;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Controller\SmtaController;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Router\RouteData;
use EndlessDreams\FaoToolkit\Tests\Registry\GlisRegistry;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Loop;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\HelpCommand;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Helper\ProgressIndicator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Response;
use Yiisoft\Aliases\Aliases;
use Yiisoft\Strings\StringHelper;
use Yiisoft\Yii\Console\ExitCode;
use function PHPUnit\Framework\stringStartsWith;

/**
 *
 */
#[AsCommand(name: 'server:ctrl', description: 'Runs simulated Fao test server in background')]
final class ServerCtrlCommand extends Command
{
    use CommandOptionsAndArgumentsTrait;
    use InitServerCommandTrait;

    /**
     * @var BackgroundProcess|null
     */
    private ?BackgroundProcess $serverProcess = null;

    /**
     * @param FaoConfig $faoConfig
     * @param Aliases $aliases
     */
    public function __construct(private readonly FaoConfig $faoConfig, private readonly Aliases $aliases)
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDefinition(
            new InputDefinition([
                new InputOption('from', 'f', InputOption::VALUE_REQUIRED),
                new InputOption('to', 't', InputOption::VALUE_REQUIRED),
                new InputOption('run-as', null, InputOption::VALUE_REQUIRED),
                new InputOption('filter', null, InputOption::VALUE_OPTIONAL),
                new InputOption('test', null, InputOption::VALUE_NONE),
                new InputOption('dry-run', null, InputOption::VALUE_NONE),
                new InputOption('disable-require-https', null, InputOption::VALUE_NONE),
                new InputOption(
                    'force-http-error',
                    null,
                    InputOption::VALUE_OPTIONAL|InputOption::VALUE_IS_ARRAY,
                    'Force HTTP Error. Use format: <Processed call number>:<Http error code>[:Force message]',
                ),
                new InputArgument(
                    'action',
                    null,
                    'Action like start, stop, status, read-stats and run (run in forground, not as a service.)',
                    'status',
                    ['start','stop', 'restart','status','read-stats', 'run']
                ),
            ])
        );
        $this->addUsage('start [options]');
        $this->addUsage('stop [options]');
        $this->addUsage('status [options]');
        $this->addUsage('read-stats [options]');
        $this->addUsage('run [options]');
        $this->setProcessTitle('Fao-Test-Server');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $returnCode =
        match ($input->getArgument('action')) {
            'run' => fn() => $this->doRun($input, $output),
            'start' => fn() => $this->doStart($input, $output),
            'stop' => fn() => $this->doStop($input, $output),
            'restart' => function () use ($input, $output) {
                 $this->doStart($input, $output);
                 return $this->doStop($input, $output);
            },
            'status' => fn() => $this->doStatus($input, $output),
            'read-stats' => fn() => $this->doReadStats($input, $output),
            default => fn() => $this->doHelp($input, $output)
        };

        return $returnCode();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function doStart(InputInterface $input, OutputInterface $output): int
    {
        $newInput = clone $input;
        $newInput->setArgument('command', 'server:ctrl');
        $newInput->setArgument('action', 'run');
        $command = (file_exists('./yii')) ? './yii' : 'fao-toolkit';
        $command .= ' -q ' . $this->serializeCommandOptionsAndArguments($newInput) . ' > /dev/null 2>&1 &';
        //$output->writeln($command);
        $callOutput = shell_exec($command);
        if ($callOutput === false) {
            $output->writeln('Unable to run command.');
        }

        return ($callOutput !== false ) ? ExitCode::OK : ExitCode::UNSPECIFIED_ERROR;
        //return ExitCode::OK;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function doStop(InputInterface $input, OutputInterface $output): int
    {
        $process = $this->getBackgroundProcess($this->getPid());
        $progress = new ProgressIndicator($output, 'verbose', 100, ['⠏', '⠛', '⠹', '⢸', '⣰', '⣤', '⣆', '⡇']);
        $progress->start($process?->isRunning() ?? false ? 'Shutting down server...' : 'Server is inactive...');
        $process?->stop();
        do {
            sleep(2);
        } while ($process?->isRunning() ?? false);
        $progress->finish('Server is inactive...');
        return ExitCode::OK;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    private function doStatus(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(
            $this->checkIfServerIsRunning()
                ? 'Server is running'
                : 'Server is inactive'
        );

        return ExitCode::OK;
    }

    private function checkIfServerIsRunning(): bool
    {
        return $this->getBackgroundProcess($this->getPid())?->isRunning() ?? false;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    private function doReadStats(InputInterface $input, OutputInterface $output): int
    {
        $pid = $this->getPid();
        $process = isset($pid) ? $this->getBackgroundProcess(($pid = $this->getPid())) : null;

        if ($process?->isRunning() ?? false) {
            /** @var int $pid */
            posix_kill($pid, SIGUSR1);
            pcntl_signal_dispatch();

            sleep(1);
            $outputFilePath = $this->aliases->get('@runtime/tests/_output/stats.txt');
            $output->writeln(file_get_contents($outputFilePath));
            return ExitCode::OK;
        } else {
            $output->writeln('Server is inactive');
            return ExitCode::UNAVAILABLE;
        }
    }

    /**
     * @throws ExceptionInterface
     */
    private function doHelp(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);
        $style->title(StringHelper::uppercaseFirstCharacter($this->getName() ?? ''));
        $style->warning('This command is not implemented yet.');

        $help = new HelpCommand();
        $help->setCommand($this);
        $help->run($input, $output);
        return ExitCode::OK;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    private function doRun(InputInterface $input, OutputInterface $output): int
    {
        $errOutput = $output instanceof ConsoleOutputInterface ? $output->getErrorOutput() : $output;
        if (!defined('SIGINT')) {
            $errOutput->writeln(
                '<error>Not supported on your platform (ext-pcntl missing or Windows?)</error>'
            );
            return ExitCode::UNSPECIFIED_ERROR;
        }
        $this->initFaoConfig($input);

        if (($providers = $this->faoConfig->getProviders()) === null) {
            $output->writeln(
                '<error>FaoTool config seems to not be properly configured. Missing providers</error>'
            );
            return ExitCode::CONFIG;
        }

        $this->initProviderRegistry($providers, $output);

        if (($parser = $this->faoConfig->getParser()) === null) {
            $output->writeln(
                '<error>FaoTool config seems to not be properly configured. Missing parser</error>'
            );
            return ExitCode::CONFIG;
        }

        GlisRegistry::getInstance()->setParserService($parser);

        $env = $input->getOption('test') ? 'test' : 'prod';

        // Starts and displays the progress indicator with a custom message
        $stats = new Stats($output);

        // Add any number of forced HTTP errors in to a list
        $httpErrors = Response::$statusTexts;


        $forcedHttpErrors = [];
        if ($input->hasOption('force-http-error')) {
            foreach ($input->getOption('force-http-error') ?? [] as $error) {
                $errorArr = explode(':', $error);
                if (($size = sizeof($errorArr)) > 2 && is_numeric($errorArr[0]) && is_int(intval($errorArr[0]))) {
                    $forcedHttpErrors[ ($index = intval($errorArr[0])) ]
                        = ["error_code" => ($errorCode = intval($errorArr[1]))];
                    $forcedHttpErrors[$index]["error_message"] = match (true) {
                        $size === 3 => $errorArr[2],
                        in_array($errorCode, array_keys($httpErrors)) => $httpErrors[$errorCode],
                        default => "Unknown error",
                    };
                }
            }
        }

        $loop = Loop::get();

        $doDisableRequireHttp = $input->getOption('disable-require-https');

        $registerSmtaController =
            function (ServerRequestInterface $request) use (
                &$stats,
                &$loop,
                &$parser,
                &$forcedHttpErrors,
                &$doDisableRequireHttp
            ): \React\Http\Message\Response {
                return (
                    new SmtaController(
                        $stats,
                        $loop,
                        $parser,
                        $this->faoConfig,
                        $forcedHttpErrors,
                        $doDisableRequireHttp
                    )
                )->process($request);
            };

        $glisController =
            function (ServerRequestInterface $request) use (
                &$stats,
                &$loop,
                &$parser,
                &$forcedHttpErrors,
                &$doDisableRequireHttp
            ): \React\Http\Message\Response {
                return (
                    new GlisController(
                        $stats,
                        $loop,
                        $parser,
                        $this->faoConfig,
                        $forcedHttpErrors,
                        $doDisableRequireHttp
                    )
                )->process($request);
            };

        if (($apiUrl = $this->getServerPaths($env, $input)) === null) {
            $output->writeln('<error>Server api url seems to not be properly configured.</error>');

            $output->writeln(var_export($this->faoConfig->getServerUrls(), true));

            $urls = $this->faoConfig->getServerUrls();
            $smtaApiUrls = $urls['smta'];
            $glisApiUrls = $urls['glis'];
            $output->writeln(var_export($smtaApiUrls, true));
            $output->writeln(var_export($glisApiUrls, true));

            if (!(is_array($smtaApiUrls) && array_key_exists($env, $smtaApiUrls))) {
                $output->writeln("Fail 1");
            } else {
                $smtaApiUrl = parse_url((string)$smtaApiUrls[$env]);
            }
            if (!(is_array($glisApiUrls) && array_key_exists($env, $glisApiUrls))) {
                $output->writeln("Fail 2");
            } else {
                $glisApiUrl = parse_url((string)$glisApiUrls[$env]);
            }

            if (
                !(
                    array_key_exists('host', $smtaApiUrl)
                    && (
                        StringHelper::startsWith($smtaApiUrl['host'], '127.0.')
                        || $input->getOption('disable-require-https')
                    )
                    && array_key_exists('port', $smtaApiUrl)
                    && array_key_exists('path', $smtaApiUrl)
                    && array_key_exists('path', $glisApiUrl)
                )
            ) {
                $output->writeln("Fail 3:" . $smtaApiUrl['host']);
            }

            return ExitCode::CONFIG;
        }

        $mockServerLoop = ServerFactory::create(
            $apiUrl['host'],
            (int)$apiUrl['port'],
            $loop,
            [
                new RouteData('post', $apiUrl['path_smta'], $registerSmtaController),
                new RouteData('post', $apiUrl['path_glis'], $glisController)
            ],
            $stats
        );

        $pidFilePath = $this->aliases->get('@runtime/tests/_output/fakerServer.pid');

        Loop::addSignal(
            SIGINT,
            $func = function (int $signal) use (&$func, &$loop, $pidFilePath) {
                //echo 'Signal: ', (string)$signal, PHP_EOL;
                Loop::removeSignal(SIGINT, $func);
                $loop->stop();
                unlink($pidFilePath);
            }
        );

        Loop::addSignal(
            SIGUSR1,
            $func = function (int $signal) use ($stats) {
                $outputFilePath = $this->aliases->get('@runtime/tests/_output/stats.txt');
                file_put_contents($outputFilePath, $stats->getMessage());
            }
        );

        $mypid = getmypid();

        file_put_contents($pidFilePath, (string)$mypid);

        if (!file_exists($pidFilePath)) {
            echo "File $pidFilePath does not exist with pid: $mypid";
        }

        $loop->run();

        return ExitCode::OK;
    }

    /**
     * @return int|null
     */
    protected function getPid(): ?int
    {
        $pidFilePath = $this->aliases->get('@runtime/tests/_output/fakerServer.pid');
        return file_exists($pidFilePath) ? (int)file_get_contents($pidFilePath) : null;
    }

    /**
     * @param int|null $pid
     * @return BackgroundProcess|null
     */
    protected function getBackgroundProcess(?int $pid): BackgroundProcess|null
    {
        return $pid === null ? null : BackgroundProcess::createFromPID($pid);
    }

    /**
     * @param string $env
     * @return string[]|null
     */
    private function getServerPaths(string $env, InputInterface $input): ?array
    {
        $urls = $this->faoConfig->getServerUrls();
        if (
            !(
                is_array($urls)
                && array_key_exists('smta', $urls)
                && array_key_exists('glis', $urls)
            )
        ) {
            return null;
        }
        $smtaApiUrls = $urls['smta'];
        $glisApiUrls = $urls['glis'];
        if (!(is_array($smtaApiUrls) && array_key_exists($env, $smtaApiUrls))) {
            return null;
        } else {
            $smtaApiUrl = parse_url((string)$smtaApiUrls[$env]);
        }
        if (!(is_array($glisApiUrls) && array_key_exists($env, $glisApiUrls))) {
            return null;
        } else {
            $glisApiUrl = parse_url((string)$glisApiUrls[$env]);
        }

        if (
            !(
                array_key_exists('host', $smtaApiUrl)
                && (
                    StringHelper::startsWith($smtaApiUrl['host'], '127.0.')
                    || $input->getOption('disable-require-https')
                )
                && array_key_exists('port', $smtaApiUrl)
                && array_key_exists('path', $smtaApiUrl)
                && array_key_exists('path', $glisApiUrl)
            )
        ) {
            return null;
        }

        return [
            'host' => (string) $smtaApiUrl['host'],
            'port' => (string) $smtaApiUrl['port'],
            'path_smta' => (string) $smtaApiUrl['path'],
            'path_glis' => (string) $glisApiUrl['path'],
        ];
    }
}
