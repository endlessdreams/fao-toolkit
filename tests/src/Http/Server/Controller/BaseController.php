<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Controller;

use EndlessDreams\FaoToolkit\Entity\FaoConfig\FaoConfig;
use EndlessDreams\FaoToolkit\Entity\Glis\Get;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Entity\Smta\Smta;
use EndlessDreams\FaoToolkit\Service\Parser\ParserService;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Command\Stats;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\LoopInterface;
use React\Http\Message\Response;

/**
 *
 */
abstract class BaseController implements ControllerInterface
{
    /**
     * @param Stats $stats
     * @param LoopInterface $loop
     * @param ParserService $parserService
     * @param FaoConfig $faoConfig
     * @param array<int,array{error_code:int,error_message:string}> $forcedHttpErrors
     */
    public function __construct(
        protected Stats &$stats,
        protected LoopInterface &$loop,
        protected ParserService $parserService,
        protected FaoConfig $faoConfig,
        protected mixed $forcedHttpErrors,
        protected bool $doDisableRequireHttp
    ) {
    }

    /**
     * @param ServerRequestInterface $request
     * @return Response
     * @throws Exception
     */
    abstract public function process(ServerRequestInterface $request): Response;

    /**
     * @param string $xmlBody
     * @param array $errors
     * @param string|null $root
     * @return Get|Smta|Glis|null
     * @throws Exception
     */
    protected function parseXmlBody(string $xmlBody, array &$errors, ?string &$root): Get|Smta|Glis|null
    {
        $re = '/^\<\?xml .+?>\s\<(?P<root>\w+[^>]+)>/u';
        if (($result = preg_match($re, $xmlBody, $matches)) === 0 || !$result) {
            $result = false;
            $symbol = false;
            $pid = false;

            $errors[] = [
                'code' => 'Empty XML document',
                'msg' => 'The xml parameter in the HTTPS POST request is mandatory but it was missing or it was empty.
Please fix your sending application.'
            ];
            return null;
        }

        $root = $matches['root'];
        $root = explode(' ', $root)[0];
        if (!array_key_exists($root, $this->getSupportedClasses())) {
            $errors[] = [
                'code' => 'XML document with unsupported root element',
                'msg' => 'The xml document contains unsupported root element.'
            ];
            return null;
        }

        $group = match ($root) {
            'smta' => 'Smta', 'glis', 'register', 'update' => 'Glis', 'get' => 'Get',
            default => null
        };
        $groups = ['Default', (string)$group, (string)$root];

        /** @var Glis | Get | Smta $requestBodyXml */
        $requestBodyXml =  $this->parserService->deserializeXml(
            xml: $xmlBody,
            class: $this->getSupportedClasses()[$root],
            xml_root_node_name: $root,
            addContext: ['groups' => $groups]
        );

        /** @var array<string,array<int,string>> $validationErrors */
        $validationErrors = $this->parserService->validateEntity(
            object: $requestBodyXml,
            groups: $groups
        );

        //array_walk($validationErrors, function ($pathErrors, $path) use (&$errors) {
        foreach ($validationErrors as $path => $errorsArray) {
            if (is_string($errorsArray)) {
                $errorsArray = [$errorsArray];
            }
            if (!is_array($errorsArray)) {
                continue;
            }
            $errors[] = [
                'code' => 'validation error',
                'msg' => 'Path: ' . $path . "\n[\n" . implode(", \n", $errorsArray) . "\n]"
            ];
        }
        return $requestBodyXml;
    }

    /**
     * @return string[]
     */
    abstract protected function getSupportedClasses(): array;
}
