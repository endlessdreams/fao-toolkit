<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Controller;

use EndlessDreams\FaoToolkit\Entity\Smta\Response as SmtaResponse;
use EndlessDreams\FaoToolkit\Entity\Smta\Smta;
use EndlessDreams\FaoToolkit\Tests\Http\Server\Command\MlsDistribution;
use EndlessDreams\FaoToolkit\Tests\Registry\SmtaRegistry;
use EndlessDreams\FaoToolkit\Tests\Registry\UserRegistry;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Message\Response;

/**
 *
 */
class SmtaController extends BaseController implements ControllerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return Response
     * @throws Exception
     */
    public function process(ServerRequestInterface $request): Response
    {
        $body = $request->getParsedBody();
        $errors = [];
        $root = null;

        // Make an exceprion test for localhost/127.x.x.x
        $re = '/127\.(\d|1?\d\d|2[0-5][0-5])\.(\d|1?\d\d|2[0-5][0-5])\.(\d|1?\d\d|2[0-5][0-5])/s';

        //echo "{$request->getUri()->getHost()}\n";


        if (
            in_array(
                ($requestNumber = $this->stats->getCurrentRequestCnt() + 1),
                array_keys($this->forcedHttpErrors)
            )
        ) {
            $this->stats->progressAdvance();
            return new Response(
                $this->forcedHttpErrors[$requestNumber]["error_code"],
                ['Content-Type' => 'text/plain'],
                '',
                '1.1',
                $this->forcedHttpErrors[$requestNumber]["error_message"]
            );
        }

        if (
            $request->getUri()->getScheme() !== 'https'
            && !(
                $request->getUri()->getHost() === 'localhost'
                || preg_match($re, $request->getUri()->getHost())
                || $this->doDisableRequireHttp
            )
        ) {
            $errors[] = [
                'code' => 'Request sent over HTTP instead of HTTPS',
                'msg' => 'For confidentiality reasons, Easy-SMTA requires HTTPS. Please adjust your '
                    . 'sending application to use HTTPS.'
            ];
        }

        // Check credentials
        try {
            $user = null;
            if (array_key_exists('username', (array)$body) && array_key_exists('password', (array)$body)) {
                /**
                 * @var string $username
                 * @var string $password
                 */
                [$username, $password] = match (gettype($body)) {
                    'object' => [(string)$body->username, (string)$body->password],
                    'array' => [(string)$body['username'],(string)$body['password']],
                    default => throw new Exception('Credentials invalid'),
                };
                $user = UserRegistry::login($username, $password);
            }
        } catch (Exception) {
        } finally {
            unset($username);
            unset($password);
        }

        // Decompress data if necessary
        $encoding = ZLIB_ENCODING_GZIP; // or ZLIB_ENCODING_RAW or ZLIB_ENCODING_DEFLATE
        $decompressor = new \Clue\React\Zlib\Decompressor($encoding);
        $decompressor->on(
            'data',
            function ($data) {
                echo $data; // decompressed data chunk
            }
        );

        /** @var string $xmlBody */
        $xmlBody = ($body['compressed'] === 'y')
            ? $decompressor->write($body['xml'])
            : $body['xml'];

        /** @var Smta|null $requestBodyXml */
        $requestBodyXml = $this->parseXmlBody($xmlBody, $errors, $root);
        $symbol = $requestBodyXml?->getSymbol() ?? false;
        $pid = $requestBodyXml?->getProvider()?->getPid() ?? false;

        // Find the providers Wiews code, to make sure if the api user is the provider or the api user is providing
        // for the provider of the order/smta
        $wiews = !!$pid ? $this->faoConfig->getProviderWiewByPid($pid) : null;
        $isAuthorizedToProvideSmta =
            isset($user) && ($user->getPid() === $pid || $user->isProvidingFor($wiews ?? ''));

        // Check if Smta is already reported
        $smtaRegistry = SmtaRegistry::getInstance();
        $smtaExistsRegistry = $smtaRegistry->offsetExists($symbol);

        $mlsDist = ($pid !== false && ($materials = $requestBodyXml?->getAnnex1()) !== null)
            ? SmtaRegistry::getInstance()->getMaterialsMlsStatusDistribution($pid, $materials)
            : new MlsDistribution();

        $responseObj = match (true) {
            (
                $isAuthorizedToProvideSmta !== false
                //&& !in_array(0, array_values($validMaterials))
                //&& $mlsDist->mls0 === 0
                && $symbol !== false && $pid !== false && count($errors) === 0
                && $smtaExistsRegistry === false
            )
                => function () use ($pid, $symbol, $smtaRegistry, $requestBodyXml): SmtaResponse {
                    // Insert Smta into registry
                    $smtaRegistry->offsetSet($symbol, $requestBodyXml);
                    /** @var SmtaResponse $response */
                    $response = $this->parserService->denormalize(
                        ['symbol' => $symbol, 'providerPID' => $pid, 'result' => 'OK'],
                        SmtaResponse::class,
                        'xml',
                        ['groups' => ['Default', 'response']]
                    );
                    return $response;
                },

            default => function () use (
                $smtaExistsRegistry,
                $mlsDist,
                $user,
                $symbol,
                $pid,
                $errors,
                $isAuthorizedToProvideSmta
            ): SmtaResponse {

                $errCnt = count($errors);

                if (!isset($user)) {
                    $errors[] = [
                        'code' => 'Access denied',
                        'msg' => 'The username and or password specified in the HTTPS POST are not valid.
                            Contact the Easy-SMTA System Administrator to obtain valid access credentials.
                            Please note that username and password to be used in the HTTPS POST are not the same
                            to be used to login to Easy-SMTA.'
                    ];
                }

                // Continue in to next case to keep on adding errors...
                if (!$isAuthorizedToProvideSmta) {
                    $errors[] = [
                        'code' => 'Access denied',
                        'msg' => 'The FAO API user was not allowed to provide data from '
                            . ($pid === false ? 'pid=[false]sa' : $pid) . '.'
                    ];
                }
                        //case (in_array(0, array_values($validMaterials))):
                    // Continue in to next case to keep on adding errors...
                    /*
                    case ($mlsDist->mls0 > 0):
                        //$invalid = array_keys(array_filter($validMaterials, fn($value) => $value === 0));
                        $errors[] = [
                            'code' => 'None Mls material',
                            'msg' => 'Smta contains material that doesn\'t belong to the Annex I group (Mls status: 1)
                            or any other Mls status besides to Mls status 0.' . PHP_EOL
                            . (string)$mlsDist
                        ];*/

                // Continue in to next case to keep on adding errors...
                if ($symbol === false) {
                    $errors[] = ['code' => 'Insufficient data', 'msg' => 'Missing symbol. Unable to register.'];
                }

                // Continue in to next case to keep on adding errors...
                if ($pid === false) {
                    $errors[] = [
                        'code' => 'Insufficient data',
                        'msg' => 'Missing provider PID. Unable to register.'
                    ];
                }

                // Continue in to next case to keep on adding errors...
                if ($smtaExistsRegistry !== false) {
                    $errors[] = ['code' => 'SMTA exists', 'msg' => 'SMTA is already provided.'];
                }

                if (count($errors) === $errCnt) {
                    $errors[] = ['code' => 'Unknown error', 'msg' => 'Unpredicted case.'];
                }

                /** @var SmtaResponse $responseArr */
                 $responseArr = $this->parserService->denormalize(
                     [
                        'symbol' => $symbol ?: '',
                        'providerPID' => $pid ?: '',
                        'result' => 'KO',
                        'error' => $errors
                     ],
                     SmtaResponse::class,
                     'xml',
                     [
                        'groups' => ['Default', 'response']
                     ]
                 );
                 return $responseArr;
            }
        };
        $res = $responseObj();
        $x = $this->parserService->serializeXml($res, 'smta', ['groups' => ['Default', 'response']]);
        $response = new Response(200, ['Content-Type' => 'text/xml'], $x);

        $this->stats->mlsDist->add($mlsDist);
        $this->stats->progressAdvance();
        return  $response;
    }

    /**
     * @return string[]
     */
    protected function getSupportedClasses(): array
    {
        return [
            'smta' => Smta::class,
        ];
    }
}
