<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Controller;

use EndlessDreams\FaoToolkit\Entity\Glis\Get;
use EndlessDreams\FaoToolkit\Entity\Glis\Glis;
use EndlessDreams\FaoToolkit\Service\Helper\StringHelper;
use Endlessdreams\FaoToolkit\Tests\Registry\GlisRegistry;
use EndlessDreams\FaoToolkit\Tests\Registry\UserRegistry;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Message\Response;

/**
 *
 */
class GlisController extends BaseController implements ControllerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return Response
     * @throws Exception
     */
    public function process(ServerRequestInterface $request): Response
    {
        $errors = [];

        // Make an exceprion test for localhost/127.x.x.x
        $re = '/127\.(\d|[1]?\d\d|2[0-5][0-5])\.(\d|[1]?\d\d|2[0-5][0-5])\.(\d|[1]?\d\d|2[0-5][0-5])/s';

        //echo "{$request->getUri()->getHost()}\n";

        try {
            $body = $request->getParsedBody()
                ?: (string)$request->getBody();

            if (
                in_array(
                    ($requestNumber = $this->stats->getCurrentRequestCnt() + 1),
                    array_keys($this->forcedHttpErrors)
                )
            ) {
                $this->stats->progressAdvance();
                return new Response(
                    $this->forcedHttpErrors[$requestNumber]["error_code"],
                    ['Content-Type' => 'text/plain'],
                    '',
                    '1.1',
                    $this->forcedHttpErrors[$requestNumber]["error_message"]
                );
            }

            if (
                $request->getUri()->getScheme() !== 'https'
                && !(
                    $request->getUri()->getHost() === 'localhost'
                    || preg_match($re, $request->getUri()->getHost())
                    || $this->doDisableRequireHttp
                )
            ) {
                throw new Exception('Request sent over HTTP instead of HTTPS');
            }

            /** @var Glis|Get|null $requestBodyXml */
            $glis = $this->parseXmlBody($body, $errors, $root);

            if (!($glis instanceof Glis || $glis instanceof Get)) {
                throw new Exception('Request was not properly formatted.');
            }

            /** @var string $xmlResponse */
            $xmlResponseStr = match ($root) {
                'register' => $this->registerGlisTask($glis),
                'update' => $this->updateGlisTask($glis),
                'transfer' => $this->transferGlisTask($glis),
                'addtargets' => $this->addTargetGlisTask($glis),
                'get' => $this->getGlisTask($glis),
                default => throw new Exception('This method is not expected.'),
            };
        } catch (Exception $exception) {
            $responseArr = [
                'sampleid' => '',
                'genus' => '',
                'error' => $exception->getMessage()
            ];
            $xmlResponseStr = $this->parserService->encodeXml($responseArr, 'response');
        }
        $this->stats->progressAdvance();
        return new Response(200, ['Content-Type' => 'text/xml'], $xmlResponseStr);
    }

    /**
     * @param Glis $glis
     * @return string
     * @throws Exception
     */
    private function registerGlisTask(Glis $glis): string
    {
        $errors = [];
        $user = null;
        $doi = null;
        $sampleid = null;
        $genus = null;
        try {
            // Validate credentials
            $user = UserRegistry::login(
                (string)$glis->getUsername(),
                (string)$glis->getPassword()
            );

            // Get key element values
            $doi = $glis->getSampledoi();
            $sampleid = $glis->getSampleid();
            $genus = $glis->getGenus();
            $pid = $glis->getLocation()?->getPid();
            $wiews = $glis->getLocation()?->getWiews();

            $alreadyRecordedGlis = !empty($doi)
                ? GlisRegistry::get($doi, false)
                : GlisRegistry::getAlternative(
                    [
                        'sampleid' => $sampleid,
                        'genus' => $genus,
                        'pid' => $pid,
                    ]
                );

            if (
                isset($alreadyRecordedGlis)
            ) {
                /** @var string $olddoi */
                $olddoi = $alreadyRecordedGlis->getSampledoi();
                $errors[] = "Has already a Doi. Unable to register. Consider to update doi '$olddoi'.";
            }
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
        }

        switch (true) {
            case (
                empty($errors)
                && empty($doi)
                && !empty($sampleid)
                && !empty($genus)
                && !isset($alreadyRecordedGlis)
                && isset($user)
                && ($user->getPid() === $glis->getLocation()?->getPid()
                    || $user->isProvidingFor($wiews ?? '')
                )
            ):
                // Generate unique Doi
                do {
                    $newDoi = '10.18730/' . substr(sha1(uniqid('', true)), 0, 10);
                } while (GlisRegistry::isRegistered($newDoi));
                $glis->setSampledoi($newDoi);

                // It was unique. Create record and add the new doi to it.
                GlisRegistry::set($newDoi, $glis);

                // Create response.
                $responseArr = [
                    'sampleid' => $sampleid,
                    'genus' => $genus,
                    'doi' => $newDoi
                ];

                break;
            case (
                empty($errors)
                && !empty($doi)
                && !empty($sampleid)
                && !empty($genus)
                && !isset($alreadyRecordedGlis)
                && isset($user)
                && ($user->getPid() === $glis->getLocation()?->getPid()
                    || (!empty($wiews) && $user->isProvidingFor($wiews))
                )
            ):
                $glis->setSampledoi($doi);

                // It was unique. Create record and add the new doi to it.
                GlisRegistry::set($doi, $glis);

                // Create response.
                $responseArr = [
                    'sampleid' => $sampleid,
                    'genus' => $genus,
                    'doi' => $doi
                ];

                break;
            default:
                $errors2 = match (true) {
                    !isset($user)
                        => fn() => ['User not located.'],
                    (isset($user) && $user->getPid() !== $glis->getLocation()?->getPid()
                        && !$user->isProvidingFor($glis->getLocation()?->getWiews() ?? ''))
                        => fn() => [
                                'Provider (submitter) seems not to correspond '
                                . 'with provided glis location/legally provider of.'
                            ],
                    default
                        => function () use ($doi, $sampleid, $genus): array {
                            $arr = [];
                            if (!empty($doi)) {
                                $arr[] = 'Doi was submitted. Unable to register.';
                            }
                            if (empty($sampleid)) {
                                $arr[] = 'Missing sampleid. Unable to register.';
                            }
                            if (empty($genus)) {
                                $arr[] = 'Missing genus.';
                            }
                            return $arr;
                        }
                };

                $responseArr = [
                    'sampleid' => $sampleid ?? '',
                    'genus' => $genus ?? '',
                    'error' => implode(PHP_EOL, array_merge($errors, $errors2()))
                ];
                break;
        }
        return $this->parserService->encodeXml($responseArr, 'response');
    }

    /**
     * @param Glis $glis
     * @return string
     * @throws Exception
     */
    private function updateGlisTask(Glis $glis): string
    {
        $errors = [];
        $user = null;
        try {
            // Validate credentials
            $user = UserRegistry::login(
                (string)$glis->getUsername(),
                (string)$glis->getPassword()
            );

            // Get key element values
            $doi = $glis->getSampledoi();
            $sampleid = $glis->getSampleid();
            $genus = $glis->getGenus();
            $pid = $glis->getLocation()?->getPid();
            $wiews = $glis->getLocation()?->getWiews();

            $recordedGlisRecords = GlisRegistry::get($doi);
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
        }

        switch (true) {
            case !empty($doi)
                && isset($recordedGlisRecords)
                && isset($user)
                && ($recordedGlisRecords->getLocation()?->getPid() === $user->getPid()
                    || $user->isProvidingFor($recordedGlisRecords->getLocation()?->getWiews() ?? '')
                ):
                // It was unique. Create record and add the new doi to it.
                GlisRegistry::set($doi, $glis, true);

                // Create response.
                $responseArr = [
                    'sampleid' => $sampleid ?? '',
                    'genus' => $genus ?? '',
                    'doi' => $doi
                ];
                $this->stats->glisUpd++;

                break;
            default:
                $errorMsg = match (true) {
                    empty($user), !empty($errors) => implode(PHP_EOL, $errors),
                    ($user->getPid() !== $glis->getLocation()?->getPid()
                        && !$user->isProvidingFor($recordedGlisRecords?->getLocation()?->getWiews() ?? '')) =>
                    'Provider (submitter) seems not to correspond with provided glis location/legally provider of.',
                    empty($doi) =>
                    'Doi was not submitted. Unable to update.',
                    // no break
                    default =>
                    'Some other error.',
                };

                $responseArr = [
                    'sampleid' => $sampleid ?? '',
                    'genus' => $genus ?? '',
                    'error' => $errorMsg
                ];

                break;
        }
        return $this->parserService->encodeXml($responseArr, 'response');
    }

    /**
     * @param Glis $glis
     * @return string
     * @throws Exception
     */
    private function transferGlisTask(Glis $glis): string
    {

        throw new Exception('This method is not yet implemented.');
    }

    /**
     * @param Glis $glis
     * @return string
     * @throws Exception
     */
    private function addTargetGlisTask(Glis $glis): string
    {
        throw new Exception('This method is not yet implemented.');
    }

    /**
     * @param Get $get
     * @return string
     */
    private function getGlisTask(Get $get): string
    {
        try {
            // Validate credentials
            $user = UserRegistry::login(
                (string)$get->getUsername(),
                (string)$get->getPassword()
            );

            // Get doi key element value to get glis
            $doi = StringHelper::nullif((string)$get->getSampledoi());

            /** @var Glis|null $recordedGlisRecords */
            $recordedGlisRecords = GlisRegistry::get($doi);

            $glisPid = $recordedGlisRecords?->getLocation()?->getPid();
            $providedPid = $user->getPid();
        } catch (Exception $e) {
            $responseArr = [
                'doi' => $doi ?? '',
                'error' => $e->getMessage()
            ];
            return $this->parserService->encodeXml($responseArr, 'response');
        }

        switch (true) {
            case !empty($doi)
                && isset($recordedGlisRecords)
                && (
                    (string)$recordedGlisRecords->getLocation()?->getPid() === $user->getPid()
                    ||
                    $user->isProvidingFor($recordedGlisRecords->getLocation()?->getWiews() ?? '')
                ):
                return $this->parserService->serializeXml($recordedGlisRecords, 'glis');

            default:
                $errorMsg = match (true) {
                    ($user->getPid() !== $get->getLocation()?->getPid()
                        && !$user->isProvidingFor($get->getLocation()?->getWiews() ?? '')) =>
                        'Provider (submitter) seems not to correspond with provided glis location/legally provider of.',
                    !isset($recordedGlisRecords) =>
                        "No glis was found that matched submitted doi '$doi'. Unable to get.",
                    // no break
                    default =>
                        'An unpredicted error occured',
                };

                $responseArr = [
                    'doi' => $doi ?? '',
                    'error' => $errorMsg,
                ];
                break;
        }
        return $this->parserService->encodeXml($responseArr, 'response');
    }

    /**
     * @return string[]
     */
    protected function getSupportedClasses(): array
    {
        return [
            'register' => Glis::class,
            'update' => Glis::class,
            'get' => Get::class,
        ];
    }
}
