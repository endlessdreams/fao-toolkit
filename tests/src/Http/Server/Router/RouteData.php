<?php

/**
 * FAO Command Line Toolkit
 * Copyright (C) 2018- Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018- Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/fao-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 * @package   EndlessDreams\FaoToolkit\Tests\Http\Server\Router
 */

declare(strict_types=1);

namespace EndlessDreams\FaoToolkit\Tests\Http\Server\Router;

/**
 * Class RouteData
 * @package endlessdreams\faoToolkit\Tests\Http\Server\Router
 */
class RouteData
{
    /**
     * @var string
     */
    protected string $action;

    /**
     * @var string
     */
    protected string $path;

    /**
     * @var callable
     */
    protected $requestToResponseCallback;

    /**
     * RouteData constructor.
     * @param string $action
     * @param string $path
     * @param callable $requestToResponseCallback
     */
    public function __construct(string $action, string $path, callable $requestToResponseCallback)
    {
        $this->action = $action;
        $this->path = $path;
        $this->requestToResponseCallback = $requestToResponseCallback;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return callable
     */
    public function getRequestToResponseCallback(): callable
    {
        return $this->requestToResponseCallback;
    }

    /**
     * @param string $method
     * @param array $args
     * @return false|mixed
     */
    public function __call(string $method, array $args)
    {
        if (isset($this->$method) && is_callable($this->$method)) {
            return call_user_func_array(
                $this->$method,
                $args
            );
        }
        return false;
    }
}
